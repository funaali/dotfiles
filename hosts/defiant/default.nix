{ lib, config, suites, profiles, nixos-hardware, ... }:

# Host: 11th gen Intel Framework 13 (laptop)

{
  imports = [
    nixos-hardware.nixosModules.framework-11th-gen-intel
    #"${nixos-hardware}/common/pc/laptop/acpi_call.nix"
    "${nixos-hardware}/common/pc/laptop/ssd"
  ]
  ++ suites.desktop
  ++ (with profiles; [
    laptop
    printing
    dev
    tor
  ]);

  custom = {
    arch = "tigerlake";
    displaylink.enable = true;
  };

  fileSystems."/" =
    {
      device = "/dev/disk/by-uuid/885bc8f5-8eae-4475-87bd-fd581aa5d181";
      fsType = "btrfs";
      options = [ "noatime" "compress=zstd:5" ];
    };

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/0628-98C5";
      fsType = "vfat";
      options = [ "fmask=0022" "dmask=0022" ];
    };

  swapDevices = [
    { device = "/dev/disk/by-uuid/d1b259ef-0f5d-41ca-95be-253fea635fda"; }
  ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "thunderbolt" "nvme" "usb_storage" "sd_mod" ];

  boot.kernelModules = [ "kvm-intel" ];

  boot.kernelParams = [
    # Try make baacklight work properly...
    "acpi_backlight=native"
    # Force to use the new "xe" intel GPU driver
    "i915.force_probe=!9a49" "xe.force_probe=9a49"
  ];

  hardware.intelgpu.driver = "xe";

  services.xserver.videoDrivers = [ "modesetting" ];

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";

  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  powerManagement.cpuFreqGovernor = "powersave";

  networking.networkmanager.enable = true; # Easiest to use and most distros use this by default.

  services.avahi.enable = true;
  services.avahi.nssmdns4 = true;
}
