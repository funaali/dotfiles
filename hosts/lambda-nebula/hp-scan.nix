{ pkgs, ... }:

{
  services.avahi = {
    enable = true;
    #hostName = "lambda-nebula";
    #domainName = "funktionaali.com";
    allowInterfaces = [ "net0" "net10" "eno1" "lo" ];
    nssmdns6 = true;
    nssmdns4 = true;
    ipv6 = true;
    publish = {
      enable = true;
      workstation = true;
      userServices = true;
      hinfo = true;
      domain = true;
      addresses = true;
    };
  };

  services.ipp-usb.enable = true;

  # hplip seems to need this?
  environment.systemPackages = with pkgs; [
    #ipp-usb # for usb scanners? also ipp-usb service should be enabled?
    ghostscript
    libjpeg
    xsane
    libusb1
    libusb-compat-0_1
  ];
}
