
# NOTE: setup http binary cache:
# sign key creation:
# nix key generate-secret --key-name lamba-nebula.lan-1 > /var/lamba-nebula.lan-1.pem
# nix key convert-secret-to-public < /var/lamba-nebula.lan-1.pem > /var/lamba-nebula.lan-1-pub.pem
# chown nix-serve /var/lamba-nebula.lan-1.pem
# chmod 600 /var/lamba-nebula.lan-1.pem
#
# Verify:
# nix store verify --store http://lambda-nebula.lan --trusted-public-keys 'lamba-nebula.lan-1:CbLfyiZUm4TJAAlmfJ+1ID5ILupXyoseHtZlSBZ839c=' /nix/store/widgzq59vg4i70ck8by1ns510n748899-nix-direnv-3.0.1

{ config, pkgs, lib, suites, profiles, nixos-hardware, ... }:

let
  hostName = config.system.name;
  domain = "funktionaali.com";

  # Static webpage(s) showing information about the host.
  nginxHomeRoot =
    let vhostLocations = builtins.mapAttrs (_: x: lib.filterAttrs (k: _: k != "= /") (builtins.getAttr "locations" x)) config.services.nginx.virtualHosts;
    in
    pkgs.runCommandLocal "homepage" { buildInputs = [ pkgs.mdhtml ]; } ''
      mkdir -p $out
      cat >index.md <<'END'
      # NGINX on ${config.networking.fqdnOrHostName}

      `nixpkgs.version`: __${lib.version}__<br>
      `system.stateVersion`: __${config.system.stateVersion}__

      Virtual hosts:

      ```nix
      ${lib.generators.toPretty { } vhostLocations}
      ```
      END
      mdhtml convert -o $out/index.html index.md
    '';
in

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
      nixos-hardware.nixosModules.common-cpu-amd-pstate
      nixos-hardware.nixosModules.common-gpu-nvidia-nonprime
      nixos-hardware.nixosModules.common-hidpi
      ./borkowski-direct.nix
      ./hp-scan.nix
      ./proton-smtprelay.nix
    ]
    ++ suites.desktop
    ++ (with profiles; [
      my-kernel-zen
      network-manager
      nvidia
      printing
      tor
      dev
      monitoring
      virtualbox
      #hydra
    ]);

  # The new switch-to-configuration has bugs?
  #system.switch.enableNg = false;

  custom = {
    arch = "znver2";
    enableNativeOptimizations = true; # Enable building with GCC optimizations for Ryzen CPU
    cpu.vcores = 128;
    memory.total = 157;
  };

  services.ucodenix = {
    enable = true;
    cpuModelId = "00830F10"; # AMD Ryzen Threadripper PRO 3995WX 64-Cores
  };

  boot = {
    # Never version of the Aquacomputer module
    extraModulePackages = [
      (config.boot.kernelPackages.callPackage ./aquacomputer_d5next.nix { })
    ];

    # Enable interfacing with the BMC (IPMI) watchdog.
    # Related.. https://github.com/systemd/systemd/issues/26325
    # https://advantech-ncg.zendesk.com/hc/en-us/articles/360028285872-How-to-use-ipmitool-command-to-set-BMC-watchdog-timer
    initrd.kernelModules = [ /* "ipmi_watchdog" */ ];
  };

  fileSystems."/data/moore" = {
    device = "applicative.lan:/moore";
    fsType = "nfs";
    options = [
      "nfsvers=4"
      "ro"
      "nofail"
      "x-systemd.automount" # auto-mount on first access
      #"noauto"
    ];
  };

  # Age secrets
  age.secrets = {
    "ddns.conf".file = ../../secrets/ddns.conf.age;
    "lambda-nebula.lan-1.pem".file = ../../secrets/lambda-nebula.lan-1.pem.age;
    "hetzner-dns.env".file = ../../secrets/hetzner-dns.env.age;
    "lnd-mainnet-wallet-password" = {
      file = ../../secrets/lnd-bitcoin-mainnet-wallet-password.age;
      mode = "660";
      group = "lnd-mainnet";
    };
  };

  networking.domain = "funktionaali.com";

  networking.firewall.allowedTCPPorts = [
    config.services.nginx.defaultHTTPListenPort
    443 # HTTPS
    25 # smtp
    465 # smtp (TLS)
    587 # smtp (STARTTLS)
    6600 # mpd
    6905 # bittorrent
    18080 # monero
    23045 # a2ln
    24800 # barriers
  ];

  networking.firewall.allowedUDPPorts = [
    6881 # bittorrent DHT
  ];

  # ACME
  security.acme.defaults.email = "samuli.thomasson@pm.me";
  security.acme.defaults.webroot = "/var/lib/acme/acme-challenge";

  services.udev.extraRules = ''
    # {{{ Rename the ethernet interfaces on the Supermicro motherboard

    # net2: *:6a:32 is the 1Gbit interface
    SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="3c:ec:ef:7b:6a:32", NAME="net2"

    # net0: *:6c:88 is the 10Gbit interface
    SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="3c:ec:ef:7b:6c:88", NAME="net0"

    # }}}

    # Increase transmit queue lengths for all ethernet links.
    # TODO do for all hosts.
    SUBSYSTEM=="net", ACTION=="add", KERNEL=="eth*", ATTR{tx_queue_len}="10000"
  '';

  environment.systemPackages = with pkgs; [
    # Tool for Android hacking
    #edl
    #openfortivpn
    ipmitool
    liquidctl
    ledger-live-desktop
    #nixgl.auto.nixGLNvidia # XXX requires --impure
  ];

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };

  # setup ssh binary cache
  nix.sshServe.enable = true;
  nix.sshServe.keys = [ ];
  # The nix-ssh user needs to be allowed to nix-daemon
  nix.settings.extra-allowed-users = [ "@nix-ssh" ];

  # Nix binary cache at port 5000. Signed just in time with the supplied key.
  services.harmonia = {
    enable = true;
    signKeyPaths = [ config.age.secrets."lambda-nebula.lan-1.pem".path ];
  };

  security.acme.defaults = {
    dnsProvider = "hetzner";
    environmentFile = config.age.secrets."hetzner-dns.env".path;
  };

  services.nginx = {
    enable = true;
    enableReload = true;
    recommendedProxySettings = true;
    recommendedOptimisation = true;
    recommendedTlsSettings = true;
    recommendedGzipSettings = true;

    virtualHosts = let

      # Some info shown on root index page
      vhosts.root-info = {
        root = "${nginxHomeRoot}";
        index = "index.html";
        tryFiles = "/index.html =404";
      };

      # Nix binary cache over HTTP.
      # harmonia seems to be the most stable right now.
      vhosts.nix-cache = {
        proxyPass = "http://127.0.0.1:5000";
      };

      # /scrutiny (hdd monitoring)
      vhosts.scrutiny.subpath = {
        extraConfig = ''
          rewrite ^/scrutiny/(.*) /$1 break;
          proxy_redirect / /scrutiny/;
        '';
        proxyPass = "http://127.0.0.1:${toString config.services.scrutiny.settings.web.listen.port}";
      };

      # hydra: nix build cluster
      #vhosts.hydra.subpath = {
      #  extraConfig = ''
      #    rewrite ^/hydra/(.*) /$1 break;
      #    proxy_redirect / /hydra/;
      #    proxy_set_header X-Request-Base /hydra;
      #  '';
      #  proxyPass = "http://127.0.0.1:${toString config.services.hydra.port}";
      #};
    in
    lib.mkMerge [
      (lib.mkBefore {
        "${hostName}.${domain}" = {
          onlySSL = true;
          enableACME = true;
          locations."= /" = vhosts.root-info;
          locations."/" = vhosts.nix-cache;
          locations."^~ /scrutiny/" = vhosts.scrutiny.subpath;
          #locations."^~ /hydra/" = vhosts.hydra.subpath;
        };
      })
      {
        "nix.${domain}" = {
          #enableSSL = true;
          enableACME = true;
          locations."/" = vhosts.nix-cache;
        };
      }
    ];
  };

  services.fan2go = {
    enable = true;
    settings = {
      sensors = {
        cpu_temp = {
          hwmon.platform = "k10temp";
          hwmon.index = 2; # Tccd3
        };
        coolant_temp = {
          hwmon.platform = "highflownext";
          hwmon.index = 1; # Coolant temp
        };
        #ext_sensor = {
        #  hwmon.platform = "highflownext";
        #  hwmon.index = 2; # External sensor
        #};
        octo_sensor_1 = {
          hwmon.platform = "octo";
          hwmon.index = 1; # Sensor 1
        };
      };
      curves = {
        cpu_curve = {
          linear.sensor = "cpu_temp";
          linear.min = 40;
          linear.max = 75;
        };

        pump_from_coolant = {
          linear.sensor = "coolant_temp";
          linear.steps = [
            { "25" = 170; } # ~40%
            { "35" = 200; } # -60%
            { "40" = 255; } # ~100%
          ];
        };

        fans_top_from_coolant = {
          linear.sensor = "coolant_temp";
          linear.steps = [
            { "25" = 50; }
            { "35" = 80; }
            { "45" = 255; }
          ];
        };

        fans_front_from_coolant = {
          linear.sensor = "coolant_temp";
          linear.steps = [
            { "27" = 40; }
            { "36" = 105; }
            { "42" = 255; }
          ];
        };

        fans_front_curve = {
          function.type = "maximum";
          function.curves = [
            "fans_front_from_coolant"
            "cpu_curve"
          ];
        };
      };

      fans =
        let
          aquactl = dev: args: { exec = ./quadroctl; args = [ dev ] ++ args; };
          octoctl = aquactl "octo";
          octoCmds = i: attrs: {
            cmd.setPwm = octoctl [ "set" "%pwm%" "pwm${toString i}" ];
            cmd.getPwm = octoctl [ "get" "pwm${toString i}" ];
            cmd.getRpm = octoctl [ "get" "fan${toString i}_input" ];
          } // attrs;
        in
        {
          pump = octoCmds 1 {
            curve = "pump_from_coolant";
            minPwm = 200; # stops otherwise
          };
          fans_top = octoCmds 2 {
            curve = "fans_top_from_coolant";
          };
          fans_front = octoCmds 3 {
            curve = "fans_front_curve";
          };
        };
      api.enabled = true;
      display = ":0";
    };
  };

  services.tor = {
    relay.enable = true;
    relay.role = "private-bridge";
    settings = {
      AssumeReachable = true;
      Address = "lambda-nebula-10G.GUA.lan";
      ORPort = [ { port = 9061; flags = [ "IPv6Only" ]; } ];
      ExtendByEd25519ID = true;
    };
  };

  services.monero = {
    enable = true;
  };

  services.btcd.testnet = {
    enable = false;
    openFirewall = true;
    testnet = true;
    port = 18333;
    rpc.port = 18334;
    rpc.user.name = "admin";
    rpc.user.pass = "admin";
    extraCmdlineOptions = [
      "--txindex"
      "--externalip=144.208.197.112"
    ];
  };

  services.btcd.mainnet = {
    enable = true;
    openFirewall = true;
    port = 8333;
    rpc.port = 8334;
    rpc.user.name = "admin";
    rpc.user.pass = "admin";
    extraCmdlineOptions = [
      "--txindex"
      "--externalip=144.208.197.112"
    ];
  };

  services.lnd.testnet = {
    enable = false;
    openFirewall = true;
    bitcoin.node = "btcd";
    bitcoin.testnet = true;
    btcd.rpc.user = "admin";
    btcd.rpc.pass = "admin";
    port = 19735;
    rpc.port = 19008; # 8080
    rpc.restPort = 19009;
    extraCmdlineOptions = [
      "--debuglevel=info"
      "--externalip=144.208.197.112"
      "--wallet-unlock-password-file=/var/lib/lnd-testnet/wallet-password"
      "--wallet-unlock-allow-create"
    ];
  };

  services.lnd.mainnet = {
    enable = true;
    openFirewall = true;
    bitcoin.node = "btcd";
    bitcoin.testnet = false;
    btcd.rpc.user = "admin";
    btcd.rpc.pass = "admin";
    extraCmdlineOptions = [
      "--debuglevel=info"
      "--externalip=144.208.197.112"
      "--wallet-unlock-password-file=${config.age.secrets."lnd-mainnet-wallet-password".path}"
      "--wallet-unlock-allow-create"
    ];
  };
}
