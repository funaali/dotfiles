{ lib, pkgs, config, ... }:
let
  name = "proton-smtprelay";

  certDir = config.security.acme.certs."lambda-nebula.funktionaali.com".directory;

  certPEM = "${certDir}/cert.pem";
  keyPEM = "${certDir}/key.pem";

  username = "automations@funktionaali.com";
  password = "o4x9n5P2xIi4EcoW98Nh5w";

  cfgIni = builtins.toFile "smtprelay.ini" ''
    hostname = ${config.networking.fqdnOrHostName}
    welcome_msg = "<hostname> ESMTP ready. Ports: 25 (unencrypted), 465 (TLS), 587 (STARTTLS)."
    listen = 0.0.0.0:25 tls://0.0.0.0:465 starttls://0.0.0.0:587
    local_cert = ${certPEM}
    local_key = ${keyPEM}
    local_forcetls = true
    strict_sender = false
    read_timeout = 45s
    write_timeout = 30s
    data_timeout = 45s
    max_message_size = 45240000
    log_format = plain
    allowed_nets = 127.0.0.0/8 ::1/128 192.168.0.0/16 fc00::/7 116.203.49.115/32 2a01:4f8:1c1c:2605::/64
    allowed_sender =
    allowed_recipients =
    remotes = starttls://${username}:${password}@127.0.0.1:1025/${username}?skipVerify=true
  '';
in
{
  config = {
    age.secrets.smtprelay-tls-key.file = ../../secrets/smtprelay-tls.key.age;

    systemd.services.${name} = {
      description = ''Local SMTP relay for using ProtonMail to send emails. Requires protonmail-bridge running locally, too!'';
      after = [ "network.target" "network-online.target" ];
      wants = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
      restartTriggers = [ cfgIni ];

      #path = with pkgs; [
      #  inetutils
      #];

      serviceConfig = {
        #ExecStartPre = '' ${lib.getExe pkgs.bash} -c "echo 'waiting for bridge up...'; while !nc -zw3 ::1 587; do sleep 5; done" '';
        ExecStart = "${lib.getExe pkgs.smtprelay} -config ${cfgIni}";
        Restart = "always";
        RestartSec = 60;
      };
    };

    # Add to PATH for users, so they can initialize the bridge manually
    environment.systemPackages = [ pkgs.protonmail-bridge ];

    # The bridge as user service, for getting the credentials from pass
    systemd.user.services.protonmail-bridge = {
      description = "Local ProtonMail Bridge (IMAP + SMTP)";
      after = [ "network.target" "network-online.target" ];
      wants = [ "network-online.target" ];
      serviceConfig = {
        ExecStart = lib.concatStringsSep " " [
          "${pkgs.protonmail-bridge}/bin/protonmail-bridge"
          "--log-level info"
          "--grpc"
          "--cli"
          "--noninteractive"
          "--log-smtp"
        ];
        Restart = "always";
      };
      wantedBy = [ "default.target" ];
    };
  };
}
