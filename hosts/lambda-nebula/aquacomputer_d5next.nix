{ stdenv, fetchFromGitHub, kernel }:

# aquacomputer_d5next hwmon module. Latest dev version with latest features.

stdenv.mkDerivation rec {
  pname = "aquacomputer_d5next";
  version = "${kernel.version}-${builtins.substring 0 8 src.rev}";

  src = fetchFromGitHub {
    owner = "aleksamagicka";
    repo = "aquacomputer_d5next-hwmon";
    rev  = "f20c53c7edaee2a57b7aee7a64358864d207e75f"; # May 14 2024
    hash = "sha256-iujm7CFn3cJnE7UPyaM8+DjY6Ope+bt8pi2CopMlUc0=";
  };

  postPatch = ''
    # renamed in kernel 6.12.2 or so
    sed -i 's|asm/unaligned\.h|linux/unaligned.h|' aquacomputer_d5next.c
  '';

  hardeningDisable = [ "pic" "format" ];
  nativeBuildInputs = kernel.moduleBuildDependencies;

  makeFlags = [
    "KDIR=${kernel.dev}/lib/modules/${kernel.modDirVersion}/build"
    "INSTALL_MOD_PATH=$(out)"
  ];
}
