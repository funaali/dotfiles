{ lib, ... }:

let
  label = "Borkowski";

  mountPoint = "/data/${label}";

  # The encrypted devices (by label)
  blkDevLabels = [
    "${label}_43S0A0BWFJ"
    "${label}_43S0A0BPFJ"
    "${label}_ZR5ELD7W"
    "${label}_000073CQ"
  ];

  luksOptions = [
    "luks"
    "nofail"
    "x-systemd.device-timeout=5m"
    "x-initrd.attach" # unmap the encrypted device AFTER other filesystems are unmounted
  ];

in
{
  config = {
    environment.etc.crypttab.text = lib.concatLines (builtins.map
      (label:
        ''${label} LABEL="${label}" - ${lib.concatStringsSep "," luksOptions}''
      )
      blkDevLabels);

    # Mount the decrypted filesystem by label
    fileSystems."${mountPoint}" = {
      inherit label;
      fsType = "btrfs";
      options = [
        # no panic if encryption failed
        "nofail"
        # "relatime" triggers metadata updates which slows down file reads on
        # btrfs considerably. Therefore use "noatime".
        "noatime"
        # To ensure existing data is compressed: btrfs fi defrag -r -czstd
        "compress-force=zstd:15"
      ];
    };
  };
}
