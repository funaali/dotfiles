{ lib, modulesPath, suites, ... }:
{
  imports = [
    (modulesPath + "/installer/cd-dvd/installation-cd-minimal-new-kernel-no-zfs.nix")
  ] ++ suites.base;

  isoImage.isoBaseName = "nixos-sim";
  isoImage.edition = "sim";

  # use faster compression
  isoImage.squashfsCompression = "zstd -Xcompression-level 5";

  # disable configuration switching functionality
  system.switch.enable = false;

  networking.networkmanager.enable = true; # nmcli for wi-fi
  networking.wireless.enable = lib.mkForce false;

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIlIECoPAF/Uy+ZB3tpbMhXwF/5xNdUFLwLifn5zUZgf samuli@F3D3AB3309F04D1CA4D851D068F82A4F3ECA091D"
  ];
}
