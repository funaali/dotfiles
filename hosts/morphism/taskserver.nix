# Enable Taskwarrior server. taskdrc(5)

{ config, lib, ... }:

{
  config = {
    services.taskchampion-sync-server = {
      enable = true;
    };

    services.nginx.virtualHosts."task-champion.${config.networking.domain}" = {
      forceSSL = true;
      enableACME = true;
      locations."/".proxyPass =
        "http://127.0.0.1:${config.services.taskchampion-sync-server.port}";
    };
  };
}
