{ lib, config, pkgs, ... }:

# Note: create admin user for basic auth protected endpoints like:
#
#    nix shell nixpkgs#apacheHttpd -c htpasswd -c /etc/nginx/.htpasswd admin

let
  user = "firmexa";
  group = "firmexa";
  hostname = "firmexa.com";
  directory = "/srv/${hostname}";
in
{
  config = {
    # Setup system user & group
    users.users.firmexa = {
      home = directory;
      inherit group;
      isSystemUser = true;
    };
    users.groups.firmexa.members = [ group config.services.nginx.user ];

    # Setup database & database user
    services.mysql = {
      enable = true;
      package = pkgs.mariadb;
      ensureDatabases = [ "firmexa" ];
      ensureUsers = [{
        name = user;
        ensurePermissions = { "${user}.*" = "ALL PRIVILEGES"; };
      }];
    };

    # Setup PHP
    services.phpfpm.pools.firmexa = {
      inherit user group;
      phpEnv = {
        PATH = "/run/wrappers/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin:/usr/bin:/bin";
      };
      settings = {
        "listen.owner" = config.services.nginx.user;
        "listen.group" = config.services.nginx.group;
        "pm" = "dynamic";
        "pm.max_children" = "32";
        "pm.start_servers" = "2";
        "pm.min_spare_servers" = "2";
        "pm.max_spare_servers" = "4";
        "pm.max_requests" = "500";
      };
    };


    services.nginx.virtualHosts = {
      ${hostname} = {
        forceSSL = true;
        enableACME = true;
        root = directory;
        locations = {
          # Basic Auth settings for protected paths
          "~ ^(?:/cron/update-sitemap|/install/)" = {
            priority = 400;
            extraConfig = ''
              satisfy any;

              allow 127.0.0.1;
              allow ::1;
              deny all;

              auth_basic "Restricted Area";
              auth_basic_user_file /etc/nginx/.htpasswd;
            '';
          };

          "~ \\.php(?:$|/)" = {
            priority = 500;
            extraConfig = ''
              include ${config.services.nginx.package}/conf/fastcgi.conf;
              fastcgi_split_path_info ^(.+?\.php)(/.*)$;
              if (!-f $document_root$fastcgi_script_name) {
                return 404;
              }
              fastcgi_param HTTP_PROXY "";
              fastcgi_pass unix:${config.services.phpfpm.pools.firmexa.socket};
              fastcgi_index index.php;
              fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;

              try_files $fastcgi_script_name =404;
              fastcgi_param front_controller_active true;
              fastcgi_intercept_errors on;
              fastcgi_request_buffering off;
            '';
          };

          # files
          "~ /" = {
            priority = 1600;
            tryFiles = "$uri $uri/ /index.php?$uri&$args";
          };

          # security
          "~ /(internal_data|library)" = {
            extraConfig = "internal;";
          };

          # Deny access to sensitive files
          "~ /\.ht" = {
            extraConfig = "deny all;";
          };
        };
        extraConfig = ''
          index index.php index.html;
        '';
      };
    };

    # Setup cron jobs
    services.cron = {
      enable = lib.mkDefault true;
      systemCronJobs = [
        "0 3 * * * curl -sSfL https://${hostname}/cron/update-sitemap"
      ];
    };
  };
}
