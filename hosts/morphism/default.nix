{ config, pkgs, suites, profiles, modulesPath, ... }:

let
  # Hetzner static IPv6
  address6 = { prefix = "2a01:4f8:1c1c:2605"; prefixLength = 64; };
  address4 = "116.203.49.115";
in
{
  imports = with profiles; [
    (modulesPath + "/profiles/qemu-guest.nix")
    ./vpnstuff.nix
    ./collabora-cloud.nix
    ./nextcloud.nix
    ./firmexa.nix
    tor
  ] ++ suites.base;

  # Load some modules in initrd. Detected by nixos-generate.
  # Unsure if all are strictly needed. Better safe than sorry I guess.
  boot.initrd.availableKernelModules = [
    "ata_piix"
    "virtio_pci"
    "virtio_scsi"
    "xhci_pci"
    "sd_mod"
    "sr_mod"
  ];

  # Add the root and boot filesystems.
  fileSystems."/" = {
    device = "UUID=2fdf63b8-06f1-49ce-903c-50270c5191a7";
    fsType = "btrfs";
    options = [ "noatime" "compress=zstd:5" ];
  };
  fileSystems."/boot" = {
    device = "UUID=ecea9cff-798d-4087-a577-0d8ffbf8ceca";
    fsType = "ext4";
  };

  # Add the data extra storage filesystem.
  #fileSystems."/data" = {
  #  device = "UUID=9c898e1a-4598-48f1-b735-ce33251b25c4";
  #  fsType = "btrfs";
  #  options = [ "noatime" "compress=zstd:5" ];
  #};

  # Add a swapfile
  swapDevices = [
    { device = "/swap/swapfile"; }
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
  boot.loader.systemd-boot.enable = false;

  networking.domain = "funktionaali.com";

  # Configure IPv6 statically (Hetzner)
  networking.useDHCP = false;
  networking.interfaces.ens3.useDHCP = true;
  networking.interfaces.ens3.ipv6.addresses = [
    { address = "${address6.prefix}::1"; prefixLength = address6.prefixLength; }
  ];
  networking.defaultGateway6 = { address = "fe80::1"; interface = "ens3"; };

  networking.nat = {
    enable = true;
    externalInterface = "ens3";
    internalInterfaces = [ "docker0" ];
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];

  services.sshguard.whitelist = [
    address4
    "${address6.prefix}::/${toString address6.prefixLength}"
  ];

  # ACME
  security.acme.defaults.email = "samuli.thomasson@pm.me";
  security.acme.defaults.webroot = "/var/lib/acme/acme-challenge";

  # NGINX
  services.nginx.enable = true;
  services.nginx.enableReload = true;
  services.nginx.recommendedOptimisation = true;
  services.nginx.recommendedTlsSettings = true;
  services.nginx.recommendedGzipSettings = true;
  services.nginx.recommendedProxySettings = true;
  services.nginx.sslCiphers = "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";

  services.nginx.virtualHosts = {
    "tmi-samuli-thomasson.${config.networking.domain}" = {
      forceSSL = true;
      enableACME = true;
      locations = {
        "/" = {
          root = ./tmi-website;
          tryFiles = "$uri $uri.html $uri/ =404";
          #index = "apple-developer-merchantid-domain-association";
        };
      };
    };

    "overleaf.morphism.${config.networking.domain}" = {
      forceSSL = true;
      enableACME = true;
      locations = {
        "/" = {
          proxyPass = "http://127.0.0.1:8097";
        };
      };
    };

  };

  # Wetty
  services.wetty = {
    enable = true;
    vhost = "shell.${config.networking.domain}";
    sshHost = address4;
  };

  services.cron.mailto = "admin@funktionaali.com";

  services.tor = {
    relay.onionServices = {
      # Set up a hidden service
      my_website = {
        path = "/var/lib/tor/my_website";
        map = [{
          port = 80;
          target = {
            addr = "127.0.0.1";
            port = 3006;
            #unix = "/var/run/tor_my-website.sock";
          };
        }];
        settings = {
          #HiddenServiceNumIntroductionPoints = 10;
          #HiddenServiceExportCircuitID = "haproxy";
        };
      };
    };
  };

  virtualisation.docker = {
    enable = true;
    storageDriver = "btrfs";
    extraPackages = with pkgs; [
      criu
      docker-compose
    ];
    autoPrune = {
      enable = true;
    };
    daemon.settings = {
      fixed-cidr-v6 = "fd00:1000::/80";
      ipv6 = true;
    };
    rootless = {
      enable = true;
      setSocketVariable = true;
      daemon.settings = {
        fixed-cidr-v6 = "fd00:2000::/80";
        ipv6 = true;
      };
    };
  };
}
