{ config, lib, ... }:

# Admin console at /browser/dist/admin/admin.html
#
# Metrics at /cool/getMetrics
#
# Admin interface:
#  <https://*hostname*:*port*/browser/dist/admin/admin.html>

let
  inherit (lib) mkEnableOption mkOption mkIf types;

  cfg = config.services.collabora-cloud;
in
{
  options.services.collabora-cloud = {
    enable = mkEnableOption "collabora-cloud";

    imageTag = mkOption {
      type = types.str;
      default = "collabora/code";
      description = "The docker tag of collabora server.";
    };

    version = mkOption {
      type = types.str;
      default = "24.04.9.2.1";
      description = "The container tag/version to use.";
    };

    port = mkOption {
      type = types.port;
      default = 9980;
    };

    host = mkOption {
      type = types.str;
      description = "The outside hostname e.g. office.domain";
    };

    aliasgroup1 = mkOption {
      type = types.str;
      description = "Allowed domain to access the service";
    };
  };

  config = mkIf cfg.enable {

    virtualisation.oci-containers.containers."collabora" = {
      image = "${cfg.imageTag}:${cfg.version}";
      ports = [
        "127.0.0.1:${toString cfg.port}:9980"
      ];
      extraOptions = [
        "--cap-add=MKNOD"
        "--cap-add=CHOWN"
        "--privileged" # Needed really?
        # TODO conflicts with --rm which is added by nix
        #"--restart=unless-stopped"
      ];
      environment = {
        # Allowed WOPI hosts.
        aliasgroup1 = cfg.aliasgroup1;
        server_name = cfg.host;
        # Terminate SSL at the reverse proxy instead.
        extra_params = lib.concatStringsSep " " [
          "--o:ssl.enable=false"
          "--o:ssl.termination=true"
          "--o:admin_console.username=cool"
          "--o:admin_console.password=coolcoolcool" # FIXME
          #"--o:net.post_allow.host=2a01:4f8:1c1c:2605::1" # FIXME parameterize
        ];
      };
      hostname = "collabora-cloud";
      volumes = [
        "/data/nextcloud-file/coolwsd.xml:/etc/coolwsd/coolwsd.xml"
      ];
    };

    # seems to run out of memory otherwise...
    services.nginx.appendHttpConfig = ''
      gzip_buffers 128 4k;
    '';

    services.nginx.virtualHosts."${cfg.host}" = {
      forceSSL = true;
      enableACME = true;
      extraConfig = ''
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-NginX-Proxy true;
      '';

      locations = {
        # static files
        "^~ /browser" = {
          extraConfig = "proxy_pass http://127.0.0.1:${toString cfg.port};";
        };

        # WOPI discovery URL
        "^~ /hosting/discovery" = {
          extraConfig = "proxy_pass http://127.0.0.1:${toString cfg.port};";
        };

        # Capabilities
        "^~ /hosting/capabilities" = {
          extraConfig = "proxy_pass http://127.0.0.1:${toString cfg.port};";
        };

        # Main websocket
        "~ ^/cool/(.*)/ws$" = {
          proxyWebsockets = true;
          extraConfig = ''
            proxy_pass http://127.0.0.1:${toString cfg.port};
            proxy_read_timeout 36000s;
          '';
        };

        # download, presentation and image upload
        "~ ^/(c|l)ool" = {
          # is important so that this comes *after* the /ws endpoint!
          priority = 1050;
          extraConfig = ''
            proxy_pass http://127.0.0.1:${toString cfg.port};
          '';
        };

        # Admin Console websocket
        "^~ /cool/adminws" = {
          proxyWebsockets = true;
          extraConfig = ''
            proxy_pass http://127.0.0.1:${toString cfg.port};
            proxy_read_timeout 36000s;
          '';
        };
      };
    };
  };
}
