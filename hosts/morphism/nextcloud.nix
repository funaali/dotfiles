# nextcloud

{ config, lib, pkgs, ... }:
let
  nextcloudVersion = 30;
  dbname = "nextcloud";
  dbuser = "nextcloud";


  # TODO hide this, this is the postgres user password!
  # Nextcloud authenticates as linux user it doesn't need a password.
  dbpass = "shNbrPWMywrJ4PfTwkco";

  # Hetzner static IPv6
  address4 = "116.203.49.115";
  address6 = { prefix = "2a01:4f8:1c1c:2605"; prefixLength = 64; };

  ffmpegPackage = pkgs.ffmpeg-headless;
  libreofficePackage = pkgs.libreoffice;
  extraPackagesToPath = with pkgs; [ nodejs_20 ffmpegPackage ];
in
{
  config = {
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_16;
      enableTCPIP = true;
      ensureUsers = [
        {
          name = dbuser;
          ensureDBOwnership = true;
        }
      ];
      ensureDatabases = [ dbname ];

      # TODO not the way to do this...
      # Set superuser password
      initialScript = pkgs.writeText "init-sql-script" ''
        ALTER USER postgres WITH PASSWORD '${dbpass}';
      '';
    };

    services.nextcloud = {
      enable = true;
      package = pkgs."nextcloud${toString nextcloudVersion}";
      datadir = "/data/nextcloud-file";
      hostName = "cloud.${config.networking.domain}";
      https = true;
      cli.memoryLimit = "1G"; # Applies to nextcloud-cron.service

      autoUpdateApps.enable = true;

      extraApps = {
        inherit (pkgs."nextcloud${toString nextcloudVersion}Packages".apps)
          # TODO some migration issue
          #bookmarks
          calendar
          #contacts # hash mismatch on build with ver. 30...
          groupfolders
          impersonate
          integration_openai
          notify_push
          #maps # not available in 30
          notes
          previewgenerator
          richdocuments
          twofactor_webauthn
          whiteboard
          ;
      };
      extraAppsEnable = true;
      appstoreEnable = true;

      phpOptions = {
        # The PHP OPcache module is not properly configured. The OPcache interned strings buffer is nearly full. To assure that repeating strings can be effectively cached, it is recommended to apply "opcache.interned_strings_buffer" to your PHP configuration with a value higher than "8"..
        "opcache.interned_strings_buffer" = 16;
        "opcache.revalidate_freq" = 30;
        # PHP 8.0 and above has JIT
        "opcache.jit" = "1255";
        "opcache.jit_buffer_size" = "128M";
      };

      phpExtraExtensions = exts: with exts; [
        #php-systemd
        intl # for better translations(?)
        sodium # for stronger password encryption
        inotify # for INotify file watcher app
        gmp # for SFTP storage
        exif # for image rotation in pictures app
        ftp # for FTP storage
        imagick # for image previews
        pcntl # for command-line processing (press Control-C)
      ];

      notify_push = {
        enable = true;
      };

      webfinger = true; # Enables use of the webfinger plugin

      config = {
        adminpassFile = "/var/nextcloud-admin-password";
        dbtype = "pgsql";
        dbhost = "/var/run/postgresql";
        inherit dbname dbuser;
      };

      settings = {
        # needs php-systemd php extension which isn't packaged...
        #log_type = "systemd";

        trusted_domains = [
          "nextcloud.${config.networking.domain}"
          address4
          "[${address6.prefix}::1]"
        ];

        trusted_proxies = [
          address4
          "${address6.prefix}::/${toString address6.prefixLength}"
        ];

        default_language = "en";
        default_phone_region = "AT";

        overwriteprotocol = "https";
        "overwrite.cli.url" = "https://cloud.${config.networking.domain}/";
        "profile.enabled" = true;
        "db.log_request_id" = true;
        "knowledgebase.embedded" = false;

        remember_login_cookie_lifetime = 3600 * 24 * 45; # 45 days
        session_lifetime = 3600 * 24 * 5; # 5 days

        # 'logo_url' => 'https://example.org',

        # Update checker is annoying.
        updatechecker = false;

        defaultapp = "dashboard,files";

        #apps_paths = [ ];

        preview_max_memory = 700;
        preview_max_scale_factor = 3;
        preview_libreoffice_path = lib.getExe libreofficePackage;
        preview_ffmpeg_path = lib.getExe ffmpegPackage;

        #"preview_imaginary_url" => 'http://previews_hpb:8088/',
        #"preview_imaginary_key" = "";
        enabledPreviewProviders = [
          # Optional
          #OC\Preview\Font
          #OC\Preview\HEIC
          #OC\Preview\Illustrator
          #OC\Preview\Movie
          "OC\Preview\MSOffice2003"
          "OC\Preview\MSOffice2007"
          "OC\Preview\MSOfficeDoc"
          "OC\Preview\PDF"
          #"OC\Preview\Photoshop"
          "OC\Preview\Postscript"
          "OC\Preview\StarOffice"
          "OC\Preview\SVG"
          "OC\Preview\TIFF"
          "OC\Preview\EMF"
          # Defaults:
          "OC\Preview\BMP"
          "OC\Preview\GIF"
          "OC\Preview\JPEG"
          "OC\Preview\Krita"
          "OC\Preview\MarkDown"
          "OC\Preview\MP3"
          "OC\Preview\OpenDocument"
          "OC\Preview\PNG"
          "OC\Preview\TXT"
          "OC\Preview\XBitmap"
        ];

        maintenance_window_start = 2;
      };
    };

    services.collabora-cloud = {
      enable = true;
      host = "office.${config.networking.domain}";
      aliasgroup1 = "https://${config.services.nextcloud.hostName}:443";

      # TODO https://github.com/NixOS/nixpkgs/blob/nixos-unstable/nixos/modules/services/web-apps/collabora-online.nix#L37
      #settings = {
      #  remote_font_config.url = "https://${config.services.nextcloud.hostName}/apps/richdocuments/settings/fonts.json";
      #  home_mode.enable = "true";
      #  security.enable_metrics_unauthenticated = "true";
      #  security.enable_macros_execution = "true";
      #  allowed_languages = "de_DE en_GB en_US fi";
      #};
    };

    # Enable backend for the Nextcloud Whiteboard app
    services.nextcloud-whiteboard-server = {
      enable = true;

      # List of environment files. One these should set JWT_SECRET_KEY.
      # The secret key can be set like this:
      # occ config:app:set whiteboard jwt_secret_key --value="some-random"
      # Also the backend app url should be configured:
      # occ config:app:set whiteboard collabBackendUrl --value="http://localhost:3002"
      secrets = [
        config.age.secrets.nextcloud-whiteboard-env.path
      ];

      settings = {
        NEXTCLOUD_URL = "https://${config.services.nextcloud.hostName}";
      };
    };

    age.secrets.nextcloud-whiteboard-env = {
      file = ../../secrets/nextcloud-whiteboard.env.age;
      mode = "640";
      group = "nextcloud";
    };

    services.nginx.virtualHosts.${config.services.nextcloud.hostName} = {
      forceSSL = true;
      enableACME = true;
    };

    # Add extra stuff to PATH (used by some modules)
    services.phpfpm.pools.nextcloud.phpEnv.PATH = lib.mkForce
      (lib.makeBinPath (extraPackagesToPath ++ [
        "/run/wrappers"
        "/nix/var/nix/profiles/default"
        "/run/current-system/sw"
      ]));
  };
}
