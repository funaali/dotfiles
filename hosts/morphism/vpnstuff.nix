{ config, pkgs, lib, ... }:

with lib;

let

  server = {
    iface = "wg-rtorrent";
    ips = [ "172.16.16.1/24" ];
    privateKeyFile = "/var/lib/private/wg-rtorrent.key";
    listenPort = 51820; # UDP (wireguard)
    nat.subnet = "172.16.16.0/24";
    nat.oif = config.networking.nat.externalInterface;
    port.P2P = 64334; # TCP (rtorrent)
    port.DHT = 64434; # UDP (rtorrent)
  };

  peers = [{
    IP = "172.16.16.2";
    publicKey = "AbiYXgSrwCrPR8MOQcMDPfl/Qa/T1kHTKXqUvmi16A8=";
    forwardPorts = [
      { source = "P2P"; port = 64334; proto = "tcp"; } # rtorrent
      { source = "DHT"; port = 64434; proto = "udp"; } # rtorrent
    ];
  }];


in
{
  networking.firewall.allowedTCPPorts = [ server.port.P2P ];
  networking.firewall.allowedUDPPorts = [ server.listenPort server.port.DHT ];
  networking.firewall.checkReversePath = "loose";

  networking.nat.internalInterfaces = [ server.iface ];

  networking.nat.forwardPorts =
    concatMap
      (peer: map
        (fwd: {
          sourcePort = server.port.${fwd.source};
          destination = "${peer.IP}:${toString fwd.port}";
          proto = fwd.proto or "tcp";
        })
        (peer.forwardPorts or [ ])
      )
      peers;

  networking.wireguard.interfaces.${server.iface} = {
    inherit (server) ips listenPort privateKeyFile;
    # This allows the wireguard server to route your traffic to the internet and hence be like a VPN
    # For this to work you have to set the dnsserver IP of your router (or dnsserver of choice) in your clients
    postSetup = ''
      ${pkgs.iptables}/bin/iptables -t nat -A POSTROUTING -s ${server.nat.subnet} -o ${server.nat.oif} -j MASQUERADE
    '';
    # This undoes the above command
    postShutdown = ''
      ${pkgs.iptables}/bin/iptables -t nat -D POSTROUTING -s ${server.nat.subnet} -o ${server.nat.oif} -j MASQUERADE
    '';

    peers = map
      (peer: {
        allowedIPs = [ peer.IP ];
        inherit (peer) publicKey;
      })
      peers;
  };

  services.sshguard.whitelist = [
    #"2a02:8388:4081:ef70::/64" # Magenta-AT
  ];
}
