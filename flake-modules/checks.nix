{ self, lib, inputs, ... }:

# pre-commit and other checks

{
  imports = [
    inputs.pre-commit-hooks.flakeModule
  ];

  config = {
    perSystem = { system, ... }: {
      pre-commit.settings = {
        hooks.beautysh.enable = true;
        hooks.deadnix.enable = true;
        hooks.nixpkgs-fmt.enable = true;
        hooks.shellcheck.enable = true;
      };

      checks =
        let
          nixosConfigurations = lib.mapAttrs'
            (name: config: lib.nameValuePair "nixos-${name}" config.config.system.build.toplevel)
            ((lib.filterAttrs (_: config: config.pkgs.stdenv.system == system)) self.nixosConfigurations);

          homeConfigurations = lib.mapAttrs'
            (name: config: lib.nameValuePair "home-standalone-${name}" config.config.home.activationPackage)
            ((lib.filterAttrs (_: config: config.pkgs.stdenv.system == system)) self.homeConfigurations);
        in
        nixosConfigurations // homeConfigurations;
    };
  };
}
