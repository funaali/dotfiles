{ self, lib, config, inputs, withSystem, ... }:

# Configure repository layout e.g. where certain files/directories are located
# and how they should be loaded.

let
  inherit (lib) mapAttrs mkOption types;

  mylib = self.lib;
  cfg = config.layout;
in
{
  options.layout = {
    nixos = {
      hosts = null; # mylib.loadPaths # "Path to NixOS host definition modules.";
      modules = null; # mylib.loadPaths # "Path from which NixOS modules are loaded.";
      profiles = null; # mylib.loadPaths # "Path from which NixOS profile modules are loaded.";
      suites = null; # import suites/nixos.nix { inherit (nixos) profiles; }
    };
    home-manager = {
      users = null; # import ./home-users.nix;
      standalone = null; # import ./home-users-standalone.nix;
      modules = null; # modules/home/module-list.nix
      profiles = null; # mylib.loadPaths profiles/home;
      suites = null; # import suites/home.nix { inherit (hm) profiles; };
    };
  };

  config = { };
}
