{ self, lib, config, flake-parts-lib, inputs, withSystem, ... }:

# A flake-parts module for setting up nixpkgs channels and overlays.

let
  inherit (lib) mkOption types;
  inherit (flake-parts-lib) mkPerSystemOption;

  cfg = config.nixpkgs;

  overlayType = lib.mkOptionType {
    name = "nixpkgs-overlay";
    description = "nixpkgs overlay";
    check = builtins.isFunction;
    merge = lib.mergeOneOption;
  };

  # Get 'pkgs' for a system.
  pkgsFor = platform:
    withSystem platform.system ({ pkgs, ... }:
      if lib.systems.equals platform pkgs.stdenv.hostPlatform then pkgs else
      import pkgs.path {
        inherit (pkgs) config overlays;
        localSystem = platform;
      }
    );
in
{
  options.nixpkgs = {
    defaultChannel = mkOption {
      type = types.str;
      default = "nixpkgs";
      description = "The channel that will be used for the `pkgs` attribute.";
    };

    channels = mkOption {
      type = types.lazyAttrsOf types.bool;
      default = { nixpkgs = true; };
      description = "Channels in inputs to use.";
    };

    config = mkOption {
      type = types.lazyAttrsOf types.raw;
      default = { };
      description = "Nixpkgs config applied to all instances of Nixpkgs.";
    };

    overlays = mkOption {
      type = types.listOf overlayType;
      default = [ ];
      description = "Overlays applied to all instances of Nixpkgs.";
    };
  };

  options.perSystem = mkPerSystemOption ({ ... }: {
    options.channels = mkOption {
      type = types.lazyAttrsOf types.pkgs;
      readOnly = true;
      description = "An attribute set of channels' package sets.";
    };
  });

  config = {
    _module.args = {
      inherit pkgsFor;
    };

    perSystem = { system, config, pkgs, ... }: {

      # Set the perSystem 'channels' option value.
      channels = lib.mapAttrs (name: _:
        import inputs.${name} {
          inherit system;
          inherit (cfg) config overlays;
        }) cfg.channels;

      # Set the 'pkgs' flake-parts perSystem module argument.
      _module.args.pkgs = config.channels.${cfg.defaultChannel};

      # Populate 'packages' with packages defined in overlays.
      packages = self.lib.packagesFromOverlays pkgs self.overlays;
    };
  };
}
