{
  description = "dotfiles + home-manager + NixOS";

  nixConfig = {
    # Lower priority means higher priority. cache.nixos.org has priority 40.
    extra-substituters = [
    #  "http://nix.funktionaali.com?priority=20"
    #  "https://cache.iog.io" # IOG haskell.nix
    ];
    extra-trusted-public-keys = [
      "lamba-nebula.lan-1:CbLfyiZUm4TJAAlmfJ+1ID5ILupXyoseHtZlSBZ839c="
      "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
    ];
  };

  inputs = {
    systems.url = "github:nix-systems/default";

    nixpkgs-lib.url = "github:nix-community/nixpkgs.lib";

    # Use the large repo for more binary cache hits (instead of -small).
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    nixpkgs2211.url = "github:NixOS/nixpkgs/nixos-22.11";

    nixpkgs-dev.url = "github:NixOS/nixpkgs/master";

    hydra = {
      url = "github:NixOS/hydra";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Weekly updated nix-index database
    nix-index-database = {
      url = "github:nix-community/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };

    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.systems.follows = "systems";
    };

    flake-utils-plus = {
      url = "github:gytis-ivaskevicius/flake-utils-plus";
      inputs.flake-utils.follows = "flake-utils";
    };

    nixos-hardware.url = "github:NixOS/nixos-hardware";

    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nixpkgs-stable.follows = "nixpkgs";
    };

    nur = {
      url = "github:nix-community/NUR";
      inputs.flake-parts.follows = "flake-parts";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager = {
      url = "github:SimSaladin/home-manager?ref=develop2";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixGL = {
      url = "github:guibou/nixGL";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

    rtorrent-ps = {
      url = "github:SimSaladin/rtorrent-ps.nix";
      #url = "/home/sim/r/rtorrent-ps.nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flutils.follows = "flake-utils-plus";
    };

    rnix-lsp = {
      url = "github:nix-community/rnix-lsp";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.utils.follows = "flake-utils";
    };

    nix-colors = {
      url = "github:Misterio77/nix-colors";
      inputs.nixpkgs-lib.follows = "nixpkgs-lib";
    };

    agenix = {
      url = "github:ryantm/agenix";
      inputs.home-manager.follows = "home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.systems.follows = "systems";
    };

    haumea = {
      url = "github:nix-community/haumea";
      inputs.nixpkgs.follows = "nixpkgs-lib";
    };

    # Always latest AMD microcode
    ucodenix = {
      url = "github:e-tho/ucodenix";
      #inputs.nixpkgs.follows = "nixpkgs";
    };

    openwrt-imagebuilder = {
      url = "github:astro/nix-openwrt-imagebuilder";
      inputs.flake-parts.follows = "flake-parts";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.systems.follows = "systems";
    };
  };

  outputs = { self, flake-parts, nixpkgs2211, ... }@inputs:
    let
      mylib = self.lib;
    in

    flake-parts.lib.mkFlake { inherit inputs; } ({ ... }:
      {
        imports = [
          ./flake-module.nix
        ];

        config = {
          #debug = true;

          systems = [ "x86_64-linux" ];

          flake = {
            # Overlays defined here in this repo.
            overlays = mylib.loadOverlays ./overlays;

            flakeModules.default = import ./flake-module.nix;
          };

          nixpkgs = {
            # Configuration that is shared between all channels.
            config = import ./nixpkgs-config.nix;

            overlays = [
              inputs.hydra.overlays.default
              inputs.flake-utils-plus.overlay
              inputs.nur.overlays.default
              inputs.nixGL.overlays.default
              inputs.rtorrent-ps.overlays.default
              (mylib.genPkgOverlay inputs.rnix-lsp "rnix-lsp")
              (mylib.genPkgOverlay nixpkgs2211 "nix-linter") # nix-linter is broken in unstable
              (self: _: {
                __nixpkgs-dev = import inputs.nixpkgs-dev {
                  inherit (self) system;
                  config = import ./nixpkgs-config.nix;
                };
              })
            ] ++ builtins.attrValues self.overlays;
          };

          myConfigs = {

            nixos = {
              configurationsDirectory = ./hosts;
              modulesDirectory = ./modules/nixos;
              profilesDirectory = ./profiles/nixos;
              sharedModules = [
                # Modules shared by all NixOS configurations.
                inputs.flake-utils-plus.nixosModules.autoGenFromInputs
                inputs.agenix.nixosModules.default
                inputs.nur.modules.nixos.default
                inputs.home-manager.nixosModules.home-manager
                inputs.ucodenix.nixosModules.ucodenix
                self.nixosModules.my-modules
                self.nixosModules.hm-users
              ];
              specialArgs = {
                inherit inputs mylib;
                inherit (inputs) nixos-hardware;
              };
            };

            hm = {
              sharedModules = [
                inputs.agenix.homeManagerModules.age
                inputs.rtorrent-ps.hmModules.default
                inputs.nix-index-database.hmModules.nix-index
                self.hmModules.my-modules
              ];
              extraSpecialArgs = {
                inherit (inputs) nix-colors;
              };
            };
          };
        };
      });
}

