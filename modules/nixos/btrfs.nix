{ config, lib, ... }:

# Configure periodic scrub for all btrfs filesystems.

with lib;

let
  cfg = config.custom.btrfs;

  btrfsFileSystems = filterAttrs (_: x: x.fsType == "btrfs") config.fileSystems;

in {
  options.custom.btrfs = {
    enable = mkEnableOption "custom.btrfs" // { default = true; };
  };

  config = mkIf cfg.enable {
    services.btrfs.autoScrub = {
      enable = mkDefault (btrfsFileSystems != { });
      interval = mkDefault "weekly";
      fileSystems = attrNames btrfsFileSystems;
    };
  };
}
