{ config, lib, pkgs, ... }:

with lib;
with types;

let
  cfg = config.services.fan2go;
  format = pkgs.formats.yaml { };

  unvalidatedConfigFile = format.generate "fan2go.yaml" cfg.settings;

  # Validate the config file
  configFile = pkgs.runCommand "fan2go.yaml"
    {
      nativeBuildInputs = [
        pkgs.util-linux
      ];
    } ''
    cp ${unvalidatedConfigFile} $out
    unshare -r ${cfg.package}/bin/fan2go -c $out config validate
  '';

  filterNotEmpty = filterAttrsRecursive (_: v: v != null && v != { });

  pwmType = ints.between 0 255;

  execCmdType = submodule {
    options = {
      exec = mkOption { type = either nonEmptyStr path; };
      args = mkOption { type = listOf str; default = [ ]; };
    };
  };

  pidControlLoopOptions = {
    p = mkOption { type = float; };
    i = mkOption { type = float; };
    d = mkOption { type = float; };
  };

  fanOptions =
    let
      fanFileOptions.options = {
        path = mkOption {
          type = path;
          description = "Path to a file to set the PWM target for this fan";
        };
        rpmPath = mkOption {
          type = nullOr path;
          default = null;
          description = "Path to a file to read the current RPM value of this fan";
        };
      };

      fanHwmonOptions.options = {
        platform = mkOption {
          # NOTE: should always match a sensor.platform
          type = nonEmptyStr;
          description = "The platform of the controller which is connected to this fan (see sensor.platform)";
          example = "nct6798";
        };
        rpmChannel = mkOption {
          type = ints.positive;
          description = "The channel of this fan's RPM sensor as displayed by `fan2go detect`";
          example = 1;
        };
        pwmChannel = mkOption {
          type = nullOr ints.positive;
          default = null;
          description = "The pwm channel that controls this fan; fan2go defaults to same channel number as fan RPM";
          example = 1;
        };
      };

      fanCmdOptions = {
        options = {
          setPwm = mkOption { type = execCmdType; };
          getPwm = mkOption { type = execCmdType; };
          getRpm = mkOption { type = nullOr execCmdType; default = null; };
        };
      };
    in
    {
      options = {
        curve = mkOption {
          # NOTE: should always match a curve in curves
          type = nonEmptyStr;
          description = "The curve ID that should be used to determine the speed of this fan";
        };
        neverStop = mkOption {
          type = bool;
          default = false;
          description = "Indicates whether this fan should never stop rotating, regardless of how low the curve value is";
        };
        # One of hwmon, file, cmd may be defined
        hwmon = mkOption { type = nullOr (submodule fanHwmonOptions); default = null; };
        file = mkOption { type = nullOr (submodule fanFileOptions); default = null; };
        cmd = mkOption { type = nullOr (submodule fanCmdOptions); default = null; };
        # Other
        minPwm = mkOption {
          type = nullOr pwmType;
          default = null;
          description = "Override for the lowest PWM value at which the fan is able to maintain rotation if it was spinning previously.";
        };
        startPwm = mkOption {
          type = nullOr pwmType;
          default = null;
          description = ''
            (Optional) Override for the lowest PWM value at which the fan will still be able to start rotating.
            Note: Settings this to a value that is too small may damage your fans. Use at your own risk!
          '';
        };
        maxPwm = mkOption {
          type = nullOr pwmType;
          default = null;
          description = ''
            (Optional) Override for the highest PWM value which still yields
            an increased rotational speed compared to lower values.
            Note: you can also use this to limit the max speed of a fan
          '';
        };
        pwmMap = mkOption {
          type = attrsOf pwmType;
          default = { };
        };
        controlLoop = mkOption {
          type = nullOr (submodule { options = pidControlLoopOptions; });
          default = null;
        };
      };
    };

  curveType =
    let
      linearCurveOptions = {
        options = {
          # NOTE needs to match a defined sensor
          sensor = mkOption { type = nonEmptyStr; };
          min = mkOption { type = nullOr int; default = null; };
          max = mkOption { type = nullOr int; default = null; };
          # NOTE: steps is incompatible with min/max
          steps = mkOption {
            type = nullOr (nonEmptyListOf (attrsOf pwmType));
            default = null;
            description = "Steps to define a section-wise defined speed curve function. Sensor value -> Speed (in pwm)";
          };
        };
      };

      pidCurveOptions = {
        options = {
          sensor = mkOption { type = nonEmptyStr; };
          setPoint = mkOption { type = int; };
        } // pidControlLoopOptions;
      };

      functionCurveOptions = {
        options = {
          type = mkOption {
            type = enum [ "minimum" "maximum" "average" "delta" "sum" "difference" ];
            default = "average";
          };
          curves = mkOption {
            # NOTE: each must match a curve id (that is not this curve)
            type = nonEmptyListOf nonEmptyStr;
            description = "A list of curve IDs to use";
          };
        };
      };

    in
    submodule {
      options = {
        # NOTE linear|pid|function are mutually exclusive
        linear = mkOption { type = nullOr (submodule linearCurveOptions); default = null; };
        pid = mkOption { type = nullOr (submodule pidCurveOptions); default = null; };
        function = mkOption { type = nullOr (submodule functionCurveOptions); default = null; };
      };
    };

  # The type of sensor configuration, one of: hwmon | file | cmd
  # NOTE hwmon file and cmd are mutually exclusive
  sensorType =
    let
      hwmonType = submodule {
        options = {
          platform = mkOption {
            type = nonEmptyStr;
            description = ''
              A regex matching a controller platform displayed by `fan2go detect`, f.ex.:
              "coretemp", "it8620", "corsaircpro-*" etc.
            '';
            example = "coretemp";
          };

          index = mkOption {
            type = ints.positive;
            description = "The index of this sensor as displayed by `fan2go detect`";
            example = 1;
          };
        };
      };
      fileType = submodule {
        options = {
          path = mkOption {
            type = path;
            description = "Path to the file containing sensor values. The file contains a value in milli-units, like f.ex. milli-degrees.";
          };
        };
      };
    in
    submodule {
      options = {
        hwmon = mkOption { type = nullOr hwmonType; default = null; };
        file = mkOption { type = nullOr fileType; default = null; };
        cmd = mkOption { type = nullOr execCmdType; default = null; };
      };
    };

in
{
  options.services.fan2go = {
    enable = mkEnableOption "services.fan2go";

    package = mkPackageOption pkgs "fan2go" { };

    settings = mkOption {
      type = submodule {
        freeformType = format.type;

        options = {
          dbPath = mkOption {
            type = path;
            default = "/var/lib/fan2go.db";
            description = "The path of the database file.";
          };

          fanResponseDelay = mkOption {
            type = ints.positive;
            default = 2;
          };

          runFanInitializationInParallel = mkOption {
            type = bool;
            default = true; # fan2go default: false
            description = "Allow the fan initialization sequence to run in parallel for all configured fans";
          };

          fans = mkOption {
            type = attrsOf (submodule fanOptions);
            apply = xs: mapAttrsToList (id: x: x // { inherit id; }) (filterNotEmpty xs);
            default = { };
          };

          curves = mkOption {
            type = attrsOf curveType;
            apply = xs: mapAttrsToList (id: x: x // { inherit id; }) (filterNotEmpty xs);
            default = { };
          };

          sensors = mkOption {
            type = attrsOf sensorType;
            apply = xs: mapAttrsToList (id: x: x // { inherit id; }) (filterNotEmpty xs);
            default = { };
          };

          api = {
            enabled = mkEnableOption "API server";
            host = mkOption {
              type = str;
              default = "localhost";
            };
            port = mkOption {
              type = port;
              default = 9001;
            };
          };

          statistics = {
            enabled = mkEnableOption "Prometheus exporter";
            port = mkOption {
              type = port;
              default = 9000;
            };
          };

          profiling = {
            enabled = mkEnableOption "profiling webserver";
            host = mkOption {
              type = str;
              default = "localhost";
            };
            port = mkOption {
              type = port;
              default = 6060;
            };
          };
        };
      };

      default = { };
      description = ''
        Configuration for fan2go (fan2go.yaml).
        See: <https://github.com/markusressel/fan2go/blob/master/fan2go.yaml>
      '';
    };

    openFirewall = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = "Open the firewall for fan2go API, statistics and profiling.";
    };

    display = mkOption {
      type = str;
      default = "";
      description = "Set DISPLAY variable. Enables desktop notifications.";
      example = ":0";
    };
  };

  config = mkIf cfg.enable {

    networking.firewall.allowedTCPPorts =
      lib.optionals (cfg.openFirewall && cfg.settings.api.enabled) [ cfg.settings.api.port ] ++
      lib.optionals (cfg.openFirewall && cfg.settings.statistics.enabled) [ cfg.settings.statistics.port ] ++
      lib.optionals (cfg.openFirewall && cfg.settings.profiling.enabled) [ cfg.settings.profiling.port ];

    environment.systemPackages = [ cfg.package pkgs.fan2go-tui ];

    environment.etc."fan2go/fan2go.yaml".source = configFile;

    systemd.services.fan2go = {
      description = "Advanced Fan Control program";
      wantedBy = [ "multi-user.target" ];
      restartTriggers = [ configFile ];

      unitConfig.Documentation = "https://github.com/markusressel/fan2go";

      # Avoid stopping the unit when deploying updates.
      stopIfChanged = false;

      path = [
        cfg.package
        pkgs.bash
        pkgs.coreutils
        pkgs.procps
      ];

      environment.DISPLAY = mkIf (cfg.display != "") cfg.display; # for notifications

      serviceConfig = {
        ExecStart = "${cfg.package}/bin/fan2go -c /etc/fan2go/fan2go.yaml --no-style";
        Restart = "always";
        RestartSec = 1;
        LimitNOFILE = 8192;
      };
    };
  };
}
