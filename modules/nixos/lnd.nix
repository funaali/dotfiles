{ config, pkgs, lib, ... }:

with lib;
let
  eachLnd = filterAttrs (_: cfg: cfg.enable) config.services.lnd;

  lndOpts = { config, name, ... }: {
    options = {
      enable = mkEnableOption "lnd daemon";

      package = mkPackageOption pkgs "lnd" { };

      openFirewall = mkOption {
        type = types.bool;
        default = false;
        description = "Open firewall for the P2P port.";
      };

      bitcoin = {
        node = mkOption {
          type = types.enum [ "btcd" ];
        };

        testnet = mkOption {
          type = types.bool;
          default = true;
          description = "Whether to use the testnet instead of mainnet.";
        };

      };

      btcd = {
        rpc.host = mkOption {
          type = types.str;
          default = "127.0.0.1"; # :8334
        };

        rpc.user = mkOption {
          type = types.str;
        };

        rpc.pass = mkOption {
          type = types.str;
        };
      };

      user = mkOption {
        type = types.str;
        default = "lnd-${name}";
        description = "The user as which to run lnd.";
      };

      group = mkOption {
        type = types.str;
        default = config.user;
        description = "The group as which to run lnd.";
      };

      lndDir = mkOption {
        type = types.path;
        default = "/var/lib/lnd-${name}";
        description = "The root directory for lnd.";
      };

      rpc = {
        port = mkOption {
          type = types.nullOr types.port;
          default = null;
          description = "Override the default RPC port.";
        };

        restPort = mkOption {
          type = types.nullOr types.port;
          default = null;
          description = "Override the default REST port.";
        };

        user.name = mkOption {
          type = types.nullOr types.str;
          default = null;
          description = "RPC admin user name";
        };

        user.pass = mkOption {
          type = types.nullOr types.str;
          default = null;
          description = "RPC admin user password";
        };
      };

      port = mkOption {
        type = types.nullOr types.port;
        default = 9735;
        description = "Override the default port on which to listen for connections. Default is 9735.";
      };

      configFile = mkOption {
        type = types.nullOr types.path;
        default = null;
        example = "/var/lib/${name}/lnd.conf";
        description = "The configuration file path to supply lnd.";
      };

      extraConfig = mkOption {
        type = types.lines;
        default = "";
        description = ''
          Additional configurations to be appended to {file}`lnd.conf`.
        '';
      };

      extraCmdlineOptions = mkOption {
        type = types.listOf types.str;
        default = [];
        description = ''Extra command line options to pass to lnd.'';
      };
    };
  };

  mk-lncli-wrapper = name: cfg: pkgs.writeShellScriptBin "lncli-${name}" ''
    sudo -u ${cfg.user} ${cfg.package}/bin/lncli --lnddir=${cfg.lndDir} \
      ${optionalString cfg.bitcoin.testnet "--network testnet"} \
      "$@"
  '';
in

{
  options = {
    services.lnd = mkOption {
      type = types.attrsOf (types.submodule lndOpts);
      default = { };
      description = "Lnd instances";
    };
  };

  config = mkIf (eachLnd != {}) {
    environment.systemPackages = flatten (mapAttrsToList (name: cfg: [
      cfg.package
      (mk-lncli-wrapper name cfg)
    ]) eachLnd);

    networking.firewall.allowedTCPPorts = flatten (mapAttrsToList (_: cfg:
      optional cfg.openFirewall cfg.port
    ) eachLnd);

    systemd.services = mapAttrs' (lndName: cfg: (
      nameValuePair "lnd-${lndName}" (
        let
          configFile = pkgs.writeText "lnd.conf" ''
            [Application Options]
            alias=${config.system.name}.${config.networking.domain}

            [bolt]
            db.bolt.auto-compact=true

            [Bitcoin]
            ${optionalString (cfg.bitcoin.testnet) "bitcoin.testnet=true"}
            ${optionalString (!cfg.bitcoin.testnet) "bitcoin.mainnet=true"}

            ${cfg.extraConfig}
          '';
        in
        {
        description = "Lnd daemon";
        wants = [ "network-online.target" ];
        after = [ "network-online.target" "btcd-${lndName}.service" ];
        partOf = [ "btcd-${lndName}.service" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          SyslogIdentifier = "lnd-${lndName}";
          User = cfg.user;
          Group = cfg.group;
          Type = "notify";
          ExecStart = ''
            ${cfg.package}/bin/lnd \
            --lnddir=${cfg.lndDir} \
            ${if (cfg.configFile != null) then
              "--configfile=${cfg.configFile}"
            else
              "--configfile=${configFile}"
            } \
            ${optionalString (cfg.port != null) "--listen=:${toString cfg.port}"} \
            ${optionalString (cfg.rpc.port != null) "--rpclisten=:${toString cfg.rpc.port}"} \
            ${optionalString (cfg.rpc.restPort != null) "--restlisten=:${toString cfg.rpc.restPort}"} \
            ${toString (optionals (cfg.bitcoin.node == "btcd") [
              "--btcd.rpccert=/var/lib/btcd-${lndName}/rpc.cert"
              (optionalString (cfg.btcd.rpc.host != null) "--btcd.rpchost=${toString cfg.btcd.rpc.host}")
              (optionalString (cfg.btcd.rpc.user != null) "--btcd.rpcuser=${toString cfg.btcd.rpc.user}")
              (optionalString (cfg.btcd.rpc.pass != null) "--btcd.rpcpass=${toString cfg.btcd.rpc.pass}")
            ])} \
            ${toString cfg.extraCmdlineOptions}
          '';
          ExecStop = ''
            ${cfg.package}/bin/lncli --lnddir=${cfg.lndDir} \
            ${optionalString cfg.bitcoin.testnet "--network testnet"} stop
          '';
          Restart = "on-failure";
          RestartSec = "60";
          TimeoutStartSec = "1200";
          TimeoutStopSec = "3600";
          SupplementaryGroups = "btcd-${lndName}";

          # Hardening measures
          PrivateTmp = "true";
          ProtectSystem = "full";
          NoNewPrivileges = "true";
          PrivateDevices = "true";
          MemoryDenyWriteExecute = "true";
        };
      }
      )
    )) eachLnd;

    systemd.tmpfiles.rules = flatten (mapAttrsToList (_: cfg: [
      "d '${cfg.lndDir}' 0770 '${cfg.user}' '${cfg.group}' - -"
    ]) eachLnd);

    users.users = mapAttrs' (lndName: cfg: (
      nameValuePair "lnd-${lndName}" {
      name = cfg.user;
      group = cfg.group;
      description = "lnd daemon user for instance ${lndName}";
      home = cfg.lndDir;
      isSystemUser = true;
    })) eachLnd;

    users.groups = mapAttrs' (_: cfg: (
      nameValuePair cfg.group { }
    )) eachLnd;
  };
}
