{ config, pkgs, lib, ... }:

with lib;
let
  eachBtcd = filterAttrs (_: cfg: cfg.enable) config.services.btcd;

  btcdOpts = { config, name, ... }: {
    options = {
      enable = mkEnableOption "btcd daemon";

      package = mkPackageOption pkgs "btcd" { };

      openFirewall = mkOption {
        type = types.bool;
        default = false;
        description = "Open firewall for the P2P port.";
      };

      configFile = mkOption {
        type = types.nullOr types.path;
        default = null;
        example = "/var/lib/${name}/btcd.conf";
        description = "The configuration file path to supply btcd.";
      };

      extraConfig = mkOption {
        type = types.lines;
        default = "";
        description = ''
          Additional configurations to be appended to {file}`btcd.conf`.
          See <https://github.com/btcsuite/btcd/blob/master/sample-btcd.conf>
          for available options.
        '';
      };

      dataDir = mkOption {
        type = types.path;
        default = "/var/lib/btcd-${name}";
        description = "The data directory for btcd.";
      };

      user = mkOption {
        type = types.str;
        default = "btcd-${name}";
        description = "The user as which to run btcd.";
      };

      group = mkOption {
        type = types.str;
        default = config.user;
        description = "The group as which to run btcd.";
      };

      rpc = {
        port = mkOption {
          type = types.nullOr types.port;
          default = null;
          description = "Override the default RPC port. Default is 8334.";
        };

        user.name = mkOption {
          type = types.nullOr types.str;
          default = null;
          description = "RPC admin user name";
        };

        user.pass = mkOption {
          type = types.nullOr types.str;
          default = null;
          description = "RPC admin user password";
        };

        limituser.name = mkOption {
          type = types.nullOr types.str;
          default = null;
          description = "RPC limited user name";
        };

        limituser.pass = mkOption {
          type = types.nullOr types.str;
          default = null;
          description = "RPC limited user password";
        };
      };

      testnet = mkOption {
        type = types.bool;
        default = false;
        description = "Whether to use the testnet instead of mainnet.";
      };

      port = mkOption {
        type = types.nullOr types.port;
        default = null;
        description = "Override the default port on which to listen for connections. Default is 8333 (mainnet). For testnet custom is 18333.";
      };

      extraCmdlineOptions = mkOption {
        type = types.listOf types.str;
        default = [];
        description = ''
          Extra command line options to pass to btcd.
          Run btcd --help to list all available options.
        '';
      };
    };
  };

  configFile = cfg: pkgs.writeText "btcd.conf" ''
    [Application Options]
    ; RPC users
    ${optionalString (cfg.rpc.user.name != null) "rpcuser=${cfg.rpc.user.name}"}
    ${optionalString (cfg.rpc.user.pass != null) "rpcpass=${cfg.rpc.user.pass}"}
    ${optionalString (cfg.rpc.limituser.name != null) "rpclimituser=${cfg.rpc.limituser.name}"}
    ${optionalString (cfg.rpc.limituser.pass != null) "rpclimitpass=${cfg.rpc.limituser.pass}"}
    ; Extra config options (from btcd nixos service)
    ${cfg.extraConfig}
  '';

  mk-btcctl-wrapper = name: cfg: pkgs.writeShellScriptBin "btcctl-${name}" ''
    sudo -u ${cfg.user} ${cfg.package}/bin/btcctl \
      ${if (cfg.configFile != null) then
        "--configfile=${cfg.configFile}"
      else
        "--configfile=${configFile cfg}"
      } \
      --rpccert=${cfg.dataDir}/rpc.cert \
      ${optionalString (cfg.rpc.port != null) "--rpcserver=localhost:${toString cfg.rpc.port}"} \
      ${optionalString (cfg.rpc.user.name != null) "--rpcuser=${cfg.rpc.user.name}"} \
      ${optionalString (cfg.rpc.user.pass != null) "--rpcpass=${cfg.rpc.user.pass}"} \
      ${optionalString cfg.testnet "--testnet"} \
      "$@"
  '';
in

{
  options = {
    services.btcd = mkOption {
      type = types.attrsOf (types.submodule btcdOpts);
      default = { };
      description = "One or more btcd instances.";
    };
  };

  config = mkIf (eachBtcd != {}) {

    assertions = [
      {
        assertion = length (flatten (mapAttrsToList (_: cfg: [ cfg.package ]))) == 1;
        message = ''
          Currently only one distinct btcd package may be defined across all
          instances.
        '';
      }
    ];

    environment.systemPackages = flatten (mapAttrsToList (name: cfg: [
      cfg.package
      (mk-btcctl-wrapper name cfg)
    ]) eachBtcd);

    networking.firewall.allowedTCPPorts = flatten (mapAttrsToList (_: cfg:
      optional cfg.openFirewall cfg.port
    ) eachBtcd);

    systemd.services = mapAttrs' (btcdName: cfg: (
      nameValuePair "btcd-${btcdName}" (
       {
        description = "Btcd daemon";
        wants = [ "network-online.target" ];
        after = [ "network-online.target" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          SyslogIdentifier = "btcd-${btcdName}";
          User = cfg.user;
          Group = cfg.group;
          ExecStart = ''
            ${cfg.package}/bin/btcd \
            ${if (cfg.configFile != null) then
              "--configfile=${cfg.configFile}"
            else
              "--configfile=${configFile cfg}"
            } \
            --datadir=${cfg.dataDir} \
            --logdir=${cfg.dataDir}/logs \
            --rpccert=${cfg.dataDir}/rpc.cert \
            --rpckey=${cfg.dataDir}/rpc.key \
            ${optionalString cfg.testnet "--testnet"}\
            ${optionalString (cfg.port != null) "--listen=:${toString cfg.port}"}\
            ${optionalString (cfg.rpc.port != null) "--rpclisten=:${toString cfg.rpc.port}"}\
            ${toString cfg.extraCmdlineOptions}
          '';
          #RuntimeDirectory = "btcd/${btcdName}";
          Restart = "on-failure";

          # Hardening measures
          PrivateTmp = "true";
          ProtectSystem = "full";
          NoNewPrivileges = "true";
          PrivateDevices = "true";
          MemoryDenyWriteExecute = "true";
        };
      }
    ))) eachBtcd;

    systemd.tmpfiles.rules = flatten (mapAttrsToList (_: cfg: [
      "d '${cfg.dataDir}' 0770 '${cfg.user}' '${cfg.group}' - -"
    ]) eachBtcd);

    users.users = mapAttrs' (btcdName: cfg: (
      nameValuePair "btcd-${btcdName}" {
      name = cfg.user;
      group = cfg.group;
      description = "btcd daemon user for instance ${btcdName}";
      home = cfg.dataDir;
      isSystemUser = true;
    })) eachBtcd;

    users.groups = mapAttrs' (_: cfg: (
      nameValuePair cfg.group { }
    )) eachBtcd;
  };
}
