{ lib, config, pkgs, ... }:

let
  cfg = config.custom.displaylink;
in
{
  options.custom.displaylink = {
    enable = lib.mkEnableOption "DisplayLink drivers";
  };

  config = lib.mkIf cfg.enable {
    boot.extraModulePackages = [ config.boot.kernelPackages.evdi ];

    # Needed?
    boot.kernelModules = [ "evdi" ];

    environment.systemPackages = [ pkgs.displaylink ];

    services.xserver.videoDrivers = [ "evdi" ];

    systemd.services.displaylink = {
      description = "DisplayLink Manager Service";
      after = [ "display-manager.service" ];
      #conflicts=getty@tty7.service
      serviceConfig = {
        # ExecStartPre=/sbin/modprobe evdi
        ExecStart = lib.getExe pkgs.displaylink;
        Restart = "always";
        WorkingDirectory = "/";
        RestartSec = 5;
      };
      wantedBy = [ "graphical.target" ];
    };
  };
}
