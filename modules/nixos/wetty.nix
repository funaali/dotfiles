{ config, lib, ... }:

# Wetty web terminal
# Usage: https://github.com/butlerx/wetty

with lib;

let
  cfg = config.services.wetty;
in
{
  options.services.wetty = {
    enable = mkEnableOption "services.wetty";

    image = mkOption {
      type = types.str;
      default = "wettyoss/wetty:latest@sha256:cea6d5f7284d5e943b55f939b7d347e1c642d5ed1c53c84b9a41c880d60ecaf4";
      description = "The wetty container image to use.";
    };

    name = mkOption {
      type = types.str;
      default = "wetty";
      description = "The container hostname.";
    };

    hostPort = mkOption {
      type = types.int;
      default = 3000;
      description = "The port on the host to bind to (localhost only).";
    };

    sshHost = mkOption {
      type = types.str;
      default = "127.0.0.1";
      description = "Target host";
    };

    vhost = mkOption {
      type = types.str;
      default = "localhost";
      description = "vhost proxy";
    };
  };

  config = mkIf cfg.enable {
    virtualisation.oci-containers.containers."wetty" = {
      image = cfg.image;
      ports = [ "127.0.0.1:${builtins.toString cfg.hostPort}:3000" ];
      hostname = cfg.name;
      cmd = [
        "--base=/"
        "--ssh-host=${cfg.sshHost}"
      ];
    };

    services.nginx.virtualHosts."${cfg.vhost}" = {
      forceSSL = true;
      enableACME = true;
      locations."^~ /" = {
        priority = 200;
        extraConfig = ''
          proxy_pass http://127.0.0.1:${builtins.toString cfg.hostPort};
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "upgrade";
          proxy_read_timeout 43200000;

          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header Host $host;
          proxy_set_header X-NginX-Proxy true;
        '';
      };
    };
  };
}
