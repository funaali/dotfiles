{ lib, config, pkgs, ... }:
with lib;
let
  name = "ddns";

  cfg = config.services.${name};

  updateScript = pkgs.writeShellApplication rec {
    name = "update-ddns";
    meta.mainProgram = name;
    runtimeInputs = with pkgs; [ getopt curl jq gawk ];
    text = builtins.readFile ./ddns.sh;
  };
in
{
  options.services.${name} = {
    enable = mkEnableOption "Dynamic DNS using one of supported providers (DNSimple, Hetzner)";

    provider = mkOption {
      type = types.enum [ "dnsimple" "hetzner" ];
      default = "dnsimple";
      description = "The provider to use.";
    };

    zone = mkOption {
      type = types.str;
      description = "DNS zone name or ID (\"foobar.com\")";
    };

    recordName = mkOption {
      type = types.str;
      default = config.networking.hostName;
      description = "Record name. Defaults to host name.";
    };

    envFile = mkOption {
      type = types.nullOr types.str;
      description = ''
        Env file for secrets and further configuration. Provider-dependent.

        For DNSimple, needs at least:
          ACCOUNT_ID=...
          TOKEN=...
      '';
    };
  };

  config = mkIf cfg.enable {
    systemd.timers.${name} = {
      timerConfig.OnActiveSec = "10s";
      timerConfig.OnUnitActiveSec = "30min";
      wantedBy = [ "timers.target" ];
    };
    systemd.services.${name} = {
      path = with pkgs; [ bashInteractive curl gawk jq ];
      environment = {
        PROVIDER = cfg.provider;
        ZONE_ID = cfg.zone;
        RECORD_NAME = cfg.recordName;
      };
      serviceConfig = {
        Type = "oneshot";
        StateDirectory = name;
        EnvironmentFile = mkIf (cfg.envFile != null) cfg.envFile;
        ExecStart = getExe updateScript;
      };
    };
  };
}
