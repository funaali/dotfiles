#!/usr/bin/env bash

set -peuo pipefail

VERBOSE=${VERBOSE:-}
stateDirectory=${STATE_DIRECTORY:-/var/lib/ddns}
recordTTL=${RECORD_TTL:-600}

# Current is: "<id> <name> <value> <ttl>"
stateSchemaVersion=1

die(){
    echo "$(basename "$0"): $*" >&2
    exit 1
}

info(){
    printf 'INFO: %s\n' "$*"
}

err(){
    echo "ERROR: $*" >&2
}

dbg(){
    if [[ -n $VERBOSE ]]; then
        echo "DEBUG: $*" >&2
    fi
}

if [[ ! -d $stateDirectory ]]; then
    die "State directory $stateDirectory does not exist"
fi

preStartSanityChecks(){
    for v in PROVIDER TOKEN ZONE_ID RECORD_NAME; do
        if [[ ! -v $v ]] || [[ -z $v ]]; then
            die "Required variable $v not defined or empty"
        fi
    done

    if [[ $PROVIDER == dnsimple ]]; then
        # shellcheck disable=SC2043
        for v in ACCOUNT_ID; do
            if [[ ! -v $v ]] || [[ -z $v ]]; then
                die "Required variable $v for provider $PROVIDER, but not defined or empty"
            fi
        done
    fi

    dbg "Configuration from environment and adapted:"
    dbg "  PROVIDER=$PROVIDER"
    dbg "  ZONE_ID=$ZONE_ID"
    dbg "  RECORD_NAME=$RECORD_NAME"
    dbg "  STATE_DIRECTORY=$STATE_DIRECTORY"
}

usage(){
    cat <<END
    $0 [OPTIONS]

    Note that commandline switches take precedence over environment variables.

    Options:
      -h,     --help          Print this
      -v,     --verbose       Log more verbosely
      -pENUM, --provider=ENUM Pick the DNS API provider. (\$PROVIDER) [dnsimple, hetzner]
      -nTEXT, --name=TEXT     The record name. (\$RECORD_NAME)
      -sTYPE, --set=TYPE      The DNS record type to set. Either A or AAAA is supported.
      -jFILE, --json-file=FILE Provide more comprehensive instructions in a JSON file. See below.
      -zTEXT, --zone-id=TEXT   The zone id (or name seems to work too with most providers). (\$ZONE_ID)

    JSON config format:
      [
        {
          "type" "",
          "name": "?",        /* optional, RECORD_NAME by default */
          "value": "?",       /* optional, if exists, it forces the given value instead of looking up the iP ad-hoc for the value.*/
          "ttl": 3600,        /* optional */
        },
        [...]
      ]

    A minimal config that adds a CNAME record that points to the domain
    being configured:

      [{"type":"CNAME","name":"myalias"}]
END
}

main() {
    local TEMP
    TEMP=$(getopt -n "$0" --options "hvs:j:n:p:z:" --longoptions "help,verbose,set:,json-file:,name:,provider:,zone-id:" -- "$@") || exit $?
    eval set -- "$TEMP"
    unset TEMP

    local actions='' # comma-separated list of actions to perform
    local actionsJsonSrc=''

    while :; do case $1 in
        -h|--help ) usage; return ;;
        -v|--verbose ) VERBOSE=1; shift ;;
        -s|--set ) actions="${actions}${actions:+,}${2}"; shift 2 ;;
        -f|--json-file ) actionsJsonSrc=$2; shift 2 ;;
        -p|--provider ) PROVIDER=$2; shift 2 ;;
        -n|--name ) RECORD_NAME=$2; shift 2 ;;
        -z|--zone-id ) ZONE_ID=$2; shift 2 ;;
        --) shift; break ;;
        * ) echo 'Internal error' >&2; exit 1 ;;
    esac done

    preStartSanityChecks

    read -ra actionsArray <<< "${actions//,/ }"
    if [[ ${#actionsArray[@]} == 0 && -z $actionsJsonSrc ]]; then
        info "No explicit operation(s) given, defaulting to updating the IPV66 peer IP."
        actionsArray=("AAAA")
    fi

    # Sanity-check --set input
    for a in "${actionsArray[@]}"; do
        if ! [[ $a =~ ^(A|AAAA)$ ]]; then
            die "Unrecognized set action: $a"
        fi
    done

    # Read actions from a json file if specified.
    if [[ -f "$actionsJsonSrc" ]]; then
        info "Reading json config from $actionsJsonSrc"
        while read -r _type _name _value _ttl; do
            case $_type in
                A|AAAA )
                    # Direct IP address records get lookup treatment by
                    # default.
                    # shellcheck disable=SC2016
                    if [[ $_value == "?" ]]; then _value='$lookup_ip'; fi
                    ;;
                CNAME )
                    # In case of CNAME, default the value to the the record
                    # name as long at it doesn't conflict with the specified
                    # name.
                    if [[ $_value == "?" && $_name != "$RECORD_NAME" ]]; then _value=$RECORD_NAME; fi
                    ;;
                NS|MX|RP|TXT|SOA|HINFO|SRV|DANE|TLSA|DS|CAA )
                    : # probably shouldn't try to change all of these via the apis though...
                    ;;
                * )
                    die "Unsupported type encountered while parsing the json config: $_type"
                    ;;
            esac
            actionsArray+=("$_type $_name $_value $_ttl")
        done < <(jq -rc '.[]|"\(.type) \(.name//"'"$RECORD_NAME"'") \(.value//"?") \(.ttl//'"$recordTTL"')"' < "$actionsJsonSrc")
    fi

    local failed=''
    info "Will execute these actions (in order): ${actionsArray[*]}"
    for a in "${actionsArray[@]}"; do
        read -ra args <<< "$a"
        if ! setOrUpdateRecord "${args[@]}"; then
            failed+="$a"$'\n'
        fi
    done
    if [[ -n $failed ]]; then
        err "The following DNS updates failed:"$'\n'"$failed"
        return 1
    fi
}

# curlProviderAPI <path> <curl-args> ::: <jq-args>
curlProviderAPI(){
    local -a curlArgs=(
        -sS --fail-early --fail-with-body
        -H "Content-Type: application/json"
        -H "Accept: application/json"
    )
    local -a jqArgs=( -rc )
    local path=$1 apiUrl
    shift

    if [[ -n $VERBOSE ]]; then
        curlArgs+=(-v)
    fi

    local argsFor=curl
    while [[ $# -gt 0 ]]; do
        case $argsFor/${1::3} in
            curl/::: ) argsFor=jq ;;
            curl/*   ) curlArgs+=("$1") ;;
            jq/*     ) jqArgs+=("$1") ;;
            *        ) die "internal error" ;;
        esac
        shift
    done

    case $PROVIDER in
        dnsimple )
            apiUrl="https://api.dnsimple.com/v2/$ACCOUNT_ID"
            curlArgs+=(-H "Authorization: Bearer $TOKEN")
            ;;
        hetzner )
            apiUrl="https://dns.hetzner.com/api/v1"
            curlArgs+=(-H "Auth-API-Token: $TOKEN")
            ;;
        * )
            die "Unknown provider: $PROVIDER"
            ;;
    esac
    curl "${curlArgs[@]}" "$apiUrl/$path" | jq "${jqArgs[@]}"
    #if curl "${curlArgs[@]}" "$apiUrl/$path" | jq "${jqArgs[@]}"; then
    #    dbg "pipestatus: ${PIPESTATUS[@]}"
    #    [[ "${PIPESTATUS[0]}" == 0 ]] || return 1
    #fi
}

# getRecord <type> <name>
# return line: "<record_id> <value> <ttl>"
getRecord(){
    local type=$1 name=$2
    case $PROVIDER in
        dnsimple )
            curlProviderAPI "zones/$ZONE_ID/records?type=$type&name=$name" ::: '.data[]|"\(.id//"") \(.content//"") \(.ttl//"")"'
            ;;
        hetzner )
            # shellcheck disable=SC2016
            curlProviderAPI "records?zone_id=$ZONE_ID" ::: \
                --arg type "$type" --arg name "$name" '.records[]|select(.type==$type and .name==$name)|"\(.id//"") \(.value//"") \(.ttl//"")"'
            ;;
        * )
            die "Unknown provider: $PROVIDER"
            ;;
    esac
}

# setRecord <type> <name> <value> <ttl>
# Returns the created record's ID
setRecord(){
    local type=$1 name=$2 value=$3 ttl=$4
    case $PROVIDER in
        dnsimple )
            body=$(jq -ncM '{$type,$name,$ttl,$value}' --arg type "$type" --arg name "$name" --arg ttl "$ttl" --arg value "$value")
            curlProviderAPI "zones/$ZONE_ID/records" -d "$body" ::: '.data.id'
            ;;
        hetzner )
            body=$(jq -ncM '{$type,$name,$ttl,$value,$zone_id}' --arg type "$type" --arg name "$name" --arg ttl "$ttl" --arg value "$value" --arg zone_id "$ZONE_ID")
            curlProviderAPI "records" -d "$body" ::: '.record.id // .message'
            ;;
        * )
            die "Unknown provider: $PROVIDER"
            ;;
    esac
}

# updateRecord <id> <type> <name> <value> <ttl>
updateRecord(){
    local id=$1 type=$2 name=$3 value=$4 ttl=$5
    case $PROVIDER in
        dnsimple )
            curlProviderAPI "zones/$ZONE_ID/records/$id" -X PATCH -d "{\"content\":\"$value\",\"ttl\":$ttl}"
            ;;
        hetzner )
            curlProviderAPI "records/$id" -X PUT -d "{\"type\":\"$type\",\"name\":\"$name\",\"ttl\":$ttl,\"value\":\"$value\",\"zone_id\":\"$ZONE_ID\"}"
            ;;
        * )
            die "Unknown provider: $PROVIDER"
            ;;
    esac
}

checkRecordID(){
    local id=$1
    case $PROVIDER in
        dnsimple )
            #curlProviderAPI "zones/$ZONE_ID/records/$id" -X PATCH -d "{\"content\":\"$value\",\"ttl\":$ttl}"
            return 0 # TODO
            ;;
        hetzner )
            q_selection=( -j '""' )
            if [[ -n $VERBOSE ]]; then
                q_selection=( 'if has("error") then .error else .record end' )
            fi
            if curlProviderAPI "records/$id" ::: "${q_selection[@]}"; then
                dbg "The record with $id is still valid."
            else
                return 1
            fi
            ;;
        * )
            die "Unknown provider: $PROVIDER"
            ;;
    esac
}

# resolveCurrentIP <4|6>
resolveCurrentIP(){
    local type=$1 ip
    if [[ $type != [46] ]]; then
        die "resolveCurrentIP: type not recognized: $type (must be 4 or 6)"
    fi
    if   ip=$(curl -fsS -"$type" https://one.one.one.one/cdn-cgi/trace | awk -F= '/ip/ {print $2}') && [[ -n $ip ]]; then
        echo "$ip"
    elif ip=$(curl -fsS -"$type" https://icanhazip.com/) && [[ -n $ip ]]; then
        echo "$ip"
    else
        err "couldn't figure out IPv$type address"
        return 1
    fi
}

# getStateFile RECORD_TYPE [EXTRA_ID]
getStateFile(){
    local type=$1 extra=${2-}
    if ! [[ $type =~ ^(A|AAAA|CNAME)$ ]]; then
        die "getStateFile: record type $type is not supported."
    fi
    echo "${stateDirectory}/${PROVIDER}_${ZONE_ID}_${type}${extra:+_${extra}}_v${stateSchemaVersion}.txt"
}

# setOrUpdateRecord <type> [<name> [<value> [<ttl>]]]
setOrUpdateRecord(){
    local recordType=$1 recordName=${2:-$RECORD_NAME} recordValue=${3-} recordTTL=${4:-$recordTTL} recordID='' ipType='' stateFile
    case $recordType in
        A    ) ipType=4 ;;
        AAAA ) ipType=6 ;;
    esac
    info "Updating Dynamic DNS $recordType record for $recordName in zone $ZONE_ID"

    stateFile=$(getStateFile "$recordType")

    # Read state file
    if [[ -e $stateFile ]]; then
        dbg "Reading the record state from $stateFile"
        if [[ $(wc -l "$stateFile") = 1\ * ]]; then
            read -r recordID currentName currentValue currentTTL < "$stateFile"
            dbg "Discovered record values from local state [id=$recordID, value=$currentValue, ttl=$currentTTL]"
        else
            err "The state file $stateFile contains multiple zero or entries, but only one matching name+type pair is supported."
            return 1
        fi
    fi

    # Ensure that the state file is still up-to-date.
    if [[ -z $recordID ]] || ! checkRecordID "$recordID"; then
        info "No record state file available or the record $recordID appears to be no longer valid. Will attempt to generate a new record instead. Querying $PROVIDER API..."
        getRecord "$recordType" "$recordName" >"$stateFile.new"
        read -r recordID currentName currentValue currentTTL < "$stateFile.new"
        if [[ -n $recordID ]]; then
            dbg "Discovered a matching record [id=$recordID, value=$currentValue, ttl=$currentTTL]"
            mv "$stateFile.new" "$stateFile"
        else
            dbg "No existing record could was found for type=$recordType in zone $ZONE_ID."
            rm "$stateFile.new"
        fi
    fi

    # Dynamically look up the value for dynamic IP when no other is specified.
    # shellcheck disable=SC2016
    if [[ -z $recordValue || $recordValue == '$lookup_ip' ]] && [[ -n $ipType ]]; then
        recordValue=$(resolveCurrentIP "$ipType")
        info "Discovered IPv$ipType address: $recordValue"
        if [[ -z $recordValue ]]; then
            err "Discovered IP is empty, cannot proceed with record update!"
            return 1
        fi
    fi

    if [[ $recordName,$recordValue,$recordTTL == "$currentName,$currentValue,$currentTTL" ]]; then
        info "$recordType record for $recordName is already up-to-date ($recordValue)"
        return
    elif [[ -n $recordID ]]; then
        info "Updating the $recordType record $recordName (id=$recordID) with [value=[$currentValue -> $recordValue], ttl=[$currentTTL -> $recordTTL]]"
        updateRecord "$recordID" "$recordType" "$recordName" "$recordValue" "$recordTTL"
    else
        info "Creating a $recordType record $recordName with [value=$recordValue, ttl=$recordTTL]"
        recordID=$(setRecord "$recordType" "$recordName" "$recordValue" "$recordTTL")
        info "New record created successfully (id=$recordID)"
    fi
    info "Writing a new local state file."
    echo "$recordID $recordName $recordValue $recordTTL" > "$stateFile"
}

main "$@"
