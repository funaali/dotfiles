{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.home.file-direct;

  # under $HOME
  stateDirectory = ".home-manager";

  ignorePatterns = toList config.programs.git.ignores;

  checkIgnore = pkgs.writeText "check-ignore" (concatStringsSep "\n" ignorePatterns);

  activationScript = pkgs.writeShellScript "files-direct-activate" ''
    ${config.lib.bash.initHomeManagerLib}

    export PATH=${pkgs.delta}/bin:$PATH
    checkIgnore=${checkIgnore}

    ${readFile ./files-direct-activate.sh}
  '';

  # $1: path, $2: source, $3: enforceSync
  toArg = k: v: escapeShellArgs [
    (if v ? target && v.target != null then v.target else k)
    (toString v.source)
    (if v.enforceSync then "1" else "0")
  ];
in

{
  options = {
    home.file-direct = mkOption {
      description = "Like home.file but managed copy instead of symlink";
      type = with types; attrsOf (submodule {
        options = {
          source = mkOption { type = path; };
          target = mkOption { type = nullOr str; default = null; };
          enforceSync = mkOption { type = bool; default = false; };
        };
      });
    };
  };

  config = {
    home.file = listToAttrs (mapAttrsToList
      (n: v: {
        name = "${stateDirectory}/${n}";
        value = {
          inherit (v) source;
          target = "${stateDirectory}/${if v ? target && v.target != null then v.target else n}";
        };
      })
      cfg);

    home.activation.checkDirectFiles = hm.dag.entryBefore [ "writeBoundary" ] ''
      bash ${activationScript} check ${escapeShellArg stateDirectory} "$newGenPath/home-files" "''${oldGenPath+"$oldGenPath/home-files"}" ${concatStringsSep " " (mapAttrsToList toArg cfg)}
    '';

    home.activation.syncDirectFiles = hm.dag.entryAfter [ "linkGeneration" ] ''
      bash ${activationScript} sync ${escapeShellArg stateDirectory} "$newGenPath/home-files" "''${oldGenPath+"$oldGenPath/home-files"}" ${concatStringsSep " " (mapAttrsToList toArg cfg)}
    '';
  };
}
