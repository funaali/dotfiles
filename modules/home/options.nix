{ lib, config, pkgs, ... }:

with lib;

let
  # allows custom options
  patchedPackage = pkgs.git-sync.overrideAttrs (old: old // {
    src = pkgs.fetchFromGitHub {
      owner = "SimSaladin";
      repo = "git-sync";
      rev = "371c0330734355d932bb1b3b24b836ae1070fa7d";
      hash = "sha256-DWmA/huEIRmCLNaROW9q1wNR9ash/iUVCwGubHr9bf4=";
    };
  });

  cfg = config;

  repoConfigModule = types.submodule ({ name, config, ... }: {
    options = {
      path = mkOption {
        type = types.path;
        default = "${cfg.home.homeDirectory}/${name}";
        description = "Path to the repo (local)";
      };
      uri = mkOption {
        type = types.str;
        default = "";
        description = "Origin (upstream) URI";
      };
      sync = mkEnableOption "${name}";
      interval = mkOption { type = types.int; default = 24 * 3600; };
      noCommit = mkOption { type = types.bool; default = false; };
    };

    config = {
      sync = mkDefault (config.uri != "");
    };
  });

  syncedRepos = filterAttrs (_: r: r.sync) cfg.sim.repos;

  serviceExtras = mapAttrs' (name: repo: {
    name = "git-sync-${name}";
    value = {
      Service.Environment = [
        "GIT_SYNC_DONT_AUTOCOMMIT=${toString repo.noCommit}"
      ];
    };
  }) syncedRepos; # cfg.services.git-sync.repositories;
in

{
  options.sim = {
    name = mkOption {
      type = types.nullOr types.str;
      default = "Samuli Thomasson";
      description = "Real name";
    };

    email = mkOption {
      type = types.nullOr types.str;
      default = "samuli.thomasson@pm.me";
      description = "Primary email address";
    };

    gpgKeyId = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "Primary GPG key-id";
    };

    repos = mkOption {
      type = types.attrsOf repoConfigModule;
      default = { };
      description = "Git repositories used by configurations";
    };

    hostName = mkOption {
      type = types.nullOr types.str;
      default = null;
    };
  };

  config = mkMerge [{
    services.git-sync = {
      enable = lib.mkDefault true;
      package = patchedPackage;

      repositories =
        let
          repoSync = _: x: { inherit (x) path uri interval; };
        in
        mapAttrs repoSync syncedRepos;
    };
  }
  (mkIf cfg.services.git-sync.enable { systemd.user.services = serviceExtras; })
  ];
}
