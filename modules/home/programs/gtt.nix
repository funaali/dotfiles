# Google Translator TUI
# Usage: gtt ‐src "English" ‐dst "Chinese (Traditional)"

{ lib, pkgs, config, ... }:
let
  inherit (lib) types mkIf mkOption mkEnableOption mkPackageOption;

  cfg = config.programs.gtt;

  yamlFormat = pkgs.formats.yaml { };

  translators = {
    Argos   = { }; # free
    Bing    = { }; # free
    DeepL   = { }; # free
    DeepLX  = { }; # free
    Google  = { }; # free
    Reverso = { }; # free
    DeepL   = { "api_key" = "required"; };
    DeepLX  = { "host" = "required"; "api_key" = "optional"; };
    ChatGPT = { "api_key" = "required"; };
    Libre   = { "api_key" = "optional"; "host" = "optional"; };
  };

  serversPretty = {
    api_key = lib.pipe cfg.servers [
      (lib.filterAttrs (_: x: x ? api_key))
      (lib.mapAttrs (_: { api_key, ... }: api_key))
      (lib.filterAttrsRecursive (_: v: (v != null) && (v != {})))
      (lib.filterAttrsRecursive (_: v: (v != null) && (v != {})))
    ];
    host = lib.pipe cfg.servers [
      (lib.filterAttrs (_: x: x ? host))
      (lib.mapAttrs (_: { host, ... }: host))
      (lib.filterAttrsRecursive (_: v: (v != null) && (v != {})))
    ];
  };

  __key = lib.strings.toLower;

  __translator = { api_key ? false, host ? false }:
    (lib.optionalAttrs (api_key != false) { api_key = mkApiKeyOption api_key; }) //
    (lib.optionalAttrs (host    != false) { host = mkHostOption host; });

  mkApiKeyOption = desc: mkOption {
    type = with types; submodule {
      options = {
        value = mkOption { type = nullOr str; default = null; description = "Key as raw string"; };
        file = mkOption { type = nullOr str; default = null; description = "Path to a file containing the key"; };
      };
    };
    default = { };
    description = "Provide API key for the service (${desc}).";
  };

  mkHostOption = desc: mkOption {
    type = with types; nullOr str;
    default = null;
    description = "Provide the ip:port for the  service (${desc})";
  };

  colors = [ "bg" "fg" "gray" "red"  "green"  "yellow"  "blue" "purple" "cyan" "orange" ];
in
{
  options.programs.gtt = {
    enable = mkEnableOption "programs.gtt";

    package = mkPackageOption pkgs "gtt" { };

    settings = mkOption {
      type = types.submodule {
        options = {
          translator = mkOption {
            type = with types; enum (builtins.attrNames translators);
            default = "Google";
            description = "Selected translator service";
          };

          theme = mkOption {
            type = with types; nullOr (enum [ "gruvbox" "nord" ]);
            default = "nord";
            description = "Theme";
          };

          source.border_color = mkOption {
            type = types.enum colors;
            default = "orange";
            description = "Source language box outline color.";
          };

          destination.border_color = mkOption {
            type = types.enum colors;
            default = "blue";
            description = "Destination language box outline color.";
          };

          transparent = mkEnableOption "Use transparent background";

          hide_below = mkEnableOption ''Hide the "Definition/Example" and "Part of speech" sections?'';
        };
      };

      default = { };

      description = ''
        Settings for GTT. Written in `~/.config/gtt/gtt.yaml`.
      '';
    };

    servers = lib.pipe translators [
      (lib.filterAttrs (_: x: x ? api_key || x ? host))
      (lib.mapAttrs' (name: value: lib.nameValuePair (__key name) (__translator value)))
    ];
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];

    xdg.configFile."gtt/gtt.yaml".text = ''
      # Generated by home-manager
      ${yamlFormat.generate "gtt" cfg.settings}
    '';

    xdg.configFile."gtt/server.yaml".text = ''
      # Generated by home-manager
      ${yamlFormat.generate "servers" serversPretty}
    '';
    #xdg.configFile."gtt/keymap.yaml" = {};
    #xdg.configFile."gtt/theme.yaml" = {};
  };
}
