{ config, lib, pkgs, ... }:

with builtins;
with lib;

let
  cfg = config.programs.libreoffice;

  getDictsFor = langs: dicts:
    let
      f = lang: getAttr lang dicts;
    in
    map f langs;
in
{
  options.programs.libreoffice = {
    enable = mkEnableOption "programs.libreoffice";

    variant = mkOption {
      type = types.enum [ "still" "fresh" "collabora" "bin" ];
      default = "collabora";
      description = "The bin variant is for Darwin.";
    };

    kdeIntegration = mkOption {
      type = types.bool;
      default = false;
      description = "Build with KDE integration.";
    };

    useQt6 = mkOption {
      type = types.bool;
      default = false;
    };

    #package = mkOption {
    #  type = types.package;
    #  default = pkgs.__cacheMax.libreoffice-qt6;
    #  #default = pkgs.libreoffice-fresh-nologo;
    #};

    finalPackage = mkOption {
      type = types.package;
      readOnly = true;
    };

    spellcheck.enable = mkEnableOption "programs.libreoffice.spellcheck";

    spellcheck.languages = mkOption {
      type = with types; listOf str;
      default = [ "en_US" "en_CA" "en_AU" "de_DE" "de_AT" ];
    };

    disableLogo = mkOption {
      type = types.bool;
      default = true;
      description = "Disable showing the logo on startup.";
    };
  };

  config = mkIf cfg.enable {
    programs.libreoffice.finalPackage =
      let
        package =
          if cfg.useQt6 then
            pkgs."libreoffice-qt6-${cfg.variant}"
          else
            pkgs."libreoffice-${cfg.variant}";
      in
      package.override (old:
      lib.optionalAttrs (cfg.variant != "collabora" && cfg.disableLogo) {
        extraMakeWrapperArgs = old.extraMakeWrapperArgs ++ [ "--set" "Logo" "0" ];
      }
      //
      lib.optionalAttrs cfg.kdeIntegration {
        unwrapped = old.unwrapped.override (_: { kdeIntegration = cfg.kdeIntegration; });
      });

    home.packages =
      [ cfg.finalPackage ]
      ++ optionals cfg.spellcheck.enable ([ pkgs.hunspell ] ++ getDictsFor cfg.spellcheck.languages pkgs.hunspellDicts);

    # TODO support modifying
    # ~/.config/libreoffice/*/user/registrymodifications.xcu
    # See: https://askubuntu.com/questions/83605/how-do-i-export-customized-libreoffice-config-files
    #
    #
    # <item oor:path="/org.openoffice.Office.Calc/Layout/Other/MeasureUnit">
    #   <prop oor:name="Metric" oor:op="fuse"><value>8</value></prop>
    # </item>
    # <item oor:path="/org.openoffice.Office.Writer/Layout/Other">
    #   <prop oor:name="MeasureUnit" oor:op="fuse"><value>8</value></prop>
    # </item>
    # millimeters: 1
    # centimeters: 2
    # inches: 8
  };
}
