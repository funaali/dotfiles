{ options, config, lib, pkgs, ... }:

with lib;

# Documentation: <lnk:man:lbdbq.1> <lnk:/etc/lbdb.rc>

# With (neo)mutt use display_filter =
# ''"sh -c '{ tee /dev/stderr | { lbdb-fetchaddr -a 2>/dev/null || cat >/dev/null; }; } 2>&1'"''


let
  cfg = config.programs.lbdb;

  renderSettings = attrs:
    let
      quote = s: if hasInfix " " s then ''"${s}"'' else s;
      rhsString = v: if false == v then "no" else if true == v then "yes" else toString v;
      rhs = v: quote (concatStringsSep " " (map rhsString (toList v)));
    in
    concatStringsSep "\n" (mapAttrsToList (n: v: "${toUpper n}=${rhs v}") (filterAttrs (_: v: null != v && [ ] != v) attrs));

  settings = {
    methods = map (mn: "m_" + mn) (attrNames cfg.methods);
    modules_path = optional (cfg.modules_path != [ ]) "$MODULES_PATH" ++ cfg.modules_path;
    inherit (cfg) keep_dupes sort_output;
  } //
  fold recursiveUpdate { } (mapAttrsToList
    (
      method: mapAttrs' (option: nameValuePair "${method}_${option}")
    )
    cfg.methods);

in
{
  options.programs.lbdb = {
    enable = mkEnableOption "lbdb integrations";

    package = mkOption {
      type = types.package;
      default = pkgs.lbdb.override {
        abook = pkgs.abook;
        gnupg = pkgs.gnupg;
        khard = pkgs.khard;
      };
      description = "The lbdb derivation to use.";
    };

    modules_path = mkOption {
      type = types.listOf types.str;
      default = [ ];
      example = [ "$HOME/.lbdb_modules" ];
    };

    sort_output = mkOption {
      type = types.enum [ "name" "comment" "reverse_comment" "address" ];
      default = "address";
    };

    keep_dupes = mkOption {
      type = types.bool;
      default = false;
    };

    methods = mkOption {
      type =
        with types; let
          methodCfg = attrsOf (oneOf [ str bool (listOf str) ]);
        in
        attrsOf (either bool methodCfg);
      default = { };
      example = { inmail.db = "$HOME/.lbdb/m_inmail_utf-8"; };
      description = ''
        Methods to enable. Optionally configuration can be specified.
      '';
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];

    home.file.".lbdb/rc".text = ''
      # lbdb configuration file
      ${renderSettings settings}
    '';
  };
}
