{ config, lib, pkgs, ... }:

# Modified since khard was added to home-manager upstream. Can remove?

with lib;

let
  cfg = config.programs.khard;

  addressbookType = types.submodule {
    options = {
      path = mkOption {
        type = types.str;
        example = "~/.local/share/dav/contacts/addressbook/";
      };
    };
  };

  toINI = settings: addressbooks: concatStringsSep "\n" [
    "[addressbooks]"
    (generators.toINI { mkKeyValue = generators.mkKeyValueDefault { } " = "; mkSectionName = name: "[${name}]"; } addressbooks)
    (generators.toINI { mkKeyValue = generators.mkKeyValueDefault { } " = "; } settings)
  ];

in
{
  #options.programs.khard = {
  #  addressbooks = mkOption {
  #    type = types.attrsOf addressbookType;
  #    default = { };
  #    description = ''
  #      The addressbooks.
  #    '';
  #  };

  #  settings = mkOption {
  #    type = with types; attrsOf (attrsOf (either str (either bool (either int float))));
  #    default = { };
  #    description = ''
  #      Settings written to <filename>khard.conf</filename>.
  #    '';
  #  };
  #};

  #config = mkIf cfg.enable {
  #  xdg.configFile."khard/khard.conf".text = toINI cfg.settings cfg.addressbooks;
  #};
}

