{ config, lib, pkgs, ... }:

# https://github.com/wfxr/forgit
#
# FORGIT_NO_ALIASES
# FORGIT_COPY_CMD='xclip -selection clipboard'
# FORGIT_PAGER
# FORGIT_{SHOW,DIFF,IGNORE}_PAGER
# FORGIT_GLO_FORMAT
# FORGIT_LOG_FORMAT
# FORGIT_FZF_DEFAULT_OPTS
# FORGIT_{{CMD}}_FZF_OPTS

with lib;

let
  cfg = config.programs.forgit;
in
{
  options.programs.forgit = {
    enable = mkEnableOption "programs.forgit";

    options = mkOption {
      type = types.attrs;
      default = { };
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ pkgs.forgit ];
    home.sessionVariables = mapAttrs' (k: v: nameValuePair "FORGIT_${toUpper k}" v) cfg.options;
  };
}
