# TODO remove obsolete, vdirsyncer modules are in upstream
{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.programs.vdirsyncer;

  pairType = types.submodule {
    options = {
      a = mkOption { type = types.str; };
      b = mkOption { type = types.str; };

      collections = mkOption {
        type = with types; listOf (either str (listOf str));
        default = [ "from a" "from b" ];
      };
    };
  };

  storageParamType = with types; nullOr (either str (submodule {
    options = {
      fetch = mkOption { type = types.listOf str; };
    };
  }));

  storageType = with types; submodule {
    options = {
      type = mkOption {
        type = str;
        description = "See https://vdirsyncer.pimutils.org/en/stable/config.html#storages";
      };
      path = mkOption { type = storageParamType; default = null; };
      fileext = mkOption { type = storageParamType; default = null; };
      url = mkOption { type = storageParamType; default = null; };
      username = mkOption { type = storageParamType; default = null; };
      password = mkOption { type = storageParamType; default = null; };
      client_id = mkOption { type = storageParamType; default = null; };
      client_secret = mkOption { type = storageParamType; default = null; };
      token_file = mkOption {
        type = storageParamType;
        default = "~/.local/share/vdirsyncer/google-oauth-token_%s";
        description = ''
          Only if type = google_calendar or google_contacts.
        '';
      };
    };
  };

  renderValue = v:
    if isString v then ''"${v}"''
    else if isList v then "[" + concatStringsSep ", " (map renderValue v) + "]"
    else if isNull v then "null"
    else if true == v then "true"
    else if false == v then "false"
    else if isInt v then toString v
    else abort "renderValue: don't know what to do with ${v}";

  toConfigFile = cfg:
    with cfg;
    let
      toINI = generators.toINI {
        mkKeyValue = generators.mkKeyValueDefault
          {
            mkValueString = renderValue;
          } " = ";
      };

      toAttrs = name: mod: attrs:
        foldl recursiveUpdate { } (mapAttrsToList (k: v: { "${name} ${k}" = mod k (filterAttrs (_: x: !isNull x && x != [ ]) v); }) attrs);

      toStorageParams = toAttrs "storage" (name: attrs:
        foldl recursiveUpdate { } (mapAttrsToList (k: v: if isAttrs v then { "${k}.fetch" = v.fetch; } else { "${k}" = v; })
          (if elem attrs.type [ "google_calendar" "google_contacts" ] then attrs // { token_file = replaceStrings [ "%s" ] [ name ] attrs.token_file; } else removeAttrs attrs [ "token_file" ])));

    in
    toINI ({ general.status_path = statusPath; } // toAttrs "pair" (name: x: x) pair // toStorageParams storage);

  timerUnit = {
    Unit.Description = "Synchronize vdirs";
    Timer = {
      OnBootSec = "5m";
      OnUnitActiveSec = "15m";
      AccuracySec = "5m";
    };
    Install.WantedBy = [ "timers.target" ];
  };

  serviceUnit = {
    Unit.Description = "Synchronize vdirs";
    Service = {
      ExecStart = "${cfg.package}/bin/vdirsyncer sync";
      Type = "oneshot";
    };
  };

in
{
  options.programs.vdirsyncer = {
    enable = mkEnableOption "vdirsyncer";

    package = mkOption {
      type = types.package;
      default = pkgs.vdirsyncer;
    };

    statusPath = mkOption {
      type = types.str;
      default = "~/.local/share/dav/vdirsyncer/";
    };

    pair = mkOption {
      type = types.attrsOf pairType;
      default = { };
    };

    storage = mkOption {
      type = types.attrsOf storageType;
      default = { };
    };
  };

  config = mkIf cfg.enable {
    assertions =
      let
        pairValues = attrValues cfg.pair;
        storageNames = attrNames cfg.storage;
        pairStorageNames = catAttrs "a" pairValues ++ catAttrs "b" pairValues;
        missing = filter (n: ! elem n storageNames) pairStorageNames;
      in
      [{
        message = "Missing storage sections: ${toString missing}";
        assertion = missing == [ ];
      }];

    home.packages = [ cfg.package ];

    xdg.configFile."vdirsyncer/config".text = toConfigFile cfg;

    systemd.user.services.vdirsyncer = serviceUnit;
    systemd.user.timers.vdirsyncer = timerUnit;
  };
}
