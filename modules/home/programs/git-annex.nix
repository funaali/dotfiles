{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.programs.git.annex;

in
{
  options.programs.git.annex = {
    enable = mkEnableOption "config.programs.git.annex";
  };

  config = mkIf cfg.enable {
    home.packages = [ pkgs.git-annex ];

    programs.git.extraConfig.annex = {
      jobs = mkDefault 3;
      cachecreds = mkDefault false; # Default: true. Caches credentials on disk unencrypted (.git/annex/creds).
      genmetadata = mkDefault true;
      synccontent = mkDefault true; # enable --content for git annex sync by default.
      queuesize = mkDefault 120250; # Default: 1024. Larger values require more memory.
    };
  };
}
