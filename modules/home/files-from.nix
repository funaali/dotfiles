{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.home.files-from;

  traverseDir =
    root:
    let

      toFile = path: name: {
        target = cfg.prefix + removePrefix "/" "${path}/${name}";
        source = root + "/${path}/${name}";
      };

      toText = path: name: {
        target = cfg.prefix + removePrefix "/" "${path}/${name}";
        text = readFile (root + "/${path}/${name}");
      };

      fn = path: mapAttrs
        (
          name: value:
            if value == "regular" then toFile path name
            else if value == "symlink" then toText path name
            else if value == "directory" then fn (removePrefix "/" (path + "/${name}"))
            else "???"
        )
        (builtins.readDir (root + "/${path}"));
    in
    fn "";

  fileList = collect (x: x ? target);

  files = fileList (traverseDir cfg.source);
in

{
  options = {
    home.files-from = mkOption {
      type = with types; nullOr (submodule {
        options = {
          source = mkOption { type = path; };
          prefix = mkOption { type = str; default = ""; };
        };
      });
      default = null;
    };
  };

  config = {
    home.file-direct = lib.optionalAttrs (cfg != null) (listToAttrs (map
      (v: {
        name = v.target;
        value = v;
      })
      files));
  };
}
