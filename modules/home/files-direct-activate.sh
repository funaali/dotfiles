# shellcheck shell=bash
# files-direct-activate.sh

# usage: $0 <check|sync> pathPrefix newGenFilesPath [oldGenFilesPath|""] checkIgnore [path source enforceSync[=0|1] ...]

set -euo pipefail

pp-path() {
	echo "$(cd "$(dirname "$1")" && dirs)/$(basename "$1")"
}

list-files() {
	local path
	for path in "$@"; do
		[ -e "$path" ] || continue
		find -L "$path" \( -type f -o -type l \) -exec sh -c "echo \${1#*/$rootDir/}" - {} \;
	done | sort | uniq
}

remove-file() {
	local path=$1
	local root=$2
	local target=$root/$path
	if [ -e "$target" ]; then
		$DRY_RUN_CMD rm $VERBOSE_ARG "$target"
	fi
	target=$(dirname "$target")
	while [[ $target == "$root"/* ]] && [ -d "$target" ]; do
		if [ -z "$(ls -a "$target")" ]; then
			$DRY_RUN_CMD rmdir $VERBOSE_ARG "$target"
		fi
		target=$(dirname "$target")
	done
}

sync-file() {
	local path=$1 # .Xresources
	local src=$2  # /path/to/source/.Xresources
	local dst=$HOME/$path
	local newSrc=$newFiles/$rootDir/$path

	if [ -e "$newSrc" ]; then
		if [ ! -e "$dst" ] || [ -n "$(diff "$newSrc" "$dst")" ]; then
			$DRY_RUN_CMD install $VERBOSE_ARG -D --mode="$(stat -c %a "$src")" "$newSrc" "$dst"
		elif [ "$(stat -c %a "$src")" != "$(stat -c %a "$dst")" ]; then
			$DRY_RUN_CMD chmod $VERBOSE_ARG "$(stat -c %a "$src")" "$dst"
		fi
	elif [ -e "$dst" ] && [ ! -L "$dst" ]; then
		remove-file "$path" "$HOME"
	fi
}

check-file() {
	local path=$1
	local dst=$HOME/$path
	local new=$newFiles/$rootDir/$path
	local old=$oldFiles/$rootDir/$path

	if [ ! -e "$dst" ] || cmp -s "$new" "$dst"; then
		return
	elif [ -e "$oldFiles" ]; then
		if cmp -s "$old" "$dst"; then
			$VERBOSE_ECHO "File $(pp-path "$dst") contents will be changed."
		else
			errorEcho "File $(pp-path "$dst") has local changes!"
			$VERBOSE_ECHO "$(diff -u "$old" "$dst" | delta)"
			return 1
		fi
	elif [ -e "$new" ]; then
		errorEcho "File $(pp-path "$dst") exists but is not tracked!"
		return 1
	fi
}

diff-directory() {
	local one two
	one=$(readlink -e -- "$1") || return
	two=$(readlink -e -- "$2") || return
	! comm -3 \
		<(find -L "$one" \( -type f -o -type l \) -printf %P\\n | sort) \
		<(find -L "$two" \( -type f -o -type l \) -printf %P\\n | sort) \
		| grep -v -f "$checkIgnore"
}

check-directory() {
	local path=$1
	local src=$2
	local dst=$HOME/$path
	local new=$newFiles/$rootDir/$path
	local old=$oldFiles/$rootDir/$path
	local diff
	if diff=$(diff-directory "$new" "$dst"); then
		return
	else
		$VERBOSE_ECHO "Directory $(pp-path "$dst") and $(pp-path "$new") (new gen) differ:"
		$VERBOSE_ECHO "$diff"
	fi
	if diff=$(diff-directory "$old" "$dst"); then
		return
	else
		$VERBOSE_ECHO "Directory $(pp-path "$dst") and $(pp-path "$old") (old gen) differ:"
		$VERBOSE_ECHO "$diff"
	fi
	errorEcho "Directory $(pp-path "$dst") has different content than $(pp-path "$src")"
	return 1
}

MODE=$1
rootDir=$2
newFiles=$3
oldFiles=$4
shift 4

retCode=0

while [ $# -ge 3 ]; do
	path=$1
	from=$2
	enforceSync=$3
	shift 3

	if [ "$MODE,$enforceSync" == "check,1" ] && [ -d "$from" ]; then
		if ! check-directory "$path" "$from"; then
			retCode=1
		fi
	fi

	for file in $(list-files "$newFiles/$rootDir/$path" "$oldFiles/$rootDir/$path"); do
		case $MODE in
			sync) sync-file "$file" "$from${file#"$path"}" ;;
			check) if ! check-file "$file"; then retCode=1; fi ;;
		esac
	done
done

exit "$retCode"
