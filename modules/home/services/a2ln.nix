{ lib, config, pkgs, ... }:

# Only useful together with the associated Android application.
# Linking the app is currently not handled very well by this module, you may
# want to do that manually.

with lib;

let
  cfg = config.services.a2ln;

in {
  options.services.a2ln = {
    enable = lib.mkEnableOption "Enable Android 2 Linux Server";

    ip = mkOption {
      type = types.str;
      default = "*";
      description = "The interface(s) to bind the service to.";
    };

    port = mkOption {
      type = types.port;
      default = 23045;
      description = "The port on which the service will run.";
    };

    openFirewall = mkOption {
      type = types.bool;
      default = false;
      description = "Whether to open the port in firewall.";
    };
  };

  config = mkIf cfg.enable {
    ## Can only do this as a system service...
    #network.firewall.allowedTCPPorts = mkIf cfg.openFirewall [
    #  cfg.port
    #];

    systemd.user.services.a2ln = {
      Unit.PartOf = [ "graphical-session.target" ];
      Unit.After = [ "graphical-session-pre.target" ];
      Service = {
        Slice = "graphical.slice";
        ProtectSystem = "strict";
        ExecStart = lib.concatStringsSep " " [
          "${getExe pkgs.a2ln}"
          "--ip ${cfg.ip}"
          "--port ${toString cfg.port}"
        ];
      };
      Install.WantedBy = [ "graphical-session.target" ];
    };
  };
}
