{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.autocutsel;

  mkUnit = selection: {
    Unit = {
      Description = "Synchronize cutbuffer with ${selection}";
      Documentation = "http://www.nongnu.org/autocutsel/";
      PartOf = [ "graphical-session.target" ];
      After = [ "graphical-session-pre.target" ];

    };
    Service.ExecStart = "${pkgs.autocutsel}/bin/autocutsel -selection ${toUpper selection}";
    Service.Restart = "always";
    Service.Slice = "graphical.slice";
    Install.WantedBy = [ "graphical-session.target" ];
  };

  genUnit = s: {
    name = "autocutsel-${s}";
    value = mkUnit s;
  };

in
{
  #meta.maintainers = [ ];

  options.services.autocutsel = {
    enable = mkEnableOption "autocutsel";

    selections = mkOption {
      type = with types; listOf (enum [ "clipboard" "primary" ]);
      default = [ "clipboard" ];
      description = "Selections to synchronize.";
    };
  };

  config = mkIf cfg.enable {
    systemd.user.services = listToAttrs (map genUnit cfg.selections);
  };
}
