{ lib, options, config, pkgs, ... }:

with lib;

let
  inherit (builtins) filter typeOf;
  opts = options.services.barrier;
  cfg = config.services.barrier;

  renderServerConfig = cfg:
    let
      render1 = v: if v == false then "false" else if v == true then "true"
      else if typeOf v == "string" then v
      else if typeOf v == "int" || typeOf v == "float" then toString v
      else if typeOf v == "list" then concatStringsSep " " (map render1 v)
      else throw "Can't render a value of type ${typeOf v}";

      renderVals = indent: xs: pval: concatStringsSep "" (map (v: optionalString (v!=null&&pval v!="") (indent + pval v + "\n")) xs);

      renderKeyEqVals = indent: xs: pkey: pval: concatStringsSep "" (filter (x: x != "") (
        if typeOf xs == "set"
        then mapAttrsToList (k: v: optionalString (v!=null&&pval     v!="") (indent + pkey k +     " = " + pval v        + "\n")) xs
        else map            (x:    optionalString (x!=null&&pval x.key!="") (indent + pkey x.key + " = " + pval x.action + "\n")) xs
      ));

      renderSectionsIndented = indent: xs: pval: concatStringsSep "" ((
        mapAttrsToList (k: v: indent + k + ":\n" + optionalString (v!= null&&(pval (indent+indent) v)!="") (pval (indent+indent) v)) xs
      ));

      renderSection = name: content: concatStringsSep "" ([ "section: ${name}\n" ] ++ content ++ [ "end" ]);
    in

    ''
      # barriers.conf - barrier server configuration

      ${renderSection "screens" [(renderSectionsIndented "  " cfg.screens (ind: s: renderKeyEqVals ind s.settings toString render1))]}
      ${renderSection "links"   [(renderSectionsIndented "  " cfg.screens (ind: s: renderKeyEqVals ind s.links toString render1))]}
      ${renderSection "aliases" [(renderSectionsIndented "  " cfg.screens (ind: s: renderVals ind s.aliases render1))]}
      ${renderSection "options" [
        (renderKeyEqVals "  " cfg.options id render1)
        (renderKeyEqVals "  " cfg.keybindings (x: "keystroke("+x+")") toString)
      ]}
    '';

  screenOpts = { ... }: {
    options = {
      name = mkOption {
        type = types.str;
        readOnly = true;
        description = "The screen name/identifier (hostname)";
      };

      settings = mkOption {
        type = with types; attrsOf (oneOf [ bool int str ]);
        default = { };
        description = "Screen config settings, eg. { preserveFocus = false; }";
      };

      links = genAttrs [ "left" "right" "up" "down" ] (dir: mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "Link other screen ${dir}";
      });

      aliases = mkOption {
        type = types.listOf types.nonEmptyStr;
        default = [ ];
        description = "Aliases for this screen.";
      };
    };
  };

  keybindingOpts = { ... }: {
    options = {
      key = mkOption {
        type = types.str;
        description = "The key (combination) to bind.";
      };
      action = mkOption {
        type = types.str;
        description = "The action to perform.";
      };
    };
  };
in
{
  options = {
    services.barrier.server = {
      enable = lib.mkEnableOption "Start barrier server";

      package = mkOption {
        type = types.package;
        default = pkgs.barrier;
        description = "Package providing barrier server.";
      };

      config.screens = mkOption {
        type = types.attrsOf (types.submodule screenOpts);
        default = { };
        description = "Screens configuration";
      };

      config.options = mkOption {
        type = with types; attrsOf (nullOr (oneOf [ bool int str ]));
        default = { };
        description = "Options to set in the options-section (note: not keystrokes)!";
      };

      config.keybindings = mkOption {
        type = with types; listOf (submodule keybindingOpts);
        default = [ ];
        description = ''
          Key strokes that are bound to actions. For example
          [
            { key = "Control+Alt+Shift+F4"; action = "toggleScreen"; }
            [ ... ]
          ]
        '';
      };

    };
  };

  config = lib.mkMerge [

    { # barrier client options
      services.barrier.client = {
        name = lib.mkDefault config.sim.hostName;
        enable = lib.mkDefault (
          opts.client.server.isDefined &&
          cfg.client.server != null &&
          cfg.client.server != cfg.client.name
        );
      };

      # XXX: not sure why this is necessary
      systemd.user.services.barrierc.Service.ExecStartPre = mkIf (cfg.client.enable)
        "/bin/sh -c '[ -n \"$DISPLAY\" ] || { sleep 1; exit 1; }'";
    }

    # barrier server config
    (lib.mkIf cfg.server.enable {

      xdg.configFile."barrier/barriers.conf".text = renderServerConfig cfg.server.config;

      systemd.user.services.barriers = {
        Service.ExecStart = "${getExe' cfg.server.package "barriers"}";
        Service.Environment = "BARRIER_CONFIG=${config.xdg.configHome}/barrier/barriers.conf";
      };
    })
  ];
}
