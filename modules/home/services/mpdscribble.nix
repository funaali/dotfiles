{ options, config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.mpdscribble;

in
{
  options.services.mpdscribble = {
    enable = mkEnableOption "services.mpdscribble";
    package = mkOption {
      type = types.package;
      default = pkgs.mpdscribble;
      description = "mpdscribble package";
    };

    options = mkOption {
      type = types.anything;
      default = {
        log = "syslog";
        host = "localhost";
        port = 6600;
        #proxy = "";
        #verbose = 1;
      };
    };

    scrobblers = mkOption {
      type = types.anything;
      default = {
        "file".file = "mpdscribble.cache";
      };
      #default = {
      #  "last.fm" = {
      #    url = "http://post.audioscrobbler.com/";
      #    postCommand = "pass show web/last.fm | sed -ne '1s/^/password = /p; /^username:/s/:/ =/p'";
      #  };
      #};
      description = "description";
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];

    systemd.user.services.mpdscribble = {
      Unit = {
        Description = "mpdscribble - Last.fm updater for MPD";
        Documentation = "man:mpdscribble(1) https://mpd.wikia.com/wiki/Client:mpdscribble";
        BindsTo = "mpd.service";
        After = [ "mpd.service" ];
      };

      Service = {
        Slice = "background.slice";
        CacheDirectory = "%p";
        RuntimeDirectory = "%p";
        Restart = "always";
        RestartSec = 30;
        NoNewPrivileges = true;
        Nice = 15;
        ProtectSystem = "strict";
        MemoryDenyWriteExecute = true;
        RestrictNamespaces = true;
        RestrictRealtime = true;
        Environment = [
          "MPDSCRIBBLE_CONF=%t/%p/mpdscribble.conf"
          "MPDSCRIBBLE_ROOT=%C/%p"
        ];
        ExecStartPre =
          let

            showOption = k: v: "echo ${k} = ${toString v}";
            showScrobbler = name: opts: ''
              echo "[${name}]"
              ${if opts ? file then showOption "file" opts.file else showOption "url" opts.url}
              echo "journal = $MPDSCRIBBLE_ROOT/mpdscribble.${name}.cache"
              ${opts.postCommand or ""}
            '';
            generateConf = pkgs.writeShellScript "generate-mpdscribble-conf" ''
              set -euo pipefail
              exec > "$MPDSCRIBBLE_CONF"
              ${concatStringsSep "\n" (mapAttrsToList showOption cfg.options)}
              ${concatStringsSep "\n\n" (mapAttrsToList showScrobbler cfg.scrobblers)}
            '';
          in
          "${generateConf}";
        ExecStart = "${cfg.package}/bin/mpdscribble --no-daemon --conf '$MPDSCRIBBLE_CONF'";
      };

      Install = {
        Also = "mpd.service";
        WantedBy = [ "default.target" ];
      };
    };
  };
}
