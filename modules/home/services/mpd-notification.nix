{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.mpd-notification;

in
{
  options.services.mpd-notification = {
    enable = mkEnableOption "MPD Notification daemon";
    package = mkOption {
      type = types.package;
      default = pkgs.mpd-notification;
      description = "mpd-notification package";
    };
    musicDir = mkOption {
      type = types.str;
      default = "%h/Music";
      description = "Music directory (for artwork lookup)";
    };
    host = mkOption {
      type = types.str;
      default = "localhost";
      description = "MPD hostname";
    };
    port = mkOption {
      type = types.int;
      default = 6600;
      description = "MPD host port";
    };
    timeout = mkOption {
      type = types.int;
      default = 15;
      description = "Notification timeout (seconds)";
    };
  };

  config = mkIf cfg.enable {
    systemd.user.services = {
      mpd-notification = {
        Unit = {
          Description = "MPD Notification daemon";
          Requisite = [ "graphical-session.target" ];
          Requires = [ "dbus.socket" ];
          PartOf = [ "graphical-session.target" ];
          After = [
            "graphical-session-pre.target"
            "dbus.socket"
            "mpd.socket"
            "dunst.service"
          ];
        };
        Service = {
          Type = "notify";
          Restart = "always";
          RestartSec = "2s";

          # mpd-notification often doesn't exit cleanly when mpd is stopped or restarted.
          RestartForceExitStatus = "SIGABRT";

          ExecStart =
            let
              args = [
                "-H ${cfg.host}"
                "-p ${toString cfg.port}"
                "-m ${cfg.musicDir}"
                "-t ${toString cfg.timeout}"
              ];
            in
            "${cfg.package}/bin/mpd-notification ${concatStringsSep " " args}";

          NoNewPrivileges = "yes";
          ProtectSystem = "strict";
          MemoryDenyWriteExecute = "yes";
        };
        Install = {
          WantedBy = [ "graphical-session.target" ];
          UpheldBy = [ "mpd.service" ];
        };
      };
    };

  };
}
