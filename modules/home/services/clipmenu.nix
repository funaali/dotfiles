{ config, lib, pkgs, ... }:

with lib;

# NOTE: this extends the module in home-manager a bit.

let
  cfg = config.services.clipmenu;

  # Renders variable assignment for the Environment= systemd unit file directive
  renderVar = k: v:
    let kv = toUpper k + "=" + escape [ "\\" ] (toString v);
    in if builtins.match ".*=.*[ =].*" kv != null then ''"${kv}"'' else kv;

in
{
  options.services.clipmenu = {
    maxClips = mkOption {
      type = types.int;
      default = 5000;
      description = "How many history items to store at most. ($CM_MAX_CLIPS)";
    };

    ownClipboard = mkOption {
      type = types.bool;
      default = true;
      description = " take ownership of the clipboard. Note: this may cause
      missed copies if some other application also handles the clipboard
      directly. ($CM_OWN_CLIPBOARD)";
    };

    selections = mkOption {
      type = types.listOf (types.enum [ "clipboard" "primary" ]);
      # XXX custom default, move elsewhere
      #default = ["clipboard" "primary"];
      default = [ "clipboard" ];
      description = "space separated list of the selections to manage. ($CM_SELECTIONS)";
    };

    ignoreWindow = mkOption {
      type = types.nullOr types.str;
      default = null; # "\\[[[:digit:]]\\]@%H";
      description = ''
        disable recording the clipboard in windows where the windowname
        matches the given regex (e.g. a password manager), do not ignore any
        windows if unset or empty

        Bash [[ =~ ]] is used to match this pattern to window name.
        You can find out the name of a window with: xdotool getactivewindow getwindowname
      '';
    };
  };

  config =
    mkIf cfg.enable {
      # For xmonad
      systemd.user.sessionVariables = mkIf (cfg.launcher != null) { CM_LAUNCHER = cfg.launcher; };

      systemd.user.services.clipmenu = {
        Unit.PartOf = [ "graphical-session.target" ];
        # XXX: Original has After=graphical-session.target, which I don't
        # understand; <url:sim@applicative:~/.config/systemd/user>
        Unit.After = lib.mkOverride 10 [ "graphical-session-pre.target" ];
        Service.Restart = "always";
        Service.RestartSec = "5s";
        Service.ProtectSystem = "strict";
        Service.PrivateTmp = true;
        Service.NoNewPrivileges = true;
        Service.MemoryDenyWriteExecute = true;
        Service.RestrictAddressFamilies = "";
        Service.RestrictRealtime = true;
        Service.Nice = 10;
        Service.Slice = "graphical.slice";
        Service.Environment =
          let
            vars = {
              # NOTE: This PATH value is copied from the upstream home-manager module
              PATH = makeBinPath (with pkgs; [ coreutils findutils gnugrep gnused systemd ]);
              CM_DIR = "%t";
              CM_MAX_CLIPS = cfg.maxClips;
              CM_OWN_CLIPBOARD = if cfg.ownClipboard then 1 else 0;
            }
            // optionalAttrs (cfg.selections != [ ]) { CM_SELECTIONS = concatStringsSep " " cfg.selections; }
            // optionalAttrs (isString cfg.ignoreWindow) { CM_IGNORE_WINDOW = cfg.ignoreWindow; };
          in
          mkForce (mapAttrsToList renderVar vars);
      };
    };
}
