{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.xandikos;

in
{
  options.services.xandikos = {
    enable = mkEnableOption "xandikos";

    directory = mkOption {
      type = types.str;
      default = "\${XDG_DATA_HOME}/xandikos";
      description = "Data directory";
    };

    principal = mkOption {
      type = types.str;
      default = "/user";
      description = "Current user principal";
    };

    routePrefix = mkOption {
      type = types.str;
      default = "/";
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ pkgs.xandikos ];

    systemd.user.services.xandikos = {
      Unit = {
        Description = "Xandikos CalDAV/CardDAV server";
        Requires = "xandikos.socket";
        After = "xandikos.socket";
      };

      Service = {
        ExecStart = "${pkgs.xandikos}/bin/xandikos ${escapeShellArgs [
          "--defaults"
          "--autocreate"
          "--directory" cfg.directory
          "--current-user-principal" cfg.principal
          "--route-prefix" cfg.routePrefix
          "--listen-address" "%t/xandikos/socket"
        ]}";
        ProtectSystem = "strict";
        Restart = "on-failure";
        KillSignal = "SIGQUIT";
        NotifyAccess = "all";
      };
    };

    systemd.user.sockets.xandikos = {
      Unit.Description = "Xandikos CalDAV/CardDAV server socket";
      Unit.StopWhenUnneeded = true;
      Socket.ListenStream = "%t/xandikos/socket";
    };
  };
}
