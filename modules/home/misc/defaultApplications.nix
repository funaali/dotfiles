{ lib, config, ... }:

let
  inherit (lib) types;

  cfg = config.custom.defaultApplications;

  mkAppOption = name: lib.mkOption {
    type = with types; nullOr (oneOf [ nonEmptyStr (nonEmptyListOf nonEmptyStr) ]);
    default = null;
    description = ''
      Application for ${name}. Should refer to a .desktop file.
    '';
  };

  mimeTypes = {
    directories = [
      "inode/directory"
    ];
    # XML-like text files
    xml-files = [
      "application/xhtml+xml"
      "text/html"
      "text/xml"
    ];
    documents = [
      "application/pdf"
      "application/msword"
    ];
    text-files = [
      "application/x-shellscript"
      "text/plain"
      "text/markdown"
      "text/x-c"
      "text/x-c++"
      "text/x-c++hdr"
      "text/x-c++src"
      "text/x-chdr"
      "text/x-csrc"
      "text/x-java"
      "text/x-makefile"
      "text/x-moc"
      "text/x-pascal"
      "text/x-tcl"
      "text/x-tex"
    ];
    archives = [
      "application/zip"
      "application/x-tar"
      "application/x-compressed-tar"
    ];
  };
in
{
  options.custom.defaultApplications = {
    browser = mkAppOption "browser";
    editor = mkAppOption "editor";
  };

  config = {
    xdg.mimeApps.defaultApplications = lib.mkMerge [
      # Stuff to open in browser
      (lib.mkMimeAssociations {
        app = cfg.browser;
        types = mimeTypes.xml-files ++ [
          "x-scheme-handler/about"
          "x-scheme-handler/http"
          "x-scheme-handler/https"
        ];
      })

      # Open XML/HTML in text editor if browser not available
      (lib.mkMimeAssociations {
        app = cfg.editor;
        types = mimeTypes.xml-files;
        priority = 1500;
      })

      # Open in text editor
      (lib.mkMimeAssociations {
        app = cfg.editor;
        types = mimeTypes.text-files;
      })
    ];
  };
}
