{ config, lib, ... }:

with lib;

let
  cfg = config.mailcap;

  commandType = types.str;

  entryModule = types.submodule {
    options = {
      command = mkOption {
        type = commandType;
        description = ''
          Command to execute. Passed to shell.
          Replacements:
            %s: path to (temporary) file.
            %{<ct-option>} for Content-Type header options, e.g. %{charset}
            %n (multipart only): number of parts
            %F (multipart only): series of arguments: content-type + filename for each part
          If there is no %s in the command then contents are passed to standard input.
        '';
      };

      test = mkOption {
        description = "Command that has to evaluate to true for the entry to be chosen.";
        type = types.nullOr commandType;
        default = null;
      };

      print = mkOption {
        description = "Command to print the data instead of displaying it interactively.";
        type = types.nullOr commandType;
        default = null;
      };

      compose = mkOption {
        description = "Command to compose a new body or body part in given format.";
        type = types.nullOr commandType;
        default = null;
      };

      needsterminal = mkOption {
        description = "Command requires a TTY";
        type = types.bool;
        default = false;
      };

      copiousoutput = mkOption {
        description = "Pipe standard output (non-interactive command)";
        type = types.bool;
        default = false;
      };
    };
  };

  renderCommand = s: escape [ "\\" ] (replaceStrings [ "\n" ] [ "\\n" ] s);

  renderEntry = type: v:
    let
      command = renderCommand v.command;
      fields = collect isString (mapAttrs (k: v: if isString v then "${k}=${v}" else null) (removeAttrs v [ "command" ]));
      flags = collect isString (mapAttrs (k: v: if v == true then k else null) v);
    in
    concatStringsSep "; " ([ type command ] ++ fields ++ flags) + ";";

  renderEntries = entries:
    ''# Managed by home-manager
    '' + concatStringsSep "\n" (mapAttrsToList (type: v: concatStringsSep "\n" (map (renderEntry type) (if isAttrs v then [ v ] else v))) entries) + "\n";
in
{
  options.mailcap = {
    enable = mkEnableOption "Manage ~/.mailcap";

    entries = mkOption {
      type = with types; attrsOf (either entryModule (listOf entryModule));
      default = { };
      example = {
        "text/html" = {
          command = "w3m -I %{charset} -T text/html";
          copiousoutput = true;
        };
      };
    };
  };

  config = mkIf cfg.enable {
    home.file.".mailcap".text = mkIf (cfg.entries != { }) (renderEntries cfg.entries);
  };
}
