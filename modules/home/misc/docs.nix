{ lib, config, pkgs, ... }:

let
  inherit (builtins) isNull;
  inherit (lib) mkOption mkIf optional optionals types;

  cfg = config.custom.docs;

  makeInfoPath = paths:
    lib.concatStringsSep ":" (lib.concatMap (path: [
      (path + "/info")
      (path + "/share/info")
    ]) paths);
in
{
  options.custom.docs = {
    enable = lib.mkEnableOption "custom.docs";

    man.extraPackages = mkOption {
      type = types.listOf types.package;
      default = [ ];
      description = ''
        Extra derivations to install just for their man pages.
      '';
    };

    info.paths = mkOption {
      type = types.listOf types.nonEmptyStr;
      default = [
      ];
      description = ''
        Entries to add to INFOPATH. '/info' and '/share/info' are appended to
        each entry.
      '';
    };
  };

  config = mkIf cfg.enable {
    custom.docs.man.extraPackages = [
      pkgs.man-pages
      pkgs.man-pages-posix
    ] ++ optional (!isNull config.nix.package) config.nix.package.man;

    # TODO: really only needed on non-nixos.
    custom.docs.info.paths = [
      config.home.profileDirectory
      "/nix/var/nix/profiles/default"
    ];

    home.packages = optionals config.programs.man.enable cfg.man.extraPackages;

    home.extraOutputsToInstall = [ "doc" "devdoc" ];

    home.sessionVariables = {
      # XXX: this contains ALL locales (220MB filesize). Why is this here?
      # On NixOS at least could be just /run/current-system/sw/lib/locale/locale-archive
      # or not set at all.
      #LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";

      INFOPATH = makeInfoPath cfg.info.paths + "\${INFOPATH:+:}$INFOPATH";
    };

    programs.man = {
      enable = true;

      # on archlinux, must link against gdbm in order to read the system man db
      package = pkgs.man-db.override { db = pkgs.gdbm; };

      generateCaches = true;
    };

    programs.info.enable = true;
  };
}
