# This is intended to be imported like `lib.extend (import ./. inputs)

{ haumea, home-manager, ... }: # required flake inputs

lib: luper:

with lib;
with builtins;

{
  # use path relative to the root of the project
  relativeToRoot = lib.path.append ../.;

  types = luper.types // {
    module = lib.mkOptionType {
      name = "nixpkgs-module";
      description = "nixpkgs module";
      merge = lib.mergeOneOption;
    };
  };

  # Load all .nix files in a directory into a nested attrset of paths. Useful
  # for instance to load NixOS/HomeManager modules (without importing).
  loadPaths = src: haumea.lib.load {
    inherit src;
    loader = haumea.lib.loaders.path;
    transformer = _: mod: mod.default or mod;
  };

  flattenPaths = arg0:
    let
      go = prefix: concatMapAttrs (name: value: if isAttrs value then go "${name}." value else { "${prefix}${name}" = value; });
    in go "" arg0;

  # Load directory contents, importing paths, with no extra arguments supplied to the values.
  loadOverlays = src: haumea.lib.load {
    inherit src;
    loader = haumea.lib.loaders.verbatim;
  };

  # Create an overlay that adds a package from a flake input's "packages"
  # attribute set to pkgs.
  #
  #   FlakeInput -> String -> Overlay
  #
  genPkgOverlay = input: pname:
    final: _: {
      __dontExport = true;
      ${pname} =
        input.packages.${final.stdenv.system}.${pname}
          or input.defaultPackage.${final.stdenv.system}
          or input.legacyPackages.${final.stdenv.system}.${pname}
      ;
    };

  # Extract derivations from the PkgSet defined in the overlays.
  #
  #   PkgSet -> (AttrsOf Overlay) -> (AttrsOf Pkg)
  #
  packagesFromOverlays = pkgs: overlays:
    let
      getPackage = name: if pkgs?${name} then pkgs.${name} else null;

      packageNames = concatMap (ov: attrNames (ov null null)) (attrValues overlays);
    in
    filterAttrs
      (_: x: x != null && x?type && x.type == "derivation")
      (genAttrs packageNames getPackage);

  # Calls `lib.nixosSystem` with certain presets.
  nixosSystem =
    { pkgsFor # hostPlatform -> nixpkgs
    , sharedModules ? [ ] # cfg.nixos.sharedModules
    , specialArgs ? { } # cfg.nixos.specialArgs
    , system ? "x86_64-linux"
    }:
    name: module:
    let
      defaults = { lib, config, ... }: {
            config.networking.hostName = lib.mkDefault name;
            config.nixpkgs.hostPlatform = lib.mkDefault system;
            config.nixpkgs.pkgs = pkgsFor config.nixpkgs.hostPlatform;
          };
    in
    luper.nixosSystem {
      modules = [ defaults ] ++ sharedModules ++ lib.toList module;
      inherit specialArgs;
    };

  # Export a standalone home-manager configuration.
  mkHMUserStandalone =
    { channelsWith # system -> { nixpkgs = pkgs; ... }
    , channelName ? "nixpkgs"
    , username ? null
    , homeDirectory ? null
    , system ? "x86_64-linux"
    , modules ? [ ]
    , sharedModules ? [ ]
    , extraSpecialArgs ? { }
    }:
    let
      defaults = { config, ... }: {
        home.homeDirectory = mkIf (!isNull config.home.username) (mkDefault ("/home/${config.home.username}"));
      };
      overrides = { ... }: {
        home.username = mkIf (!isNull username) (mkForce username);
        home.homeDirectory = mkIf (!isNull homeDirectory) (mkForce homeDirectory);
      };
    in
    home-manager.lib.homeManagerConfiguration {
      modules = flatten [
        sharedModules
        modules
        defaults
        overrides
      ];
      pkgs = (channelsWith system).${channelName};
      inherit extraSpecialArgs;
    };

  # Parse a home-manager user@host key.
  # "user@somemachine" -> { username = "user"; hostName = "somemachine"; }
  fromUserHostKey = k:
    let
      m = match "([^[:space:]]*)@([^[:space:]]*)" k;
      get = i: m: if isList m && elemAt m i != "" then elemAt m i else null;
    in
    {
      user = get 0 m;
      host = get 1 m;
    };

  getDesktopEntries = app:
    let
      f = x: toList (f_str (toString x));
      f_str = x: if match ".+\.desktop" x == [ ] then x else x + ".desktop";
    in
    if isNull app then null else concatMap f (toList app);

  mkMimeAssociations = { app, types, priority ? modules.defaultOrderPriority }:
    let rhs = getDesktopEntries app; in
    optionalAttrs (!isNull app) (genAttrs types (_: mkOrder priority rhs));
}
