# README


DSA networking port configuration:

```
Syntax	Member Port Is
lanx	untagged ~PVID
lanx:u	untagged
lanx:t	tagged
lanx:*	PVID untagged
lanx:u*	PVID untagged
lanx:t*	PVID tagged
```


Two kinds of ports:
- Access port: Untagged in one VLAN. Absent in all the others.
- Trunk port: Tagged in one or more VLANs. Untagged in none.
