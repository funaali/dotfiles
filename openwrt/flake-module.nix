{ lib, config, inputs, flake-parts-lib, ... }:

let
  inherit (lib) mapAttrs' mkOption types nameValuePair;
  inherit (flake-parts-lib) mkPerSystemOption;

  openwrt-image-builder-lib = inputs.openwrt-imagebuilder.lib;

  cfg = config.openwrt;

  openwrtConfiguration =
    { modules ? [ ]
    , pkgs
    , lib ? pkgs.lib
    , extraSpecialArgs ? { }
    }:
    import ./modules {
      inherit pkgs lib openwrt-image-builder-lib extraSpecialArgs;
      configuration = { ... }: {
        imports = modules;
      };
    };

  openwrtPackages = { pkgs, lib ? pkgs.lib, configurations, extraSpecialArgs ? {} }:
    let
      build = name: module:
        let
          defModule = { lib, ... }: {
            system.hostname = lib.mkDefault name;
          };
          result = openwrtConfiguration {
            inherit pkgs lib extraSpecialArgs;
            modules = [ module defModule ];
          };
        in result.config.image.build;
    in mapAttrs' (k: v: nameValuePair ("openwrt-image_" + k) (build k v)) configurations;
in
{
  options = {
    openwrt = {
      lib = mkOption {
        type = types.lazyAttrsOf types.raw;
        default = { };
      };

      configs = mkOption {
        type = types.attrsOf types.raw;
        default = { };
      };
    };

    perSystem = mkPerSystemOption ({ ... }: {
      #options.openwrt.profiles = mkOption {
      #  type = types.lazyAttrsOf types.raw;
      #  readOnly = true;
      #};
    });
  };

  config = {
    openwrt = {
      lib = {
        inherit openwrtConfiguration openwrtPackages;
        openwrt = (import ./modules/lib/extended.nix lib).openwrt;
      };

      configs = import ./configs.nix { };
    };

    perSystem = { pkgs, ... }: {
      packages = openwrtPackages {
        inherit pkgs;
        configurations = cfg.configs;
      };
    };
  };
}
