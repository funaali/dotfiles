{ }:

{
  tp-link = { ... }: {
    image.profile = "tplink_tl-wdr4900-v1";

    image.extraImageName = "sim";

    # add package to include in the image, ie. packages that you don't
    # want to install manually later
    image.packages = [
      # Optimize for space by removing unnecessary packages
      "-ppp"
      "-ppp-mod-pppoe"
      # Replace dnsmasq with dnsmasq-dhcpv6
      "-dnsmasq"
      "dnsmasq-dhcpv6"
      # Replace basic wpad with full version
      "-wpad-basic-mbedtls"
      "wpad-mbedtls"
      # Add DOH Proxy
      "https-dns-proxy"
      # USB EHCI devices support
      "kmod-usb-ehci"
      # Useful CLI tools
      "tcpdump-mini"
      "vim"
      "ip-bridge"
      "bind-dig"
      "bind-nslookup"
      "kitty-terminfo"
      # Adblock
      "adblock"
    ];

    image.extraMakeArgs = [
      "CONFIG_PACKAGE_MAC80211_DEBUGFS=n"
    ];

    #disabledServices = [ ];

    system = {
      description = "Router .LAN (TP-LINK WDR-4900)";
      notes = "Nix";
      zonename = "Europe/Vienna";
      ntp = {
        enable_server = true;
        use_dhcp = false;
      };
    };

    dropbear = {
      settings = {
        GatewayPorts = true;
      };
      authorizedKeys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIlIECoPAF/Uy+ZB3tpbMhXwF/5xNdUFLwLifn5zUZgf my-ecc-20181015"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1hM1c1BIzAN35gS6aoYRRVz4icXFT3KQcAhY5R5quhFTtsXYw74UddSWqp0eBWV4nSjQtYh+GGJxZ53nn5R1BTI8nwSxHJebROJAhdEYyX4j1Hn5pJV15ucfd2gP9wE0ejfRxbXxh+RF/evTOac/7F/iBACTmnYcqYvpOjIOGxWsqWKg2gL4ROGIMqSKDG1gqbXaaaCg60SZLEcBdRUQ9nyXQF9z1hjXjMyzWcxbA4hSmvZGw7cb4K4oWuje7ETKyvltanzRKimbW+zGzAXNu/1nYTpVQ7c5wj3mroW/8Y3TvJ5ETbgvF6LR5diork8AC+xHbY4oD9Jr9JmQwrRdV id_rsa_applicative"
      ];
    };

    users.root.encryptedPassword = "$1$GJGhYUUj$VPpnsubkFO1iTJhIKEZ4Z1";

    network = {
      globals.ula_prefix = "fdf8:9c5c::/48";
      globals.packet_steering = true;
      devices.br-lan = {
        type = "bridge";
        ports = [
          "lan1"
          "lan2"
          "lan3"
          "lan4"
          "phy0-ap0"
          "phy1-ap0"
        ];
        vlan_filtering = true; # ???
        bridge-vlans = [
          {
            vlan = 10;
            ports = [
              "lan1:u*"
              "lan2:u*"
              "lan3:u*"
              "lan4:u*"
              "phy0-ap0:u*"
              "phy1-ap0:u*"
            ];
          }
          {
            vlan = 50;
            ports = [
              "lan1:t"
              "lan2:t"
              "lan3:t"
              "lan4:t"
            ]; }
        ];
      };

      interfaces.lan = {
        device = "br-lan.10";
        proto = "static";
        ipaddr = "192.168.2.1";
        netmask = "255.255.255.0";
        ip6assign = 64;
        ip6hint = 10;
        dns_search = "lan";
      };

      interfaces.mgmt = {
        device = "br-lan.50";
        proto = "static";
        ipaddr = "192.168.50.1";
        netmask = "255.255.255.0";
        defaultroute = false;
        delegate = false;
        ip6assign = 64;
        ip6hint = 50;
        ip6class = "local";
      };
    };

    firewall.defaults = {
      flow_offloading = true;
      synflood_protect = true;
    };

    firewall.updateZone.wan = {
      masq = true;
    };

    firewall.zone.wan6 = {
      network = "wan6";
      input = "ACCEPT";
      output = "ACCEPT";
      forward = "REJECT";
    };

    firewall.rules = [
      {
        name = "Allow-mDNS";
        src = "*";
        proto = "udp";
        src_port = "5353";
        dest_port = "5353";
        dest_ip = "224.0.0.251";
        target = "ACCEPT";
      }
      {
        name = "Allow-WAN6-to-LAN";
        src = "wan6";
        dest = "lan";
        family = "ipv6";
        target = "ACCEPT";
      }
    ];

    wireless = {
      wifi-device-defaults = {
        country = "NZ";
        disabled = false;
        txpower = 28;
        cell_density = "Very High";
        rx_stbc = true;
      };

      wifi-device-overrides = [
        {
          match.band = "5g";
          values = {
            htmode = "HT40";
            noscan = true;
            channel = "132";
          };
        }
        {
          match.band = "2g";
          values = {
            channel = "auto";
            htmode = "HT20";
          };
        }
      ];

      wifi-iface-defaults = {
        network = "lan";
        ssid = "CwRouzopzyU";
        key = "Tts6seaQTiE"; # TODO
        #encryption = "psk2";
        encryption = "sae-mixed"; # need to disable ieee80211r/w for this
        ieee80211w = false;
        ieee80211r = false;
        ft_over_ds = false;
        ft_psk_generate_local = false;
        mobility_domain = "fd8f";
      };
    };

    dhcp = {
      dnsmasq = {
        nonegcache = true;
        add_local_fqdn = 2;
      };

      dhcp = {
        lan = {
          dhcpv4 = "server";
          dhcpv6 = "server";
          ra = "server";
          ndp = "relay";
          ra_flags = [ "managed-config" "other-config" ];
          dhcp_option = [ "44" "42,192.168.2.1" ];
        };
      };

      hosts = let mkHost = args: { dns = true; slaac = true; } // args; in
        {
          lambda-nebula-BMC = mkHost {
            ip = "192.168.2.3";
            hostid = "5020";
            duid = "0E:00:00:01:00:01:28:78:C8:FD:3C:EC:EF:7B:6D:D6";
            mac = "3c:ec:ef:7b:6d:d6";
          };
          lambda-nebula-1G = mkHost {
            ip = "192.168.2.4";
            hostid = "1020";
            mac = "3c:ec:ef:7b:6a:32";
          };
          lambda-nebula-10G = mkHost {
            ip = "192.168.2.5";
            hostid = "1021";
            mac = "3c:ec:ef:7b:6c:88";
          };
          samuli-cyan-nix = mkHost {
            ip = "192.168.2.20";
            hostid = "1050";
            mac = "9c:eb:e8:7b:20:8f";
          };
          applicative = mkHost {
            ip = "192.168.2.2";
            hostid = "1010";
            mac = "2c:4d:54:e5:4e:ad";
          };
          pl1 = mkHost {
            ip = "192.168.2.6";
            hostid = "2010";
            mac = "e4:65:b8:b1:d8:f8";
          };
        };
    };

    file."root/.exrc".text = ''
      " https://stackoverflow.com/questions/70838574/vim-error-1187-cannot-source-the-defaults-vim-file-after-installing-vundle
    '';

    file."etc/profile.d/prompt.sh".text = ''
      # Red background if last command failed
      export PS1="\$([ \$? -ne 0 ] && echo \\\[\\\e[41m\\\])\u@\h:\w\\$\[\e[0m\] "
    '';

    ip6neigh = {
      enable = true;
      settings = {
        ula_label = "ULA";
        wula_label = false;
        lan_wipe = false;
        probe_eui64 = 2;
        flush = 3;
        log = 1;
      };
    };
  };
}
