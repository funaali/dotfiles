{ pkgs
# Note: should be the standard nixpkgs library with OpenWrt extensions.
, lib
}:

let
  modules = [
    ./image-builder.nix
    ./files.nix
    ./shlib.nix
    ./uci-defaults.nix
    ./system.nix
    ./dropbear.nix
    ./users.nix
    ./network.nix
    ./crontab.nix
    ./firewall.nix
    ./wireless.nix
    ./dhcp.nix
    ./ip6neigh.nix
  ];

  defsModule = { ... }: {
    options = {
      lib = lib.mkOption {
        type = lib.types.lazyAttrsOf lib.types.raw;
        default = lib;
        description = "Extra library functions that may be used by modules.";
      };
    };

    config = {
      _module.args.baseModules = modules;
      _module.args.pkgs = lib.mkDefault pkgs;
    };
  };

in modules ++ [ defsModule ]
