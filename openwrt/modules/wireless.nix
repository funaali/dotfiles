{ lib, config, ... }:

with lib;

let
  cfg = config.wireless;

  cellDensityValues = [ "Disabled" "Normal" "High" "Very High" ];

  commonDeviceOpts = {
    freeformType = types.attrsOf types.anything;

    options = {
      cell_density = mkOption {
        type = types.nullOr (types.enum cellDensityValues);
        default = null;
        apply = v: lists.findFirstIndex (x: x == v) null cellDensityValues;
      };
    };
  };

  ifaceOpts = {
    freeformType = types.attrsOf types.anything;
    options = {
    };
  };

  deviceSettingsType = types.submodule [ commonDeviceOpts ];
  ifaceSettingsType = types.submodule [ ifaceOpts ];

  overridesFor = valuesType: {
    options = {
      match = mkOption {
        type = valuesType;
        apply = filterAttrs (_: v: v != null);
        default = { };
      };
      values = mkOption {
        type = valuesType;
        default = { };
      };
    };
  };
in
  {
    options = {
      wireless = {
        wifi-device-defaults = mkOption {
          type = deviceSettingsType;
          default = { };
          description = "Settings to apply to all wifi-devices by default.";
        };

        wifi-device-overrides = mkOption {
          type = with types; listOf (submodule (overridesFor deviceSettingsType));
          default = [ ];
          description = "Settings to matched wifi-devices.";
        };

        wifi-iface-defaults = mkOption {
          type = ifaceSettingsType;
          default = { };
          description = "Settings to apply to all wifi-ifaces by default.";
        };
      };
    };

    config = {
      uci-defaults."66-nix-wireless" = {
        update.wireless = [
          {
            type = "wifi-device";
            values = cfg.wifi-device-defaults;
          }
          {
            type = "wifi-iface";
            values = cfg.wifi-iface-defaults;
          }
        ] ++ builtins.map (v: { type = "wifi-device"; } // v) cfg.wifi-device-overrides;
      };
    };
  }
