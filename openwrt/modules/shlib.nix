{ ... }:
{
  config = {
    file."lib/functions-nix.sh".text = ''
      . /lib/functions.sh

      getSectionBy() {
        local config=$1 type=$2 option=$3 value=$4 option2=''${5:-$3} value2=''${6:-$4}
        uci -X show "$config" | awk -F= -v TYP="$type" -v M1="[.]$option='$value'\$" -v M2="[.]$option2='$value2'\$" \
          'BEGIN { ec=1 }
          /^[^.]*\.[^.]*$/ { sub(/[^.]*\./,""); s=$1; t=$2; r1=0; r2=0; next }
          $0 ~ M1 { r1=1 }
          $0 ~ M2 { r2=1 }
          t == TYP && r1==1 && r2==1 { print s; ec=0; r1=0 }
          END { exit ec }'
      }

      addOrFindSection() {
        getSectionBy "$@" || uci add "$1" "$2"
      }
    '';
  };
}
