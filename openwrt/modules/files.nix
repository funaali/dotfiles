{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.file;

  fileType = types.attrsOf (types.submodule ({ name, config, ... }: {
    options = {
      target = mkOption {
        type = types.str;
        defaultText = literalExpression "name";
        description = "Path to target file (relative to /).";
      };

      text = mkOption {
        type = types.nullOr types.lines;
        default = null;
        description = "Text of the file.";
      };

      source = mkOption {
        type = types.path;
        description = "Path of the source file or directory.";
      };

      executable = mkOption {
        type = types.nullOr types.bool;
        default = null;
        description = "Set the executable bit.";
      };

      mode = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "Set the file mode.";
      };
    };

    config = {
      target = mkDefault name;
      source = mkIf (config.text != null) (mkDefault (pkgs.writeTextFile {
        inherit name;
        inherit (config) text;
        executable = config.executable == true;
      }));
    };
  }));
in
  {
    options = {
      file = mkOption {
        type = fileType;
        default = { };
        description = "Attribute set of files to add to the image.";
      };

      image-files = mkOption {
        internal = true;
        type = types.package;
        description = "The package to contain all files added to the image.";
      };
    };

    config = {
      image-files = pkgs.runCommandLocal "image-files"
      { }
      (''
        mkdir -p $out

        insertFile() {
          local source=$1 relTarget=$2 executable=$3

          local target=$out/$relTarget

          # Check for target file collision
          if [[ -e $target ]]; then
            echo "File conflict for file $relTarget" >&2
            return 1
          fi

          mkdir -p "$(dirname "$target")"

          if [[ -d $source ]]; then
            mkdir -p "$target"
            cp --reflink=auto -rT "$source" "$target"
          else
            cp --reflink=auto "$source" "$target"

            # Set the executable bit if necessary
            if [[ $executable && ! -x $target ]]; then
              chmod +x "$target"
            fi
          fi
        }
      '' + concatStrings (
        mapAttrsToList (n: v: ''
          insertFile ${escapeShellArgs [
            v.source
            v.target
            (toString v.executable)
          ]}
        '') cfg
      ));

      image.postConfigure = mkMerge (mapAttrsToList (n: v:
          "chmod -R ${toString v.mode} ./files/${toString v.target}") (filterAttrs (_: v: v.mode != null) cfg));
    };
  }
