{ lib, config, ... }:

with lib;

let
  cfg = config.network;

  anyOpts = {
    freeformType = types.attrsOf types.anything;
  };

  nameOpts = s: { name, ... }: {
    options = {
      name = mkOption {
        type = types.str;
        description = "${s} name";
      };
    };
    config = {
      name = mkDefault name;
    };
  };

  networkGlobalsOpts = {
    options = {
      ula_prefix = mkOption {
        type = types.nullOr types.str; # TODO
        default = null;
      };
      packet_steering = mkOption {
        type = types.nullOr types.bool;
        default = null;
      };
    };
  };

  deviceOpts = {
    options = {
      macaddr = mkOption { type = types.nullOr types.str; default = null; };
      type = mkOption { type = types.nullOr types.str; default = null; };
      ifname = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "only relevant if using the macvlan type";
      };
      ports = mkOption {
        type = with types; nullOr (nonEmptyListOf str);
        default = null;
        description = "List of L2 device names.";
      };

      bridge-vlans = mkOption {
        type = with types; listOf (submodule [ bridgeVlanOpts anyOpts ]);
        default = [ ];
      };
    };

    config = {
      ".anonymous" = mkDefault true;
      ".replaceBy" = mkDefault "name";
    };
  };

  interfaceOpts = {
    options = {
      device = mkOption {
        type = types.str;
      };
      dns_search = mkOption {
        type = with types; either str (listOf str);
        apply = x: if x == [ ] then null else toList x;
        default = [ ];
      };
      ip6class = mkOption {
        type = with types; either str (listOf str);
        apply = x: if x == [ ] then null else toList x;
        default = [ ];
      };
    };
    config = { };
  };

  bridgeVlanOpts = {
    options = {
      # device = mkOption { type = types.str; };
      vlan = mkOption { type = types.int; };
      ports = mkOption { type = types.listOf types.str; default = []; };
      local = mkOption {
        type = types.nullOr types.bool;
        default = null;
        description = "Handle this vlan in the switch (default true).";
      };
    };
    config = { };
  };
in
{
  options = {
    network = {
      globals = mkOption {
        type = types.submodule [ networkGlobalsOpts anyOpts ];
        default = { };
      };
      devices = mkOption {
        type = types.attrsOf (types.submodule [ deviceOpts anyOpts (nameOpts "device") ]);
        default = { };
      };
      interfaces = mkOption {
        type = types.attrsOf (types.submodule [ interfaceOpts anyOpts ]);
        default = { };
      };
    };
  };

  config = {
    uci-defaults."65-nix-network" = {
      settings = {
        network.globals = cfg.globals;

        network.device =
          mapAttrs (_: v: builtins.removeAttrs v [ "bridge-vlans" ]) cfg.devices;

        network.bridge-vlan =
          flatten (mapAttrsToList
            (device: v: builtins.map
              (x: x // {
                inherit device;
                ".replaceBy" = [ "device" "vlan" ];
              })
              v.bridge-vlans)
            cfg.devices);

        network.interface = cfg.interfaces;
      };
    };
  };
}
