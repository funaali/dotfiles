{ lib, config, ... }:

with lib;

let
  cfg = config.firewall;

  networkInterfaceType = types.singleLineStr;
  trafficPolicyType = types.enum ["ACCEPT" "REJECT" "DROP"];
  familyType = types.enum ["any" "ipv4" "ipv6"];

  mkBoolOption = s: mkOption {
    type = with types; nullOr bool;
    default = null;
    description = "Enable ${s}";
  };

  mkTrafficPolicyOption = desc: mkOption {
    type = types.nullOr trafficPolicyType;
    default = null;
    description = "Default policy for ${desc} traffic.";
  };

  familyOption = mkOption {
    type = types.nullOr familyType;
    default = null;
    description = ''
      Specifies the address family (ipv4, ipv6 or any) for which rules are
      generated. Defaults to any if unspecified.
    '';
  };

  targetOption = mkOption {
    type = types.enum [ "ACCEPT" "REJECT" "DROP" "MARK" "NOTRACK" ];
    default = "DROP";
    description = "Firewall rule action.";
  };

  enabledDefaultOption = mkOption {
    type = types.bool;
    apply = v: if v then null else v;
    default = true;
  };

  # Options only available on firewall defaults sections.
  firewallDefaultOpts = {
    options = {
      drop_invalid = mkBoolOption "drop_invalid";
      synflood_protect = mkBoolOption "synflood_protect";
      flow_offloading = mkBoolOption "flow_offloading";
      flow_offloading_hw = mkBoolOption "flow_offloading_hw";
    };
  };

  # Options available on default and zone sections.
  firewallDefaultZoneOptions = {
    # TODO allows whatever...
    freeformType = types.attrsOf types.anything;

    options = {
      input = mkTrafficPolicyOption "incoming";
      forward = mkTrafficPolicyOption "forwarded";
      output = mkTrafficPolicyOption "outgoing";
    };
  };

  # Options available only on zones.
  firewallZoneOptions = { name, ... }: {
    options = {
      name = mkOption {
        type = types.str;
        description = "Zone name";
      };

      network = mkOption {
        type = with types; nullOr (either networkInterfaceType (nonEmptyListOf networkInterfaceType));
        default = null;
        apply = x: if x != null then toList x else x;
        description = "List of network interfaces attached to the zone.";
      };

      masq = mkBoolOption "masquerading outgoing zone IPv4 traffic";
      masq6 = mkBoolOption "masquerading outgoing zone IPv6 traffic";
      mtu_fix = mkBoolOption "MSS clamping for outgoing traffic";
      family = familyOption;

      #".mapBy" = mkOption {
      #  type = types.nullOr types.str;
      #  default = "name";
      #};
    };

    config = {
      name = mkDefault name;
      ".anonymous" = mkDefault true;
      ".replaceBy" = mkDefault "name";
    };
  };

  firewallRuleOpts = { ... }: {
    # TODO allows whatever...
    freeformType = types.attrsOf types.anything;

    options = {
      name = mkOption { type = types.nullOr types.str; default = null; };
      enabled = enabledDefaultOption;
      family = familyOption;
      target = targetOption;
    };

    config = {
      ".replaceBy" = mkDefault "name";
    };
  };

in
{
  options = {
    firewall = {
      defaults = mkOption {
        type = types.submodule [ firewallDefaultOpts firewallDefaultZoneOptions ];
        default = { };
        description = "Firewall options for the 'defaults' section.";
      };

      zone = mkOption {
        type = types.attrsOf (types.submodule [ firewallDefaultZoneOptions firewallZoneOptions ]);
        default = { };
        description = "Firewall zones.";
      };

      updateZone = mkOption {
        type = types.attrsOf (types.submodule [ firewallDefaultZoneOptions firewallZoneOptions ]);
        default = { };
        description = "Firewall zones.";
      };

      rules = mkOption {
        type = types.listOf (types.submodule [ firewallRuleOpts ]);
        default = [ ];
        description = "Firewall rules to add/modify.";
      };
    };
  };

  config = {
    uci-defaults."70-nix-firewall" = {
      settings.firewall = {
        defaults = {
          ".anonymous" = true;
          ".index" = 0;
        } // cfg.defaults;

        zone = cfg.zone;

        rule = cfg.rules;
      };

      update.firewall = mapAttrsToList (name: v: {
        type = "zone";
        match = { inherit name; };
        values = v;
      }) cfg.updateZone;
    };
  };
}
