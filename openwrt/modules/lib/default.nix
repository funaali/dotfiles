# Library functions for dealing with OpenWrt image builder, uci and stuff.

{ lib }:

with lib;

rec {
  uci.sectionOrder = {
    system = [ "system" ];
    network = [ "globals" "device" "bridge-vlan" "interface" ];
    firewall = [ "defaults" "zone" "forwarding" "rule" "redirect" "include" ];
    dhcp = [ "dnsmasq" "dhcp" "host" ];
  };

  uci.renderOptionValue = v:
    if isBool v then if v then "'1'" else "'0'"
    else if isString v then "'${replaceStrings [ "'" ] [ "'\\''" ] v}'"
    else if isInt v || isFloat v then "'${toString v}'"
    else throw "renderOptionValue: cannot render option value of type: ${builtins.typeOf v}";

  # Works for list values too.
  # uci set ${config}.${section}.${option}='${value}'
  uci.set-option' = { config, section, option, value }:
    if isList value
    then [ "uci -q delete ${config}.${section}.${option}" ]
      ++ builtins.map (v: "uci add_list ${config}.${section}.${option}=${uci.renderOptionValue v}") value
    else [ "uci set ${config}.${section}.${option}=${uci.renderOptionValue value}" ];

  # uci set ${config}.${section}=${type}
  # uci set ${config}.${section}.${option}='${value}'
  # ...
  uci.set-section' = { config, section, type ? null, values }:
    let
      setType = optional (type != null) "uci set ${config}.${section}=${type}";
      setValues = flatten (mapAttrsToList (option: value: uci.set-option' { inherit config section option value; }) values);
    in
     setType ++ setValues;

  uci.set-section = { config, section, index ? null, anonymous ? null, type ? section, values, replaceBy ? [ ] }:
    let
      section' = if anonymous == false then section
        else if index != null then "@${section}[${toString index}]"
        else sectionVar';
      sectionVar = "section";
      sectionVar' = "\${${sectionVar}}";

      addOrFindSection = optional (section' == sectionVar') (
        if replaceBy != []
        then "${sectionVar}=$(addOrFindSection ${escapeShellArgs ([ config type ] ++ concatMap (n: [ n values.${n} ]) replaceBy)})"
        else "${sectionVar}=$(uci add ${escapeShellArgs [ config type ]})");
    in
    [ "" ] ++
    addOrFindSection ++
    uci.set-section' {
      inherit config values;
      section = section';
      type = if (section' == sectionVar') then null else type;
    };

  # { system.system = [
  #      { hostname = "tp-link"; }
  #      ...
  #   ];
  # }
  #
  # =>
  # ...
  # set system.@system[0].hostname='tp-link'
  # ...
  uci.fromAttrs = settings:
    let
      isNestedAttrs = x: isAttrs x && all isAttrs (attrValues x);

      getSectionSort = config: let
        sorder = uci.sectionOrder.${config} or [];
      in sortOn (k: lists.findFirstIndex (x: x == k) 100 sorder);

      goWith = sorter: f: x:
        concatStringsSep "\n" (
          if isAttrs x then builtins.map (n: f n x.${n}) (sorter (attrNames x))
          else throw "goWith: not attribute set!"
        );

      goConfig = config: x:
        if isAttrs x then goWith (getSectionSort config) (goSection { inherit config; }) x
        else concatStringsSep "\n" (builtins.map (v: goSection { inherit config; } v.".name" v) x);

      goSection = args: name: val:
        let
          args' = args // { type = name; };
          goList = val: concatStringsSep "\n" (builtins.map (goSection' args' name) val);
          goNestedAttrs = goWith id (goSection' args');
        in
        if isList val then goList val
        else if isNestedAttrs val then goNestedAttrs val
        else goSection' args' name val;

      goSection' = args: name: values:
        let
          type = if values.".type" != null then values.".type" else (args.type or null);
        in
        concatStringsSep "\n" (uci.set-section (args // {
          inherit type;
          section = name;
          index = values.".index";
          anonymous = values.".anonymous";
          replaceBy = values.".replaceBy";
          values = filterAttrs (n: v: !hasPrefix "." n && v != null) values;
        }));
    in
    goWith id goConfig settings;

  # cat > $out/etc/uci-defaults/99-system <<EOF
  # uci -q batch << EOI
  # ${self.lib.uci.fromAttrs systemCfg}
  # commit
  # EOI
  # EOF
  uci.batch = { settings ? { }, update ? { }, postCommit ? null }:
    ''
      . /lib/functions-nix.sh

      # Set options
      ${uci.fromAttrs settings}

      # Update existing sections
      ${concatStringsSep "\n" (mapAttrsToList (config: values':
      let
        types = unique (catAttrs "type" values');

        perType = type: let
          values = filter (v: v.type == type) values';
          funcName = replaceStrings [ "-" ] [ "_" ] "my_setup_${config}_${type}";
          matchPred = match:
            let r = concatStringsSep " && " (mapAttrsToList (name: value:
                ''[ "$(uci -q get "${config}.''${name}.${name}")" = '${value}' ]''
              ) match);
            in if r == "" then ":" else r;
        in ''
          ${funcName}() {
            local name=$1
            ${concatStringsSep "\n" (imap1 (i: { match ? { }, values, ... }: ''
              if ${matchPred match}; then
                ${concatStringsSep "\n  " (flatten (mapAttrsToList (option: v: uci.set-option' {
                  inherit config option;
                  section = if isAttrs v && v ? section then v.section else "\${name}";
                  value = if isAttrs v then v.value else v;
                }) (filterAttrs (n: v: !hasPrefix "." n && v != null && (!elem n (attrNames match) || (isAttrs v && v.section != "\${name}"))) values)))}
              fi
            '') values)}
          }
          config_foreach ${funcName} ${type}
        '';
      in
      ''
        config_load ${config}

        ${concatMapStringsSep "\n" perType types}
      '') update)}

      # Commit changes
      uci commit
    ''
    + optionalString (postCommit != null) ''
      # Post-commit actions
      ${postCommit}
    ''
  ;

  uci.batch-script = commands:
    let
      marker = "__END__";
    in
    concatStringsSep "\n" ([ ''uci -q batch <<-"${marker}"'' ] ++ commands ++ [ marker ]);

}

# getSectionBy() {
#   local config=$1 type=$2 option=$3 value=$4
#   uci -X show "$config" | awk -F= -v TYP="$type" -v OPT="$option" -v VAL="$value" \
#     '/^[^.]*\.[^.]*$/{s=$1;t=$2;next}
#     t == TYP && $1 ~ s "." OPT "$" && $2 ~ "^." VAL ".$" {print s; exit}
#     END {exit 1}'
# }
