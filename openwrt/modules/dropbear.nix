{ lib, config, ... }:

with lib;

let
  cfg = config.dropbear;

  mkBoolOption = s: mkOption {
    type = with types; nullOr bool;
    default = null;
    description = "Enable ${toString s}";
  };
in
{
  options.dropbear = {
    enable = mkOption {
      type = types.bool;
      default = true;
      description = "Whether to enable dropbear";
    };

    settings = {
      verbose = mkBoolOption "verbose";
      GatewayPorts = mkBoolOption "GatewayPorts";
      PasswordAuth = mkBoolOption "PasswordAuth";
      RootPasswordAuth = mkBoolOption "RootPasswordAuth";
      RootLogin = mkBoolOption "RootLogin";
    };

    authorizedKeys = mkOption {
      type = with types; nullOr (listOf str);
      default = null;
      description = "Keys to add to dropbear authorized_keys file";
    };
  };

  config = mkIf cfg.enable {
    uci-defaults."61-nix-dropbear" = {
      settings.dropbear.dropbear = cfg.settings // {
        ".anonymous" = true;
        ".index" = 0;
      };
      postCommit = "service dropbear restart";
    };

    file."etc/dropbear/authorized_keys" = mkIf (cfg.authorizedKeys != null) {
      text = concatStringsSep "\n" cfg.authorizedKeys + "\n";
      mode = "0640";
    };
  };
}
