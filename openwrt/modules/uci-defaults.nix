# to set UCI configuration, create a uci-defauts scripts as per
# official OpenWRT ImageBuilder recommendation.

{ config, lib, ... }:

with lib;

let
  cfg = config.uci-defaults;

  uciValueType = with types; nullOr (oneOf [ str bool number (listOf str) ]);

  # Sections names may only contain alphanum and "_".
  sectionNameType =
    let
      isValidSectionName = s: builtins.match "[[:alnum:]_]+" s != null;
    in
    types.addCheck types.str isValidSectionName;

  uciSectionOpts = {
    freeformType = with types; lazyAttrsOf uciValueType;

    options = {
      ".name" = mkOption {
        type = types.nullOr sectionNameType;
        default = null;
        description = "Name of this section (does not apply for anonymous sections).";
      };
      ".type" = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "Type of this section.";
      };
      ".anonymous" = mkOption {
        type = types.bool;
        default = false;
        description = "Is this an anonymous section.";
      };
      ".index" = mkOption {
        type = types.nullOr types.int;
        default = null;
        description = "Section index (in case of anonymous section).";
      };
      ".replaceBy" = mkOption {
        type = with types; either str (listOf str);
        default = [ ];
        apply = toList;
        description = "The option name by which to match to replace existing sections.";
      };
    };
  };

  uciSectionOpts' = { name, config, ... }: {
    config = {
      ".name" = name;
      #".type" = mkIf (config.".anonymous") (mkDefault name);
    };
  };

  sectionsType =
    let
      isNestedAttrs = x: isAttrs x && all isAttrs (attrValues x);

      elemType = types.submodule [ uciSectionOpts uciSectionOpts' ];
      elemType' = types.submodule [ uciSectionOpts ];
      listType = types.listOf elemType';
      attrsType = types.attrsOf elemType // { check = isNestedAttrs; };
      combinedType = types.oneOf [ listType attrsType elemType ];
      combinedType' = types.attrsOf combinedType;
    in
    combinedType' // {
      merge = loc: defs:
        let
          vals = combinedType'.merge loc defs;
          t1 = n: v:
            if isList v
            then builtins.map (t2 n) v
            #else if isNestedAttrs v then mapAttrsToList (t3 n) v
            else v;
          t2 = n: v: v // {
            ".anonymous" = true;
            ".type" = if v.".type" != null then v.".type" else n;
          };
          #t3 = t: _n: v: v // { ".type" = t; };
        in
        mapAttrs t1 vals;
    };

  # <config>.<section>.<option>=<string>
  uciConfigsType = types.submodule ({ ... }: {
    options = {
      preScript = mkOption {
        type = types.str;
        default = "";
      };

      settings = mkOption {
        type = types.attrsOf sectionsType;
        default = { };
        description = "UCI config values to set.";
      };

      update = mkOption {
        type = types.attrsOf types.raw;
        default = { };
        description = "UCI config values to update.";
      };

      postCommit = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "Commands run after committing the uci settings.";
      };
    };
  });
in
{
  options = {
    uci-defaults = mkOption {
      type = types.attrsOf uciConfigsType;
      default = { };
      description = "UCI defaults.";
    };
  };

  config = {
    file =
      let
        mk = name: value: {
          "etc/uci-defaults/${name}".text = ''
            #!/bin/sh
            ${value.preScript}
            ${config.lib.openwrt.uci.batch (removeAttrs value [ "preScript" ])}
          '';
        };
      in
      mkMerge (mapAttrsToList mk cfg);
  };
}
