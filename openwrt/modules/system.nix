{ lib, config, pkgs, ... }:

with lib;

let
  cfg = config.system;

  getPosixTZ = zonename:
    let
      tzinfo = "${pkgs.tzdata}/share/zoneinfo/${zonename}";
      tz = pkgs.runCommandLocal "TZ" { } "tail -n1 ${tzinfo} >$out";
    in
    fileContents tz;
in
{
  options = {
    system = {
      hostname = mkOption {
        type = types.singleLineStr;
        default = "OpenWrt";
        description = "Set the hostname";
      };

      description = mkOption {
        type = types.nullOr types.singleLineStr;
        default = null;
        description = "Host description";
      };

      notes = mkOption {
        type = types.nullOr types.lines;
        default = null;
        description = "System notes";
      };

      urandom_seed = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "Set urandom seed path";
      };

      timezone = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = ''
          POSIX.1 time zone string.
          <https://github.com/openwrt/luci/blob/master/modules/luci-base/ucode/zoneinfo.uc>
        '';
      };

      zonename = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = "IANA/Olson time zone string. Requires zoneinfo-* packages present.";
      };

      ntp = {
        enable_server = mkOption {
          type = types.nullOr types.bool;
          default = null;
        };

        use_dhcp = mkOption {
          type = types.nullOr types.bool;
          default = null;
          description = "Enable use of DHCP NTP servers (default: true)";
        };

        server = mkOption {
          type = types.nullOr (types.listOf types.str);
          default = null;
          description = ''
            List of NTP servers.
          '';
        };
      };
    };
  };

  config = {
    # Figure out timezone string from zonename in case zoneinfo is not
    # installed.
    system.timezone = mkIf (cfg.zonename != null) (mkDefault (getPosixTZ cfg.zonename));

    crontab.user.root = [{
      comment = "Reload kernel timezone to properly apply DST.";
      schedule = "0 0 * * *";
      command = "service system restart";
    }];

    uci-defaults."60-nix-system" = {
      settings = {
        system.system = builtins.removeAttrs cfg [ "ntp" ] // {
          ".anonymous" = true;
          ".index" = 0;
        };

        system.timeserver.ntp = cfg.ntp;
      };
    };
  };
}
