{ configuration
, pkgs
, lib ? pkgs.lib
, openwrt-image-builder-lib
, extraSpecialArgs ? { }
}:

let
  extendedLib = import ./lib/extended.nix lib;

  openwrtModules = import ./modules.nix {
    inherit pkgs;
    lib = extendedLib;
  };

  specialArgs' = {
    inherit openwrt-image-builder-lib;
    modulesPath = builtins.toString ./.;
  } // extraSpecialArgs;

  rawModule = extendedLib.evalModules {
    modules = [ configuration ] ++ openwrtModules;
    specialArgs = specialArgs';
  };

  withExtraAttrs = rawModule:
    {
      inherit (rawModule) options config;

      extendModules = args: withExtraAttrs (rawModule.extendModules args);
    };

in
withExtraAttrs rawModule
