{ config, pkgs, lib, openwrt-image-builder-lib, ... }:

with lib;

let
  cfg = config.image;
in
{
  options = {
    image.release = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "The OpenWRT release. Defaults to latest release from
      openwrt-image-builder if null.";
    };

    image.profile = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "The model.id in your board.json, e.g. \"tplink_tl-wdr4900-v1\"";
    };

    image.extraImageName = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "Extra string to insert to the image name.";
    };

    image.disabledServices = mkOption {
      type = types.listOf types.str;
      default = [];
      description = "List of services to disable in the image.";
    };

    image.packages = mkOption {
      type = types.listOf types.str;
      default = [];
      description = "Packages to include (or exclude) in the image. Prefix
      with - to exclude a package.";
    };

    image.files = mkOption {
      type = types.nullOr types.package;
      default = config.image-files;
      description = "Files to add to the image. Note: you should not be using
      this attribute directly but files.* etc. instead.";
    };

    image.build = mkOption {
      type = types.package;
      description = "The result sysupgrade image.";
    };

    image.postConfigure = mkOption {
      type = types.lines;
      default = "";
      description = "Commands to run in post-configure.";
    };

    image.extraMakeArgs = mkOption {
      type = with types; listOf str;
      default = [];
    };
  };

  config = {
    image.build =
      let
        profiles = openwrt-image-builder-lib.profiles { inherit pkgs; };

        profile = if !isNull cfg.profile then
          profiles.identifyProfile cfg.profile
          else throw "image.profile cannot not be null!";

        buildArgs = profile // {
          inherit (cfg)
            packages
            files
            disabledServices
            extraImageName
            extraMakeArgs
            postConfigure
            ;
        } //  optionalAttrs (!isNull cfg.release) { inherit (cfg) release; };
      in
      (openwrt-image-builder-lib.build buildArgs).overrideAttrs (old: {
        passthru = (old.passthru or {}) // {
          inherit (cfg) files;
          inherit config;
        };
      });
  };
}
