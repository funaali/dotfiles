{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.ip6neigh;

  setupScript = pkgs.fetchurl {
    url = "https://raw.githubusercontent.com/AndreBL/ip6neigh/master/ip6neigh-setup.sh";
    hash = "sha256-OSAfPbPyjXC5uJN8ERScYrr6PmzjM+nBBdght/sVv6o=";
  };

in {
  options.ip6neigh = {
    enable = mkEnableOption "ip6neigh";

    settings = mkOption {
      type = types.attrsOf types.anything;
      default = { };
    };
  };

  config = mkIf cfg.enable {
    image.packages = [ "ip-full" "curl" ];

    ip6neigh.settings.router_name = mkDefault config.system.hostname;

    dhcp.dnsmasq.addnhosts = [ "/tmp/hosts/ip6neigh" ];

    file."lib/ip6neigh-setup.sh" = {
      source = setupScript;
      mode = "0755";
    };

    uci-defaults."90-setup-ip6neigh.sh" = {
      preScript = ''
        /lib/ip6neigh-setup.sh install
      '';

      settings = {
        ip6neigh.ip6neigh.config = cfg.settings;
      };

      postCommit = ''
        service ip6neigh enable
        service ip6neigh start
      '';
    };
  };
}
