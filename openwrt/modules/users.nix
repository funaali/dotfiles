# root:$1$GJGhYUUj$VPpnsubkFO1iTJhIKEZ4Z1:19804:0:99999:7:::
# root:::0:99999:7:::
#
# set password (encrypted)

{ lib, config, ... }:

with lib;

let
  cfg = config.users;

in {
  options = {
    users = mkOption {
      type = types.attrsOf (types.submodule {
        options = {
          encryptedPassword = mkOption {
            type = types.nullOr types.str;
            default = null;
            description = "The shadow encrypted password to set for the user.";
          };
        };
      });
      default = { };
    };
  };

  config = mkIf (cfg != {}) {
    file."etc/uci-defaults/55-nix-users.sh".text = ''
      #!/bin/sh

      setEncryptedPassword() {
        local user=$1 encrypted=$2
        sed -i -e "s/^$user:[^:]*:[^:]*:/$user:$encrypted::/" /etc/shadow
      }

      # Set passwords of existing users
      ${concatStringsSep "\n" (mapAttrsToList (user: v:
        "setEncryptedPassword ${escapeShellArgs [user v.encryptedPassword]}")
        (filterAttrs (_: v: v.encryptedPassword != null) cfg))}
    '';
  };
}
