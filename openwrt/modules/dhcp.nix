{ lib, config, ... }:

with lib;

let
  cfg = config.dhcp;

  dhcpOpts = { name, ... }: {
    freeformType = types.attrsOf types.anything;

    options = {
      interface = mkOption { type = types.str; };
    };

    config = {
      interface = mkDefault name;
    };
  };

  hostOpts = { name, ... }: {
    freeformType = types.attrsOf types.anything;

    options = {
      name = mkOption { type = types.str; };
      hostid = mkOption { type = types.nullOr types.str; default = null; };
      ip = mkOption { type = types.nullOr types.str; default = null; };
      mac = mkOption { type = types.nullOr types.str; default = null; };
      duid = mkOption { type = types.nullOr types.str; default = null; };
      slaac = mkOption { type = types.nullOr types.bool; default = null; };
      dns = mkOption { type = types.nullOr types.bool; default = null; };
    };

    config = {
      name = mkDefault name;
      ".anonymous" = mkDefault true;
      ".replaceBy" = mkDefault "name";
    };
  };
in
  {
    options = {
      dhcp = {
        dnsmasq = mkOption {
          type = types.attrsOf types.anything;
          default = { };
        };

        dhcp = mkOption {
          type = with types; attrsOf (submodule [ dhcpOpts ]);
          default = { };
        };

        hosts = mkOption {
          type = with types; attrsOf (submodule [ hostOpts ]);
          default = { };
        };
      };
    };

    config = {
      uci-defaults."67-nix-dhcp" = {
        settings = {
          dhcp.dnsmasq = cfg.dnsmasq // {
            ".anonymous" = true;
            ".index" = 0;
          };
          dhcp.dhcp = cfg.dhcp;
          dhcp.host = attrValues cfg.hosts;
        };
      };
    };
  }
