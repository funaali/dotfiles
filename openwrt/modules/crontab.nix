{ config, lib, ... }:

with lib;

let
  cfg = config.crontab;

  # TODO validate
  scheduleType = types.singleLineStr;
  crontabOpts = {
    options = {
      comment = mkOption { type = types.lines; default = ""; };
      schedule = mkOption { type = scheduleType; };
      command = mkOption { type = types.singleLineStr; };
    };
  };
  crontabEntry = types.submodule [ crontabOpts ];
  crontabEntries = types.listOf crontabEntry;

in
{
  options.crontab = {
    enable = mkEnableOption "crontab" // { default = true; };

    user = mkOption {
      type = types.attrsOf crontabEntries;
      default = { };
      description = "User crontabs.";
    };
  };

  config =
    let
      renderCron = concatMapStringsSep "\n" ({ comment, schedule, command }:
        concatStringsSep "\n" (
          commentLines comment ++ [ "${schedule} ${command}" "" ]
        ));
      commentLines = s: builtins.map (x: "# " + x) (splitString "\n" s);

      userCrontabFile = u: v: {
        "etc/crontabs/${u}".text = renderCron v;
      };
    in
    mkIf cfg.enable {
      file = mkMerge (mapAttrsToList userCrontabFile cfg.user);
    };
}
