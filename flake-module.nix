{ self, lib, config, inputs, withSystem, pkgsFor, ... }:

let
  inherit (finalLib) mapAttrs mkOption types;

  finalLib = lib.extend
    (self: super:
      let mine = import ./lib inputs self super; in
      { inherit mine; } // mine);

  mylib = self.lib.mine;
  cfg = config.myConfigs;

  usersForHost = hostName:
    lib.concatMapAttrs
      (k: hmMod:
        let inherit (mylib.fromUserHostKey k) user host; in
        lib.optionalAttrs (!isNull user && host == hostName) { ${user} = hmMod; });

  # Get nixosConfiguration.
  nixosSystem' = mylib.nixosSystem {
    inherit pkgsFor;
    inherit (cfg.nixos) sharedModules specialArgs;
  };

  # Standalone home-manager configurations
  mkHMUserStandalone' = k: args:
    mylib.mkHMUserStandalone ({
      channelsWith = system: withSystem system ({ config, ... }: config.channels);
      inherit (cfg.hm) extraSpecialArgs sharedModules;
      username = (mylib.fromUserHostKey k).user;
    } // args);

  nixos = {
    modules = if isNull cfg.nixos.modulesDirectory then { } else mylib.loadPaths cfg.nixos.modulesDirectory;
    profiles = if isNull cfg.nixos.profilesDirectory then { } else mylib.loadPaths cfg.nixos.profilesDirectory;
    suites = import suites/nixos.nix { inherit (nixos) profiles; };
    configurations = if isNull cfg.nixos.configurationsDirectory then { } else mylib.loadPaths cfg.nixos.configurationsDirectory;
  };

  nixosModules = nixos.modules // {
    my-modules = { ... }: {
      imports = builtins.attrValues nixos.modules;
    };
    hm-users = { lib, config, ... }: {
      config.home-manager = lib.mkMerge [
        {
          inherit (cfg.hm) extraSpecialArgs sharedModules;
          users = lib.mkDefault (usersForHost config.networking.hostName hm.users);
        }
        {
          useGlobalPkgs = lib.mkDefault true;
          useUserPackages = lib.mkDefault true;
          sharedModules = [{ nix.registry = config.nix.registry; }];
        }
      ];
    };
  };

  hm = {
    modules = mylib.flattenPaths (mylib.loadPaths modules/home);
    profiles = mylib.loadPaths profiles/home;
    suites = import suites/home.nix { inherit (hm) profiles; };
    users = import users/default.nix;
    standaloneUsers = import users/standalone.nix;
  };

  hmModules = hm.modules // {
    my-modules = { imports = builtins.attrValues hm.modules; };
    default = { imports = cfg.hm.sharedModules ++ builtins.attrValues hm.modules; };
  };

in
{
  imports = [
    ./flake-modules/nixpkgs.nix
    ./flake-modules/layout.nix
    ./flake-modules/checks.nix
    ./openwrt/flake-module.nix
    ./shells/flake-module.nix
  ];

  options = {
    myConfigs = {
      nixos = {
        configurationsDirectory = mkOption {
          type = types.nullOr types.path;
          default = null;
          description = "Path to NixOS host definition modules.";
        };

        modulesDirectory = mkOption {
          type = types.nullOr types.path;
          default = null;
          description = "Path from which NixOS modules are loaded.";
        };

        profilesDirectory = mkOption {
          type = types.nullOr types.path;
          default = null;
          description = "Path from which NixOS profile modules are loaded.";
        };

        sharedModules = mkOption {
          type = types.listOf types.module;
          default = [ ];
          description = "NixOS modules applied to all NixOS configurations.";
        };

        specialArgs = mkOption {
          type = types.lazyAttrsOf types.raw;
          default = { };
          description = "Importable arguments to NixOS modules.";
        };
      };

      hm = {
        sharedModules = mkOption {
          type = types.listOf types.raw;
          default = [ ];
          description = "HM modules applied to all configurations.";
        };

        extraSpecialArgs = mkOption {
          type = types.lazyAttrsOf types.raw;
          default = { };
          description = "Importable arguments to HM modules.";
        };
      };
    };
  };

  config = {
    myConfigs = {
      nixos.specialArgs = { inherit (nixos) profiles suites; };
      hm.extraSpecialArgs = { inherit (hm) profiles suites; };
    };

    flake = {
      lib = finalLib;
      inherit nixosModules hmModules;
      nixosConfigurations = mapAttrs nixosSystem' nixos.configurations;
      homeConfigurations = mapAttrs mkHMUserStandalone' hm.standaloneUsers;
      hydraJobs = {
        nixos = mapAttrs (_: x: x.config.system.build.toplevel) self.nixosConfigurations;
        homeStandalone = mapAttrs (_: x: x.activationPackage) self.homeConfigurations;
      };
    };
  };
}
