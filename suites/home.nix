{ profiles }:

rec {
  base = [
    profiles.base
    profiles.git
    profiles.pass
    profiles.vim
  ] ++ builtins.attrValues profiles.shell;

  server = base;

  desktop = base ++ [
    profiles.graphical
    profiles.desktop
  ] ++ builtins.attrValues profiles.apps;

  workstation = desktop ++ [
    profiles.taskwarrior
    profiles.develop
  ];
}
