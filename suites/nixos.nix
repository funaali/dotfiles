{ profiles }:

rec {
  base = with profiles; [
    core
    users
  ];

  desktop = with profiles; base ++ [
    hardware
    udisks2
    pulseaudio
    pipewire
    graphical
  ];
}
