{ ... }:
{
  sim.email = "samuli.thomasson@cyansecurity.com";
  sim.gpgKeyId = "894E3E6796C6FA33D10481697E912C4B88C7D1D6";
  sim.repos = {
    password-store.uri = "git@gilab.cyansecurity.com:sthomasson/password-store.git";
  };
}
