{ config, pkgs, ... }:
{
  imports = [
    ./nix.nix
  ];

  config = {
    home.stateVersion = "24.11";

    home.packages = with pkgs; [
      python3
      qrencode-wifi-password
      unzip
      util-linux
      xdg-user-dirs # install the "xdg-user-dir" executable
      yq
    ];

    home.sessionPath = [
      "$HOME/scripts"
      "$HOME/.local/bin"
    ];

    # for openssl
    home.sessionVariables.RANDFILE = "\${RANDFILE-\${XDG_DATA_HOME:-$HOME/.local/share}/openssl/rnd}";

    home.file.".pam_environment".source = ./pam_environment;

    age = {
      secretsDir = "${config.home.homeDirectory}/.agenix/agenix";
      secretsMountPoint = "${config.home.homeDirectory}/.agenix/agenix.d";
      identityPaths = [ "${config.home.homeDirectory}/.ssh/id_agenix_ed25519" ];
    };

    programs.jq.enable = true;

    mailcap = {
      enable = true;
      entries."text/html" = [
        # Order matters; prefer to open via xdg-open when possible
        { command = "mv %s %s.html && xdg-open %s.html"; test = ''test -n "$DISPLAY"''; needsterminal = true; }
        { command = "${pkgs.w3m}/bin/w3m -I %{charset} -T text/html"; copiousoutput = true; }
      ];
    };

    xdg = {
      enable = true;
      mime.enable = true;
      mimeApps.enable = true;
      userDirs = {
        enable = true;
        createDirectories = true;
      };
    };
  };
}
