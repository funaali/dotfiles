{ lib, config, pkgs, ... }:

let
  nixpkgsConfigPath = ../../../nixpkgs-config.nix;
in
{
  config = {

    # Nixpkgs config
    nixpkgs.config = import nixpkgsConfigPath;

    xdg.configFile."nixpkgs/config.nix".source = lib.mkIf (!isNull config.nixpkgs.config) nixpkgsConfigPath;

    home.packages = [
      pkgs.nix-bash-completions
    ];

    nix = {
      package = lib.mkDefault pkgs.nixVersions.latest;
      settings = {
        #cores = 2;
        #max-jobs = "auto";
        builders-use-substitutes = true;
        extra-experimental-features = [
          "nix-command"
          "flakes"
          "recursive-nix"
          "ca-derivations"
        ];
        log-lines = 120;
        warn-dirty = false;
        allow-import-from-derivation = true;
      };
    };

    # Load nix profile (non-nixos)
    # TODO: something like: https://gist.github.com/colinxs/2898ecfb610ee613fef1329fe2c821d4
    programs.bash.profileExtra = lib.mkBefore ''
      if [ -z "''${NIX_PROFILES-}" ] || ! [[ " $NIX_PROFILES " =~ " $HOME/.nix-profile " ]]; then
        . "${config.nix.package}/etc/profile.d/nix.sh"
        # in case of single-user nix
        if test -r "$HOME/.nix-profile/etc/profile.d/nix.sh"; then . "$_"; fi
      fi
    '';
  };
}
