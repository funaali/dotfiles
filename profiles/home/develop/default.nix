# Development stuff.
{ lib, pkgs, config, ... }:

with lib;

let
  hp = pkgs.haskellPackages;

  pkgSets = with pkgs; {
    nix = [
      cachix
      nixfmt-rfc-style
      nixpkgs-fmt
      nix-linter
      nix-prefetch # Prefetcher
      nurl # Prefetcher
      rnix-lsp # Nix LSP
      nil # Nix LSP
    ];

    haskell = [
      hp.stack
      hp.ghc
      hp.ghcid
      hp.stylish-haskell
      hp.hlint
      hp.apply-refact # provides the "refactor" executable
      hp.hasktags
    ];

    devops = [ ansible-completion ];

    misc = [
      #git-sync # Included in newer git?
      pandoc
      shellcheck
      yq
      yj # Convert YAML/TOML/JSON/HCL
      parallel
      sqlite
      skopeo
      glab
    ];
  };
in
{
  imports = [
    ./ghci
  ];

  config = {
    sim.repos = {
      nixpkgs = { # symlinked to ~/nixpkgs for convenience
        path = "${config.home.homeDirectory}/_/github.com/NixOS/nixpkgs";
        uri = "https://github.com/NixOS/nixpkgs.git";
      };
    };

    nix.settings.keep-outputs = true;

    home.packages = concatLists (attrValues pkgSets);

    programs.direnv = {
      enable = true;
      enableBashIntegration = false; # custom integration
      config.global = {
        load_dotenv = true; # load .env in addition to .envrc
        strict_env = true; # load .envrc with "set -euo pipefail"
      };
      nix-direnv.enable = true;
    };

    home.sessionVariables = {
      # pylint: defaults to ~/.pylint.d
      PYLINTHOME = "\${XDG_DATA_HOME:-$HOME/.local/share}/pylint}";
    };

    xdg.configFile."stylish-haskell/config.yaml".source = ./stylish-haskell.config.yaml;

    home.file.".yamllint.yaml".source = ./yamllint.yaml;
  };
}
