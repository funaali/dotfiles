{ lib, pkgs, ... }:
with lib;

# GHCi global configuration.
# See also: haskeline: <https://github.com/judah/haskeline/wiki/UserPreferences>

# XXX: enable?
# GHC_COLORS=header=:message=1:warning=1;35:error=1;31:fatal=1;31:margin=1;34
# GHC_ENVIRONMENT=

# XXX enable? Control GHC output
# :set
#  -fprint-unicode-syntax
#  -fprint-expanded-synonyms
#  -fprint-explicit-foralls
#  -ferror-spans

let
  cfg = {
    # +m :: Enable multi-line commands in GHCi scripts: :{\n ..lines.. \n:}\n
    # +c :: Collect type/location info after loading modules
    set.options = [ "+m" "+c" ];

    # if 'source', sources `./.ghci' files. If `ignore` doesn't source `./.ghci`
    # automatically.
    # TODO: make a check if the local config is in a trusted directory and load
    # it only in that case.
    set.local-config = "ignore";

    # Enable most warnings for interpreted code
    interpreted.warnings = [
     "everything"
     "no-incomplete-uni-patterns"
     "no-missing-export-lists"
     "no-missing-import-lists"
     "no-missing-local-signatures"
     "no-name-shadowing"
     "no-partial-type-signatures"
     "no-type-defaults"
     "no-prepositive-qualified-module"
    ];

    # Default language extensions for interpreted code
    # TODO this can be cleaned up, lot is included in GHC2021, see :showi language
    interpreted.languageExts = [
      "NoImplicitPrelude"
      "NondecreasingIndentation"
      "ApplicativeDo"
      "Arrows"
      "BangPatterns"
      "BinaryLiterals"
      "BlockArguments"
      "ConstraintKinds"
      "DataKinds"
      "DefaultSignatures"
      "DeriveAnyClass"
      "DeriveDataTypeable"
      "DeriveGeneric"
      "DeriveLift"
      "DeriveTraversable"
      "DerivingStrategies"
      "DerivingVia"
      "EmptyCase"
      "ExistentialQuantification"
      "ExplicitNamespaces"
      "ExtendedDefaultRules"
      "FlexibleContexts"
      "FlexibleInstances"
      "ForeignFunctionInterface"
      "FunctionalDependencies"
      "GADTs"
      "GeneralizedNewtypeDeriving"
      "HexFloatLiterals"
      "ImplicitParams"
      "InstanceSigs"
      "LambdaCase"
      "MagicHash"
      "MonadComprehensions"
      "MultiParamTypeClasses"
      "MultiWayIf"
      "NamedFieldPuns"
      "NamedWildCards"
      "NegativeLiterals"
      "NumDecimals"
      "NumericUnderscores"
      "OverloadedLabels"
      "OverloadedLists"
      "PackageImports"
      "ParallelListComp"
      "PartialTypeSignatures"
      "PatternGuards"
      "PatternSynonyms"
      "PolyKinds"
      "PostfixOperators"
      "QuantifiedConstraints"
      "QuasiQuotes"
      "RankNTypes"
      "RecordWildCards"
      "RecursiveDo"
      "RoleAnnotations"
      "ScopedTypeVariables"
      "StandaloneDeriving"
      "TemplateHaskellQuotes"
      "TransformListComp"
      "TupleSections"
      "TypeApplications"
      "TypeFamilies"
      "TypeFamilyDependencies"
      "TypeOperators"
      "UnboxedSums"
      "UnboxedTuples"
      "UnicodeSyntax"
      "ViewPatterns"
    ];
  };

  ppsh = getExe pkgs.haskellPackages.pretty-show;
  HsColour = getExe pkgs.haskellPackages.hscolour;

  escapeStringGHCi = s: replaceStrings ["\n"] ["\\n"] (escape [ "\\" "\"" ] s);

  mkMacro = cmd: { expr ? null, text ? null }: let
    expr' =
      if !isNull expr then expr else
      if !isNull text then fromTexts (flatten (singleton text)) else
      throw "mkMacro: either expr or text must be set.";
    fromTexts = ts: ''\_ -> return "${concatMapStringsSep "\\n" escapeStringGHCi ts}"'';
    in ":def! ${cmd} ${expr'}";

  myMacros = {
    nopretty.text = ":seti -interactive-print System.IO.print";
    pretty.text = [
      ":unset +t"
      ":seti -interactive-print dotGHCI_pp"
      ":set +t"
    ];
  };
in
{
  config = {
    home.file.".ghci".text = ''
      -- vim:syntax=haskell:et:ts=4:cms=--\ %s:com=s1fl\:{-,mb\:-,ex\:-},\:--

      :set ${cfg.set.local-config or "source"}
      :set ${concatStringsSep " " cfg.set.options}

      -- Warning flags for interpreted code
      :seti ${concatMapStringsSep " " (f: "-W${f}") cfg.interpreted.warnings}

      -- Language extensions for interpreted code
      ${concatMapStringsSep "\n" (ext: ":seti -X${ext}") cfg.interpreted.languageExts}

      ${fileContents ./macros-and-stuff.hs}

      :{
      dotGHCI_pp x = Prelude.putStrLn
            Prelude.=<< catch' (rp "${HsColour}" [])
            Prelude.=<< catch' (rp "${ppsh}" []) (show x)
          where
            rp = System.Process.readProcess
            catch' f x = Control.Exception.catch (f x) (h x)
            h :: String -> Control.Exception.SomeException -> IO String
            h x _ = return x
      :}

      ${concatStringsSep "\n" (mapAttrsToList mkMacro myMacros)}
    '';
  };
}
