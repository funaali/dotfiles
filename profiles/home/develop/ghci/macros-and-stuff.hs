-- Unqualified imports from base modules. Exported as BASE to avoid clutter in the prompt.
import "base" Control.Applicative        as BASE (Applicative(..))
import "base" Control.Arrow              as BASE (Arrow(..))
import "base" Control.Category           as BASE
import "base" Control.Concurrent         as BASE (ThreadId, forkIO)
import "base" Control.Exception          as BASE (Exception(..), IOException)
import "base" Control.Monad              as BASE (Monad(return,(>>),(>>=)),MonadPlus(..),join,when)
import "base" Data.Bits                  as BASE (Bits(xor, (.&.), (.|.)))
import "base" Data.Bool                  as BASE
import "base" Data.Char                  as BASE (Char,isUpper)
import "base" Data.Coerce                as BASE (Coercible, coerce)
import "base" Data.Data                  as BASE (Data)
import "base" Data.Dynamic               as BASE (Dynamic, fromDynamic, toDyn)
import "base" Data.Either                as BASE
import "base" Data.Eq                    as BASE
import "base" Data.Foldable              as BASE
import "base" Data.Function              as BASE (const, flip, on, ($), (&))
import "base" Data.Functor               as BASE (Functor(..), void, ($>), (<$>), (<&>))
import "base" Data.Functor.Compose       as BASE (Compose(..))
import "base" Data.Functor.Const         as BASE (Const(..))
import "base" Data.Functor.Contravariant as BASE (Comparison(..), Contravariant(..), Equivalence(..), Op(..), Predicate(..))
import "base" Data.Int                   as BASE (Int, Int16, Int32, Int64, Int8)
import "base" Data.IORef                 as BASE
import "base" Data.Kind                  as BASE
import "base" Data.Maybe                 as BASE
import "base" Data.Monoid                as BASE
import "base" Data.Ord                   as BASE
import "base" Data.Proxy                 as BASE
import "base" Data.Ratio                 as BASE
import "base" Data.Semigroup             as BASE
import "base" Data.String                as BASE (IsString(fromString), String)
import "base" Data.Traversable           as BASE
import "base" Data.Tuple                 as BASE
import "base" Data.Type.Bool             as BASE (type (&&), If, Not, type (||))
import "base" Data.Type.Coercion         as BASE (Coercion(..), TestCoercion(..))
import "base" Data.Type.Equality         as BASE ((:~:)(Refl), (:~~:)(HRefl), type (==), TestEquality(..), type (~~))
import "base" Data.Typeable              as BASE (TypeRep, Typeable, cast, eqT, typeOf)
import "base" Data.Unique                as BASE
import "base" Data.Void                  as BASE (Void)
import "base" Data.Word                  as BASE (Word, Word16, Word32, Word64, Word8)
import "base" GHC.Enum                   as BASE (Bounded(..), Enum(..))
import "base" GHC.Err                    as BASE (error, undefined)
import "base" GHC.Float                  as BASE (Double, Float, Floating(..), RealFloat(..))
import "base" GHC.Num                    as BASE (Integer, Num(..), subtract)
import "base" GHC.Real                   as BASE (Fractional(..), Integral(..), Real(..), RealFrac(..), (^), (^^))
import "base" GHC.Stack                  as BASE (HasCallStack)
import "base" GHC.Exts                   as BASE (IsList(Item,fromList))
import "base" System.IO                  as BASE (IO)
import "base" Text.Printf                as BASE (printf)
import "base" Text.Read                  as BASE (Read, read, readEither, readMaybe)
import "base" Text.Show                  as BASE (Show(show))
-- END unqualified imports

-- Also import some qualified
import qualified "base" Data.List                    as L
import qualified "base" System.IO                    as IO
import qualified "base" Text.ParserCombinators.ReadP as ReadP
import qualified "base" System.Environment
-- END qualified imports

-- prompt and print functions                                      {{{1
-- λ
:{
:set prompt-function \modules _lnum ->
    let color fg x   = concat ["\ESC[", fg, "m\STX", x, "\ESC[m\STX"]
        colorPrompt  = color "38;5;3"
        colorModLast = color "38;5;12"
        colorModInit = color "38;5;11"
        colorMod m   = case L.span (/= '.') m of
            (a:_,d:bs) -> colorModInit (a:d:[]) <> colorMod bs
            _          -> colorModLast m
        ignored = ["BASE"]
        prettyModules = L.unwords [colorMod m | m <- L.nub modules, m `notElem` ignored]
    in return (prettyModules <> "\n" <> colorPrompt "λ" <> " ")
:}

-- Π
:set prompt-cont-function \_modules _lnum -> return "\ESC[1;35m\STXΠ\ESC[m\STX "

-- Σ

-- Macros                                                          {{{1

-- :k[i]                    same as :kind[!]
-- :rr                      reload GHCi configuration
-- :verbose                 control GHC verbosity
-- :silent                  control GHC verbosity
-- :env                     display environment variables and their values
-- :env <key>               display value of named environment variable
-- :env <key> <value>       set value of an environment variable
-- :pwd                     show current working directory
-- :open <path>|<uri>       open (using xdg-open)
-- :build                   run `stack build`
-- :ls [<args>]             run `ls`
-- :ghc-pkg <args>          run ghc-pkg
-- :describe <package>      describe a package
-- :find-package <pattern>  list matching packages
-- :find-module <pattern>   list matching modules with their packages
-- :haddock                 open documentation root
-- :haddock <module>        open module documentation
-- :haddock <package>       open package documentation
-- :hoogle[!] <query>       invoke hoogle. [!] rebuilds the database first

:def! k  \x -> return ("::kind "  <> x)
:def! k! \x -> return ("::kind! " <> x)

:def! rr \_ -> return "::script ~/.ghci"

:def! verbose \_ -> return ":set -v1 -fshow-loaded-modules"
:def! silent  \_ -> return ":set -v0 -fno-show-loaded-modules"

--- Command :hlint  (Integration with the hlint code style tool) {{{1
:{
:def! hlint \extra -> return $ L.unlines
        [":unset +t +s"
        ,":set -w"
        ,":redir hlintvar1 :show modules"
        ,":cmd return $ \":! hlint \" L.++ unwords (map (takeWhile (/= ',') . drop 2 . dropWhile (/= '(')) $ lines hlintvar1) ++ \" \" ++ " L.++ show extra
        ,":set +t +s -Wall"
        ]
:}

-- Command :redir <var> <cmd> {{{
-- <http://www.cs.kent.ac.uk/people/staff/cr3/toolbox/haskell/dot-squashed.ghci641>
:{
:def! redir \varcmd -> return $
        case L.break Data.Char.isSpace varcmd of
            (var,_:cmd) -> L.unlines
                [":set -fno-print-bind-result"
                ,"tmp <- System.Directory.getTemporaryDirectory"
                ,"(f,h) <- System.IO.openTempFile tmp \"ghci\""
                ,"sto <- GHC.IO.Handle.hDuplicate System.IO.stdout"
                ,"GHC.IO.Handle.hDuplicateTo h System.IO.stdout"
                ,"System.IO.hClose h"
                ,cmd
                ,"GHC.IO.Handle.hDuplicateTo sto System.IO.stdout"
                ,"let readFileNow f = readFile f >>= \\t->Data.List.length t `seq` return t"
                ,var L.++" <- readFileNow f"
                ,"System.Directory.removeFile f"
                ]
            _ -> "System.IO.putStrLn \"usage: :redir <var> <cmd>\""
:}
-- }}}

:{
:def! env \case
    "" -> System.Environment.getEnvironment >>= mapM_ (\(k,v) -> IO.putStrLn (k <> "=" <> v)) >> pure ""
    s | (k,_:v) <- L.span (/= ' ') s -> System.Environment.setEnv k v $> ""
      | otherwise                    -> System.Environment.lookupEnv s >>= IO.putStrLn . maybe "Not set!" (printf "%s=%s" s) >> pure ""

:}

:def! pwd   \_ -> return (":! dirs +0") -- uses tilde for home directory unlike the pwd command
:def! open  \x -> return (printf ":! xdg-open '%s'" x)
:def! build \x -> return (printf ":! stack build '%s'" <> x)
:def! ls    \x -> return (printf ":! ls '%s'" <> x)

:def! ghc-pkg  \s -> return (":! ghc-pkg " <> s)
:def! describe \s -> return (":! ghc-pkg describe " <> s)
:def! find-package \s -> return (":! " <> concat [printf "ghc-pkg --simple-output list '%s'|tr ' ' '\\n';" p | p <- L.words (if null s then "*" else s)])

:{
:def! find-module \m ->
    let listModules :: String -> String -> String
        listModules s s' = printf "ghc-pkg --simple-output field %s exposed-modules" s <> " | while read -r -a ms; do for m in \"${ms[@]}\"; do case $m in " <> s' <> " ) printf '  %s\\n' $m ;; esac;done;done"
    in return (":! " <> concat [printf "ghc-pkg --simple-output find-module --ignore-case '%s'" x <> " | while read -r -a pkgs; do for pkg in \"${pkgs[@]}\";do printf '%s:\\n' \"$pkg\";" <> listModules "$pkg" x <> ";done;done;" | x <- L.words m])
:}

:{
:def! haddock \s -> case s of
    ""              -> return $ printf ":open $(stack path --local-doc-root)/index.html"
    c:_ | isUpper c -> return $ printf ":open $(ghc-pkg --simple-output field \"$(ghc-pkg --simple-output find-module %s)\" haddock-html)/%s.html" s [if c' == '.' then '-' else c|c'<-s]
        | otherwise -> return $ printf ":open $(ghc-pkg --simple-output field '%s' haddock-html)/index.html" s
:}

:def! hoogle  \s -> return (":! stack hoogle -- '" <> s <> "' | vimcat -f haskell")
:def! hoogle! \s -> return (":! stack hoogle --rebuild; stack hoogle -- '" <> s <> "' | vimcat -f haskell")
