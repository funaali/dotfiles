{ config, lib, pkgs, ... }:

with lib;

{
  config = {
    home.packages = [ pkgs.pulsemixer ];
    xdg.configFile."pulsemixer.cfg".source = ./pulsemixer.cfg;
  };
}
