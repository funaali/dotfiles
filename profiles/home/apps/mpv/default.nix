{ lib, pkgs, ... }:

# TODO: cleanup scripts installation & configuration:
#   - https://github.com/4e6/mpv-reload
#   - https://gist.github.com/garoto/e0eb539b210ee077c980e01fb2daef4a
#   - https://github.com/cvzi/mpv-youtube-download
#
# Links:
# - https://github.com/Argon-/mpv-config/blob/master/mpv.conf
# - https://github.com/mpv-player/mpv/wiki/User-Scripts

with lib;

let
  mpvHistory = pkgs.stdenvNoCC.mkDerivation {
    name = "mpv-history";
    src = ./.;
    dontBuild = true;
    installPhase = ''
      install -D -t $out/share/mpv/scripts ./mpvhistory.lua
    '';
    passthru.scriptName = "mpvhistory.lua";
  };
in

{
  programs.mpv = {
    enable = true;

    scripts = [ mpvHistory ] ++ (with pkgs.mpvScripts; [
      autodeint # ctrl-d
      thumbnail
      mpv-playlistmanager
    ]);

    config = {
      vo = "gpu,drm,tct,caca,null";
      ao = "pulse,alsa,null";
      alang = "jpn,en,eng";
      slang = "fi,fin,en,eng";
      volume-max = 150;
      sub-scale-by-window = false;
      audio-file-auto = "exact";
      rtsp-transport = "lavf";
      cookies = true;
      x11-netwm = true; # XMonad workaround
      demuxer-readahead-secs = 3; # default is 1.0
      write-filename-in-watch-later-config = true;
      force-seekable = true;
      use-filedir-conf = true; # Enable loading of "mpv.conf" or "<filename>.conf" from the directory of the file
      # Cache
      cache = true;
      demuxer-max-bytes = "400MiB";
      demuxer-max-back-bytes = "200MiB";
      # Screenshots
      screenshot-template = "%X{.}/mpv-shots/%F [%P] (%02n)";
      screenshot-format = "png";
      screenshot-png-compression = 8;
      # Subtitles
      demuxer-mkv-subtitle-preroll = true;
      demuxer-mkv-subtitle-preroll-secs = 2;
      sub-auto = "fuzzy";
      # Video
      opengl-early-flush = false;
      opengl-pbo = true; # "yes" is currently bugged: https://github.com/mpv-player/mpv/issues/4988
    };

    profiles = {
      audio-hdmi = {
        audio-client-name = "mpv-hdmi";
        audio-stream-silence = true;
        audio-wait-open = 2;
      };

      high-quality = {
        profile-desc = "Higher image quality, resource utilization and latency";
        scale = "ewa_lanczossharp";
        cscale = "ewa_lanczossharp";
        dscale = "mitchell";
        tscale = "bicubic";
        dither-depth = "auto";
        correct-downscaling = true;
        linear-downscaling = true;
        sigmoid-upscaling = true;
        deband = true;
      };

      low-quality = {
        # profile-desc=cond:is_low(get('width', 0), get('height', 0), get('estimated-vf-fps', 0))
        scale = "bilinear";
        cscale = "bilinear";
        dscale = "bilinear";
        dither-depth = false;
        correct-downscaling = false;
        sigmoid-upscaling = false;
        deband = false;
      };

      preview = {
        profile-desc = "Lower quality and resource usage";
        #profile-append = "low-latency";
        volume = 50;
        autofit-larger = "67%x67%";
        panscan = 1.0;
        hls-bitrate = "min";
        ytdl-format = "worst"; # see `youtube-dl --list-formats <url>' for other formats (site-specific)
      };

      gpu-custom = {
        profile-desc = "Custom GPU-accelerated settings.";
        volume = 50;
        autofit-larger = "67%x67%";
        panscan = 1.0;
        hwdec = "auto";
        gapless-audio = "no";
        demuxer-readahead-secs = "2";
        cache = "yes";
        cache-secs = "60";
        cache-pause-wait = "2";
        cache-pause-initial = "yes";
        network-timeout = "5";
        osd-on-seek = "msg";
        osd-duration = "500";
        osd-scale = "0.8";
        osd-scale-by-window = "no";
        #msg-level = "vaapi=info";
      };

      movie = {
        profile-desc = "[hq] + fullscreen + save play position on quit";
        profile-append = "high";
        fullscreen = true;
        save-position-on-quit = true; # to ~/.config/mpv/watch_later (as if shift-q)
      };

      # Note: --input-ipc-client=fd://<N> can probably be used for systemd socket activiation?
      #
      # jq -nc '{command:["get_property","filename"]}' | socat - ~/.S.mpv-input
      remotemode = {
        idle = true;
        input-ipc-server = "~/.S.mpv-input";
        keep-open = true;
      };

      network = {
        profile-cond = "demuxer_via_network";
        # profile-append = "low-latency";
        vd-lavc-threads = 1;
        video-sync = "audio";
        stream-buffer-size = "32k";
      };

      # TODO
      #[4K-lavc-threads]
      #profile-desc=cond:get('width', -math.huge) >= 3840
      #vd-lavc-threads=32
      #
      #[4K-lavc-threads-inverted]
      #profile-desc=cond:get('width', math.huge) < 3840
      #vd-lavc-threads=0
    };

    bindings = { };
  };

  xdg.mimeApps.defaultApplications = {
    "video/mpeg" = [ "mpv.desktop" ];
  };
}
