{ config, lib, pkgs, ... }:

with lib;
let
  mutt_oauth_pass_path = "api/GCP/oauth/mutt-gmail";

  settings = {
    # General
    help = null;
    beep_new = null;
    imap_passive = null;
    mark_old = null;
    imap_idle = "yes";
    confirmappend = "no";
    pipe_decode = "yes";
    prompt_after = "no";
    wait_key = "no";
    timeout = "10";
    fast_reply = "yes";
    crypt_autosign = "yes";
    imap_keepalive = "300";
    imap_check_subscribed = "yes";
    mail_check = "60";
    tmpdir = "$XDG_RUNTIME_DIR/neomutt";
    strict_threads = "yes";
    sort = "threads";
    sort_browser = "reverse-date";
    sort_aux = "last-date-received";
    pager_stop = "yes";
    pager_index_lines = "5";
    browser_abbreviate_mailboxes = "no";
    send_charset = "'us-ascii:utf-8'";

    # http://www.mutt.org/doc/manual/#oauth
    # https://github.com/google/gmail-oauth2-tools/raw/master/python/oauth2.py
    # oauth2.py ... --generate_oauth2_token to create refresh_token

    my_oauth2_py = ''"${
    pkgs.writeScript "oauth2.py" (builtins.readFile ./oauth2.py)
  } --client_id '$my_mutt_client_id' --client_secret '$my_mutt_client_secret'"'';

    my_mutt_client_id = ''"`pass ${mutt_oauth_pass_path} | sed -n '/^client_id:/s/\\S*\\s*//p'`"'';
    my_mutt_client_secret = ''"`pass ${mutt_oauth_pass_path} | sed -n '/^client_secret:/s/\\S*\\s*//p'`"'';

    # Mimetype query
    mime_type_query_command = "'xdg-mime query filetype %s'";
    mime_type_query_first = "yes";

    # sidebar https://neomutt.org/feature/sidebar
    sidebar_visible = "yes";
    sidebar_folder_indent = "yes";
    sidebar_format = "'%D%?F?[%F]?%* %?N?%N/?%S'";
    sidebar_indent_string = " ";
    sidebar_next_new_wrap = "yes";
    sidebar_non_empty_mailbox_only = "no";
    sidebar_on_right = "yes";
    sidebar_short_path = "yes"; # Drop the [Gmail]/ prefix in listing
    sidebar_delim_chars = "'/'";
    sidebar_sort_method = "path";
    #sidebar_component_depth        = "0";

    # PGP (GPG)
    pgp_default_key = config.sim.gpgKeyId;
    pgp_use_gpg_agent = "yes";
    pgp_check_gpg_decrypt_status_fd = "yes";

    # PGP commands (/usr/share/doc/mutt/samples/gpg.rc)
    pgp_decode_command = "'gpg --no-verbose -q --batch --status-fd=2 %?p?--passphrase-fd 0? -o - %f'";
    pgp_verify_command = "'gpg --no-verbose -q --batch --status-fd=2 -o - --verify %s %f'";
    pgp_decrypt_command = "'gpg --no-verbose -q --batch --status-fd=2 %?p?--passphrase-fd 0? -o - %f'";
    pgp_sign_command = "'gpg --no-verbose -q --batch -o - %?p?--passphrase-fd 0? -a -b --textmode %?a?-u %a? %f'";
    pgp_clearsign_command = "'gpg --no-verbose -q --batch -o - %?p?--passphrase-fd 0? -a --textmode --clearsign %?a?-u %a? %f'";
    pgp_encrypt_only_command = "'pgpewrap gpg --no-verbose -q --batch -o - -e --textmode -a --always-trust -- -r %r -- %f'";
    pgp_encrypt_sign_command = "'pgpewrap gpg --no-verbose -q --batch %?p?--passphrase-fd 0? --textmode -o - -e -s %?a?-u %a? -a --always-trust -- -r %r -- %f'";
    pgp_import_command = "'gpg --no-verbose --import %f'";
    pgp_export_command = "'gpg --no-verbose --export -a %r'";
    pgp_verify_key_command = "'gpg --verbose --batch --fingerprint --check-sigs %r'";
    pgp_list_pubring_command = "'gpg --no-verbose -q --batch --with-colons --with-fingerprint --with-fingerprint --list-keys %r'";
    pgp_list_secring_command = "'gpg --no-verbose -q --batch --with-colons --with-fingerprint --with-fingerprint --list-secret-keys %r'";
    pgp_good_sign = ''"^\\[GNUPG:\\] GOODSIG"'';

    # Contact management
    reverse_alias = "yes";
    display_filter = ''"sh -c '{ tee /dev/stderr | { lbdb-fetchaddr -a 2>/dev/null || cat >/dev/null; }; } 2>&1'"'';
    query_command = "'lbdbq %s 2>/dev/null'";
    #query_command = "'khard email --parsable %s 2>/dev/null'";
    query_format = "'%4c %t %-25.40n %a %> %?e?(%e)?'";

    ts_enabled = "yes";
    #ts_status_format = "";

    # Formats
    markers = "no"; # don't put '+' at the beginning of wrapped lines
    alias_format = "'%4n %2f %t %-10a %r'";
    status_format = "'[%r] %f [Msgs:%?M?%M/?%m%?n? New:%n?%?o? Old:%o?%?d? Del:%d?%?F? Flag:%F?%?t? Tag:%t?%?p? Post:%p?%?b? Inc:%b?%?l? %l]--?%?V?[Limit:%V]?%>-(%s/%S)-(%h %v)-(%P)---'";
    status_chars = "'-*%A'"; # unchanged/changed/read-only/attach-message
    pager_format = "'[%Z] %C/%m: %-20.20n   %s%*  -- (%P)'";
    index_format = "'%4C %Z %{%b %d} %-15.15L %s %> (%e/%-2E %?l?%4l&%4c?)'";

    new_mail_command =
      let
        title = "NeoMutt [%n NEW]";
        body = "Mailbox: %D Unread: %u New mail in: %b mailboxes.\\n%v (Host %h)";
        script = pkgs.writeShellScript "neomutt-notify" "${pkgs.dunst}/bin/dunstify ${escapeShellArgs [
            "-a" "NeoMutt"
            "-t" "${toString (15 * 1000)}"
            "-i" "${config.programs.neomutt.package}/share/doc/neomutt/logo/neomutt.svg"
            "-h" "string:category:email.arrived"
            "-h" "string:synchronous:neomutt"
          ]} \"$@\"";
      in
      "\"${script} ${escapeShellArgs [title body]}\"";
  };

in
{
  config = {
    programs.neomutt = {
      enable = true;

      inherit settings;

      binds = [
        # unbind defaults
        { map = [ "pager" ]; key = "-"; action = null; }
        { map = [ "index" "pager" ]; key = "V"; action = null; }

        # misc.
        { map = [ "index" ]; key = "<Space>"; action = "tag-entry"; }
        { map = [ "index" "pager" ]; key = "<backtab>"; action = "previous-new-then-unread"; }
        { map = [ "editor" ]; key = "<Tab>"; action = "complete-query"; }
        { map = [ "editor" ]; key = "^T"; action = "complete"; }

        # sidebar
        { map = [ "index" "pager" ]; key = "\\Ck"; action = "sidebar-prev"; }
        { map = [ "index" "pager" ]; key = "\\Cj"; action = "sidebar-next"; }
        { map = [ "index" "pager" ]; key = "\\Cl"; action = "sidebar-open"; }
        { map = [ "index" "pager" ]; key = "\\Ct"; action = "sidebar-toggle-visible"; }
      ];

      macros = [
        {
          description = "Mark all messages read";
          map = [ "index" ];
          key = "M";
          action = "T.*\\n;WN";
        }
        {
          description = "Mark all messages read";
          map = [ "index" ];
          key = "M";
          action = "T.*\\n;WN";
        }
        {
          description = "Add sender address to addressbook";
          map = [ "index" "pager" ];
          key = "A";
          action = "<pipe-message>khard add-email<return>";
        }
        {
          description = "Quick view html with mailcap";
          map = [ "index" "pager" ];
          key = "V";
          action = "<view-attachments><search>html<enter><view-mailcap><exit>";
        }
        {
          description = "Change folder";
          map = [ "index" ];
          key = "c";
          action = "<change-folder>?<change-dir><home>^K=<enter>";
        }
        {
          description = "Convert from markdown to text/html";
          map = [ "compose" ];
          key = "\\e5";
          action = "F pandoc -s -f markdown -t html \\nytext/html; charset=us-ascii\\n";
        }
      ];

      extraConfig = ''
        # Colors
        source ${./colors-solarized-dark-16.neomuttrc}

        # Hooks
        startup-hook "source ${./hook.startup.neomuttrc}"
        charset-hook ^us-ascii$ iso-8859-12

        # Misc.
        mime_lookup application/octet-stream
        auto_view text/html application/ics text/calendar
      '';
    };
  };
}
