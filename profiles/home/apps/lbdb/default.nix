{ lib, ... }:
{
  config = {
    programs.lbdb = {
      enable = lib.mkDefault true;
      methods = {
        inmail.db = "$HOME/.lbdb/m_inmail.utf-8";
        abook.files = "$HOME/.abook/addressbook";
        muttalias.files = "$HOME/.config/neomutt/alias_local.rc";
        getent = { };
        gpg = { };
        khard = { };
        #vcf.files = "...";
      };
    };
  };
}
