{ lib, ... }:
with lib;
{
  config = {
    programs.abook = {
      enable = true;
      extraConfig =
        # Manual: <lnk:man:abookrc.5> <lnk:man:abook.1>
        #
        # Standard fields:
        #    nick, name, email,
        #    address, address2, city, state, zip, country,
        #    phone, workphone, fax, mobile,
        #    nick, url, notes, anniversary
        #
        # Colors:
        #   default, black, red, green, yellow, blue, magenta, cyan, white
        let
          field.birthday = [ "Birthday" "date" ];
          field.mobile2 = "Mobile2";
          field.__name = [ "Name" "string" ];

          view.CONTACT = [ "name" "nick" "email" "url" "birthday" "notes" ];
          view.ADDRESS = [ "address" "address2" "city" "state" "zip" "country" ];
          view.PHONE = [ "mobile" "mobile2" "phone" "workphone" "fax" ];

          set.autosave = "false";
          set.preserve_fields = "all"; # preserve also non-standard fields in the database file
          set.index_format = "{__name:20|nick|name} {email:32} {mobile:18} {phone:18}";
          set.www_command = "xdg-open"; # how to open contact's URL
          set.use_colors = "true";

          # foreground
          color.header_fg = "default";
          color.footer_fg = "default";
          color.list_even_fg = "default";
          color.list_odd_fg = "default";
          color.list_header_fg = "white";
          color.list_highlight_fg = "cyan";
          color.tab_border_fg = "default";
          color.tab_label_fg = "white";
          color.field_name_fg = "white";
          color.field_value_fg = "cyan";
          # background
          color.header_bg = "default";
          color.footer_bg = "default";
          color.list_even_bg = "default";
          color.list_odd_bg = "default";
          color.list_header_bg = "default";
          color.list_highlight_bg = "default";
          color.tab_border_bg = "default";
          color.tab_label_bg = "default";
          color.field_name_bg = "default";
          color.field_value_bg = "default";
        in
        let
          mkValue = x: concatStringsSep ", " (map (s: if isNull (builtins.match "[a-zA-Z0-9]*" s) then "\"${s}\"" else s) (toList x));
          mkSection = title: method: values: ''
            # ${title}
            ${concatStringsSep "\n" (mapAttrsToList (k: v: "${method k} = ${mkValue v}") values)}
          '';
        in
        concatStringsSep "\n" [
          (mkSection "Fields" (k: "field ${k}") field)
          (mkSection "Views" (k: "view ${k}") view)
          (mkSection "Options" (k: "set ${k}") set)
          (mkSection "Colors" (k: "set color_${k}") color)
        ];
    };
  };
}
