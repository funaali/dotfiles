{ config, ... }:
{
  config = {
    accounts.contact.accounts.localDAV = {
      khard.enable = true;
      local.path = "${config.xdg.dataHome}/dav/contacts/addressbook/";
    };

    programs.khard = {
      enable = true;

      settings = {
        general = {
          debug = false;
          editor = "vim";
          merge_editor = "vimdiff";
        };
        "contact table" = {
          display = "first_name";
          group_by_addressbook = false;
          reverse = false;
          show_nicknames = false;
          show_uids = false;
          sort = "last_name";
          localize_dates = true;
          preferred_phone_number_type = "pref, cell, home";
          preferred_email_address_type = "pref, work, home";
        };
        vcard = {
          preferred_version = "3.0";
          search_in_source_files = false;
          skip_unparseable = false;
        };
      };
    };
  };
}
