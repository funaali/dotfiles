{ pkgs, ... }:
{
  programs.ncmpcpp = {
    enable = true;
    package = pkgs.ncmpcpp.override { visualizerSupport = true; };
    settings = {
      ncmpcpp_directory = "~/.cache/ncmpcpp";
      #mpd_music_dir = "~/Music";
      mpd_crossfade_time = 5;
      lyrics_directory = "~/Music/lyrics";
      # music visualizer
      visualizer_data_source = "~/.cache/mpd/pcm.fifo";
      visualizer_output_name = "MPD Visualizer Feed";
      visualizer_in_stereo = false;
      # formats
      alternative_header_first_line_format = "$b$7$aqqu$/a$9 $b$2{%t}|{%f}$9$b $7$atqq$/a$9$/b";
      alternative_header_second_line_format = "{{$4$b%a$/b$9}{ - $5%b$9}{ ($4%y$9)}}|{%D}";
      now_playing_prefix = "$2";
      now_playing_suffix = "$9";
      song_columns_list_format = "(20)[cyan]{a} (6f)[green]{NE} (50)[]{t|f:Title} (20)[blue]{b} (7f)[magenta]{l}";
      # various settings
      playlist_show_mpd_host = true;
      playlist_show_remaining_time = true;
      playlist_shorten_total_times = true;
      playlist_separate_albums = true;
      playlist_display_mode = "columns";
      browser_display_mode = "columns";
      search_engine_display_mode = "columns";
      playlist_editor_display_mode = "columns";
      centered_cursor = true;
      progressbar_look = "┅╉┄";
      user_interface = "alternative";
      media_library_primary_tag = "album_artist";
      default_find_mode = "normal";
      follow_now_playing_lyrics = true;
      fetch_lyrics_for_current_song_in_background = true;
      screen_switcher_mode = "previous";
      startup_screen = "lyrics";
      startup_slave_screen = "playlist";
      startup_slave_screen_focus = true;
      locked_screen_width_part = 30;
      ask_for_locked_screen_width_part = false;
      ask_before_clearing_playlists = true;
      clock_display_seconds = true;
      display_bitrate = true;
      display_remaining_time = true;
      ignore_leading_the = true;
      ignore_diacritics = true;
      mouse_support = false;
      external_editor = "vim";
      # color definitions
      # one of: default, black, red, green, yellow, blue, magenta, cyan, white
      empty_tag_color = "magenta";
      progressbar_color = "cyan";
      progressbar_elapsed_color = "default";
      state_flags_color = "green";
      main_window_color = "default";
      window_border_color = "black";
    };
    bindings = import ./bindings.nix;
  };
}
