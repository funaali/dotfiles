# General rules                                                     {{{1
##
## 1) Because each action has runtime checks whether it's
##    ok to run it, a few actions can be bound to one key.
##    Actions will be bound in order given in configuration
##    file. When a key is pressed, first action in order
##    will test itself whether it's possible to run it. If
##    test succeeds, action is executed and other actions
##    bound to this key are ignored. If it doesn't, next
##    action in order tests itself etc.
##
## 2) It's possible to bind more that one action at once
##    to a key. It can be done using the following syntax:
##
##    def_key "key"
##      action1
##      action2
##      ...
##
##    This creates a chain of actions. When such chain is
##    executed, each action in chain is run until the end of
##    chain is reached or one of its actions fails to execute
##    due to its requirements not being met. If multiple actions
##    and/or chains are bound to the same key, they will be
##    consecutively run until one of them gets fully executed.
##
## 3) When ncmpcpp starts, bindings configuration file is
##    parsed and then ncmpcpp provides "missing pieces"
##    of default keybindings. If you want to disable some
##    bindings, there is a special action called 'dummy'
##    for that purpose. Eg. if you want to disable ability
##    to crop playlists, you need to put the following
##    into configuration file:
##
##    def_key "C"
##      dummy
##
##    After that ncmpcpp will not bind any default action
##    to this key.
##
## 4) To let you write simple macros, the following special
##    actions are provided:
##
##    - push_character "character" - pushes given special
##      character into input queue, so it will be immediately
##      picked by ncmpcpp upon next call to readKey function.
##      Accepted values: mouse, up, down, page_up, page_down,
##      home, end, space, enter, insert, delete, left, right,
##      tab, ctrl-a, ctrl-b, ..., ctrl-z, ctrl-[, ctrl-\\,
##      ctrl-], ctrl-^, ctrl-_, f1, f2, ..., f12, backspace.
##      In addition, most of these names can be prefixed with
##      alt-/ctrl-/shift- to be recognized with the appropriate
##      modifier key(s).
##
##    - push_characters "string" - pushes given string into
##      input queue.
##
##    - require_runnable "action" - checks whether given action
##      is runnable and fails if it isn't. This is especially
##      useful when mixed with previous two functions. Consider
##      the following macro definition:
##
##      def_key "key"
##        push_characters "custom_filter"
##        apply_filter
##
##      If apply_filter can't be currently run, we end up with
##      sequence of characters in input queue which will be
##      treated just as we typed them. This may lead to unexpected
##      results (in this case 'c' will most likely clear current
##      playlist, 'u' will trigger database update, 's' will stop
##      playback etc.). To prevent such thing from happening, we
##      need to change above definition to this one:
##
##      def_key "key"
##        require_runnable "apply_filter"
##        push_characters "custom_filter"
##        apply_filter
##
##      Here, first we test whether apply_filter can be actually run
##      before we stuff characters into input queue, so if condition
##      is not met, whole chain is aborted and we're fine.
##
##    - require_screen "screen" - checks whether given screen is
##      currently active. accepted values: browser, clock, help,
##      media_library, outputs, playlist, playlist_editor,
##      search_engine, tag_editor, visualizer, last_fm, lyrics,
##      selected_items_adder, server_info, song_info,
##      sort_playlist_dialog, tiny_tag_editor.
##
##    - run_external_command "command" - runs given command using
##      system() function.
##
## 5) In addition to binding to a key, you can also bind actions
##    or chains of actions to a command. If it comes to commands,
##    syntax is very similar to defining keys. Here goes example
##    definition of a command:
##
##      def_command "quit" [deferred]
##        stop
##        quit
##
##    If you execute the above command (which can be done by
##    invoking action execute_command, typing 'quit' and pressing
##    enter), ncmpcpp will stop the player and then quit. Note the
##    presence of word 'deferred' enclosed in square brackets. It
##    tells ncmpcpp to wait for confirmation (ie. pressing enter)
##    after you typed quit. Instead of 'deferred', 'immediate'
##    could be used. Then ncmpcpp will not wait for confirmation
##    (enter) and will execute the command the moment it sees it.
##
##    Note: while command chains are executed, internal environment
##    update (which includes current window refresh and mpd status
##    update) is not performed for performance reasons. However, it
##    may be desirable to do so in some situration. Therefore it's
##    possible to invoke by hand by performing 'update enviroment'
##    action.
##
## Note: There is a difference between:
##
##         def_key "key"
##           action1
##
##         def_key "key"
##           action2
##
##       and
##
##         def_key "key"
##           action1
##           action2
##
##      First one binds two single actions to the same key whilst
##      second one defines a chain of actions. The behavior of
##      these two is different and is described in (1) and (2).
##
## Note: Function def_key accepts non-ascii characters.
##
# Unbound actions / dummy bindings                                  {{{1
##
## The following actions are not bound to any key/command:
##
##  - set_volume
##  - delete_browser_items
##  - edit_directory_name
##  - show_lyrics
##  - toggle_lyrics_fetcher
##  - toggle_fetching_lyrics_in_background
##  - mouse_event
##  - refetch_lyrics
##  - toggle_display_mode
##  - fetch_lyrics_in_background
##  - toggle_library_tag_type
##  - toggle_mouse
[
  # undefine
  { key = "mouse"; command = "dummy"; }
  { key = "t"; command = "dummy"; }
  { key = "T"; command = "dummy"; }
  { key = "M"; command = "dummy"; }
  { key = "F"; command = "dummy"; }
  { key = "insert"; command = "dummy"; }
  { key = "."; command = "dummy"; }
  { key = ","; command = "dummy"; }
  { key = "|"; command = "dummy"; }
  { key = "="; command = "dummy"; }

  # Custom commands                                                 {{{1

  #def_command "append_selected_to_playlist" [immediate]
  #  add_selected_items
  #  require_screen "selected_items_adder"
  #  run_action

  # key: y                                                          {{{1
  # { key = "y"; command = "save_tag_changes"; }
  # { key = "y"; command = "start_searching"; }

  # key: enter                                                      {{{1
  { key = "enter"; command = [ ''require_screen "playlist"'' "play_item" ]; }
  { key = "enter"; command = [ ''require_screen "browser"'' "enter_directory" ]; }
  { key = "enter"; command = "run_action"; }
  { key = "enter"; command = "add_item_to_playlist"; }
  { key = "enter"; command = "toggle_output"; }

  # key: space                                                      {{{1
  { key = "space"; command = "add_item_to_playlist"; }
  { key = "space"; command = "toggle_lyrics_update_on_song_change"; }
  { key = "space"; command = "toggle_visualization_type"; }

  # key: tab (toggle screen)                                        {{{1
  { key = "tab"; command = "next_screen"; }
  { key = "shift-tab"; command = "previous_screen"; }

  # key: F1 (help screen)                                           {{{1
  { key = "f1"; command = "show_help"; }
  { key = "f1"; command = "previous_screen"; }

  # key: 1..9 (show screen)                                         {{{1
  { key = "1"; command = "show_playlist"; }
  { key = "2"; command = "show_browser"; }
  { key = "2"; command = "change_browse_mode"; }
  { key = "3"; command = "show_search_engine"; }
  { key = "3"; command = "reset_search_engine"; }
  { key = "4"; command = "show_media_library"; }
  { key = "4"; command = "toggle_media_library_columns_mode"; }
  { key = "5"; command = "show_playlist_editor"; }
  { key = "6"; command = "show_tag_editor"; }
  { key = "7"; command = "show_outputs"; }
  { key = "8"; command = "show_visualizer"; }
  { key = "8"; command = "show_clock"; }
  { key = "9"; command = "show_lyrics"; }

  # key: @, i (show info)                                           {{{1
  { key = "@"; command = "show_server_info"; }
  # { key = "i"; command = "show_song_info"; }
  # { key = "I"; command = "show_artist_info"; }

  # key: h, j, k, l                                                 {{{1
  { key = "h"; command = [ ''require_screen "song_info"'' "show_song_info" ]; }
  { key = "h"; command = "previous_column"; }
  { key = "h"; command = "jump_to_parent_directory"; }
  { key = "h"; command = "master_screen"; }
  { key = "H"; command = "jump_to_parent_directory"; }
  { key = "ctrl-h"; command = "master_screen"; }
  { key = "j"; command = "scroll_down"; }
  { key = "J"; command = [ "select_item" "scroll_down" ]; }
  { key = "ctrl-j"; command = "move_sort_order_down"; }
  { key = "ctrl-j"; command = "move_selected_items_down"; }
  { key = "k"; command = "scroll_up"; }
  { key = "K"; command = [ "select_item" "scroll_up" ]; }
  { key = "ctrl-k"; command = "move_sort_order_up"; }
  { key = "ctrl-k"; command = "move_selected_items_up"; }
  { key = "l"; command = "next_column"; }
  { key = "l"; command = "enter_directory"; }
  { key = "l"; command = "slave_screen"; }
  { key = "l"; command = "run_action"; }
  { key = "l"; command = "show_song_info"; }
  { key = "L"; command = "enter_directory"; }
  { key = "ctrl-l"; command = "slave_screen"; }

  # key: arrow keys                                                 {{{1
  { key = "up"; command = ''push_characters "k"''; }
  { key = "shift-up"; command = ''push_characters "K"''; }
  { key = "down"; command = ''push_characters "j"''; }
  { key = "shift-down"; command = ''push_characters "J"''; }
  { key = "right"; command = ''push_characters "l"''; }
  { key = "left"; command = ''push_characters "h"''; }

  # key: o, G, ~ (jump)                                             {{{1
  { key = "o"; command = "jump_to_playing_song"; }
  { key = "G"; command = "jump_to_browser"; }
  { key = "G"; command = "jump_to_playlist_editor"; }
  { key = "~"; command = "jump_to_media_library"; }

  # e: edit                                                         {{{1
  { key = "e"; command = "edit_playlist_name"; }
  { key = "e"; command = "edit_lyrics"; }
  { key = "e"; command = "edit_song"; }
  { key = "e"; command = "edit_library_tag"; }
  { key = "e"; command = "edit_library_album"; }
  { key = "E"; command = "jump_to_tag_editor"; }

  # scroll                                                          {{{1
  { key = "ctrl-u"; command = "page_up"; }
  { key = "ctrl-d"; command = "page_down"; }
  { key = "page_up"; command = "page_up"; }
  { key = "page_down"; command = "page_down"; }
  { key = "0"; command = "move_home"; }
  { key = "$"; command = "move_end"; }
  { key = "home"; command = "move_home"; }
  { key = "end"; command = "move_end"; }
  { key = "["; command = "scroll_up_album"; }
  { key = "]"; command = "scroll_down_album"; }
  { key = "{"; command = "scroll_up_artist"; }
  { key = "}"; command = "scroll_down_artist"; }

  # system                                                          {{{1
  { key = "u"; command = "update_database"; }
  { key = ":"; command = "execute_command"; }
  { key = "q"; command = "quit"; }
  { key = "alt-l"; command = "toggle_screen_lock"; }
  { key = "ctrl-a"; command = "toggle_add_mode"; }
  # { key = "U"; command = "toggle_playing_song_centering"; }
  # { key = "#"; command = "toggle_bitrate_visibility"; }
  # { key = "\\"; command = "toggle_interface"; }
  # { key = "!"; command = "toggle_separators_between_albums"; }

  # playback                                                        {{{1
  { key = "P"; command = "pause"; }
  { key = "s"; command = "stop"; }
  { key = "+"; command = "volume_up"; }
  { key = "-"; command = "volume_down"; }
  { key = ">"; command = "next"; }
  { key = "<"; command = "previous"; }
  { key = "shift-enter"; command = "play_item"; }
  { key = "backspace"; command = "replay_song"; }
  # { key = "y"; command = "toggle_single"; }
  # { key = "Y"; command = "toggle_replay_gain_mode"; }
  # { key = "r"; command = "toggle_repeat"; }
  # { key = "R"; command = "toggle_consume"; }
  # { key = "x"; command = "toggle_crossfade"; }
  # { key = "X"; command = "set_crossfade"; }
  # { key = "z"; command = "toggle_random"; }
  # { key = "Z"; command = "shuffle"; }

  # playlists                                                       {{{1
  { key = "S"; command = "save_playlist"; }
  { key = "ctrl-r"; command = "reverse_playlist"; }
  { key = "c"; command = "crop_playlist"; }
  { key = "c"; command = "crop_main_playlist"; }
  { key = "C"; command = "clear_playlist"; }
  { key = "C"; command = "clear_main_playlist"; }
  { key = "d"; command = "delete_playlist_items"; }
  { key = "d"; command = "delete_stored_playlist"; }
  { key = "delete"; command = ''push_characters "d"''; }
  { key = "`"; command = "add_random_items"; }

  # select / actions with selection                                 {{{1
  # { key = "B"; command = "select_album"; }
  # XXX: what is this?
  # { key = "ctrl-p"; command = "set_selected_items_priority"; }
  { key = "V"; command = "select_range"; }
  { key = "v"; command = "reverse_selection"; }
  { key = "ctrl-v"; command = "remove_selection"; }

  { key = "a"; command = "add_selected_items"; }

  # A: add selected to end of playlist
  {
    key = "A";
    command =
      [
        "add_selected_items"
        ''require_screen "selected_items_adder"''
        "run_action"
        "run_action"
      ];
  }

  # m: move selection to cursor position
  { key = "m"; command = "move_selected_items_to"; }

  # filter                                                          {{{1
  # { key = "ctrl-f"; command = "apply_filter"; }

  # find                                                            {{{1
  { key = "/"; command = "find"; }
  { key = "/"; command = "find_item_forward"; }
  { key = "?"; command = "find"; }
  { key = "?"; command = "find_item_backward"; }
  { key = "n"; command = "next_found_item"; }
  { key = "p"; command = "previous_found_item"; }
  # { key = "ctrl-_"; command = "select_found_items"; }
  # { key = "w"; command = "toggle_find_mode"; }

  # seek / jump song                                                {{{1
  # { key = "b"; command = "seek_backward"; }
  # { key = "f"; command = "seek_forward"; }
  # { key = "g"; command = "jump_to_position_in_song"; }

  # sort                                                            {{{1
  { key = "ctrl-s"; command = "sort_playlist"; }
  { key = "alt-s"; command = "toggle_browser_sort_mode"; }
  { key = "alt-s"; command = "toggle_media_library_sort_mode"; }
]
