{ lib, config, pkgs, ... }:
let
  format = ''
    Date:      {start-date}
    Organizer: {organizer}
    Location:  {location}

    {start-time}  {title}
    {end-necessary}

    {description}
  '';

in
{
  config = {

    #calendars.default = {
    #  path = "${config.xdg.dataHome}/dav/calendars/*";
    #  type = "discover";
    #};
    #calendars.addressbook = {
    #  path = "${config.xdg.dataHome}/dav/contacts/addressbook/";
    #  type = "birthdays";
    #};

    programs.khal = {
      enable = true;

      locale = {
        dateformat = "%d.%m.";
        datetimeformat = "%d.%m. %H:%M";
        default_timezone = "Europe/Vienna";
        firstweekday = 0;
        local_timezone = "Europe/Vienna";
        longdateformat = "%d.%m.%Y";
        longdatetimeformat = "%d.%m.%Y %H:%M";
        timeformat = "%H:%M";
        unicode_symbols = false;
        weeknumbers = "right";
      };

      # NOTE: tests broken in nixpkgs
      #package = pkgs.khal.overrideAttrs (_: { disabledTests = ["*"]; });

      #settings = {
      #  default.default_calendar = "calendar";
      #  default.highlight_event_days = true;
      #  default.print_new = "event";
      #  default.show_all_days = true;
      #  default.timedelta = "7d";
      #  highlight_days.color = "dark cyan";
      #  highlight_days.default_color = "";
      #  highlight_days.method = "fg";
      #  highlight_days.multiple = "dark red";
      #  view.agenda_day_format = "{green-bold}{date}{reset} {cyan}{name}{reset}";
      #  view.agenda_event_format = "{red}{cancelled}{reset}{calendar-color}{start-end-time-style}{cyan-bold}{title}{reset}{yellow}{repeat-symbol}{reset}{description-separator}{reset}{description}";
      #  view.theme = "dark";
      #  view.bold_for_light_color = false;
      #  view.frame = "color";
      #  view.monthdisplay = "firstfullweek";
      #  view.dynamic_days = true;
      #  view.event_view_always_visible = true;
      #  sqlite.path = "${config.xdg.dataHome}/khal/khal.db";
      #};
    };

    mailcap.entries."text/calendar" = lib.mkDefault { command = ''khal --color printics -f $'${format}' %s | tail -n+2''; copiousoutput = true; };
    mailcap.entries."application/ics" = lib.mkDefault { command = ''khal --color printics -f $'${format}' %s | tail -n+2''; copiousoutput = true; };
  };
}
