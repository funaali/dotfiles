{ config, lib, pkgs, ... }:

with lib;

{
  config = {
    home.packages = [ pkgs.mpc-cli ];

    home.sessionVariables = {
      MPC_FORMAT = "[%artist% - ][[%track%. ]%title%[ 「%album%[ (%date%)]][ (Disc %disc%)]」]|%file%]";
    };
  };
}
