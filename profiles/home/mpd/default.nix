{ config, ... }:

{
  config = {
    services.mpd = {
      enable = true;
      musicDirectory = "${config.home.homeDirectory}/Music";
      playlistDirectory = "${config.services.mpd.musicDirectory}/playlists";
      dataDir = "${config.home.homeDirectory}/.cache/mpd";
      dbFile = null; # set in extraConfig
      network.listenAddress = "any";
      extraConfig = ''
        # Bind to all IPv4 and IPv6 addresses.
        #bind_to_address "[::]"
        #bind_to_address "0.0.0.0"
        bind_to_address "*"

        database {
                plugin "simple"
                path "${config.services.mpd.musicDirectory}/mpd.db"
                cache_directory "${config.home.homeDirectory}/.cache/mpd"
                compress "yes"
        }

        ${builtins.readFile ./mpd.conf}

        # Include optional local configuration.
        include_optional "${config.home.homeDirectory}/.config/mpd/local.conf"
      '';
    };

    services.mpd-notification.enable = true;

    services.mpdscribble = {
      enable = true;
      scrobblers = {
        "last.fm" = {
          url = "http://post.audioscrobbler.com/";
          postCommand = "pass show web/last.fm | sed -ne '1s/^/password = /p; /^username:/s/:/ =/p'";
        };
      };
    };

    # Tune the mpd systemd service parameters
    systemd.user.services.mpd = {
      #Unit.RequiresMountsFor = [ "/media/moore" ];
      Service = {
        Slice = "mpd.slice";
        #ExecStartPre = ''/bin/sh -c 'while ! [ -e "${config.services.mpd.musicDirectory}" ]; do sleep 2; done' '';
        TimeoutStartSec = "10min";
        NoNewPrivileges = true;
        LimitRTPRIO = 98;
        LimitRTTIME = "infinity";
        LimitNOFILE = 32768;
        LimitMEMLOCK = "infinity";
        # XXX: mpd should be able to write the db and other state files.
        #ProtectSystem = "strict";
        MemoryDenyWriteExecute = true;
        RestrictNamespaces = true;
        RestrictRealtime = false;
        IOSchedulingClass = "best-effort";
        IOSchedulingPriority = 2;
      };
    };
  };
}
