{ ... }:
{
  sim.email = "samuli.thomasson@pm.me";
  sim.gpgKeyId = "F3D3AB3309F04D1CA4D851D068F82A4F3ECA091D";
  sim.repos = {
    password-store.uri = "git@gitlab.com:funaali/password-store.git";
  };
}
