{ config, pkgs, ... }:

{
  programs.password-store = {
    enable = true;
    package = pkgs.pass.withExtensions (exts: [
      exts.pass-audit
      exts.pass-genphrase
      exts.pass-tomb
      exts.pass-checkup
      exts.pass-import
      exts.pass-update
      exts.pass-file
      exts.pass-otp
    ]);
    settings = {
      PASSWORD_STORE_ENABLE_EXTENSIONS = "true";
    };
  };

  sim.repos.password-store = {
    path = config.programs.password-store.settings.PASSWORD_STORE_DIR;
    noCommit = true; # pass takes care of this
  };
}
