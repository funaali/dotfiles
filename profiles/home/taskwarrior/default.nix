{ config, pkgs, ... }:

# NOTE set up the synchronization like:
# cd ~/.task
# ssh morphism.funktionaali.com sudo cat /var/lib/taskserver/keys/ca.cert > ca.cert
# ssh morphism.funktionaali.com sudo cat /var/lib/taskserver/keys/default/sim/private.key > private.key
# ssh morphism.funktionaali.com sudo cat /var/lib/taskserver/keys/default/sim/public.cert > public.cert

let
  mkTagContext = tag: { read = "+${tag}"; write = "+${tag}"; };
  mkProjectContext = pro: { read = "project:${pro}"; write = "project:${pro}"; };
in
{
  config = {
    home.packages = [ pkgs.taskwarrior-tui ];

    programs.taskwarrior = {
      enable = true;
      dataLocation = "${config.xdg.dataHome}/task";
      colorTheme = "solarized-dark-256";

      config = {
        weekstart = "monday";
        verbose = "blank,header,label,new-id,affected,edit,special,project,sync,context,recur";

        calendar.details = "full";
        calendar.holidays = "full";
        calendar.monthsperline = 3;

        context.home = mkTagContext "home";
        context.cyan = mkProjectContext "cyan";

        #default.due = "eow"; # due by end of week

        taskd = {
          server = "morphism.funktionaali.com:53589";
          credentials = "default/sim/d483d052-12cc-422b-92fd-deaca4caed3f"; # <org>/<user>/<key>
          key = config.age.secrets."taskwarrior-sim.key".path;
          certificate = ./public-sim.cert; # client cert
          ca = ./ca.cert; # CA cert (for self-signed cert)
        };
      };

      extraConfig = ''
        # Set holidays
        include holidays.de-AT.rc
      '';
    };

    services.taskwarrior-sync.enable = true;

    age.secrets."taskwarrior-sim.key".file = ../../../secrets/taskwarrior-sim.key.age;
  };
}
