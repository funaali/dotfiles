{ ... }:

# Pipewire/wireplumber user customizations.

with builtins;

let
  pwConfsFilter = path: type:
    (!isNull (match ".*\.conf\.d" path) && type == "directory") ||
    (!isNull (match ".*\.conf" path)    && elem type ["regular" "symlink"]) ||
    throw "unexpected type ${type}";

in

{
  config = {

    # Pipewire configs
    xdg.configFile.pipewire = {
      source = filterSource pwConfsFilter ./pipewire;
      recursive = true;
    };

    # Wireplumber configs
    xdg.configFile."wireplumber/wireplumber.conf.d" = {
      source = ./wireplumber/wireplumber.conf.d;
      recursive = true;
    };

    # Wireplumber scripts
    xdg.dataFile."wireplumber/scripts" = {
      source = ./wireplumber/scripts;
      recursive = true;
    };
  };
}
