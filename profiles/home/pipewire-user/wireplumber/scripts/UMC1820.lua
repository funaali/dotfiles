-- Create appropriate virtual sinks for UMC1820 dynamically

-- TODO Set also clock.rate          = 96000
-- and                 allowed-rates = [ 48000, 96000 ]

local sinks = {}

sinks["headphones"] = {
        ["node.description"] = "Headphones",
        ["capture.props"]    = { ["audio.position"] = "[ FL FR ]" },
        ["playback.props"]   = { ["audio.position"] = "[ AUX2 AUX3 ]" },
}

sinks["headphones21"] = {
        ["node.description"] = "Headphones",
        ["capture.props"]    = { ["audio.position"] = "[ FL FR LFE ]" },
        ["playback.props"]   = { ["audio.position"] = "[ AUX2 AUX3 AUX5 ]" },
}

sinks["speakers20"] = {
        ["node.description"] = "Speakers 2.0",
        ["capture.props"]    = { ["audio.position"] = "[ FL FR ]" },
        ["playback.props"]   = { ["audio.position"] = "[ AUX0 AUX1 ]" },
}

sinks["speakers21"] = {
        ["node.description"] = "Speakers 2.1",
        ["capture.props"]    = { ["audio.position"] = "[ FL FR LFE ]" },
        ["playback.props"]   = { ["audio.position"] = "[ AUX0 AUX1 AUX5 ]" },
}

local default_capture_props = {
        ["media.class"]             = "Audio/Sink",
        ["monitor.channel-volumes"] = true,
}

local default_playback_props = {
        ["stream.dont-remix"]       = true,
        ["node.passive"]            = true,
        ["node.dont-reconnect"]     = true,
        ["monitor.channel-volumes"] = true,
}

local modules = {}

local function add_props(r, props)
        for k, v in pairs(props or {}) do
                r[k] = v
        end
end

local function create_loopback_module(master, aname, aargs)
        local masterName = master.properties["node.name"]:gsub("^alsa_output.", "")
        local masterDesc = master.properties["node.description"]
        local name = 'virtual.' .. aname .. '.' .. masterName
        local desc = (aargs["node.description"] or aname) .. ' (' .. masterDesc .. ')'

        local cProps = {}
        add_props(cProps, default_capture_props)
        add_props(cProps, aargs["capture.props"])
	cProps["node.name"] = name

        local pProps = {}
        add_props(pProps, default_playback_props)
        add_props(pProps, aargs["playback.props"])
	pProps["node.name"]     = "playback." .. name
	pProps["target.object"] = master.properties["object.serial"]

        local args = {}
        args["node.description"] = desc
        args["node.group"]       = name
	args["capture.props"]    = Json.Object(cProps)
	args["playback.props"]   = Json.Object(pProps)

        local json = Json.Object (args):to_string()

	print("New loopback instance '" .. name .. "' with args: " .. json)
	return LocalModule("libpipewire-module-loopback", json, nil)
end

local function create_virtual_devices(node)
        local mods = {}
        for k, v in pairs(sinks) do
                mods[k] = create_loopback_module(node, k, v)
        end
        return mods
end


nodes_om = ObjectManager {
  Interest {
    type = "node",
    Constraint { "node.name", "=", "alsa_output.UMC1820"},
    Constraint { "media.class", "=", "Audio/Sink"},
  }
}

nodes_om:connect("object-added", function(om, node)
  print("Added Node id=" .. node.id .. ", object.id=" .. node.properties["object.id"])
  --Debug.dump_table(node.properties)
  modules[node.id] = create_virtual_devices(node)
  -- node:connect()
end)

nodes_om:connect("object-removed", function(om, node)
  print("Removed Node ", node.id)
  -- Note: dropping all references to the loopback modules should unload it.
  modules[node.id] = nil
end)

--nodes_om:connect("objects-changed", function(om) end)

nodes_om:activate()
