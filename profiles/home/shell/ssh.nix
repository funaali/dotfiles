{ lib, pkgs, ... }:

let
  gpgWorkAround = pkgs.writeText "gpg-sshconfig" ''
    # Silly workaround for GnuPG ssh agent... see
    # # https://bugzilla.mindrot.org/show_bug.cgi?id=2824#c9
    Match exec "''${GPG_TTY+gpg-connect-agent UPDATESTARTUPTTY /bye}"
  '';
in
{
  config.programs.ssh = {
    enable = true;
    includes = [
      "${gpgWorkAround}"
      "config.local" # host-local overrides
    ];
    controlMaster = "auto";
    controlPath = "~/.ssh/master-%C"; # prefer hashes b/c ssh dislikes long filepaths
    controlPersist = "15s";
    serverAliveInterval = 25; # enable tcp keepalive
    matchBlocks = {
      # These become "Host" blocks
      "localhost 127.0.0.1 ::1" = {
        extraOptions.HostKeyAlias = "localhost";
        extraOptions.CanonicalizeHostname = "no";
      };
      "github.com gitlab.com" = {
        user = "git";
      };
      # Jump/bastion hosts
      "*_jump" = {
        forwardAgent = true;
        serverAliveInterval = 10; # enable tcp keepalive
        extraOptions.ControlPersist = "1m"; # longer than usual persistence
      };
    };
    extraConfig = lib.concatStringsSep "\n" [
      # Check IP addresses in known_hosts as well
      "CheckHostIP yes"
      # Display and ask to confirm DNS fingerprints
      "VerifyHostKeyDNS ask"
      # Exit if any forwarding fails
      "ExitOnForwardFailure yes"
      # Prefer AES-GCM, it's faster than C20P1305 on devices with hardware AES instructions.
      "Ciphers ^aes128-gcm@openssh.com,aes256-gcm@openssh.com"
      # Prefer Ed25519 keys
      "HostkeyAlgorithms ^ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com"
      "CASignatureAlgorithms ^ssh-ed25519,sk-ssh-ed25519@openssh.com"
      # Harden MACs
      # - disabled: umac-64-etm@openssh.com,umac-64@openssh.com,hmac-sha1-etm@openssh.com,hmac-sha1
      "MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com"
    ];

  };
}
