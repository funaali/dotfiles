{ lib, pkgs, config, ... }:

with lib;
let
  parseSeconds = s:
    with builtins;
    let
      f = { pat, m }:
        let res = match pat s; in
        if res != null then toInt (head res) * m else null;
      patterns = [
        { pat = "\s*([0-9]+)\s*(s|seconds)?\s*"; m = 1; } # seconds
        { pat = "\s*([0-9]+)\s*(m|mins|minutes)\s*"; m = 60; } # minutes
        { pat = "\s*([0-9]+)\s*(h|hours)\s*"; m = 60 * 60; } # hours
        { pat = "\s*([0-9]+)\s*(d|days)\s*"; m = 24 * 60 * 60; } # days
        { pat = "\s*([0-9]+)\s*(w|weeks)\s*"; m = 7 * 24 * 60 * 60; } # weeks
      ];
    in
    findFirst isInt (throw "parseSeconds: could not parse value: ${s}") (map f patterns);

  cfg = config.sim;
in
{
  home.sessionVariables = {
    GPG_AGENT_INFO = "\${GPG_AGENT_INFO:-}"; # mutt might need this
  };

  programs.gpg = {
    enable = true;
    settings = mkMerge [
      (mkIf (!isNull cfg.gpgKeyId) { default-key = cfg.gpgKeyId; })
      {
        display-charset = "utf-8";
        expert = true; # enable expert options in --edit-key by default
        greeting = false; # Suppress the initial copyright message.
        keyid-format = "0xlong";

        # Fingerprints: only enable subkey fingerprints additionally.
        # Otherwise gpg splits the fingerprints to letter groups of 4.
        with-subkey-fingerprint = true;

        # Keygrips: display always.
        with-keygrip = true;

        # Always hide recipient key ID's
        #throw-keyids = true;

        # Select how passphrases for symmetric encryption are mangled. 3
        # iterates the whole process a number of times.
        s2k-mode = "3";

        # PGP Standard only support ZIP compression!
        personal-compress-preferences = "ZIP Uncompressed";

        default-recipient-self = true;

        list-options = lib.concatStringsSep "," [
          "show-photos"
          "show-keyring"
          # These settings apply only for --check-sigs
          "show-policy-urls"
          "show-notations"
          "show-keyserver-urls"
          "show-sig-expire"
        ];

        verify-options = lib.concatStringsSep "," [
          "show-photos"
          "show-notations"
        ];

        auto-key-locate = lib.concatStringsSep "," [
          "clear"
          "local"
          "wkd"
          "cert"
          "keyserver"
        ];

        photo-viewer = "${lib.getExe (pkgs.writeShellApplication rec {
          name = "gnupg-photo-viewer";
          runtimeInputs = with pkgs; [ coreutils ];
          # Placeholders:
          # %i := filename of the photo
          # %I := like %i, but the file won't be deleted once the viewer exits
          # %k := key ID
          # %K := long key ID
          # %f := fingerprint
          # %t := extension of the image (e.g. jpg)
          # %T := MIME type of the image (e.g. "image/jpeg")
          # %v := calculated validity as a single character (e.g. "f")
          # %V := calculated validity as a string (e.g. "full")
          # %U := base32 encoded hash of the user ID
          text = ''
            if [[ $# != 5 ]]; then
              echo "usage: $0 [file] [key-id] [fpr] [validity] [user-id_b32]" >&2
              exit 2
            fi

            file=$1
            keyID=$2
            fpr=$3
            validity=$4
            userID=$(base32 -d - <<<"$5")

            if [[ -n ''${KITTY_PID-} ]] && kitty +kitten icat --detect-support >/dev/null; then
              echo "Key: $keyID $userID [$validity]"
              echo "Fingerprint: $fpr"
              kitty +kitten icat "$file"
            else
              xdg-open "$file"
            fi
          '';
          meta.mainProgram = name;
        })} %i %K %f %V %U";
      }
    ];

    # https://www.gnupg.org/documentation/manuals/gnupg/Scdaemon-Options.html
    scdaemonSettings = {
      allow-admin = true; # allow admin commands in gpg --edit-card

      #pcsc-driver = "/usr/lib/libpcsclite.so";
      pcsc-driver = "${pkgs.pcsclite.lib}/lib/libpcsclite.so";

      # This tells GnuPG to use PC/SC instead of scdaemon which is integrated
      # with GnuPG and preferred by it. In essence, this option tells scdaemon
      # to use PC/SC to communicate with the smartcard appliance instead.
      # Note: You may need to force-restart any running scdaemon process for
      # changes to this option to take effect.
      disable-ccid = true;

      pcsc-shared = true;

      # Note: If you do not set the enable-pinpad-varlen option, even if Nano
      # S is configured in On Screen mode, gpg will keep requesting the PIN
      # on the host.
      enable-pinpad-varlen = true;
    };
  };

  services.gpg-agent = {
    enable = true;
    enableSshSupport = true;
    pinentryPackage = mkDefault pkgs.pinentry-curses;
    defaultCacheTtl = parseSeconds "24h"; # (86400)
    defaultCacheTtlSsh = parseSeconds "24h";
    maxCacheTtl = parseSeconds "30d"; # (604800) 7d
    maxCacheTtlSsh = parseSeconds "30d";
    enableScDaemon = true;
    #sshKeys = []; # TODO
    extraConfig = ''
      pinentry-timeout 60

      # In case the pinentry-program is configured to use some other cache, ask it not to.
      no-allow-external-cache

      ssh-fingerprint-digest SHA256

      # Extended key format has these effects:
      # - creating new, or changing the passphrase of existing a private key, makes the private key unreadable for gpg-agent<2.1.12.
      # - the extended format is text-based and can carry additional meta data.
      # - key's protection format is also set to OCB mode.
      enable-extended-key-format
    '';
  };

  # dirmngr.conf: <url:man:dirmngr.8>
  home.file.".gnupg/dirmngr.conf".text = ''
    keyserver hkps://keys.openpgp.org
  '';
}
