{ config, lib, pkgs, ... }:

with lib;

{
  home.packages = [ pkgs.bash-complete-alias ];

  home.file.".bash_completion".text = mkIf config.programs.bash.enable ''
    __load_completion _complete_alias
  '';

  programs.bash.initExtra = mkOrder 110 ''
    # alias complete
    if [ -v BASH_COMPLETION_VERSINFO ]; then
      while read -r cmd def; do
        if [[ $def != "$cmd "* ]]; then
          complete -F _complete_alias -- "$cmd"
        fi
      done < <(alias -p | sed -e 's/alias \(-- \)\?\([^=]*\)=.\(.*\)./\2 \3/')
    fi
  '';
}
