# shellcheck shell=bash

if type neofetch >/dev/null 2>&1; then
	neofetch
fi

# disks
if type udiskie-info >/dev/null 2>&1; then
	paste <(printf "\e[1;33m%s\e[0m" disks) <(udiskie-info -a -o $'\e[0;33m{device_file}\e[0m \e[0;92m{id_type}\e[0m \e[0;36m"{id_label}"\e[0m {mount_paths}')
fi

# last, who
paste <(printf "\e[1;33m%s\e[0m" last) <(last -n5 -a --time-format notime)
paste <(printf "\e[1;33m%s\e[0m" who) <(who -uT | head -n10)

# stty
paste <(printf "\e[1;33m%s\e[0m" tty) <(stty) <(tty)
