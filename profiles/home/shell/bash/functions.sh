# shellcheck shell=sh
# Only load once
if [ -n "$__MY_SHELL_FUNCTIONS_SOURCED" ]; then return; fi
__MY_SHELL_FUNCTIONS_SOURCED=1

# ${v@Q} (kind of)
quote () {
	printf %s\\n "$1" | sed "s/\n/\\\n/g;s/\a/\\\a/g;s/\x1B/\\\e/g;s/'/'\\\\''/g;1s/^/'/;\$s/\$/'/"
}

# [[ arg = a??* ]]  <-> fnmatch arg a??*
fnmatch() {
	# shellcheck disable=SC2254
	case $2 in $1) return 0 ;; esac
	return 1
}

# Split a "$PATH" value to separate lines
path () (
	IFS=:
	set -f
	# shellcheck disable=SC2086
	printf %s\\n ${1-$PATH}
)

# Remove duplicates
clean_path () (
	printf %s "${1-$PATH}" | awk -vRS=: -vORS=: 's[$0]++<1{print}'
)

# find_in_path ITEM [SEARCHPATH]
find_in_path () (
	IFS=:
	set -f
	for x in ${2-$PATH}; do if [ -e "$x/$1" ]; then printf %s\\n "$x/$1"; return; fi done
	return 1
)

# $(command -V $1) = *function  # Starting from POSIX 2013
is_function() { fnmatch '* function' "$(type -- "$1" 2>/dev/null)"; }

is_builtin () { fnmatch '* builtin' "$(type -- "$1" 2>/dev/null)"; }

# type -P $1
is_executable() { find_in_path "$1" >/dev/null; }

# POSIX-incompatible utils                                          {{{1

# get current cursor position (not POSIX)
curpos()(
	# https://stackoverflow.com/questions/2575037/how-to-get-the-cursor-position-in-bash
	printf "\e[6n" >/dev/tty
	IFS='[;' read -r -s -dR _ ROW COL </dev/tty 2>/dev/tty
	printf %i:%i\\n "$ROW" "$COL"
)
