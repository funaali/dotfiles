{ lib, pkgs, ... }:

with lib;

{
  imports = [
    ./bash-complete-alias.nix
  ];

  config = {
    programs.bash = {
      enable = true;
      historyControl = [
        "erasedups"
        "ignorespace"
        "ignoredups"
      ];
      historyIgnore = map lib.escapeShellArg [
        "&"
        "[ ] *"
        "exit"
      ];
      shellOptions = [
        "lithist"
        "histappend"
        "histreedit"
        "histverify"
        "autocd"
        "cdspell"
        "dirspell"
        "checkhash"
        "no_empty_cmd_completion"
        "extglob"
        "globstar"
        "checkwinsize"
        "checkjobs"
        "huponexit"
        "-force_fignore"
        "progcomp_alias"
        " -o notify" # job control: report status of terminated background jobs immediately
      ];

      # top of ~/.bashrc - login or non-login, interactive or non-interactive
      bashrcExtra = ". ${./functions.sh}";

      # ~/.profile - login, before sourcing ~/.bashrc
      profileExtra = mkMerge [
        (mkBefore ". ${./functions.sh}")
        ''
          # Add gem user directory to path
          if command -v ruby >/dev/null 2>&1; then
            if gem_user_dir=$(ruby -e 'puts Gem.user_dir' 2>/dev/null); then
              case ":$PATH:" in
                *:"$gem_user_dir/bin":*) ;;
                *) PATH="$gem_user_dir/bin:$PATH"
              esac
            fi
          fi
        ''
        (mkAfter ''
          # Delete duplicates from PATH
          if type clean_path >/dev/null 2>&1; then
            if test -n "$(clean_path 2>/dev/null)"; then
              export PATH="$_"
            fi
          fi
        '')
        (mkAfter ''
          # Source ~/.profile.local if it exists
          if test -r "$HOME/.profile.local"; then . "$_"; fi
        '')
      ];

      # ~/.bashrc - interactive non-login
      initExtra = mkMerge [
        (mkOrder 100 ''
          # Unbounded history
          HISTFILESIZE=
          HISTSIZE=
          HISTFILE=~/.bash_history2
          HISTTIMEFORMAT='[%F %T]'
        '')
        ''
          # Disable CTRL-S suspend
          if [ -t 0 ]; then
              stty -ixon
          fi
        ''
        ''
          if [ -v BASH_COMPLETION_VERSINFO ]; then
            # complete sudo at least poorly
            shopt -q progcomp || complete -cf sudo || :

            complete -C packer packer

            __add_prog_comp(){
              ! complete -p "$1" &>/dev/null && command -v "$1" >/dev/null && eval "$("$@")" || :
            }
            __add_prog_comp pandoc --bash-completion
            __add_prog_comp stack --bash-completion-script stack
          fi
        ''
        ''
          # kitty terminal hack
          if [ "$TERM" = "xterm-kitty" ]; then
            alias ssh='kitty +kitten ssh'
          fi
        ''
        (mkOrder 1500 ''
          . ${pkgs.git}/share/git/contrib/completion/git-prompt.sh
          . ${./bash-prompt.sh}
        '')

        # MOTD
        # interactive shells only
        # skip if a command was given
        # stdout needs to be to tty device
        # not on nested shells
        (mkOrder 1600 ''
          # motd
          if [[ $- = *i* ]] && [[ $- != *c* ]] && [ -t 1 ] && ((SHLVL <= 1)); then
            ${pkgs.writeShellScript "motd" ''
              export PATH=${pkgs.neofetch}/bin:$PATH
              ${builtins.readFile ./motd.sh}
            ''}
          fi
        '')
      ];

      shellAliases = {
        # NOTE: "_ cmd... >&255" can restore shell sockets!
        "_" = "<&- &>/dev/null nohup";
        # Re-execute previous command
        r = "fc -s";
        # Alias-aware "which"
        which = "(alias; declare -f) | which --read-alias --read-functions --show-tilde --show-dot";
        # List readline bindings
        binds = "bind -psX";
        # Print shell aliases, colorized
        aliases = "alias -p | bat --language sh";
      };
    };
  };
}

