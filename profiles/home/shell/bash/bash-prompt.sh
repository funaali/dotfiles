# shellcheck shell=bash

if [ -z "${BASH_VERSION-}" ] || [ -n "${POSIXLY_CORRECT-}" ]; then
	return
fi

export GIT_PS1_DESCRIBE_STYLE=branch
export GIT_PS1_HIDE_IF_PWD_IGNORED=1
export GIT_PS1_SHOWCOLORHINTS=1
export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWUNTRACKEDFILES=1
export GIT_PS1_SHOWUPSTREAM="auto verbose name"

__prompt_direnv() {
	if [ "${?-0}" -eq 0 ] && command -v "${1-direnv}" >/dev/null 2>&1; then
		eval "$("${1-direnv}" export bash)"
	fi
}

__prompt_ps1() {
	local RET=$? _1 _2
	eval _1=\""$1"\" _2=\""$2"\"
	if ! IFS=' ' __git_ps1 "$_1" "$_2" "${_ps1_git:- %s}" >/dev/null 2>&1; then
		PS1="$_1$_2"
	fi
}
__prompt_ps_ecode() {
	local fg=-1 ec=$1
    local ec_s=$ec
	case "$ec,$((ec>128))" in
		,*  ) ec_s='?' ;;
		0,* ) fg=2 ;;
		*,0 ) fg=9 ;;
		*,1 ) fg=7; ec_s=$((ec&127)) ;;
	esac
	printf '%s' "\[$(tput setaf -- "$fg")\]$ec_s\[$(tput sgr0)\]"
}

# $0 [eval-string] [dict-name] [substitutions...]
__prompt_ps_varsub() {
	local RET=$?
    local -u key
    declare -A dict=()

    if [ -v 1 ]; then
        eval "${1-}"
    fi
    if [ -v 2 ]; then
        declare -na dict=${2-}
    fi
    shift 2

	dict[RET]=$RET
	dict[NIX_SHELL_STATUS]=${IN_NIX_SHELL+"╷nix:$IN_NIX_SHELL"}
	dict[RANGER_STATUS]=${RANGER_LEVEL+"╷ranger:$RANGER_LEVEL"}
	dict[PS_ECODE]="$(__prompt_ps_ecode "$RET")"
    dict[PS_SHLVL]="\[$(tput setaf "$((SHLVL+2))")\]${SHLVL-}@RESET@"

    while [ $# -gt 0 ]; do
        case $1 in
            *"="*)
                key=${1%%=*}
                dict["$key"]=${1#*=}
                ;;
            *)
            echo "warn: ignoring argument $1" >&2
            ;;
        esac
        shift
    done

    local -a placeholders
    for _ in 1 2 3; do # define max recursive substitutions
        mapfile -t placeholders < <(echo -n "$PS1" | sed -nz 's/@[^@]*[^@A-Z_][^@]*@/@@/g; s/^[^@]*//g; s/[^@]*$//g; s/@\+\([A-Z_]\+\)@\s*/\1\n/g;p' | uniq -u)
        if [ -z "${placeholders[0]-}" ]; then
            break
        fi
        for key in "${placeholders[@]}"; do
            PS1=${PS1//"@$key@"/"${dict["$key"]}"}
        done
    done

	#PS1=${PS1/@RET@/"$RET"}
	#PS1=${PS1/@NIX_SHELL_STATUS@/${IN_NIX_SHELL+"╷nix:$IN_NIX_SHELL"}}
	#PS1=${PS1/@RANGER_STATUS@/${RANGER_LEVEL+"╷ranger:$RANGER_LEVEL"}}
	#PS1=${PS1/@PS_ECODE@/"$(__prompt_ps_ecode "$RET")"}
	#PS1=${PS1/@PS_SHLVL@/"$ps_shlvl"}
}

_prompt_hostcolor() {
    local csum
	csum=$({ cat /etc/machine-id || hostname; } 2>/dev/null | cksum | cut -f1 -d\  )
	shift "$((csum % $#))"
	echo "$1"
}

# shellcheck disable=SC2016,SC2154
__prompt() {
	_frame=11 # 11
	_success=2 # 2
	_failure=9 # 9
	_signal=7
	_root=1 # 1
	_user=3 # 3
	_host=$(_prompt_hostcolor 2 3 4 13 14)
	_jobs=12

	set_reset=$(tput sgr0)
	set_frame=$(tput setaf $_frame)
    __ps_c_euid(){
        printf '\[$(tput setaf "$((EUID?%i:%i))")\]' "${_user:-3}" "${_root:-1}"
    }
	#set_euid='$(tput setaf "$((EUID?'$_user':'$_root'))")'
	#set_code='$(tput setaf $((RET?(RET>128?'$_signal':'$_failure'):'$_success')))'
	#set_code_ok=$(tput setaf "$_success")
	#set_code_err=$(tput setaf "$_failure")
	#set_code_sig=$(tput setaf "$_signal")
	#set_shlvl='$(tput setaf "$((SHLVL+2))")'

	ps_reset="\[${set_reset}\]"
	ps_time="\[$(tput setaf 12)\]\t$ps_reset"
	ps_wdir="\[$(tput setaf  6)\]\w$ps_reset"
	ps_jobs="\[$(tput setaf  $_jobs)\]%\j$ps_reset"
	ps_host="\[$(tput setaf "$_host")\]\h$ps_reset"
	#ps_euser="\[${set_euid}\]\u$ps_reset"
	#ps_dsign="\[${set_euid}\]\\\$$ps_reset"
    ps_euser="$(__ps_c_euid)\u$ps_reset"
    ps_dsign="$(__ps_c_euid)\\\$$ps_reset"
	#ps_ecode="\[${set_code}\]\$((RET&127))$ps_reset"
	ps_cmd_id="\[$(tput setaf 11)\]\#$ps_reset"
	#ps_sh_lvl="\[${set_shlvl}\]\$SHLVL$ps_reset"
	ps_sh_pid="\[$(tput setaf 10)\]\${\$}$ps_reset"

	declare -A subs=(
        [RESET]="$ps_reset"
    )

	ps1_misc="$ps_sh_pid╷@PS_SHLVL@╷$ps_jobs╷@PS_ECODE@@NIX_SHELL_STATUS@@RANGER_STATUS@"

	PS1_1=$(printf %s     "$ps_reset" ' ' "┌┤$ps_cmd_id├┤$ps_time├┄┤$ps_euser@$ps_host:$ps_wdir├┄╎$ps1_misc╎")
	PS1_2=$(printf %s \\n "$ps_reset"     "┆$ps_dsign" ' ')
	PS1="$PS1_1$PS1_2"

	# PS2
	PS2_1=$(printf %s     "$ps_reset"     "┆$ps_reset")
	PS2="$PS2_1"

	# PS0: only bash 4.4 and up
	PS0_1="${set_frame}command #\# started \D{at %T on %a %F %Z}${set_reset}\n"
	PS0="$PS0_1"

	local -a pc_cmds=()
	case "$(declare -p PROMPT_COMMAND 2>/dev/null)" in
		declare\ -a*) : ;;
		*) pc_cmds+=( "$PROMPT_COMMAND" ) ;;
	esac
	PROMPT_COMMAND=(
		"builtin history -a"
		"__prompt_direnv $(quote "$(command -v direnv)")"
		"__prompt_ps1 $(quote "$PS1_1") $(quote "$PS1_2")"
		"__prompt_ps_varsub $(quote "$(declare -p subs)") subs 2>/dev/null"
		"${pc_cmds[@]}"
	)

	declare -p PS0 PS1 PS2 PROMPT_COMMAND
}

if [ -z "$TERM" ] || [ "$TERM" = dumb ]; then
	echo 'Bogus TERM value in interactive shell' >&2
	return 1
fi

eval "$(__prompt)"
