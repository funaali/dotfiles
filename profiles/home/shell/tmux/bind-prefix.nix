{ lib, tmuxLib, ... }:

with lib;
with tmuxLib;

# attrset of key bindings: "<key> = <action>"
# Actions can be specified in many ways:
#
#   - String          (inserted as-is)             "split-window -h"
#   - List of strings (command arguments escaped): [ command arg1 arg2 ... ]
#   - Attrset { ... }

{
  ":" = "command-prompt";
  "?" = [
    # NOTE: requires somewhat recent tmux version
    # for compat with older tmux:
    # list-keys { };
    "list-keys"
    "-aN"
    "-P"
    "P  "
    "-Tprefix"
    ";"
    "list-keys"
    "-aN"
    "-P"
    "Cv "
    "-Tcopy-mode-vi"
  ];
  "]" = "choose-buffer -Z";
  "~" = "show-messages";
  C-a = { action = "send-prefix"; repeat = true; };
  PgUp = "copy-mode -u";
  t = "clock-mode"; # turn the active pane into a clock
  v = "copy-mode";
  r = [ "source-file" "~/.config/tmux/tmux.conf" ";" "display-message" "Reloaded configuration" ];

  # client                                                                 {{{2
  "(" = "switch-client -p";
  ")" = "switch-client -n";
  C-z = "suspend-client";
  D = "choose-client -Z";
  Y = "switch-client -l"; # l for last client
  d = "detach-client";

  # create W | P                                                           {{{2
  c = "new-window";
  C = "new-window -a";
  "\"" = "split-window -v  -l 25%";
  "|" = "split-window -h  -l 35%";
  "\\" = "split-window -hb -l 35%";

  # select                                                                 {{{2
  # choose in all sessions
  w = "choose-tree -GswZ";

  # C-w run-shell "tmux choose-tree -Nw -f \"##{==:##{session_id},#{q:session_id}}\""
  C-w = {
    action = [ "choose-tree" "-Nw" "-f" "##{==:##{session_id},#{q:session_id}}" ];
    note = "choose a window/pane from current session";
  };
} //

# select-pane
fold recursiveUpdate { } (
  let f = d: k: setAttrByPath [ k ] ({ action = "select-pane -${d}"; repeat = true; });
  in zipListsWith f [ "L" "D" "U" "R" ] [ "h" "j" "k" "l" ]
) //

{
  C-n = { action = "select-pane -t :.+"; repeat = true; }; # next
  C-p = { action = "select-pane -t :.-"; repeat = true; }; # prev
  C-y = "last-pane";
} //

# select-window
genAttrs (map toString [ 0 1 2 3 4 5 6 7 8 9 ]) (n: "select-window -t :=${n}") //

{
  y = "last-window";
  a = "last-window";
  A = "next-window -a";
  n = { action = "select-window -t :+"; repeat = true; };
  p = { action = "select-window -t :-"; repeat = true; };
  "/" = mkPrompt { prompt = "#{W: #I:#W} : find window by name:"; action = [ "find-window" "-Z" "--" "%%%" ]; };
  "'" = mkPrompt { prompt = "#{W: #I:#W} : select window number:"; action = [ "select-window" "-t" ":%%%" ]; };
}
  //

{
  # rotate/swap/rename S | W                                               {{{2
  o = "rotate-window";
  "{" = "swap-pane -U";
  "}" = "swap-pane -D";
  N = { action = "swap-window -t :+"; repeat = true; };
  P = { action = "swap-window -t :-"; repeat = true; };
  "$" = mkPrompt { prompt = "[#S] rename-session:"; action = [ "rename-session" "--" "%%%" ]; };
  "," = mkPrompt { prompt = "(#I:#W) rename-window:"; action = [ "rename-window" "--" "%%%" ]; };
  "." = mkPrompt { prompt = "(#I:#W) swap-window (#{W: #I:#W}):"; action = [ "swap-window" "-t" "%%%" ]; };
  #"." = mkPrompt { prompt = "#{W: #I:#W} : swap window #I with window:"; action = [ "swap-window" "-t" "%%%" ; };
  # Move a window INTO current session from some other session
  ";" = mkPrompt { prompt = "(move-window -d -s _) <session:window>"; action = [ "move-window" "-d" "-s" "%%%" ]; }; # #{S:#{session_id}.#S:[#{W:#{window_index}.#W ,#{window_index}.#W}] }

  # kill S | W | P                                                         {{{2
  # confirm-before -p "kill session #S? (y/n)" kill-session
  x = [ "confirm-before" "-p" "kill pane #P? (y/n)" "kill-pane" ];
  X = [ "confirm-before" "-p" "kill window #W? (y/n)" "kill-window" ];
  C-x = [ "confirm-before" "-p" "kill session #S? (y/n)" "kill-session" ];

  # change layout                                                          {{{2
  Space = { action = "next-layout"; repeat = true; };
  "=" = "select-layout -E"; # even all panes

  # resize P                                                               {{{2
  z = "resize-pane -Z"; # togggle pane zoom
  C-h = { action = "resize-pane -L"; repeat = true; };
  C-j = { action = "resize-pane -D"; repeat = true; };
  C-k = { action = "resize-pane -U"; repeat = true; };
  C-l = { action = "resize-pane -R"; repeat = true; };
  H = { action = "resize-pane -L 5"; repeat = true; };
  J = { action = "resize-pane -D 5"; repeat = true; };
  K = { action = "resize-pane -U 5"; repeat = true; };
  L = { action = "resize-pane -R 5"; repeat = true; };

  # mark/break/join P                                                      {{{2
  m = [ "select-pane" "-m" ";" "display-message" "Marked pane #T #F" ];
  M = [ "select-pane" "-M" ";" "display-message" "Unmarked pane" ];
  "!" = "break-pane";
  Enter = "join-pane -v";
  S-Enter = "join-pane -h";
}
