{ fetchFromGitHub
, tmuxPlugins
}:

tmuxPlugins.mkTmuxPlugin {
  pluginName = "extrakto";
  version = "unstable-2020-09-07";
  src = fetchFromGitHub {
    owner = "laktak";
    repo = "extrakto";
    rev = "30ff2cb36f8d8b9a4b57070293d6ae5433e15982";
    sha256 = "069y1d0bl5m7i673cavvnkzfzxxl9c1ff55rdvcxdzkqggxf6fag";
  };
}
