# Tmux customizations

## Plugin: resurrect

**TODO**

```
prefix + C-s save    (resurrect)
prefix + C-r restore (resurrect)
```

## Plugin: extrakto

<https://github.com/laktak/extrakto>

prefix + tab   start extrakto
   tab         insert it to the current pane,
   enter       copy it to the clipboard,
   ctrl-o      open the path/url or
   ctrl-e      edit with $EDITOR

## Notes

display-message [-apv] [-c #C] [-t #P] [msg]
       -p: print to stdout (default: statusline of #P/#C)
       -v: verbose logging while parsing format
       -a: list format variables and values
