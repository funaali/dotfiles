{ lib, pkgs, ... }:

with lib;

let
  tmuxLib = import ./lib-functions.nix { inherit lib; };

  myPlugin = pkgs.tmuxPlugins.mkTmuxPlugin {
    pluginName = "customizations";
    version = "0.0.1";
    src = ./.;
  };

in
{
  imports = [
    ./extra-options.nix
  ];

  # Set the secure socket directory in systemd user session too, so that
  # service units pick it up without tricks.
  config.systemd.user.sessionVariables.TMUX_TMPDIR = ''''${XDG_RUNTIME_DIR}'';

  config.programs.tmux = {
    enable = true;
    sensibleOnTop = false;
    keyMode = "vi";
    terminal = "tmux-256color";
    aggressiveResize = true;
    escapeTime = 0;
    historyLimit = 10000;
    clock24 = true;
    options = import ./set-options.nix { };
    #hooks = { /* g.client-attached = "update-pwd"; */ }; TODO
    binds = {
      root = import ./bind-root.nix { inherit lib tmuxLib; };
      prefix = import ./bind-prefix.nix { inherit lib tmuxLib; };
      copy-mode-vi = import ./bind-copy-vi.nix { inherit lib tmuxLib; };
    };

    plugins = [
      {
        plugin = myPlugin;
        extraConfig = ''
          # Unbind everything. Here, so that it is done early, before
          # plugins might be creating bindings.
          unbind -T root -a
          unbind -T prefix -a
          unbind -T copy-mode-vi -a
        '';
      }
      {
        plugin = pkgs.tmuxPlugins.resurrect;
        extraConfig = ""; # renderOptions "set -g" {};
      }
      {
        plugin = pkgs.callPackage ./extraktoPlugin.nix { };
        extraConfig = ""; # renderOptions "set -g" {};
      }
    ];

    #extraConfig = ''
    #'';
  };
}
