{}:

{
  # server defaults
  s.exit-empty = false;
  s.extended-keys = "always";
  s.escape-time = 0; # 500 ms
  s.focus-events = true; # for vim plugin tmux-plugins/vim-tmux-focus-events
  s.terminal-features = "xterm-kitty:clipboard:ccolour:cstyle:extkeys:focus:mouse:RGB:sync:title";
  s.terminal-overrides = ",xterm-kitty:Tc";

  # session defaults
  #g.default-command = "${SHELL}"; # if empty, default-shell is used to launch a login shell.
  g.assume-paste-time = 0; # 1
  g.display-panes-active-colour = "blue";
  g.display-panes-colour = "brightred";
  g.display-time = 5000;
  g.message-style = { fg = "brightred"; bg = "black"; };
  g.mouse = true;
  g.prefix = "C-a";
  g.prefix2 = "None"; # null;
  g.remain-on-exit = false;
  g.renumber-windows = true;
  g.set-titles = true;
  g.set-titles-string =
    let
      session = "#S#{?session_group, (#{session_group}),}#{session_alerts}";
      window = "#{window_id}:#W";
      cmds = "#{P:#{pane_current_command} ,#{pane_current_command}}";
      title = "#{pane_title}";
    in
    title + " [" + session + "/" + window + "] (" + cmds + ")";

  # window defaults
  gw.allow-passthrough = true; # allow ""\033Ptmux;\033\033[<u\033\\" etc.
  gw.allow-rename = true; # whether or not a program can set window name via termcap escape sequence
  gw.alternate-screen = true; # allows smcup/rmcup (to restore window contents when interactive program exits)
  gw.automatic-rename = true; # automatically rename windows
  gw.automatic-rename-format = "#{?pane_in_mode,[tmux],#{pane_current_command}}#{?pane_dead,[dead],}";
  gw.clock-mode-colour = "green";
  gw.mode-style = { fg = "black"; bg = "yellow"; };
  gw.pane-active-border-style = { fg = "brightgreen"; };
  gw.pane-border-style = { fg = "black"; };
  gw.window-size = "latest";
  gw.wrap-search = false;
  gw.xterm-keys = true;

  # status
  g.status = true;
  g.status-interval = 60;
  g.status-justify = "centre";
  g.status-position = "bottom";
  g.status-left = "#S :: #{=45:pane_title}";
  g.status-right = "#{?window_bigger,[#{window_offset_x}#,#{window_offset_y}] ,} [cont #{continuum_status}] #(printenv USER)@#H:#(dirs +0)";
  g.status-style = { bg = "black"; fg = "default"; };
  g.status-left-style = null;
  g.status-right-style = null;
  g.status-left-length = 0;
  g.status-right-length = 0;

  # window-status
  gw.window-status-separator = " ";
  gw.window-status-format = "#I:#W#{?window_flags,#{window_flags}, }";
  gw.window-status-current-format = "#I:#W#{?window_flags,#{window_flags}, }";
  gw.window-status-current-style = { fg = "brightred"; };
  gw.window-status-activity-style = { fg = "blue"; };
  gw.window-status-last-style = { fg = "brightmagenta"; };
  gw.window-status-bell-style = { fg = "blue"; };
  gw.window-status-style = null;

  # ALERTS
  # the *-action options determine which alerts to react to in relation to current window.
  # tmux can manage three kinds of alerts: bell, activity and silence.
  # The monitor-* window options determine which alerts tmux monitors per window.
  g.visual-activity = false; # on (message), off (bell), both (message + bell)
  g.visual-silence = false;
  g.visual-bell = false;
  g.activity-action = "other"; # any, none, current or other
  g.silence-action = "other";
  g.bell-action = "any";
  gw.monitor-activity = false; # on/off
  gw.monitor-bell = true; # on/off
  gw.monitor-silence = 0; # seconds, determines max silence interval. 0 disables
}
