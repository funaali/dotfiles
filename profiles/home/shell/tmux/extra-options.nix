{ options, config, lib, pkgs, ... }:

with lib;

let
  cfg = config.programs.tmux;
  tmuxLib = import ./lib-functions.nix { inherit lib; };
  tmuxCmds = import ./cmds.nix { inherit lib tmuxLib; };

in
{
  options.programs.tmux = {
    options = mkOption {
      type = with types;
        let nonNullOptionValue = either bool (either str (either int (attrsOf str)));
        in attrsOf (attrsOf (nullOr nonNullOptionValue));
      default = { };
      description = ''
        Set options as an attrset of attrsets. For example,

          { gw = { alternate-screen = true; } }

        translates to:

          set -gw alternate-screen on

        setting a global (g) window (w) option. Other qualifiers set the
        corresponding option instead: server (s), global session (g), etc.
      '';
      example = literalExpression ''
        {
          s.exit-empty = false;
          g.prefix = "C-a";
          gw.wrap-search = true;
          g.message-style = { fg = "brightred"; bg = "black"; };
        }
      '';
    };

    binds = mkOption {
      type = with types; attrsOf (attrsOf anything);
      default = { };
      description = "Define bindings as an attrset.";
      example = literalExpression ''
        {
          prefix = {
            "C-a" = "send-prefix"; }
          root = {
            "C-PgUp" = "copy-mode -eu"; }
        }
      '';
    };
  };

  config.programs.tmux = {
    extraConfig = ''
      ${tmuxLib.renderSets cfg.options}
      ${tmuxLib.renderBinds cfg.binds}
    '';
  };
}
