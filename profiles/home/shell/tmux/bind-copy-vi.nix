{ lib, tmuxLib }:

with lib;
with tmuxLib;

{
  # select                                                                 {{{2
  # v  V  ^V      begin char-, line- or block-wise select
  # <Esc> ^C      abort copy-mode (no buffer or clip created)
  # q             clear current selection if any; if none abort like above
  # y             copy current selection to buffer and clipboard & exit copy-mode
  # ^Y            like <y> but doesn't exist copy-mode
  # NOTE: #{selection_present} if there's selected (highlighted) text currently
  # NOTE: "choose-buffer -Z" to choose from tmux buffers
  v = "send-keys -X begin-selection"; # "send-keys -X begin-selection";
  V = "send-keys -X select-line";
  C-v = [ "send-keys" "-X" "begin-selection" ";" "send-keys" "-X" "rectangle-toggle" ];
  Escape = [ "send-keys" "-X" "cancel" ";" "display-message" "Exited copy-mode" ];
  C-c = [ "send-keys" "-X" "cancel" ";" "display-message" "Exited copy-mode" ];
  q = {
    action = [ "if-shell" "-F" "#{selection_present}" "send-keys -X clear-selection" "send-keys -X cancel" ];
    note = "Clear current selection if any or abort copy-mode";
  };
  y = [ "send-keys" "-X" "copy-pipe-and-cancel" "xclip -i" ";" "display-message" "copied selection to buffer and clipboard" ];
  C-y = [ "send-keys" "-X" "copy-pipe" "xclip -i" ";" "send-keys" "-X" "clear-selection" ";" "display-message" "copied selection to buffer and clipboard" ];
  # a = "send-keys -X append-selection-and-cancel";
  # c = "send-keys -X copy-selection-and-cancel";
  # Y = "send-keys -X copy-selection";
  # A = "send-keys -X append-selection";

  # move                                                                   {{{2
  o = "send-keys -X other-end"; # other end of current selection
  h = "send-keys -X cursor-left";
  j = "send-keys -X cursor-down";
  k = "send-keys -X cursor-up";
  l = "send-keys -X cursor-right";
  w = "send-keys -X next-word";
  W = "send-keys -X next-space";
  e = "send-keys -X next-word-end";
  E = "send-keys -X next-space-end";
  b = "send-keys -X previous-word";
  B = "send-keys -X previous-space";
  "{" = "send-keys -X previous-paragraph";
  "}" = "send-keys -X next-paragraph";
  K = "send-keys -X scroll-up";
  J = "send-keys -X scroll-down";
  g = "send-keys -X history-top";
  G = "send-keys -X history-bottom";
  C-u = "send-keys -X halfpage-up";
  C-d = "send-keys -X halfpage-down";
  "^" = "send-keys -X back-to-indentation";
  "0" = "send-keys -X start-of-line";
  "$" = "send-keys -X end-of-line";
  H = "send-keys -X top-line";
  L = "send-keys -X bottom-line";
  M = "send-keys -X middle-line";
  ":" = mkPrompt { prompt = "goto-line:"; action = [ "send" "-X" "goto-line" "%%%" ]; };

  Up = "send-keys -X cursor-up";
  Down = "send-keys -X cursor-down";
  Left = "send-keys -X cursor-left";
  Right = "send-keys -X cursor-right";
  PgUp = "send-keys -X page-up";
  PgDn = "send-keys -X page-down";

  # search                                                                 {{{2
  # f/F cursor over target
  # t/T cursor next to target

  "/" = mkPrompt {
    prompt = "/";
    action = [ "send" "-X" "search-forward" "%%%" ];
  };

  "?" = mkPrompt {
    prompt = "?";
    action = [ "send" "-X" "search-backward" "%%%" ];
  };

  f = [ "command-prompt" "-1" "-p" "jump forward" "send -X jump-forward \"%%%\"" ];
  F = [ "command-prompt" "-1" "-p" "jump backward" "send -X jump-backward \"%%%\"" ];
  t = [ "command-prompt" "-1" "-p" "jump to forward" "send -X jump-to-forward \"%%%\"" ];
  T = [ "command-prompt" "-1" "-p" "jump to backward" "send -X jump-to-backward \"%%%\"" ];
  n = "send-keys -X search-again";
  N = "send-keys -X search-reverse";
  ";" = "send-keys -X jump-again";
  "," = "send-keys -X jump-reverse";
} //

  # repeat                                                                 {{{2
(
  let
    go = n: mkPrompt {
      prompt = "repeat: ";
      options = [ "-N" "-I" n ];
      action = "send -N \"%%%\"";
    };
  in
  genAttrs (map toString (range 1 9)) go
)
