{ lib }:

with lib;

rec {
  #####################
  # action builders
  #####################

  # mkPrompt {
  #   prompt = "/";
  #   action = [ "send" "-X" "search-forward" "%%%" ];
  # }
  #   => bind -N "..." -T copy-mode-vi / command-prompt -p / "send -X search-forward \"%%%\""
  mkPrompt =
    a@{ prompt ? ""
    , options ? [ ]
    , action
    , ...
    }:
    actionToString ([ "command-prompt" ] ++ options ++ [ "-p" prompt (actionToString action) ]);

  actionToString = act:
    if isString act then act
    else if isList act then concatStringsSep " " (map quoteArg act) # XXX recursively apply actionToString to elments?
    #else if isAttrs act then ''${act.command_name}'';
    else abort "Don't know how to handle cmd type, cmd: ${toString act}";


  #####################
  # options (set...)
  #####################

  # Write out "set ..." lines
  renderSets = sets: unlines (mapAttrsToList mkSetOpts sets);

  mkSetOpts = flags: opts: mkSection "Options (-${flags})" (renderOptions "set -${flags}" opts);

  renderOptions = set: attrs:
    concatStringsSep "\n" (mapAttrsToList (opt: val: "${set} ${opt} ${renderOptionValue val}") attrs);

  renderOptionValue = val:
    if true == val then "on"
    else if false == val then "off"
    else if null == val then "none"
    else if isString val then quoteDouble val
    else if isInt val then toString val
    else if isAttrs val then concatStringsSep "," (mapAttrsToList (k: v: k + "=" + escapeSpecial v) val)
    else abort "Unknown value type ${toString val}";


  #####################
  # keys (bind...)
  #####################

  # Write out "bind ..." lines
  renderBinds = binds: unlines (mapAttrsToList mkBinds binds);

  mkBinds = table: binds: mkSection "Binds (${table})" (
    mapAttrsToList (key: v: renderBind' ((if isAttrs v then v else { action = actionToString v; }) // { inherit table key; })) binds
  );

  # Render single bind command from pieces
  #
  #   bind [-r] [-N note] [-T table] key command [arguments]
  #
  renderBind' = { table, key, action, repeat ? false, note ? "" }:
    "bind ${concatStringsSep " " (flatten [
      (optional repeat "-r")
      (optional (note != "") [ "-N" (quoteSingle note) ])
      (optional (table != "prefix") [ "-T" (quoteSingle table) ])
      (escapeSpecial key)
      (actionToString action)
    ])}";




  #####################
  # text utilities
  #####################

  mkSection = name: content: ''
    # ${name}
    ${unlines content}
  '';

  escapeSpecial = stringAsChars (c: if hasSpecial c then "\\" + c else c);

  hasSpecial = s: isNull (builtins.match "[a-zA-Z0-9^,.+:/?!=|_-]*" s);

  quoteSingle = s: if hasSpecial s then "'${s}'" else s;
  quoteDouble = s: if hasSpecial s then ''"${escape [ "\"" ] s}"'' else s;
  quoteBraces = s: if hasSpecial s then "{ ${s} }" else s;
  quoteArg = s: if s == ";" then "\\;" else quoteDouble s;

  unlines = xs: concatStringsSep "\n" (flatten xs);


  ###################
  # ???????
  ###################

  /*
    # XXX for rendering the options in a cmd type attrset? don't remember
    renderOpts = opts:
    flatten (mapAttrsToList (n: v: optional (v != false) "-${n}" ++ optional (isString v) v) opts);

    flattenAttrs' = mkName: attrs0: with rec {
    up = fold recursiveUpdate {};
    go0 = path: v: if isAttrs v then up (mapAttrsToList (go path) v) else v;
    go = path: n: v:
      let path' = path ++ [n];
      in if isAttrs v then go0 path' v else setAttrByPath (toList (mkName path')) v;
    }; go0 [] attrs0;

    flattenAttrs = flattenAttrs' last;
  */

}
