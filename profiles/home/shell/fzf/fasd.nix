{ config, pkgs, lib, ... }:

# TODO: this shold be a module instead of profile

with lib;

let
  cfg = config.programs.fasd;

  fasd = pkgs.fasd;

  initHook = pkgs.runCommandLocal "fasd-init" { } "${getExe fasd} --init bash-hook > $out";
in
{
  options.programs.fasd = {
    enable = mkEnableOption "fasd";
  };

  config = mkIf cfg.enable {
    home.packages = [ pkgs.fasd ];

    programs.bash.initExtra = ''
      # fasd --init bash-hook
      ${builtins.readFile initHook}
    '';
  };
}
