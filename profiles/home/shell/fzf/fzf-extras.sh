# shellcheck shell=bash disable=SC2016
# vim: set ft=sh tw=78 et sw=4:

# https://github.com/junegunn/fzf/wiki/Examples

#FZF_FIND="locate -0ir '^/home/'"
# fd <opts> <pat> <path>...
FZF_FIND="fd -E .git"
#FZF_PROC='ps aux | sed 1,1d | grep -v "fzf "'

FZF_MY_MODES=(
    v
    fpro
    fman
    fzf-wiki
    fzf-mpc
    sysz
    tmux-attach-fzf
)

__get_current_line(){
    IFS='[;' builtin read -p "$(tput u7)" -rs -dR _ REPLY _ </dev/tty || return $?
    builtin printf %s\\n "$REPLY"
}

# scroll down scrollback after exit
__fzf_scroll_up(){
    if [[ -n $TMUX ]]; then
        "$1"
        return $?
    fi
    local r1 r2 l1 l2
    l1=$LINES
    r1=$(__get_current_line)
    "$1"
    l2=$LINES
    r2=$(__get_current_line)
    tput -S 2>/dev/null <<-END
	rin $(( r1 - r2 - (l2-l1) ))
	vpa $(( r1 - 1  + (l2-l1) ))
END
}
builtin bind -m emacs-standard -x '"\C-r": __fzf_scroll_up __fzf_history__'
builtin bind -m emacs-standard -x '"\C-t": __fzf_scroll_up fzf-file-widget'

__fzf_cd_custom() {
    local cmd
    cmd=$(__fzf_cd__)
    if [ -n "$cmd" ]; then
        if eval "$cmd"; then
            tput cuu1
            printf '%s' "${PS1@P}" | head -n-1
            #builtin dirs -p +0
        fi
    fi
}
builtin bind -m emacs-standard -x '"\ec": __fzf_cd_custom'

# CTRL-X-1 - Invoke Readline functions by name
__fzf_readline ()
{
    local fun
    fun=$(builtin bind -l | command fzf +s +m --toggle-sort=ctrl-r)
    builtin bind '"\C-x3": '"$fun"
}
builtin bind -x '"\C-x2": __fzf_readline';
builtin bind '"\C-x1": "\C-x2\C-x3"'

# fman - man pages
function fman() {
    man -k . | fzf-tmux -q "$1" --prompt='man> ' \
        --exact \
        --preview $'echo {} | tr -d \'()\' | awk \'{printf "%s ", $2} {print $1}\' | xargs -r man | col -bx | bat -l man -p -f' \
    | tr -d '()' | awk '{printf "%s ", $2} {print $1}' | xargs -r man
}

fself(){
    mode=$(printf %s\\n "${FZF_MY_MODES[@]}" | fzf-tmux) || return
    eval "$mode"
}

# z - fasd cd
z() {
  local dir
  dir="$(fasd -Rdl "$1" | fzf-tmux -1 -0 --no-sort +m)" && cd "${dir}" || return 1
}

# cd to selected parent directory
fcd_parent() {
	local -a dirs=()
	get_parent_dirs() {
		if [[ -d $1 ]]; then dirs+=("$1"); else return; fi
		if [[ $1 == / ]]; then
			printf '%s\n' "${dirs[@]}"
		else
			get_parent_dirs "$(dirname "$1")"
		fi
	}
	local DIR
	DIR=$(get_parent_dirs "$(realpath -q "${1:-$PWD}")" | fzf-tmux --tac)
	cd -- "$DIR" || return
}

# fcd [start-dir [search-pat] | search-pat]
# cd into the directory of the selected file
fcd() {
	local file dir
	dir=$(if [[ -d $1 ]]; then cd -- "$1" || exit; shift; fi; if file=$(fzf-tmux +m -q "$1"); then dirname "$PWD/$file"; fi)
	cd -- "$dir" || return
}

# fvim - browse $PWD for file(s) to open with vim
fvim() {
    local files
    mapfile -t files < <(IFS=$'\n' fzf-tmux --query="$1" --multi --select-1 --exit-0 \
        --preview 'bat -pf --terminal-width=$FZF_PREVIEW_COLUMNS -r :$FZF_PREVIEW_LINES -- {}')
    if [[ "${#files[@]}" -gt 0 ]]; then
        printf "Opening %i files:\n" "${#files[@]}" >&2
        printf "  %s\n" "${files[@]}" >&2
        ${EDITOR:-vim} -p "${files[@]}"
    else
        echo "nothing selected." >&2
    fi
}

__fzf_viminfo_files() {
    local viminfo=~/.local/share/vim/viminfo/viminfo
    local root=''
    if [[ $# -gt 0 ]]; then
        if [ -d "$1" ]; then
            root=$(realpath -qm "$1")
        else
            root=$(realpath -qm "$(dirname "$1")")
        fi
        shift
    fi
    grep "^>" "$viminfo" | cut -c3- |
      while read -r line; do
          file=${line/#\~/$HOME}
          if [ -f "$file" ]; then
            if [[ $file = "$root"* ]]; then
                printf "%s\0" "$(realpath -qm --relative-to="$PWD" "$file")"
            fi
        fi
    done | xargs -0r dir -dhlL --color=always --quoting-style=literal
}
declare -xf __fzf_viminfo_files

# v - open files in viminfo
v() {
    local root=${1:-$PWD/}
    shift 2>/dev/null || true
    local pre1='set -- {9..-2} {-1}; bat -pf --terminal-width=$FZF_PREVIEW_COLUMNS -r :$FZF_PREVIEW_LINES -- "$1${1:+ }$2"'
    local pre2='bat -pf --terminal-width=$FZF_PREVIEW_COLUMNS -r :$FZF_PREVIEW_LINES -- {}'
    FZF_DEFAULT_COMMAND="__fzf_viminfo_files $root" fzf-tmux \
        --multi --ansi --exit-0 --select-1 --keep-right \
        --bind "ctrl-r:reload(__fzf_viminfo_files {q})+clear-query+change-preview($pre1)" \
        --bind "ctrl-f:reload($FZF_FIND --type file . {q})+change-preview($pre2)" \
        --preview "$pre1" \
        --query "$*" \
        --header="cwd: ${root/#"$HOME"/\~}\t[path]<C-r>: cd to [path]" \
        | sed -E 's/^\s*(\S+\s+){8}//' \
        | xargs -o -r -d $'\n' vim
}

_fzf_find_projects(){
    local pwd=${1:-}
    local -a repoDirs=( ~/ )
    listAll () {
        printf "%s\n" ~/
        printenv "${!XDG_@}" | tr : '\n' | xargs -rd $'\n' realpath -qes
        find -L "${repoDirs[@]}" -maxdepth 3 -name .git -printf "%h\n" 2>/dev/null
    }
    echo "Projects: $pwd"
    {
        command grep -Fxf <(listAll) <(fasd -lRd)
        listAll
    } | awk '!x[$0]++' | xargs -rd $'\n' realpath -qsm ${pwd:+--relative-base="${pwd/#\~/"$HOME"}"}
}
declare -xf _fzf_find_projects

# fpro - projects
fpro(){
    local pwd
    pwd=$(builtin dirs -p +0)
    local -a res
    mapfile -t res < <(FZF_DEFAULT_COMMAND="_fzf_find_projects ${pwd@Q}" fzf-tmux \
        --no-sort \
        --cycle \
        --tiebreak=end,begin \
        --filepath-word \
        --expect "ctrl-v,ctrl-r,ctrl-f" \
        --preview "env -C {} git -c color.status=always st 2>/dev/null || dir -hLl --color=always -- {}" \
        --header "[projects] enter: cd  ctrl-v: open vim  ctrl-r: open ranger ctrl-f: open $TERMCMD" \
        --header-lines=1 \
        --prompt "$pwd/" \
        --query "$*")
    local key=${res[0]} dir=${res[1]}
    case $key,$dir in
        '',?*    ) pushd -- "$dir" || return ;;
        ctrl-v,?*) env -C "$dir" vim +NERDTreeCWD ;;
        ctrl-r,?*) ranger "$dir" ;;
        ctrl-f,?*) "${TERMCMD:-kitty}" -1 -T "$dir" -d="$dir" ;;
        *        ) return 2 ;;
    esac
}
