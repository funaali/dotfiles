{ lib, pkgs, ... }:

with lib;

# https://github.com/junegunn/fzf/wiki/Configuring-shell-key-bindings

let
  batPreview = "${getExe pkgs.bat} -f --line-range :200 --";

  #solarized 24bit
  #--color=bg+:#073642,bg:#002b36,spinner:#719e07,hl:#586e75
  #--color=fg:#839496,header:#586e75,info:#cb4b16,pointer:#719e07
  #--color=marker:#719e07,fg+:#839496,prompt:#719e07,hl+:#719e07
  defaultColors = concatStringsSep "," [
    "fg:-1" # 10"
    "bg:-1"
    "preview-fg:-1:dim"
    "preview-bg:-1"
    "hl:white:regular:underline:dim"
    "hl+:white:underline:dim"
    "fg+:-1:bold"
    "bg+:black"
    "query:-1:regular"
    "disabled:-1:dim"
    "gutter:0"
    "info:-1:bold"
    "border:cyan:regular:dim"
    "prompt:yellow"
    "pointer:yellow:bold"
    "marker:magenta:bold:reverse"
    "spinner:red:bold"
    "header:cyan:underline"
  ];

in
{
  imports = [
    ./fasd.nix
  ];

  home.packages = with pkgs; [
    fd   # file listing
    sysz # systemd TUI
  ];

  programs.fasd.enable = mkDefault true;

  programs.bash.initExtra = mkOrder 210 ". ${./fzf-extras.sh}";

  programs.fzf = {
    enable = true;
    enableBashIntegration = true;

    # A default command that respects gitignore.
    defaultCommand = "${getExe pkgs.fd} --type f --strip-cwd-prefix --hidden --follow --exclude .git";
    defaultOptions = [
      #"--no-bold"
      "--color=${defaultColors}"
      "--prompt=' '"
      "--marker='∗'" # '∋'" # >'"
      "--pointer='⊳'" # >'"
      "--bind=change:first"
      "--bind=backward-eof:abort"
      "--bind=ctrl-]:print-query" # ..and exit
      "--bind=ctrl-space:toggle+down"
      "--bind=alt-t:toggle"
      "--bind=alt-*:select-all"
      "--bind=alt-u:deselect-all"
      "--bind=alt-p:toggle-preview"
      "--bind=alt-z:toggle-search"
      "--bind=alt-s:toggle-sort"
    ];

    tmux.enableShellIntegration = true;
    tmux.shellIntegrationOptions = [ "-p90%,90%" ];

    changeDirWidgetCommand = "${getExe pkgs.fd} --type d --strip-cwd-prefix --hidden --follow --exclude .git";
    changeDirWidgetOptions = [ # FZF_ALT_C_OPTS
      "--preview 'tree -C {} | head -200'"
    ];

    # FZF_CTRL_T_OPTS
    fileWidgetCommand = "${getExe pkgs.fd} --type d --strip-cwd-prefix --hidden --follow --exclude .git";
    fileWidgetOptions = [
      "--preview '(${batPreview} -- {} 2> /dev/null || tree -C {}) 2> /dev/null | head -200'"
    ];

    # FZF_CTRL_R_OPTS
    historyWidgetOptions = [
      "--preview 'echo {2..} | ${getExe pkgs.bat} -l bash -pf --file-name {1}'"
      "--preview-window down:3:wrap"
      "--bind '?:toggle-preview'"
    ];
  };
}
