{ lib, pkgs, ... }:

with lib;

{
  config = {
    home.packages = with pkgs; [
      bat
      #vimpager-mine
    ];

    home.sessionVariables = {
      PAGER = "bat";

      # nice colored man pages, from <https://github.com/sharkdp/bat/>
      MANROFFOPT = "-c -P-i";
      MANPAGER = "sh -c 'col -bx | bat -l man --style=header --pager='less -R' -p'";

      #MANROFFOPT = "-C -P-i"; # Enable italics.
    };

    home.shellAliases = {
      bathelp = "bat --plain --language=help";
    };

    programs.bash.bashrcExtra = ''
      # Pass any "help *" through "bat"
      help() {
        "$@" --help 2>&1 | bathelp
      }

      # combine git-diff with bat to view changes around commits.
      batdiff() {
        git diff --name-only --relative --diff-filter=d | xargs bat --diff
      }
    '';
  };
}
