{ lib, pkgs, ... }:

with lib;

{
  imports = [
    ./aliases.nix
  ];

  config = {
    home.packages = with pkgs; [
      which
      pwgen-mine
      scripts
      diff-paths
      ripgrep
    ];

    programs.home-manager.enable = true;

    programs.nix-index = {
      enable = true;
      enableBashIntegration = true;
    };

    # Usage: , <cmd> [<args>...]
    programs.nix-index-database.comma.enable = true;

    home.sessionVariables.GREP_COLORS = "ms=01;33:mc=01;33:sl=:cx=:fn=35:ln=32:bn=32:se=36";

    # <url:man:dialog>
    home.file.".dialogrc".source = ./dialogrc;

    # haskeline settings
    home.file.".haskeline".source = ./haskeline;

    programs.bat = {
      enable = true;
      config = {
        theme = "Solarized (light)";
        style = "changes,header,grid,numbers";
        pager = "less --RAW-CONTROL-CHARS --quit-if-one-screen --mouse";
        italic-text = "always";
      };
    };

    programs.htop.enable = true;

    # editing-mode: emacs(emacs-standard) vi(vi-command)
    # keymap: emacs(emacs-standard) emacs-standard emacs-meta emacs-ctlx
    #         vi(vi-command) vi-command vi-move vi-insert
    # emacs-editing-mode
    # vi-{movement,insertion,editing,append}-mode
    # https://bugzilla.redhat.com/show_bug.cgi?id=1477328
    programs.readline = {
      enable = true;
      variables = {
        bell-style = "none"; # "visible";
        bind-tty-special-chars = false;
        blink-matching-paren = true;
        expand-tilde = true;
        history-preserve-point = true;
        history-size = -1;
        mark-modified-lines = true;
        match-hidden-files = false;
        colored-completion-prefix = true;
        colored-stats = true;
        completion-ignore-case = true;
        completion-map-case = true;
        completion-prefix-display-length = 0; # NOTE: values > 0 are incompatible  with colored-completion-prefix = true
        completion-query-items = 0;
        page-completions = true;
        menu-complete-display-prefix = true;
        print-completions-horizontally = true;
        show-all-if-ambiguous = true;
        show-all-if-unmodified = true;
        skip-completed-text = true;
        mark-directories = true;
        mark-symlinked-directories = true;
        editing-mode = "emacs";
        #keymap = "emacs-meta";
        keyseq-timeout = 1500; # 500;
        enable-bracketed-paste = true;
        enable-keypad = true;
        convert-meta = false; # NOTE: Meta-/\M- won't work if "convert-meta" is off, and locale is other than C (UTF-8)
        input-meta = true;
        output-meta = true;
        enable-meta-key = true;
        show-mode-in-prompt = true;
        emacs-mode-string = ''\1\e[1;35m\2@\1\e[0m\2''; #
        vi-cmd-mode-string = ''\1\e[0;34m\2C\1\e[0m\2''; #
        vi-ins-mode-string = ''\1\e[0;33m\2I\1\e[0m\2''; #
        revert-all-at-newline = true;
      };

      extraConfig = builtins.readFile ./inputrc;
    };

    programs.dircolors = {
      enable = true;
      enableBashIntegration = true;
      settings = lib.mkForce {
        NORMAL = "00;38;5;14";
        FILE = "00;38;5;12";
        LINK = "00;38;5;37";
        MULTIHARDLINK = "01;04;38;5;12";
        FIFO = "48;5;235;38;5;161";
        SOCK = "48;5;235;38;5;161;1";
        DOOR = "48;5;235;38;5;161;1";
        # audio
        ".flac" = "00;38;5;166";
        # text
        ".tt" = "01;38;5;245";
        ".markdown" = "01;38;5;245";
        ".html" = "01;38;5;245";
        ".cabal" = "01;38;5;245";
        "*stack.yaml" = "01;38;5;245";
        "*LICENSE" = "01;38;5;245";
        ".wmd" = "00;38;5;15";
        # source code
        ".vim" = "00;38;5;29";
        ".sh" = "00;38;5;29";
        ".lua" = "00;38;5;29";
        ".lhs" = "00;38;5;29";
        ".hs" = "00;38;5;29";
        ".csh" = "00;38;5;29";
        # unimportant
        ".lock" = "00;38;5;240";
        # misc.
        ".torrent" = "00;38;5;104";
        ".pdf" = "00;38;5;104";
      };
      extraConfig = builtins.readFile "${pkgs.dircolors-solarized}/share/dircolors/solarized/dircolors.256dark";
    };

    programs.ranger = {
      #  pydoc ranger.api
      enable = true;

      settings = {

        # Colors and UI
        colorscheme = "solarized";
        unicode_ellipsis = true;
        status_bar_on_top = false;
        dirname_in_tabs = true;

        # Window title WM_NAME, WM_ICON_NAME
        update_title = true;
        update_tmux_title = true;

        # Abbreviate $HOME as ~
        tilde_in_titlebar = true;

        # VCS awareness
        vcs_aware = true;
        vcs_backend_git = "local";

        # Sorting
        sort = "basename";
        sort_unicode = true;

        # Previews
        preview_images = true;
        wrap_plaintext_previews = true;

        # Misc.
        wrap_scroll = true;
        cd_tab_case = "insensitive";
        cd_tab_fuzzy = true;

        # How many directory-changes or console-commands should be kept in history?
        max_history_size = 5000;
        max_console_history_size = 5000;
      };

      mappings = {
        "~"  = "cd ~";
        "dD" = "delete";
      };

      extraConfig = ''
        # Disable VCS awareness for large repos (better responsiveness)
        setlocal path='~/Music' vcs_aware false
      '';
    };

    xdg.mimeApps.defaultApplications = {
      # directories
      "inode/directory" = [ "ranger.desktop" ];
    };
  };
}
