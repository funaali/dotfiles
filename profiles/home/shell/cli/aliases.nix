{ lib, ... }:

with lib;

{
  config = {
    # shell-agnostic
    home.shellAliases = {
      ls = "ls --color=auto --group-directories-first --hyperlink=auto";
      lss = "ls -L";
      ll = "ls -lh";
      la = "ls -Ah";
      lla = "ls -lhA";
      llt = "ls -lht";
      llZ = "ls -lhZ --author";
      "ll." = "ls -lhd .*";
      mkdir = "mkdir -p";
      df = "df -Th -x devtmpfs --total";
      dus = "du -xh -d 1 | sort -h";
      findexts = "find -L . -type f -printf '%f\\n' | grep -o '[[:alnum:]]*$' | sort -u";

      # cd
      "-- -" = "cd -";
      "..." = "cd ../..";
      "...." = "cd ../../..";
      "cd." = "cd ..";
      "cd.." = "cd ../..";
      "cd..." = "cd ../../..";
      # cdR: cd upwards to the root of a git repository.
      cdR = ''cd "$(git rev-parse --show-superproject-working-tree)"'';

      # SYSTEM                                                            {{{2
      # systemd (system instance)
      journalctl = "journalctl -oshort-iso --no-hostname -b -q";
      # systemd (user instance)
      userctl = "systemctl --user";
      userjournal = "journalctl --user";
      # various
      ip = "ip -c=auto";
      mnt = "mount | tabulate";
      pacman = "sudo pacman";

      # DEV & OPS                                                         {{{2
      g = "git";
      gax = "git annex";
      ap = "ansible-playbook";
      tf = "terraform";
      tg = "terragrunt";
      pip = "python -m pip"; # https://snarky.ca/why-you-should-use-python-m-pip/
      doco = "docker compose";

      # Tmux
      atta = "tmux new-session -A -s"; # attach tmux session, create if it doesn't exist yet. "atta <session-name>"

      # mpc
      np = "mpc status";
      t = "mpc toggle";
      pnext = "mpc next";
      pprev = "mpc prev";

      # curl
      # - curl allows dy default HTTP,HTTPS,FTP and FTPS only since 7.65.2
      # - FILE and SCP are disabled since 7.19.4
      # - SMB and SMBS are disabled since 7.19.4
      #
      # --proto-default <protocols> tells the protocol to use when non is specified (since curl 7.45.0)
      # --proto <protocols>         limits which protcols may be used in transit
      # --proto-redir <protocols>   this option only decidedes which procotols are allowed as redirect targets {HTTP,HTTPS,FTP,FTPS by default)
      curl = "curl -j --http2 --keepalive-time 0 --proto-default https";
      myip = "myip6; myip4";
      myip4 = "curl -4fsSN https://icanhazip.com";
      myip6 = "curl -6fsSN https://icanhazip.com";

      # Misc.
      grep = "grep --color=auto";
      open = "xdg-open";
      feh = "feh --scale-down";
      hm = "home-manager --flake \"$(readlink -f ~/dotfiles)\" --impure";
      birthdays = "khal list -a birthdays -df '' -f '{calendar-color}{start}{reset} {title}' today 366d";
      ncdu = "ncdu -xL --color dark";
      snapper = "snapper -t 6";
      icat = "kitty +kitten icat";
    };
  };
}
