{ config, pkgs, ... }:

{
  config = {
    custom.defaultApplications.editor = [ "vim" "gvim" ];

    sim.repos.vimfiles = {
      path = "${config.home.homeDirectory}/.vim";
      uri = "https://gitlab.com/funaali/vimfiles.git";
    };

    home.packages = with pkgs; [
      my-vim
      # TODO: use this? vim-jetpack
      #vim-configured
      ##### stuff for plugins
      ctags
      cmake
      yapf # python
      pylint # python
      vim-vint # vim
      yamllint # yaml
      proselint # markdown
      gnumake # hexokinase (build)
      go # hexokinase (build)
      gcc # leaderf c-extension
    ];

    home.sessionVariables.EDITOR = "vim";
  };
}
