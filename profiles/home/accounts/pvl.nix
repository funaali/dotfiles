{ ... }:

let
  pass-name = "web/paivola.fi";
in
{
  accounts.email.accounts.pvl = {
    address = "samuli.thomasson@paivola.fi";
    aliases = [ "simsaladin@paivola.fi" ];
    realName = "Samuli Thomasson";
    userName = "simsaladin"; # XXX pass show web/paivola.fi/username
    primary = true;
    passwordCommand = "pass show-field -- ${pass-name} 2>/dev/null";
    maildir = null;
    folders.inbox = "INBOX";
    imap.host = "mail.paivola.fi";
    imap.port = 993;
    smtp.host = "smtp.paivola.fi";
    smtp.port = 587;
    smtp.tls.useStartTls = true;
    smtp.tls.enable = false; # NOTE: important to use smtp://, like disabling this does (get ssl errors otherwise)
    neomutt.enable = true;
    neomutt.mailboxType = "imap";
    neomutt.mailboxName = "====== PVL    ======";
    # named-mailboxes "====== PVL    ======" "$folder"
    neomutt.extraConfig = ''
      reset imap_authenticators
      set smtp_authenticators=login
      set copy=yes
    '';
  };
}
