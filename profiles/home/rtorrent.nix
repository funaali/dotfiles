{ pkgs, ... }:

{
  config = {

    programs.rtorrent-ps = {
      enable = true;
      baseDir = "~/rtorrent-ps";
      package = pkgs.rtorrentPSPackages."PS-1.1-71-gee296b1".rtorrent-ps;
    };

    #services.rtorrent-ps.enable = true;
  };
}
