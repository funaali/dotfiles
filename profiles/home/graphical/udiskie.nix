{ ... }:

{
  config = {
    services.udiskie = {
      enable = true;
      automount = false;
      notify = true;
      tray = "auto";
      settings = {
        program_options = {
          password_cache = 180; # Minutes.
          file_manager = "xdg-open";
          terminal = "my-terminal -d";
          password_prompt = "builtin:gui";
          event_hook = [ "notify-send" "--" "udiskie" "{event}: {device_presentation}" ];
        };
        device_config = [
          { is_systeminternal = true; ignore = true; }
        ];
        notifications = {
          timeout = 10; # Default timeout for all notifications.
        };
      };
    };
  };
}
