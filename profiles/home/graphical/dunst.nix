{ config, lib, pkgs, ... }:

let
  S = config.lib.colors.solarized;

  iconThemes = [
    config.gtk.iconTheme.name
    "breeze-dark"
    "hicolor"
  ];

  defFormat = lib.replaceStrings [ "\n" ] [ "\\n" ] ''
    <span weight="bold" size="larger">%s %p</span>

    %b
    <span font_family="monospace" size="smaller" line_height="1.5">%a</span>
  '';
in
{
  config = {
    home.packages = [ pkgs.dunst-sounds ];

    services.dunst = {
      enable = true;

      settings.global = {
        enable_posix_regex = true; # Enable POSIX EREs in filtering rules that match strings.

        ### Display ###
        follow = "mouse"; # "keyboard"
        width = "(80, 400)";
        height = 1200;
        origin = "top-right";
        offset = "8x20"; # horizontal x vertical
        notification_limit = 8;
        text_icon_padding = 8;
        horizontal_padding = 12;
        frame_width = 2;
        frame_color = S.base01;
        separator_color = "auto";
        idle_threshold = "1m";
        corner_radius = 5;

        ### Text ###
        font = "Noto Sans 8";
        markup = "full";
        format = defFormat;
        show_age_threshold = 30;
        ignore_newline = false;
        stack_duplicates = true;
        hide_duplicate_count = false;

        ### Icons ###
        # Default: left
        icon_position = "right";
        # Enable new recursive lookup in XDG_DATA_DIRS
        enable_recursive_icon_lookup = true;
        # icon theme names to look into (first is most relevant)
        icon_theme = lib.concatStringsSep ", " iconThemes;
        # ensure old lookup is disabled
        icon_path = lib.mkForce "";
        # min: With recursive lookup, all icons looked for are of this size.
        min_icon_size = 48;
        # max: Enables downscaling of icons larger than this
        max_icon_size = 128;

        ### History ###
        sticky_history = true;
        history_length = 20;

        ### Misc/Advanced ###
        dmenu = "${pkgs.rofi}/bin/rofi -p dunst:";
        browser = "xdg-open";
        always_run_script = true;

        ### mouse
        mouse_left_click = "close_current";
        mouse_middle_click = "do_action";
        mouse_right_click = "context";
      };

      settings.urgency_low = {
        background = S.base03;
        foreground = S.base1;
        frame_color = S.color10;
        timeout = 10;
      };

      settings.urgency_normal = {
        background = S.base02;
        foreground = S.base1;
        frame_color = S.cyan;
        timeout = 10;
      };

      settings.urgency_critical = {
        background = "#900000"; # XXX
        foreground = S.base2;
        frame_color = S.red;
        timeout = 30;
      };

      settings.transient_history_ignore = {
        match_transient = true;
        history_ignore = true;
      };

      settings.sounds-email-arrived = {
        category = "email.arrived";
        script = "${pkgs.dunst-sounds}/libexec/dunst-sounds/message";
      };

      #svg_icon_size = {
      #  icon = "\\.svg$";
      #  icon_size = 64;
      #};
    };
  };
}
