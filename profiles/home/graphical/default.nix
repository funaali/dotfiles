{ lib, pkgs, ... }:

with lib;

let
  windowManagerUnit = "my-desktop.target";
in

{
  imports = [
    ./urxvt.nix
    ./kitty.nix
    ./qutebrowser
    ./rofi
    ./dunst.nix
    ./udiskie.nix
  ];

  config = {
    custom = {
      docs.enable = true;
    };
    sim.graphical = {
      urxvt.enable = mkDefault true;
      kitty.enable = mkDefault true;
      qutebrowser.enable = mkDefault true;
      rofi.enable = mkDefault true;
    };

    # Graphical session startup
    programs.bash.profileExtra = mkOrder 1600 "my-startx --auto";

    services.barrier.server.package = pkgs.barriersXTRA;

    services.clipmenu = {
      enable = true;
      launcher = "rofi";
    };

    xsession = {
      enable = true;

      # Variables imported to the systemd (user) session.
      importedVariables = [
        "PATH"
        "NIX_SSL_CERT_FILE"
        "XDG_SEAT"
        "XDG_VTNR"
        "XINITRC"
        "XSERVERRC"
      ];

      # This goes to ~/.xprofile. Sourced at the start of ~/.xsession.
      profileExtra = ''
        # This is not set properly otherwise.
        systemctl --user set-environment XDG_SESSION_TYPE=x11
      '';

      # This goes to ~/.xsession. Run before starting the window manager.
      initExtra = ''
        # Ensure any old session is stopped first.
        systemctl --user --wait stop ${windowManagerUnit}

        # Reset state of all user units so they may be started cleanly
        # automatically.
        systemctl --user --wait reset-failed

        dbus-update-activation-environment DISPLAY XAUTHORITY
      '';
      windowManager.command = ''
        systemctl --user --wait start ${windowManagerUnit}
      '';
    };

    services.autocutsel = {
      enable = true;
      selections = [ "clipboard" "primary" ];
    };

    services.gpg-agent.pinentryPackage = pkgs.pinentry-qt.overrideAttrs { meta.mainProgram = "pinentry"; };

    systemd.user.targets.my-desktop = {
      Unit = {
        Description = "My Desktop Session";
        # Units that don't need a graphical session but want started.
        Wants = [ "user-session.target" ];
        # Units to ensure before starting the graphical session.
        Requires = [ "graphical-session-pre.target" ];
        # Ensure graphical session (X11) at all times.
        BindsTo = [ "graphical-session.target" ];
        #
        After = [ "graphical-session-pre.target" "graphical-session.target" ];
        # Units that need a graphical session.
        Upholds = [ "xmonad.service" ];
      };
    };

    systemd.user.services.xmonad = {
      Unit = {
        Description = "XMonad tiling window manager (xinit)";
        PartOf = [ "graphical-session.target" ];
        After = [ "graphical-session-pre.target" ];
      };
      Service = {
        Slice = "x11-xmonad.slice";
        ExecStart = "/bin/sh -c 'exec \"$XMONAD_CACHE_DIR/xmonad-$(uname -m)-linux\"'";
        #ExecReload=/bin/sh -c '"$XMONAD_CACHE_DIR/xmonad-$(uname -m)-linux" --restart'
        Environment = [
          "XMONAD_CONFIG_DIR=%E/%p"
          "XMONAD_CACHE_DIR=%C/%p"
          "XMONAD_DATA_DIR=%C/%p"
          "PASSWORD_STORE_ENABLE_EXTENSIONS=true"
        ];
        Restart = "on-failure";
        SyslogIdentifier = "%P";
        #StandardError=journal
        IgnoreSIGPIPE = true;
        SendSIGHUP = true;
        LimitNOFILE = 1048576;
      };
    };
  };
}
