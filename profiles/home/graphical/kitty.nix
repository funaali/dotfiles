{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.sim.graphical.kitty;
  cmd = "kitty";

  solarized = config.lib.colors.solarized;
in

{
  options.sim.graphical.kitty.enable = mkEnableOption "sim.graphical.kitty";

  config = mkIf cfg.enable {
    programs.kitty = {
      enable = true;
      font.name = "Terminess Powerline";
      font.size = 9;
      settings = {
        # Only configure colors here (in order to use the solarized constants)
        url_color = solarized.magenta;
        url_style = "curly";

        #cursor_text_color

        visual_bell_color = "#222222";
        bell_border_color = solarized.orange;
        #command_on_bell

        cursor = solarized.base01;
        cursor_text_color = "background";

        selection_foreground = solarized.base02;
        selection_background = solarized.base01;

      };
      extraConfig = lib.mkMerge [
        # Set theme
        "include ${pkgs.kitty-themes}/share/kitty-themes/Solarized_Dark_-_Original.conf"
        # Customizations directly in the kitty config file format
        (builtins.readFile ./kitty.conf)
      ];
    };

    # XXX: kitty sets the KITTY_INSTALLATION_DIR on startup but it isn't propagated
    # to subshells...
    # TODO: apparently in 2.43.3 this will no longer be necessary (shell
    # integration is enabled automatically).
    programs.bash.initExtra = ''
      # Avoid double-sourcing (it complains)
      if [[ $TERM = *kitty ]] && [[ ! -v _ksi_prompt[ps1_suffix] ]]; then
          export KITTY_SHELL_INTEGRATION="no-rc"
          for p in "$'' + ''{KITTY_INSTALLATION_DIR:-/usr/lib/kitty}/shell-integration" ${pkgs.kitty.shell_integration}; do
              if test -r "$p/bash/kitty.bash"; then
              . "$_"
              break
              fi
          done
      fi
    '';

    home.sessionVariables.TERMCMD = cmd;
    systemd.user.sessionVariables.TERMCMD = cmd;
  };
}
