{ config, lib, pkgs, ... }:

with lib;

# Fonts
#
# <url:man:xlsfonts>
# <url:man:fc-lists(1)>
#
# XXX: 12px terminus does not exist in bold
#
# URxvt font configuration notes:
#
# By default, urxvt synthesizes different styles from the main font.
# For smaller sizes, all faces are not available, so all is explicit to make changing between font sizes possible.
#  * Test fonts with
#    $ printf '\e]710;%s\007' "xft:Terminess Powerline:pixelsize=12"
#    $ terminal-font-test
#  * Use `fc-list "Font Name"` to find exact matches for a font.
#    Remember to `fc-list -vf` and start a new terminal when testing just
#    installed fonts.
#  * mixed-width fonts, for example noto by google, are not handled
#  * font set codes
#       710 = 50 = normal fontset
#       711 = bold fontset
#       712 = italic fontset
#       713 = bold italic fontset
#       704 = colour of italic (and bold italic) characters
#
#  * The community/terminus-font provided font is pretty bad. Terminess
#    Powerline works much better via Xft.
#  * "Terminess Powerline" available in px size: 12, 14, 16, 18, 20, 22, 24 28, 32
#  * "WenQuanYi Micro Hei" makes sense from about 22px upwards.
#    XXX: in 12px, WQI symbols cut off by one px at bottom
#
# ISO 14755 5.1 press & hold Control + Shift, enter hex digits (separate many with Space), release modifiers
# iSO 14755 5.2 (Keyboard symbols entry)
let
  cfg = config.sim.graphical.urxvt;

  solarized = config.lib.colors.solarized;

  get_fontstring = px: style:
    let
      family_str = if px <= 18 then "xos4 Terminess Powerline" else "TerminessTTF Nerd Font Mono";
      size_str = ":pixelsize=${toString px}";
      style_str = if style == "Normal" then "" else ":style=${style}";
      subs_str = ",[codeset=JISX0208]xft:${if px <= 20 then "WenQuanYi WenQuanYi Bitmap Song" else "WenQuanYi Micro Hei"}";
    in
    "xft:${family_str}${size_str}${style_str}${subs_str}";
  set_font = ts: fn: "\\033]${ts};${fn}\\007";
  set_font_styles = n: b: i: bi: "${set_font "710" n}${set_font "711" b}${set_font "712" i}${set_font "713" bi}";
  set_fonts = px:
    "command:" + set_font_styles (get_fontstring px "Normal") (get_fontstring px "Bold") (get_fontstring px "Italic") (get_fontstring px "Bold Italic");
in
{
  options.sim.graphical.urxvt.enable = mkEnableOption "sim.graphical.urxvt";

  config = mkIf cfg.enable {
    systemd.user.sessionVariables.RXVT_SOCKET = "$XDG_RUNTIME_DIR/urxvt/urxvtd";

    programs.urxvt = {
      enable = true;
      package = pkgs.dummy;
      scroll.bar.enable = false;
      iso14755 = false;
      extraConfig = {
        iso14755_52 = false;
        urgentOnBell = true;
        bell-command = "dunstify -a urxvt -i utilities-terminal -c Application,TerminalEmulator URxvt Bell";
        letterSpace = 0;
        lineSpace = 0;
        skipBuiltinGlyphs = true;
        meta8 = true; # Meta (Alt) + keypress sets the 8th bit instead (default Esc prefix is used).
        pastableTabs = true; # store tabs as actual tab characters (true)
        cursorBlink = false;
        pointerBlank = true;
        tripleclickwords = true;
        print-pipe = "cat >/dev/null";
        insecure = true; # Enable "insecure" escape sequences: display-answer, locale, findfont, icon label, window title
        modifier = "alt";
        perl-ext-common = "default,url-select,clipboard,searchable-scrollback,keysym-list,bell-command";
        #perl-lib:
        #perl-eval:
        # backspacekey ~ stty erase
        # use ^H:  stty erase ^H; printf '\e[?67h'
        # use ^?:  stty erase ^?; printf '\e[?67l'
        backspacekey = "\\010";
        deletekey = "\\033[3~";
        # ext: url-select
        "url-select.autocopy" = true;
        "url-select.button" = 1;
        "url-select.launcher" = "xdg-open";
        "url-select.underline" = true;
        # colors
        background = solarized.base03;
        foreground = solarized.base1;
        cursorColor = solarized.cyan;
        cursorColor2 = "#FFFFFF";
        pointerColorBackground = solarized.base01;
        pointerColor2 = solarized.base01;
        pointerColorForeground = solarized.base1;
        pointerColor = solarized.base1;
        underlineColor = "#93a1a1";
        # selection
        cutchars = ''`&()*,;<=>?@[]^{|}\040\042\047\057\072\134│'';
        "selection.pattern-0" = let chars = "&()*,;<=>?@[\\\\\\\\]^`{|│}\\034\\039│"; in "([^${chars}]+)";
        #selection.pattern-0: ( at .*? line \\d+)
        #selection.pattern-1: ^(/[^URXVT_cutchars]+)
        #selection-autotransform.0: s/^([^:[:space:]]+):(\\d+):?$/:e \\Q$1\\E\\x0d:$2\\x0d/
        #selection-autotransform.1: s/^ at (.*?) line (\\d+)$/:e \\Q$1\\E\\x0d:$2\\x0d/
        # fonts
        font = get_fontstring 18 "Normal";
        boldFont = get_fontstring 18 "Bold";
        italicFont = get_fontstring 18 "Italic";
        boldItalicFont = get_fontstring 18 "BoldItalic";
      };
      keybindings = {
        # Home/End
        Home = "\\033[7~";
        End = "\\033[8~";
        KP_Home = "\\033[7~";
        KP_End = "\\033[8~";
        # xterm-style modifyOtherKeys: \e[27;<modifiers>;<keycode>~
        # For use in VIM, see <url:vimhelp:modifyOtherKeys>
        #  Shift  1
        #  Control  2
        #  Meta  4
        #  Super  8
        #  Hyper  16
        #  Alt  32
        # XXX: incomplete; could be generated.
        C-Tab = "\\033[27;2;9~";
        S-C-Tab = "\\033[27;3;9~";
        M-Tab = "\\033[27;4;9~";
        I-Tab = "\\033[27;4;9~";
        C-BackSpace = "\\033[27;2;8~";
        C-Delete = "\\033[27;2;127~";
        # ext: url-select
        M-u = "perl:url-select:select_next";
        # misc.
        M-g = "string: | grep\\040";
        M-n = "string: >/dev/null 2>&1";
        M-h = "string: | head\\040";
        # Change font
        M-C-a = set_fonts 12;
        M-C-b = set_fonts 14;
        M-C-c = set_fonts 16;
        M-C-d = set_fonts 18;
        M-C-e = set_fonts 20;
        M-C-f = set_fonts 22;
        M-C-g = set_fonts 24;
        M-C-h = set_fonts 28;
        M-C-i = set_fonts 32;
      };
    };

    # C-Left, C-Right
    programs.readline.extraConfig = ''
      $if term=rxvt
      "\eOc": forward-word
      "\eOd": backward-word
      $endif
    '';
  };
}
