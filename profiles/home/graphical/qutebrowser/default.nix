{ lib, config, pkgs, ... }:

with lib;

let
  cfg = config.sim.graphical.qutebrowser;

  qute-pass = pkgs.runCommand "qute-pass"
    { nativeBuildInputs = [ pkgs.python3Packages.wrapPython ]; }
    ''
      buildPythonPath ${pkgs.python3.pkgs.tldextract}
      install -vD -m 0755 ${./userscripts/qute-pass} $out
      patchPythonScript $out
    '';

in
{
  options.sim.graphical.qutebrowser = {
    enable = mkEnableOption "sim.graphical.qutebrowser";
    package = mkOption {
      type = types.package;
      default = pkgs.qutebrowser;
    };
  };

  config = mkIf cfg.enable {
    custom.defaultApplications.browser = "org.qutebrowser.qutebrowser.desktop";

    xdg.configFile."qutebrowser/userscripts/qute-pass" = {
      source = "${qute-pass}"; # ./userscripts/qute-pass;
    };

    programs.qutebrowser = {
      enable = true;

      package = pkgs.runCommand "qutebrowser-wrapped"
        { nativeBuildInputs = [ pkgs.makeWrapper ]; }
        ''
          install -vD -m 0755 ${./qutebrowser} $out/bin/qutebrowser
          wrapProgram $out/bin/qutebrowser \
            --set QUTEBROWSER_PATH "${cfg.package}/bin:/usr/bin"
          cp -r ${cfg.package}/share $out/share || :
        '';


      settings = {
        content = {
          prefers_reduced_motion = true;
          dns_prefetch = false;
          default_encoding = "utf-8";
          plugins = false;
          pdfjs = true;
          mute = true;
          autoplay = false;
          webgl = true;
          notifications.enabled = false;
          webrtc_ip_handling_policy = "default-public-and-private-interfaces";
          tls.certificate_errors = "ask-block-thirdparty";
          local_content_can_access_remote_urls = true;
          print_element_backgrounds = true;
          geolocation = "ask";
          headers.accept_language = "en-US,en;q=0.5,fi-FI,fi;q=0.5";
          headers.user_agent = "Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {qt_key}/{qt_version} {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}";
          cache = {
            maximum_pages = 25;
            size = 6442450944; # (6GB) bytes, default 2GB
          };
          blocking = {
            enabled = true;
            # NOTE: Requires python-adblock to be available
            method = "adblock";
            adblock.lists = [
                "https://easylist.to/easylist/easylist.txt"
                "https://easylist.to/easylist/easyprivacy.txt"
                "https://easylist.to/easylistgermany/easylistgermany.txt"
                "https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt"
                "https://secure.fanboy.co.nz/fanboy-annoyance.txt"
                "https://www.i-dont-care-about-cookies.eu/abp/"
            ];
          };
          # Allow websites register protocol handlers (navigator.registerProtocolHandler)
          register_protocol_handler = "ask"; # true / false / ask
          fullscreen.window = true; # Limit fullscreen to the window
          fullscreen.overlay_timeout = 700; # ms
          javascript = {
            can_open_tabs_automatically = true;
          };
        };

        colors = {
          webpage.preferred_color_scheme = "dark";
        };

        scrolling.smooth = true;

        input.forward_unbound_keys = "all";
        input.insert_mode = {
          auto_enter = true; # enter insert on editable focus
          auto_leave = true; # exit insert on non-editable focus
          auto_load = false; # focus editable elem after page load
          leave_on_load = true;
          plugins = true; # focusing flash/plugins turns on insert
        };

        completion = {
          height = "70%";
          scrollbar.width = 16;
          min_chars = 2;
          delay = 200; # msec
          shrink = true;
        };

        downloads = {
          location.directory = "~/Downloads";
          location.suggestion = "both";
          position = "top";
          remove_finished = 600000; # ms; 10min
        };

        tabs = {
          position = "left";
          show = "multiple"; # 'always' / 'never' / 'multiple' / 'switching'
          show_switching_delay = 4500;
          width = "15%";
          last_close = "close";
          # Values: blank / close / default-page / ignore / startpage
          select_on_remove = "prev";
          # Size of per window retained closed stacks.
          undo_stack_size = 10;
          # middleclick/ctrl+click opens tab in the background
          background = true;
          new_position.stacking = true;
          new_position.related = "next"; # first / last / next / prev
          new_position.unrelated = "last";
          title.alignment = "left"; # left / right / center
          title.format = "{audio}{index:>2}:{current_title}";
          title.format_pinned = "{audio}{index:>2}:{current_title}";
          favicons.scale = 1.0;
          favicons.show = "always"; # always / never / pinned
        };

        url = {
          auto_search = "naive"; # 'dns'
          start_pages = ["about:blank"]; # DDG by default
          default_page = "about:blank"; # DDG by default
          open_base_url = true;
        };

        # how to open links in existing instance, when link opened from outside that instance.
        # Values: tab / tab-bg / tab-silent / tab-bg-silent / window
        new_instance_open_target = "tab-silent";
        # Which window to choose when opening links as new tabs (when new_instance_open_target != "window").
        # Values: first-opened / last-opened / last-focused / last-visible
        new_instance_open_target_window = "last-visible";
        history_gap_interval = 180; # mins
        confirm_quit = [ "multiple-tabs" "downloads" ];
        messages.timeout = 6000; # msecs, clear this old messages
        auto_save.session = true; # Restore open sites when started.
        auto_save.interval = 15000; # msec
        search.wrap = false;
        # note, spellcheck requires qtwebkit-plugins package installed.
        spellcheck.languages = [ "de-DE" "en-GB" "en-US" ];
        session.lazy_restore = true;
        # Enable HighDPI scaling
        qt.highdpi = true;
      };

      aliases = {
        dictcli =
          let
            dictcliPy = pkgs.fetchurl {
              url = "https://raw.githubusercontent.com/qutebrowser/qutebrowser/master/scripts/dictcli.py";
              executable = true;
              sha256 = "sha256-JkfXOSZ2+1ps3oJBRdHfsKz7zg4Wmzg5j9W3qM6DFLU=";
            };
          in
          "spawn --output ${dictcliPy}";
        open-mpv = "spawn mpv \\\"{url}\\\"";
      };

      extraConfig = lib.concatMapStringsSep "\n" builtins.readFile [
        ./config.py
        ./colors.py
      ];

      #searchEngines = {
      # TODO
      #  w = "https://en.wikipedia.org/wiki/Special:Search?search={}&go=Go&ns0=1";
      #
      #  user stylesheets:/col
      #  https://github.com/alphapapa/solarized-everything-css/
      #};
    };

    xdg.configFile."qutebrowser/my_util.py".source = ./my_util.py;
  };
}
