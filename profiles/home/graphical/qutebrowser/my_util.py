# pylint: disable=line-too-long,missing-module-docstring
import os
#from qutebrowser.api import cmdutils, message
#from qutebrowser.misc import utilcmds, objects

def is_windows():
    return os.environ.get('OS') == 'Windows_NT'

def session_name():
    return os.environ.get("QUTE_SESSION", "default")

def is_highdpi():
    # TODO figure this out automatically
    return True
