# vim: set fdm=marker:
# pylint: disable=line-too-long,missing-module-docstring
from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer  # noqa: F401
from qutebrowser.api import cmdutils, message
from qutebrowser.misc import utilcmds, objects
import my_util

config: ConfigAPI = config  # noqa: F821 pylint: disable=used-before-assignment
c: ConfigContainer = c  # noqa: F821 pylint: disable=used-before-assignment
SESSION_NAME = my_util.session_name()

# Proxy settings                                                         {{{1
if SESSION_NAME == 'tor':
    c.content.proxy = 'socks5://localhost:9050'
elif SESSION_NAME == 'i2p':
    c.content.proxy = 'socks5://localhost:4447'

# GNU Pass bindings                                                      {{{1
c.aliases['pass'] = ('spawn --userscript qute-pass' +
    ' -d "rofi -dmenu"' +
    ' -m -U secret' +
    ' -u "username: (.+)" -P "(.+)"' +
    ' --prefer-prefix "{SESSION_NAME}/"')

config.bind('zl', 'pass')
config.bind('zul', 'pass --username-only')
config.bind('zpl', 'pass --password-only')
config.bind('zol', 'pass --otp-only')

# Misc. bindings                                                         {{{1

# bind 'ek' - Remove overlay elements (popup whatnot shite)
# From https://www.reddit.com/r/qutebrowser/comments/mnptey/getting_rid_of_cookie_consent_barspopups/
config.bind('ek', 'jseval (function () { '+
'  var i, elements = document.querySelectorAll("body *");'+
''+
'  for (i = 0; i < elements.length; i++) {'+
'    var pos = getComputedStyle(elements[i]).position;'+
'    if (pos === "fixed" || pos == "sticky") {'+
'      elements[i].parentNode.removeChild(elements[i]);'+
'    }'+
'  }'+
'})();')

# 'zv' - open current url in mpv
config.bind('zv', 'open-mpv')

# Download the current website in MHTML format
config.bind(',sm', 'download --mhtml')

# Register cmd ':set-log-capacity'                                       {{{1
if 'set-log-capacity' not in objects.commands:
    @cmdutils.register(name='set-log-capacity')
    def set_log_capacity(capacity=300):
        """Log lines to keep in RAM
        Set to 0, so that passwords from `qute-pass` aren't persisted.
        Note: also clears the log.
        """
        utilcmds.log_capacity(capacity)

    # Set initial value
    set_log_capacity(0)

# Register cmd ':pyeval'                                                 {{{1
if 'pyeval' not in objects.commands:
    @cmdutils.register(name='pyeval', maxsplit=0, no_cmd_split=True)
    def pyeval(string, file=False, quiet=True):
        """Evaluate a python string and display the results as a web page.

        Args:
            string: The string to evaluate.
            file: Interpret s as a path to file, also implies --quiet.
            quiet: Don't show the output in a new tab.
        """
        utilcmds.debug_pyeval(string, file, quiet)

# Register cmd ':session-name'                                           {{{1
if 'session-name' not in objects.commands:
    @cmdutils.register(name='session-name')
    def session_name():
        """Show the current QUTE_SESSION.
        """
        message.info(f'SESSION_NAME: {my_util.session_name()}')

# Window settings                                                        {{{1

c.window.hide_decoration = not my_util.is_windows()
c.window.title_format = '{private}{current_title}{title_sep}{current_url}' + f' [{SESSION_NAME}]'

# Statusbar options                                                      {{{1

# Show the session name in statusbar
c.statusbar.widgets = ["keypress", "url", "scroll", "history", "tabs", "progress", f'text:[{SESSION_NAME}]']

# Editor command                                                         {{{1

if not my_util.is_windows():
    c.editor.command = [
        'urxvtc', '-name', 'info-terminal', '-geometry', '100x40',
        '-e', 'vim', '-f', '{file}', '-c', 'normal {line}G{column0}l']

# Protocol handlers                                                      {{{1

handle_protocol = [
    "https://mail.proton.me#mailto=%25s",
    #"https://calendar.google.com/*",
]

for pattern in handle_protocol:
    config.set('content.register_protocol_handler', True, pattern)

# Sounds, notifications and media capture                                {{{1

# Ask by default
c.content.notifications.enabled = 'ask'

sound_and_notify = ['app.wire.com', 'calendar.google.com']
media_capture = ['app.wire.com']

for s in sound_and_notify:
    config.set('content.mute', False, 'https://' + s + '/*')
    config.set('content.notifications.enabled', True, f'https://{s}/*')

for site in media_capture:
    config.set('content.desktop_capture', True, f'https://{site}/*')
    config.set('content.media.audio_video_capture', True, f'https://{site}/*')


# Cookies                                                                {{{1

# Block 3rdparty cookies by default but whitelist some sites
c.content.cookies.accept = "no-3rdparty"
#config.set('content.cookies.accept', 'all', 'https://mail.google.com/*')

# Javascript                                                             {{{1
c.content.javascript.enabled = True

config.set('content.javascript.enabled', True, 'file://*')
config.set('content.javascript.enabled', True, 'chrome://*/*')
config.set('content.javascript.enabled', True, 'qute://*/*')

# Disable shite trustarc javascript
config.set('content.javascript.enabled', False, 'https://access.redhat.com/*')

c.content.javascript.log['unknown'] = "debug"

# Searchengines                                                          {{{1
#
# Maps a search engine name (such as `DEFAULT`, or `ddg`) to a URL with a `{}`
# placeholder. The placeholder will be replaced by the search term, use
# `{{` and `}}` for literal `{`/`}` signs. The search engine named
# `DEFAULT` is used when `url.auto_search` is turned on and something
# else than a URL was entered to be opened. Other search engines can be
# used by prepending the search engine name to the search term, e.g.
# `:open google qutebrowser`.

searchengines = {

    # duckduckgo
    'ddg': 'https://duckduckgo.com/?q={}',
    'ddgimages': 'https://duckduckgo.com/?iax=images&ia=images&q={}',

    # google
    'google': 'https://www.google.com/?q={}',
    'googlemaps': 'https://www.google.com/maps/search/{}',
    'googlebyimage': 'https://images.google.com/searchbyimage?image_url={}',

    # wiki
    'wiki': 'https://en.wikipedia.org/w/index.php?search={}',
    'wiki-fi': 'https://fi.wikipedia.org/w/index.php?search={}',
    'archwiki': 'https://wiki.archlinux.org/?search={}',

    # shops
    'ebay': 'https://www.ebay.com/sch/i.html?_nkw={}',
    'amazon.de': 'https://www.amazon.de/s?field-keywords={}',
    'mouser.at': 'https://www.mouser.at/Search/Refine?Keyword={}',

    # python
    'pythondocs': 'https://docs.python.org/3/search.html?q={}',
    'pythonpypi': 'https://pypi.org/search/?q={}',

    # haskell
    'hackage': 'https://hackage.haskell.org/packages/search?terms={}',
    'stackage': 'https://www.stackage.org/lts/hoogle?q={}', # Note: latest lts only

    # ansible
    'ansibledocs': 'https://docs.ansible.com/ansible/latest/index.html#stq={}',

    # development
    'dockerhub': 'https://hub.docker.com/search?q={}&type=image',
    'nix-versions': 'https://lazamar.co.uk/nix-versions/?channel=nixpkgs-unstable&package={}',
    'nixos-options-unstable': 'https://search.nixos.org/options?channel=unstable&from=0&size=200&sort=relevance&type=packages&query={}',
    'tf-providers': 'https://registry.terraform.io/search/providers?q={}',
    'tf-modules': 'https://registry.terraform.io/search/modules?q={}',

    # musicbrainz.org - query syntax: https://musicbrainz.org/doc/Indexed_Search_Syntax
    'musicbrainz_artist': 'https://musicbrainz.org/search?query={}&method=advanced&limit=75&type=artist',
    'musicbrainz_release': 'https://musicbrainz.org/search?query={}&method=advanced&limit=75&type=release',

    'urbandictionary.org': 'https://www.urbandictionary.com/define.php?term={}',

    # misc
    'genius.com': 'https://genius.com/search?q={}',
    'blockchain': 'https://www.blockchain.com/search?search={}',
    'last.fm': 'https://www.last.fm/search?q={}',
    'nyaa.si': 'https://nyaa.si/?q={}&f=0&c=0_0',
    'nyaa.si_anime': 'https://nyaa.si/?q={}&f=0&c=1_2',
    'nyaa.si_music': 'https://nyaa.si/?q={}&f=0&c=2_1',
    'rutracker.org': 'https://rutracker.org/forum/tracker.php?nm={}',
    'imdb.com': 'https://www.imdb.com/find?s=all&q={}',
    'myanimelist.net': 'https://myanimelist.net/search/all?q={}',
    'youtube.com': 'https://www.youtube.com/results?search_query={}',
    'kotikokki.net': 'https://www.kotikokki.net/reseptit/?freeText={}',
    'merriam-webster.com': 'https://www.merriam-webster.com/dictionary?s={}',
    'aur': 'https://aur.archlinux.org/packages/?K={}&SB=w&PP=250',
    'tokyotosho.info': 'https://www.tokyotosho.info/search.php?terms={}',
    'tokyotosho.info_music': 'https://www.tokyotosho.info/search.php?terms={}&type=2',
    'romajidesu.com': 'http://www.romajidesu.com?w={}&m=dictionary&a=lookup',
    'romajidesu.com_kanji': 'http://www.romajidesu.com/kanji/{}',
    'btdig.com': 'http://btdig.com/search?q={}',
    'mikudb.moe': 'http://mikudb.moe/?s={}',
    'leo.org': 'https://dict.leo.org/german-english/{}',
    'chocolatey.org': 'https://community.chocolatey.org/packages?q={}',
    'geizhals.at': 'https://geizhals.at/?fs={}',
    'wob.com': 'https://www.wob.com/de-de/kategorie/alle?search={}',
}

# GHC User Guide
for v in ['latest', 'master', '8.8.4']:
    k = 'ghc' if v == 'latest' else f'ghc-{v}'
    searchengines[k] = f'https://downloads.haskell.org/ghc/{v}/docs/html/users_guide/search.html?q={{quoted}}'

# Github
for typ in ['', 'Code', 'Issues', 'Commits']:
    searchengines[f'gh-{typ.lower()}'] = f'https://github.com/search?type={typ}&q={{quoted}}'

# Translate between languages
languages = ['fi', 'en', 'de', 'ja']
for f in languages:
    for t in languages:
        if f == t:
            continue
        # specific translators
        searchengines[f'deepl_{f}-{t}'] = f'https://deepl.com/translator#{f}/{t}/{{quoted}}'
        searchengines[f'google_{f}-{t}'] = f'https://translate.google.com/#{f}/{t}/{{quoted}}'
        # default
        searchengines[f'{f}{t}'] = searchengines[f'deepl_{f}-{t}']

# Default search engine
searchengines['DEFAULT'] = searchengines['ddg']

# Set our searchengines
c.url.searchengines = searchengines

# Fonts                                                                  {{{1

if my_util.is_windows():
    FONT_ACTIVE = "Cascadia Code"
    c.fonts.default_family = FONT_ACTIVE
    #c.fonts.default_size = '18px'  # 10pt
elif my_util.is_highdpi():
    # For HighDPI scaling
    # alt 'bold 18px "xos4 Terminess Powerline"'
    c.fonts.default_family = '"NotoSans Nerd Font"'
    c.fonts.default_size = '8pt' # 10pt
else:
    # for low-DPI displays
    c.fonts.default_family = "'xos4 Terminess Powerline'"
    c.fonts.default_size = '12px'

# Can't be configured via the home-manager options                       {{{1
c.tabs.padding = {'top': 0, 'bottom': 0, 'left': 5, 'right': 0}
