# Colors                                                                  {{{1

# solarized_*                                                             {{{2
solarized_base03 = "#002b36"  # (bg)
solarized_base02 = "#073642"  # (bg)
solarized_base01 = "#586e75"  # (content)
solarized_base00 = "#657b83"  # (content)
solarized_base0 = "#839496"  # (content)
solarized_base1 = "#93a1a1"  # (content)
solarized_base2 = "#eee8d5"  # (bg)
solarized_base3 = "#fdf6e3"  # (bg)
solarized_red = "#dc322f"
solarized_orange = "#cb4b16"
solarized_yellow = "#b58900"
solarized_green = "#859900"
solarized_cyan = "#2aa198"
solarized_blue = "#268bd2"
solarized_violet = "#6c71c4"
solarized_magenta = "#d33682"

# colors.completion.*                                                     {{{2
c.colors.completion.fg = solarized_base1
c.colors.completion.odd.bg = solarized_base02
c.colors.completion.even.bg = solarized_base03
c.colors.completion.category.fg = solarized_cyan
c.colors.completion.category.bg = solarized_base02
c.colors.completion.category.border.top = solarized_base02
c.colors.completion.category.border.bottom = solarized_base01
c.colors.completion.item.selected.fg = solarized_base01
c.colors.completion.item.selected.bg = solarized_base02
c.colors.completion.item.selected.border.top = solarized_green
c.colors.completion.item.selected.border.bottom = solarized_green
c.colors.completion.match.fg = solarized_green
c.colors.completion.scrollbar.fg = solarized_base1
c.colors.completion.scrollbar.bg = solarized_base02

# colors.downloads.*                                                      {{{2
c.colors.downloads.bar.bg = solarized_base03
c.colors.downloads.start.fg = solarized_cyan
c.colors.downloads.start.bg = solarized_base02
c.colors.downloads.stop.fg = solarized_base01
c.colors.downloads.stop.bg = solarized_base02
c.colors.downloads.error.fg = solarized_red
c.colors.downloads.error.bg = solarized_base02

# colors.hints.*                                                          {{{2
c.colors.hints.bg = 'qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 rgba(88, 110, 117, 0.8), stop:1 rgba(7, 54, 66, 0.8))'
c.colors.hints.fg = "#fff"  # solarized_base2
c.colors.hints.match.fg = solarized_green

# colors.keyhint.*                                                        {{{2
c.colors.keyhint.bg = solarized_base03
c.colors.keyhint.fg = solarized_base1
c.colors.keyhint.suffix.fg = solarized_blue

# colors.messages.*                                                       {{{2
c.colors.messages.error.fg = solarized_base3
c.colors.messages.error.bg = solarized_orange
c.colors.messages.error.border = solarized_red
c.colors.messages.warning.fg = solarized_base2
c.colors.messages.warning.bg = solarized_yellow
c.colors.messages.warning.border = solarized_violet
c.colors.messages.info.fg = solarized_base1
c.colors.messages.info.bg = solarized_base02
c.colors.messages.info.border = solarized_base00

# colors.prompts.*                                                        {{{2
c.colors.prompts.fg = solarized_base0
c.colors.prompts.bg = solarized_base02
c.colors.prompts.border = solarized_cyan
c.colors.prompts.selected.bg = solarized_base01

# colors.statusbar.*                                                      {{{2
c.colors.statusbar.normal.fg = solarized_green
c.colors.statusbar.normal.bg = solarized_base02
c.colors.statusbar.insert.fg = solarized_blue
c.colors.statusbar.insert.bg = solarized_base02
c.colors.statusbar.passthrough.fg = solarized_cyan
c.colors.statusbar.passthrough.bg = solarized_base02
c.colors.statusbar.private.fg = solarized_base00
c.colors.statusbar.private.bg = solarized_base02
c.colors.statusbar.command.fg = solarized_base1
c.colors.statusbar.command.bg = solarized_base02
c.colors.statusbar.command.private.fg = solarized_base1
c.colors.statusbar.command.private.bg = solarized_base02
c.colors.statusbar.caret.fg = solarized_violet
c.colors.statusbar.caret.bg = solarized_base02
c.colors.statusbar.caret.selection.fg = solarized_blue
c.colors.statusbar.caret.selection.bg = solarized_base02
c.colors.statusbar.progress.bg = solarized_cyan

# colors.statusbar.url.*                                                  {{{2
c.colors.statusbar.url.fg = solarized_base1
c.colors.statusbar.url.error.fg = solarized_yellow
c.colors.statusbar.url.hover.fg = solarized_base1
c.colors.statusbar.url.success.http.fg = solarized_cyan
c.colors.statusbar.url.success.https.fg = solarized_green
c.colors.statusbar.url.warn.fg = solarized_violet

# colors.tabs.*                                                           {{{2
c.colors.tabs.bar.bg = solarized_base03

c.colors.tabs.indicator.start = solarized_blue
c.colors.tabs.indicator.stop = solarized_cyan
c.colors.tabs.indicator.error = solarized_red

c.colors.tabs.odd.fg = solarized_base1
c.colors.tabs.odd.bg = solarized_base02
c.colors.tabs.even.fg = solarized_base1
c.colors.tabs.even.bg = solarized_base03

c.colors.tabs.selected.odd.fg = solarized_base03
c.colors.tabs.selected.odd.bg = solarized_base1
c.colors.tabs.selected.even.fg = solarized_base03
c.colors.tabs.selected.even.bg = solarized_base1

c.colors.tabs.pinned.odd.fg = solarized_base2
c.colors.tabs.pinned.odd.bg = solarized_base02
c.colors.tabs.pinned.even.fg = solarized_base2
c.colors.tabs.pinned.even.bg = solarized_base03

c.colors.tabs.pinned.selected.odd.fg = solarized_base03
c.colors.tabs.pinned.selected.odd.bg = solarized_base2
c.colors.tabs.pinned.selected.even.fg = solarized_base03
c.colors.tabs.pinned.selected.even.bg = solarized_base2
