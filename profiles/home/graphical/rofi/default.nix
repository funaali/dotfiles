{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.sim.graphical.rofi;

in
{
  options.sim.graphical.rofi.enable = mkEnableOption "sim.graphical.rofi";

  config = mkIf cfg.enable {
    programs.rofi = {
      enable = true;
      plugins = with pkgs; [
        rofi-calc
        rofi-menugen
        rofi-mpd
        rofi-pass
        rofi-pulse-select
        rofi-systemd
      ];
      theme = "solarized";
      terminal = "/usr/bin/kitty";
      extraConfig = {
        modi = "drun,run,ssh,window,windowcd,keys,filebrowser,combi"; # calc
      };
      pass = {
        enable = true;
        #stores = [ config.programs.password-store.settingsh];
        #extraConfig = '' '';
        # todo
      };
    };
  };
}
