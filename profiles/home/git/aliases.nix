{ lib, ... }:

with lib;

# Notes:
# - shell commands are executed from the top-level directory of the repository.
# - GIT_PREFIX refers to the active directory.
# - Aliases that hide existing git commands are ignored.
# - Arguments are split by spaces.
# - Usual shell quoting and escaping (quote pair or backslash).
# - Any expansion prefixed with ! is treated as a shell command.
# - shell commands execute from the repository top-level directory!

# TODO: create new branch and track it
# TODO: delete current checked out branch

let
  # for "git log"
  # NOTE: describe can be slow
  # %C(brightgreen)%(describe:abbrev=12)%Creset
  prettyFormat =
    let C = color: content: "%C(${color})${content}%C(reset)";
    in
    "tformat:${C "red" "%h"} -${C "yellow" "%d"} %s ${C "green" "(%cr)"} ${C "bold blue" "<%an>"}";

  # for git branch, etc.
  # Formats documented in <url:man:git-for-each-ref>
  refFormat =
    let
      color = cl: cn: "%(color:${cl})" + cn + "%(color:reset)";
      #align = n: cn: "%(align:${n})" + cn + "%(end)";
      if1 = cond: cn: "%(if)" + cond + "%(then)" + cn + "%(end)";
      if2 = cond: cn1: cn2: "%(if)" + cond + "%(then)" + cn1 + "%(else)" + cn2 + "%(end)";

      object = color "red" "%(objectname:short)";
      objectAge = color "nobold green" "(%(creatordate:relative))";
      upstream = color "nobold yellow" (if1 "%(upstream:remotename)" "%(upstream:remotename)/%(upstream:lstrip=3)");
      tracking = color "nobold magenta" "%(upstream:track)";
      refname = color "cyan bold" "%(refname:lstrip=2)";
    in
    object
    + " " + if2 "%(HEAD)" (color "green" "%(refname:lstrip=2)") refname
    + " " + objectAge
    + " " + upstream
    + " " + tracking;

  aliases = {
    basic = {
      cm = "commit";
      co = "checkout";
      ls = "ls-files";
      p = "push";
      sm = "submodule";
      b = "branch --format=${escapeShellArg refFormat}";
      br = "branch --remotes"; # List remote branches
      lg = "log --color --graph --abbrev-commit --pretty=${escapeShellArg prettyFormat}";
      lgg = "lg --boundary @{u}..";
      st = "status --short --branch";
      up = "!git remote update -p; git merge --ff-only @{u}";
      # XXX: what is this
      up-all = ''!git remote update -p; eval "$(git for-each-ref refs/heads --shell --format='[ -z %(upstream) ] || git fff %(refname:lstrip=2) %(upstream)')"'';
      # Delete branch locally and remotely
      bdelete = ''!f(){
        git branch -D "$@"
        local repo
        repo=$(git branch --list --format='%(upstream:remotename)' "$(git get-branch)")
        git push --delete "$repo" "$@"
      }; f'';

      # diff, but always without a pager or ansi colors
      pdiff = "!git --no-pager diff --no-color";

      # get the toplevel work dir of the repo
      root = "rev-parse --show-superproject-working-tree";
    };

    commit = {
      # git cmam [message...]
      # Commit all changes and give the commit message as multiple arguments
      # (no need to quote whitespace).
      cmam = ''!f(){ git commit -am "$*"; }; f'';

      # Amend last commit without editing the commit message.
      amend = "commit --amend --no-edit";

      # git fixup <ref> [commit-options...]
      # Create a "fixup!" commit recognized by git rebase --autosquash.
      fixup = "commit --fixup";

      # git cmf [file...]
      # Commit selected files using a generated commit message.
      cmf = ''!f(){
        git add -v -- "$@" || return $?
        git commit -m "$(printf '%i files changed\n\n' $#; printf '* %s\n' "$@")"
        }; f
      '';
    };

    # For git submodules
    submodule = {
      # -l etc.
      sm-config = "config -f .gitmodules";
      # List all submodule sections in gitmodules. Outputs "<subsection> <path>" for each module.
      sm-list = "!git config -f .gitmodules --get-regex ${escapeShellArg ''submodule\..*\.path$''} | sed -ne ${escapeShellArg ''s/^submodule.\(.*\).path \(.*\)/\1 \2/p''}";
      # Sanity-check all submodules in gitmodules
      sm-check = ''!while read -r section path; do if ! git submodule status -- "$path" >&- 2>&-; then echo "$path"; echo "Module at path $path is not ok" >&2; fi; done < <(git sm-list)'';
      # Completely remove a submodule section
      sm-remove = "!f(){
        git sm-config --remove-section submodule.\"$1\"
      }; f";
      # Synchronize url from submodule (default sync does this the other way around). sm-up <path>
      sm-up = ''!f(){
        set -e
        path=$1
        branch=$(git sm-config --get submodule."$path".branch)
        remote=$(git -C "$path" config --get branch."$branch".remote)
        url=$(git -C "$path" remote get-url "$remote")
        git submodule set-url -- "$path" "$url"
      }; f'';
    };

    push = {
      push-all = "push --all --tags";

      # git push-matching <push-args> <remote> <branch-pattern>
      push-matching = ''!f(){
        args=' '
        while [[ $1 = -* ]]; do
          args+='$1'; shift
        done
        branches=$(git branch --list --format='%(refname:short)' '$2' | tr '\n' ' ')
        cmd='git push$args$1 $branches'
        echo "About to execute: $cmd"
        echo "(press any to continue, Ctrl-C to abort)"
        read
        eval '$cmd'
        }; f
      '';
    };

    gitlab = {
      # git gitlab-mr-co <merge-request-id>
      # Fetch and checkout a Gitlab Merge Request. The head will be
      # named "mr-${merge-request-id}".
      gitlab-mr-co = ''!f(){ git fetch origin merge-requests/"$1"/head:mr-"$1" && git checkout mr-"$1";};f'';
    };
  };

in

{
  programs.git.aliases = mkMerge (attrValues aliases);
}
