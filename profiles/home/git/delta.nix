{ config, lib, pkgs, nix-colors, ... }:

with lib;

# More custom "delta" integration
# https://github.com/chtenb/delta/blob/master/themes.gitconfig

let
  solarized = config.lib.colors.solarized;
in
{
  config.home.packages = [ pkgs.delta ];

  config.home.sessionVariables.DELTA_PAGER =
    config.programs.git.extraConfig.delta.pager;

  config.programs.git = {
    extraConfig = {
      core.pager = "delta";
      interactive.diffFilter = "delta --color-only";

      delta = {
        features = "mantis-shrimp zebra-solarized-dark";
        navigate = true;
        word-diff-regex = "\\S+";
        grep-separator-symbol = "keep"; # XXX: https://github.com/dandavison/delta/issues/1259
        interactive = {
          keep-plus-minus-markers = false;
        };
        pager = "less --tabs=4 -RFXS";
        zebra-solarized-dark = {
          minus-style = ''syntax "${solarized.base02}"''; # ''syntax "#ff0000"'';
          minus-emph-style = ''syntax "${solarized.red}"'';
          plus-style = ''syntax "${solarized.base02}"'';
          plus-emph-style = ''syntax "${solarized.green}"'';
          map-styles = ''
            bold purple => syntax "#330f29",
            bold blue   => syntax "#271344",
            bold cyan   => syntax "#0d3531",
            bold yellow => syntax "#222f14"
          '';
          zero-style = "syntax";
          whitespace-error-style = "${solarized.orange}";
        };
      };
    };

    includes = [
      { path = "${pkgs.delta.src}/themes.gitconfig"; } # delta
    ];
  };
}
