{ config, lib, pkgs, ... }:

with lib;

# <url:man:tigrc.5>
# <url:man:tigmanual.7>
# <url:https://jonas.github.io/tig>
#
# Internal commands: :           <url:man:tigrc.5#Internal user-defined commands>
# External commands: !?@<+>      <url:man:tigrc.5#External user-defined command>
# Variables:         %(var:name) <url:man:tigrc.5#Browsing state variables>
let
  mkAction = action: if action == null then "none" else "${action}";

  mkBindsSection = mapAttrsToList (key: action: "${key} ${mkAction action}");

  mkBinds = mapAttrs (section: value: mkBindsSection value);

  # TODO use diff-so-fancy?
  diff-highlight = "/usr/share/git/diff-highlight/diff-highlight";
in

{
  home.packages = [ pkgs.tig ];

  programs.git.extraConfig.tig = {
    line-graphics = "utf-8";
    truncation-delimiter = "utf-8";
    show-changes = true;
    show-untracked = true;
    diff-highlight = diff-highlight;
    commit-order = "default";
    ignore-case = "smart-case";
    wrap-lines = true;
    focus-child = false; # Whether to focus child view when opened.
    send-child-enter = false; # Don't send "enter" to unfocused child
    history-size = 1024; # ~/.tig_history
    pgrp = true; # Make tig process-group leader. Run xclip with setsid to keep clipboard.
    start-on-head = true; # Start with cursor on HEAD commit.
    #refresh-mode = auto
    #refresh-interval = 30     # Refresh update check interval (seconds), when refresh-mode is periodic.

    main-view-id = true;
    main-view-date = "custom";
    main-view-date-format = ''"%Y-%m-%d (%a) %R"'';

    bind = mkBinds {
      generic = {
        "<F1>" = "view-help";
        "0" = "view-main";
        "1" = "view-status";
        "2" = "view-refs";
        "3" = "view-log";
        "4" = "view-reflog";
        "5" = "view-stash";
        "9" = "view-help";
        m = null;
        "<Space>" = null;
        q = "view-close-no-quit";
        "<Ctrl-C>" = "stop-loading";
        z = null; # stop-loading
        # <Ctrl-L> refresh screen-redraw
      };

      main = {
        # Write selected commit as a patch file
        S = "!git format-patch -1 %(commit)";
        # Prompt for new branch and create it
        B = "?git checkout -b '%(prompt Enter new branch name: )'";
      };

      status = {
        "<Space>" = "status-update";
        u = "status-revert";
        "!" = "status-revert";
        m = "status-merge";
        M = null; # status-merge
        A = "!git add -u";
        x = "?+git clean -f -- %(file)";
      };

      stage = {
        a = "status-update";
        u = "status-revert";
        "<Space>" = "stage-update-line";
        "*" = "stage-split-chunk";
        "!" = null;
        "." = ":/^@@";
        "," = ":?^@@";
      };
    };

    color =
      let
        default = "default default";
        title = "color12 color0";
        id = "color1 default";
        head = "color13 default";
        remote = "color3 default";
        commit = "color10 default";
        author = "color12 default";
        date = "color10 default";
        diff-add = "color2 color0";
        diff-del = "color9 color0";
      in
      {
        default = "${default}";
        status = "${default}";
        cursor = "color0 color3 bold";
        title-focus = "${title} bold";
        title-blur = "${title}";
        #  search-result = default default
        #  delimiter     = default default
        #  header        = color11 color0
        #  line-number   = color6  color0
        id = "${id}";
        date = "${date}";
        author = "${author} bold";
        #  mode      = default default
        #  overflow  = default default
        #  directory = default default
        #  file      = default default
        #  file-size = default default
        graph-commit = "${commit}";

        main-head = "${head} bold";
        main-remote = "${remote}";
        main-tracked = "${remote}";
        #main-commit    = default default
        #main-annotated = default default
        #main-tag       = color3 default
        #main-local-tag = color3 default
        #main-ref       = default default
        #main-replace   = default default

        #stat-none      = default default
        #stat-staged    = default default
        #stat-unstaged  = default default
        #stat-untracked = default default

        #help-group  = default default
        #help-action = default default

        diff-add-highlight = "${diff-add}";
        diff-del-highlight = "${diff-del}";
        #diff-header = default default
        #diff-chunk  = default default
        #diff-add    = default default
        #diff-add2   = default default
        #diff-del    = default default
        #diff-del2   = default default

        #pp-refs      = default default
        #pp-reflog    = default default
        #pp-reflogmsg = default default
        #pp-merge     = default default

        palette-0 = "color3  default";
        palette-1 = "color5  default";
        palette-2 = "color6  default";
        palette-3 = "color4  default";
        palette-4 = "color7  default";
        palette-5 = "color1  default";
        palette-6 = "color9  default";
        palette-7 = "color10 default";
        palette-8 = "color11 default";
        palette-9 = "color12 default";
        palette-10 = "color13 default";
        palette-11 = "color14 default";
        palette-12 = "color15 default";
        palette-13 = "color2  default";
      };
  };
}
