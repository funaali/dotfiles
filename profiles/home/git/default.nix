{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.programs.git;
  vars = config.sim;
  gitVersionAtLeast = ver: builtins.compareVersions cfg.package.version ver >= 0;
in

{
  imports = [
    ./aliases.nix
    ./delta.nix
    ./tig.nix
  ];

  config = {
    home.packages = with pkgs; [
      git-crypt
      lennartcl-gitl
      git-extras # many extra commands
      gh # GitHub CLI
      lab # GitLab CLI
      ghq # "go-get"-like git repository management
      git-workspace # Sync repositories in bulk with github or gitlab TODO
      #git-machete # TODO use
    ];

    home.sessionVariables.GIT_COMPLETION_SHOW_ALL = "1";

    programs.git = {
      enable = mkDefault true;

      userName = vars.name;
      userEmail = vars.email;

      signing.key = vars.gpgKeyId;
      signing.signByDefault = !builtins.isNull vars.gpgKeyId;

      ignores = splitString "\n" (readFile ./ignores);

      lfs.enable = true;

      annex.enable = mkDefault true;

      extraConfig = {
        advice = {
          pushUpdateRejected = false;
          statusHints = false;
          resolveConflict = false;
          implicitIdentity = false;
          detachedHead = false;
          checkoutAmbiguousRemoteBranchName = false;
        };

        core.commentChar = ";"; # Note: if "auto", vim isn't smart enough to use correct character.
        core.quotePath = false; # Whether to quote "unusual" characters when printing filenames.

        blame.blankBoundary = true;
        blame.coloring = "highlightRecent";

        branch.sort = "committerdate"; # See: git-for-each-ref(1). Default: refname.

        color.ui = "auto";
        color.branch = { current = "green"; local = 14; remote = 11; upstream = "yellow"; plain = "blue"; };
        color.diff = { meta = "blue"; frag = 13; commit = "yellow"; };

        column.ui = "auto,column,dense";
        column.branch = "never";

        checkout.workers = -1;

        help.autoCorrect = "prompt";

        pager.status = true; # use pager for "git status" output

        fetch.parallel = 0; # enable automatically parallel fetching
        fetch.negotiationAlgorithm = "skipping"; # possibly faster?

        gc.auto = 1000; # Default 6700. if 0 (disabled), sometimes do `git repack -d; git gc; git prune`
        gc.autoDetach = false;

        #grep.column = true; # --column
        grep.lineNumber = true; # -n
        grep.patternType = "extended"; # -E

        index.version = 4; # Note: index.version takes effect in newly initialized or cloned repositories. For existing repos do `git update-index --index-version=4' version 4 from git 1.8.0 onwards.
        index.recordEndOfIndexEntries = true; # performance optimization
        index.recordOffsetTable = true; # performance optimization

        log.abbrevCommit = true; # --abbrev-commit
        log.decorate = "auto"; # --decorate=auto
        #log.showSignature = true; # --show-signature

        merge.ff = true; # Note: if false, "git-annex sync" does a lot of merges
        merge.log = 128; # Preformatted merge commit messages include descriptions of the merged commits (up to N).
        merge.branchdesc = true; # include branch descriptions in the message
        merge.renameLimit = 18654;
        merge.conflictstyle = if gitVersionAtLeast "2.35" then "zdiff3" else "diff3";
        merge.tool = "vimdiff";
        merge.guitool = "vimdiff";

        mergetool.vimdiff.layout = "LOCAL,MERGED,REMOTE"; # git-mergetool.1#"BACKEND SPECIFIC HINTS"
        mergetool.vimdiff.hideResolved = true; # overwrite LOCAL,REMOTE with resolved conflicts
        mergetool.keepBackup = false; # keep *.orig files after merge?
        mergetool.writeToTemp = true; # prefer to write BASE, LOCAL, REMOTE in tmp

        diff.mnemonicPrefix = true;
        diff.renameLimit = 18654;
        diff.renames = "copies";
        diff.submodule = "diff";
        diff.algorithm = "minimal";
        diff.colorMoved = "default";
        diff.colorMovedWS = "allow-indentation-change";

        pull.rebase = true;

        push.autoSetupRemote = true; # assume --set-upstream when pushing to default remote
        push.default = "upstream";
        push.followTags = true; # --follow-tags
        push.recurseSubmodules = "check";

        rebase.missingCommitsCheck = "warn";
        rebase.abbreviateCommands = true;

        receive.autogc = false;

        status.branch = true;
        status.showStash = true;
        status.submoduleSummary = true;

        stash.showIncludeUntracked = true;

        url."git@gitlab.com:".pushInsteadOf = "https://gitlab.com/";
        url."git@github.com:".pushInsteadOf = "https://github.com/";
      };

      includes = mkAfter [
        { path = "config.private"; }
      ];
    };

    programs.forgit = {
      enable = true;
      #options.pager = "bat";
    };
  };
}
