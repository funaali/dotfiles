{ config, lib, pkgs, ... }:

with lib;

{
  config = {
    sim.graphical.qutebrowser.package = pkgs.dummy;

    home.packages = with pkgs; [
      # XXX: not sure if these are necessary
      xdg-desktop-portal
      xdg-desktop-portal-gnome
    ];

    programs.kitty.package = pkgs.dummy;

    programs.mpv.package = pkgs.dummy;
    programs.mpv.scripts = lib.mkForce [ ]; # XXX: option package is incompatible with option scripts

    systemd.user.systemctlPath = "/usr/bin/systemctl";

    # NOTE: nix package fails to launch: qt.glx: qglx_findConfig: Failed to finding matching
    # let nixGL = pkgs.nixGL.auto.nixGLDefault;
    # FBConfig for QSurfaceFormat
    # ExecStart = lib.mkOverride 10 "${nixGL}/bin/nixGL ${config.services.nextcloud-client.package}/bin/nextcloud --background";
    services.nextcloud-client.package = pkgs.writeShellScriptBin "nextcloud" ''exec /usr/bin/nextcloud "$@"'';

    xdg.systemDirs.config = [ "/etc/xdg" ];
    xdg.systemDirs.data = [ "${config.home.profileDirectory}/share" "/usr/share" ];

    # XXX not sure it's necessary?
    systemd.user.services.udiskie = mkIf config.services.udiskie.enable (mkForce {
      Unit.Description = "Udiskie - automount media";
      Service.ExecStart = "/usr/bin/udiskie";
      Service.Restart = "on-failure";
      Service.StandardError = "journal";
      Install.WantedBy = [ "basic.target" ];
    });

    programs.gpg.settings.scdaemonSettings = {
      # Note: requires external software installed. Not tested!
      pcsc-driver = mkOverride "/usr/lib/libpcsclite.so";
    };
  };
}
