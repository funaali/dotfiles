{ config, lib, pkgs, ... }:

with lib;

let
  S = config.lib.colors.solarized;
in
{
  config = {
    home.packages = with pkgs; [
      hicolor-icon-theme
      powerline-fonts
      wqy_microhei
      wqy_zenhei
      nerd-fonts.dejavu-sans-mono
      nerd-fonts.symbols-only
      nerd-fonts.noto
      nerd-fonts.terminess-ttf
    ];

    systemd.user.sessionVariables = {
      # XXX: gpg-agent/pinentry doesn't source the hm session
      inherit (config.home.sessionVariables) GTK2_RC_FILES;
    };

    home.pointerCursor = {
      package = pkgs.vanilla-dmz;
      name = "Vanilla-DMZ";
      size = 32;
      gtk.enable = true;
      x11.enable = true;
      #x11.defaultCursor = "X_cursor";
    };

    fonts.fontconfig.enable = true;

    gtk = {
      enable = true;
      font = {
        name = "NotoSans Nerd Font";
        size = 12;
      };
      theme = {
        # This theme/package also provides GTK4 support, unlike gnome-themes-extra
        name = "adw-gtk3-dark";
        package = pkgs.adw-gtk3;
      };
      iconTheme = {
        name = "Papirus-Dark";
        package = pkgs.papirus-icon-theme;
      };
      gtk2.configLocation = "${config.xdg.configHome}/gtk-2.0/gtkrc";
      gtk3.extraConfig = {
        # gtk-query-settings
        gtk-application-prefer-dark-theme = true;
      };
      #gtk4.extraConfig = { }; # gtk-query-settings
    };

    qt = {
      enable = true;
      platformTheme = {
        name = "adwaita"; # "gtk3";
      };
      style = {
        name = "adwaita-dark";
      };
    };

    xresources = {
      properties = {
        "Xmag*font" = "-xos4-terminesspowerline-medium-*--18-*";

        "Xmessage*font" = "-xos4-terminesspowerline-medium-*--18-*";
        "Xmessage*background" = S.background;
        "Xmessage*foreground" = S.foreground;

        # black dark/light
        "*color0" = S.base02;
        "*color8" = S.base03;

        # red dark/light
        "*color1" = S.red;
        "*color9" = S.orange;

        # green dark/light
        "*color2" = S.green;
        "*color10" = S.base01;

        # yellow dark/light
        "*color3" = S.yellow;
        "*color11" = S.base00;

        # blue dark/light
        "*color4" = S.blue;
        "*color12" = S.base0;

        # magenta dark/light
        "*color5" = S.magenta;
        "*color13" = S.violet;

        # cyan dark/light
        "*color6" = S.cyan;
        "*color14" = S.base1;

        # white dark/light
        "*color7" = S.base2;
        "*color15" = S.base3;
      };
    };
  };
}
