{ pkgs, ... }:

{
  config = {
    home.packages = with pkgs; [
      feh
      mediainfo
      sox
      ffmpeg
      kid3
      unflac
      flac
      imagemagick
      wavpack
      python3Packages.chardet # for "chardetect"

      # videos
      yt-dlp

      # documents
      evince
    ];

    xdg.mimeApps.defaultApplications = {
      # documents
      "application/pdf" = [ "org.gnome.Evince.desktop" ];

      # images
      "image/bmp" = [ "feh.desktop" ];
      "image/gif" = [ "feh.desktop" ];
      "image/jpeg" = [ "feh.desktop" ];
      "image/jpg" = [ "feh.desktop" ];
      "image/pjpeg" = [ "feh.desktop" ];
      "image/png" = [ "feh.desktop" ];
      "image/tiff" = [ "feh.desktop" ];
      "image/x-bmp" = [ "feh.desktop" ];
      "image/x-pcx" = [ "feh.desktop" ];
      "image/x-png" = [ "feh.desktop" ];
      "image/x-portable-anymap" = [ "feh.desktop" ];
      "image/x-portable-bitmap" = [ "feh.desktop" ];
      "image/x-portable-graymap" = [ "feh.desktop" ];
      "image/x-portable-pixmap" = [ "feh.desktop" ];
      "image/x-tga" = [ "feh.desktop" ];
      "image/x-xbitmap" = [ "feh.desktop" ];
    };
  };
}
