{ lib, pkgs, config, ... }:

with lib;

{
  imports = [
    ./appearance.nix
    ./media.nix
  ];

  config = {
    home.packages = with pkgs; [
      bc # calculator
      xdg-dbus-proxy # used by a steam workaround
      qmk # flash keyboard fw
      sshfs
      qrtool # enable decoding and encoding QR codes from image frome
      #opensc # For smartcards
      #chrome-token-signing
      #pkcs11-provider
    ];

    # configure profile-sync-daemon
    home.file.".config/psd/psd.conf".text = ''
      # For documentation, refer to /usr/share/psd/psd.conf and psd(1).
      USE_OVERLAYFS="yes"
      BROWSERS="google-chrome qutebrowser"
      USE_BACKUPS="yes"
      BACKUP_LIMIT=3
    '';

    age.secrets.chatgpt-api-key.file = ../../../secrets/chatgpt-api-key.age;
    age.secrets.deepl-free-api-key-01.file = ../../../secrets/deepl-free-api-key-01.age;

    programs.gtt = {
      enable = true;
      servers.chatgpt.api_key.file = config.age.secrets.chatgpt-api-key.path;
      servers.deepl.api_key.file = config.age.secrets.deepl-free-api-key-01.path;
    };

    programs.chromium = {
      enable = true;

      # XXX the ungoogled chromium segfaults randomly...
      #package = pkgs.ungoogled-chromium;

      commandLineArgs = [
        # https://chromium.googlesource.com/chromium/src/+/refs/heads/main/chrome/common/chrome_switches.cc
        #"--ignore-gpu-blacklist"
        #"--disable-gpu-driver-bug-workarounds"
        #"--enable-native-gpu-memory-buffers"
        # "--disable-smooth-scrolling"
        "--force-dark-mode"
        "--enable-features=WebUIDarkMode"
        # XXX: does this work on Linux? docs only mention OS X and Windows...
        "--restore-last-session"
      ];

      dictionaries = with pkgs.hunspellDictsChromium; [ en_US en_GB de_DE ];
    };

    programs.texlive = {
      enable = true;
      extraPackages = tpkgs: {
        inherit (tpkgs) scheme-medium verse gmverse wrapfig fontawesome;
      };
    };

    # Switch to new unit definitions on activation.
    systemd.user.startServices = "sd-switch";
    systemd.user.servicesStartTimeoutMs = 15000;

    systemd.user.timers.btc_ticker = {
      Unit.Description = "BTC price ticker update timer";
      Timer.OnStartupSec = "1min";
      Timer.OnUnitActiveSec = "30min";
      Timer.AccuracySec = "10min";
      Install.WantedBy = [ "timers.target" ];
    };
    systemd.user.services.btc_ticker = {
      Unit.Description = "BTC price ticker";
      Unit.After = [ "network.target" ];
      Service.Type = "oneshot";
      Service.Slice = "background.slice";
      Service.ExecStart = "/usr/bin/env kraken_dzen";
    };

    # Enable Android notifications on the desktop!
    services.a2ln = {
      enable = true;
    };
  };
}
