{ ... }:
{
  config = {
    xdg.configFile."pulse/default.pa".source = ./default.pa;
    xdg.configFile."pulse/scripts" = {
      source = ./scripts;
      recursive = true;
    };
    xdg.configFile."pulse/client.conf".source = ./client.conf;
    xdg.configFile."pulse/daemon.conf".text = "";
    xdg.configFile."pulse/daemon.conf.d" = {
      source = ./daemon.conf.d;
      recursive = true;
    };
  };
}
