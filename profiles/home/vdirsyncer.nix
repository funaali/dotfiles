{ lib, pkgs, config, ... }:

# https://github.com/jowave/vcard2to3

with lib;

{
  config = {
    # TODO broken
    #programs.vdirsyncer = {
    #  enable = true;
    #  statusPath = "~/.vdirsyncer";

    #  pair.my_contacts = {
    #    a = "my_contacts_local";
    #    b = "my_contacts_nextcloud";
    #  };

    #  storage.my_contacts_local = {
    #    type = "filesystem";
    #    #path = "${config.xdg.dataHome}/dav/contacts/";
    #    path = "${config.home.homeDirectory}/.contacts/";
    #    fileext = ".vcf";
    #  };

    #  storage.my_contacts_nextcloud = {
    #    type = "carddav";
    #    url = "https://morphism.funktionaali.com/remote.php/dav/";
    #    username = "sim";
    #    password.fetch = [ "command" "pass" "vdirsyncer/nextcloud/sim" ];
    #  };
    #};
  };
}
