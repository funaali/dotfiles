{ pkgs, ... }:

{
  imports = [
    ./x11.nix
  ];

  config = {
    hardware.graphics = {
      enable = true;
      enable32Bit = true;
      extraPackages = with pkgs; [
        intel-media-driver # LIBVA_DRIVER_NAME=iHD (Intel Media Driver for VAAPI — Broadwell+ iGPUs)
        #vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
        libglvnd
        libva
        libva-vdpau-driver # LIBVA_DRIVER_NAME={vdpau,nvidia,s3g}
        libvdpau
        libvdpau-va-gl
        libdrm.out
        libGLU
        glew
      ];
      extraPackages32 = with pkgs.pkgsi686Linux; [
        intel-media-driver # LIBVA_DRIVER_NAME=iHD (Intel Media Driver for VAAPI — Broadwell+ iGPUs)
        #vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
        libglvnd
        libva
        libva-vdpau-driver # LIBVA_DRIVER_NAME={vdpau,nvidia,s3g}
        libvdpau
        libvdpau-va-gl
        libdrm.out
      ];
    };

    environment.systemPackages = with pkgs; [
      glxinfo # GL debugging
      libnotify # for notify-send
      libva-utils
      libvdpau
      vdpauinfo
      libdrm.bin
    ];

    environment.sessionVariables.LD_LIBRARY_PATH = [
      "/run/opengl-driver/lib"
      "/run/opengl-driver-32/lib"
    ];

    fonts.fontconfig.useEmbeddedBitmaps = true;

    programs.dconf.enable = true;

    # profile-sync-daemon (keep browser profiles in ram)
    services.psd = {
      enable = true;
      resyncTimer = "1h 30min";
    };
  };
}
