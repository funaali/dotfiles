{ config, ... }:

{
  config = {
    # HDD monitoring
    services.scrutiny = {
      enable = true;
      openFirewall = true;

      # https://github.com/AnalogJ/scrutiny/blob/master/example.scrutiny.yaml
      settings = {
        web.listen.port = 6700;
      };

      collector = {
        enable = true; # enables smartd
      };
    };

    services.smartd = {
      enable = true;
      notifications = {
        #test = true;
        systembus-notify.enable = true;
        x11 = {
          enable = true;
          display = ":0";
        };
        mail = {
          #enable = true;
          recipient = "reports@funktionaali.com";
          sender = config.networking.fqdnOrHostName;
          #mailer = "sendmail";
        };
      };
    };
  };
}
