{ config, lib, pkgs, ... }:
with lib;
let
  cfg = config.hardware.pulseaudio;
  defaults = {
    hardware.pulseaudio.enable = mkDefault true;
    hardware.pulseaudio.package = pkgs.pulseaudioFull.overrideAttrs (_: {
      advancedBluetoothCodecs = true;
    });
  };
in
{
  imports = [ defaults ];

  config = mkIf cfg.enable {
    systemd.user.services.pulseaudio.environment.LADSPA_PATH = "${pkgs.ladspaPlugins}/lib/ladspa";

    services.udev.extraRules = builtins.readFile ./pulseaudio.rules;
  };
}
