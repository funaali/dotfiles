{ config, lib, pkgs, ... }:

# Module to enable PipeWire and my customizations for it.
# Documentation (user-oriented): https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Home

let
  inherit (lib) mkIf
    makeSearchPath;

  cfg = config.services.pipewire;

  defaults = {
    services = {
      pipewire = {
        enable = true;
        audio.enable = true;
        pulse.enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        jack.enable = true;
        extraLv2Packages = with pkgs; [
          lsp-plugins # https://lsp-plug.in
          swh_lv2 # LV2 version of Steve Harris' SWH plugins
          calf
        ];

        # NOTE: you can use `pw-config' to inspect current effective pipewire
        # configuration e.g. `pw-config merge context.modules'
        extraConfig.pipewire = {
          # TODO add (some) customizations from local? Currently they are in
          # "pipewire-user" home-manager profile.
        };

        # Jack emulation config reference: https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Config-JACK
        extraConfig.jack = {
          "20-hide-midi" = {
            # Hide MIDI clients and their ports in JACK.
            "jack.properties" = {
              "jack.show-midi" = false;
            };
          };
          "50-user" = {
            "jack.properties" = {
              "jack.short-name" = true;
              "jack.merge-monitor" = true;
            };
          };
        };
      };
    };

    # wireplumber spews some warnings if xdg-desktop-portal is not available,
    # so enable the gtk version.
    xdg.portal.enable = lib.mkDefault true;
    xdg.portal.config.common.default = lib.mkDefault "gtk";
    xdg.portal.extraPortals = lib.mkDefault [ pkgs.xdg-desktop-portal-gtk ];
    # For standalone window manager to work (not sure if really necessary)
    systemd.user.services.xdg-desktop-portal.environment.XDG_CURRENT_DESKTOP = "gnome";
  };
in
{
  imports = [ defaults ];

  config = mkIf cfg.enable {

    # Make sure PA is disabled.
    hardware.pulseaudio.enable = lib.mkForce false;

    environment.systemPackages = with pkgs; [
      alsa-utils # For ALSA debugging
      qpwgraph # Qt-based graph/patchbay (inspired by QJackCtl)
      helvum # GTK-based patchbay (inspired by catia)
      pulseaudio # pulseaudio's client programs (pactl etc.) are also useful with the pipewire compat libs.
      carla # useful for playing around with LADSPA/LV2/etc. plugins
      calf # the calf executable and lv2 plugin
      v4l-utils # qv4l2 etc.
    ];

    # improve performance, audio benefits from realtime scheduling.
    security.rtkit.enable = lib.mkDefault true;

    # increase IO prio and memlock for pipewire processes.
    systemd.user.services.pipewire.serviceConfig = {
      LimitMEMLOCK = "infinity";
      # TODO enabling IO schedule changes in the user session requires some
      # hacking first. See <https://unix.stackexchange.com/questions/589175/how-to-change-linux-scheduling-policy-for-user-systemd-file>
      #IOSchedulingPriority = 1;
      #IOSchedulingClass = "realtime";
    };

    systemd.user.services.pipewire.environment = {
      LADSPA_PATH = makeSearchPath "lib/ladspa" [ pkgs.ladspaPlugins ];
    };

    systemd.user.services.wireplumber.serviceConfig.LimitMEMLOCK = "infinity";
  };
}
