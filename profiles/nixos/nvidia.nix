{ lib, pkgs, ... }:

# XXX: certain configuration requires that `allowAliases = true`, which is the
# default.
#
# Fast approximate antialiasing is an antialiasing mode supported by the NVIDIA
# graphics driver that offers advantages over traditional multisampling and supersampling
# methods. This mode is incompatible with UBB, triple buffering, and other antialiasing
# methods. To enable this mode, run:
#
#     nvidia-settings --assign FXAA=1

{
  config = {
    # Force turn off screen:
    # vbetool dpms off && read -n1; vbetool dpms on
    environment.systemPackages = [ /* pkgs.cudatoolkit */ pkgs.vbetool ];

    hardware.nvidia = {
      open = false;
      modesetting.enable = true;

      # See below X setting which is nicer
      #forceFullCompositionPipeline = true;

      #powerManagement.enable = false; # turning this on requires enabling offloading
      #dynamicBoost.enable = true;
    };

    # Documentation: <https://download.nvidia.com/XFree86/Linux-x86_64/560.35.03/README/xconfigoptions.html>
    #
    # Video memory used for the framebuffer: HR * VR * (bpp/8)
    # where HR = horizontal res., VR = vertical res., bpp = bytes per pixel
    services.xserver = {
      videoDrivers = [ "nvidia" ];

      #serverFlagsSection = ''
      #  Option "Xinerama" "True"
      #'';

      deviceSection = ''

        # Don't connect to ACPI daemon to receive events.
        Option "ConnectToAcpid" "off"

        # The size of a cache in system memory used to accelerate software
        # rendering. Specified in bytes. Default: 0x800000 (8 Megabytes).
        # 0x4000000 == 64 Megabytes.
        Option "SoftwareRenderCacheSize" "0x4000000"

        # Coolbits: Control available overclocking options.
        # NOTE: Does not work if X is run in rootless mode!
        #  1 (b0): Enable overclocking of older (pre-Fermi) cards.
        #  2 (b1): Attempt SLI even if GPUs have different amounts of VRAM.
        #  4 (b2): Enable manual configuration of GPU fan speed in nvidia-settings.
        #  8 (b3): Enable overclocking in nvidia-settings. Fermi and newer.
        # 16 (b4): Enable overvoltage using nvidia-settings CLI options. Fermi and newer.
        Option "Coolbits" "${toString (4 + 8 + 16)}"

        # When an NVIDIA GLX client is used, the related environment variable
        # __GL_ALLOW_UNOFFICIAL_PROTOCOL will need to be set as well to
        # enable support in the client.
        Option "AllowUnofficialGLXProtocol" "on"

        Option "GLShaderDiskCache" "on"

        # Enables RGB workstation overlay visuals. This is only supported on
        # NVIDIA RTX/Quadro GPUs (Quadro NVS GPUs excluded) in depth 24.
        Option "Overlay" "on"
      '';

      screenSection = ''
        # Don't start if no display devices can be found.
        Option "AllowEmptyInitialConfiguration" "off"

        # Option "UseDisplayDevice" "DFP-1, DFP-2, DFP-3, DFP-4"

        # To print available metamodes at startup.
        Option "IncludeImplicitMetaModes" "on"

        # Controls the default relationship between display devices when
        # using multiple display devices on a single X screen.
        Option "MetaModeOrientation" "RightOf"

        # Don't recompute size in millimeters of the X screen whenever the
        # size in pixels of the X screen is changed using XRandR.
        Option "ConstantDPI" "FALSE"

        # Enable Force(Full)CompositionPipeline for all displays by default.
        Option "ForceCompositionPipeline"     "on"
        Option "ForceFullCompositionPipeline" "on"
      '';
    };

    # This seems to be necessary for hardware decoding with mpv, but
    # option doesn't exist anymore. Still necessary?
    #hardware.opengl.setLdLibraryPath = true;

    boot.extraModprobeConfig = lib.concatStringsSep "\n" [
      # Enable PCIe Gen 3.x
      "options nvidia NVreg_EnablePCIeGen3=1"
      # Enable use of PAT
      "options nvidia NVreg_UsePageAttributeTable=1"
      "options nvidia NVreg_DynamicPowerManagement=0x02"
      # Never disable video memory
      #"options nvidia NVreg_DynamicPowerManagementVideoMemoryThreshold=0"
      "options nvidia TemporaryFilePath=/var/tmp"
      "options nvidia EnableResizableBar=1"
      "options nvidia NVreg_PreserveVideoMemoryAllocations=1"
      #"options nvidia NVreg_RegistryDwords=\"\""
    ];

    # https://wiki.archlinux.org/title/NVIDIA/Troubleshooting
    boot.kernelParams = [ "rcutree.rcu_idle_gp_delay=1" ];

    # XXX: https://unix.stackexchange.com/questions/617897/how-can-i-get-the-correct-resolution-with-an-aspeed-graphics-controller-under-de
    boot.blacklistedKernelModules = [ "ast" ];
  };
}
