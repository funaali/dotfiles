{ lib, pkgs, ... }:

with lib;

# NOTE: Might need to set SANE_USB_WORKAROUND=1 before starting scan
# frontend:
#
# The HP shitbox needs to be used via the network scanning method, otherwise
# gives weind errors. And that in turn requires Avahi to be properly
# configured.
#
#    scanimage -L

{
  config = {
    services.printing = {
      enable = true;
      listenAddresses = [ "*:631" ];
      tempDir = "/tmp/cups";
      defaultShared = true;
      drivers = with pkgs; [
        hplipWithPlugin
      ];
      cups-pdf = {
        enable = true;
        #pdf = { };
      };
      browsing = true;
      allowFrom = [
        "localhost"
        "192.168.0.0/16"
        "[FC00::]/7"
      ];
    };

    hardware.sane = {
      enable = true;
      openFirewall = true;

      extraBackends = with pkgs; [
        hplipWithPlugin
        sane-airscan
      ];

      # Disable unnecessary backends because they can be slow to iterate over.
      disabledDefaultBackends = [ "v4l" ];

      # Network hosts which are probed for remote scanners.
      # "localhost" is included by default.
      netConf = ''
        lambda-nebula
      '';
    };

    services.saned = {
      enable = true;
      extraConfig = ''
        127.0.0.0/8
        169.254.0.0/16
        172.16.0.0/12
        192.168.0.0/16
        192.18.0.0/15
        10.0.0.0/8
        [::1/128]
        [FC00::/7]
        [FE80::/10]
        [64:FF9B:1::/48]
      '';
    };

  };
}
