{ pkgs, ... }:

# This profile enables development-related stuff.

{
  config = {
    environment.systemPackages = with pkgs; [
      nixos-option
    ];

    # Generate documentation
    documentation.man.generateCaches = true;
    documentation.dev.enable = true;

    # longer than default
    nix.gc.options = "--delete-older-than 14d";

    # Avoid build inputs from being GC'd
    nix.settings.keep-outputs = true;

    # Keep build-time dependencies of derivations stored in user environments.
    nix.settings.keep-env-derivations = true;

    # Make sure dockerd is available
    virtualisation.docker.enable = true;
  };
}
