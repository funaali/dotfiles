{ ... }:

{
  config = {
    users.mutableUsers = true;

    users.users.sim = {
      isNormalUser = true;
      extraGroups = [
        "audio"
        "video"
        "wheel"
        "docker"
        "vboxusers"
        "kvm"
        "plugdev"
        "lp"
        "scanner"
      ];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIlIECoPAF/Uy+ZB3tpbMhXwF/5xNdUFLwLifn5zUZgf samuli@F3D3AB3309F04D1CA4D851D068F82A4F3ECA091D"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEw7Zj7J6x5+rHsD0j3GvC7Kfsn0YFNBCkQbrzFQlTaF sim@applicative"
      ];
    };
  };
}
