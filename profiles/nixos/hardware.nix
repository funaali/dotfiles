{ config, pkgs, ... }:

# Sets some opinionated defaults for bare-metal machines (laptop & desktop).

let
  sata_lpm_policy = "med_power_with_dipm";
in
{
  config = {
      environment.systemPackages = with pkgs; [
        yubikey-manager # The ykman tool.
        yubikey-personalization # Older ykpersonalize tool.
        yubikey-touch-detector # Detect when yubikey wants to be touched.
        yubikey-agent # Seamless ssh-agent for Yubikeys.
        yubico-piv-tool # For interacting with the PIV Card App.
        age-plugin-yubikey
        web-eid-app
        libdigidocpp
        qdigidoc
        config.boot.kernelPackages.turbostat
        powertop
        pcsc-tools # For pcsc_scan etc.
        opensc
      ];

      environment.etc."chromium/native-messaging-hosts/eu.webeid.json".source = "${pkgs.web-eid-app}/share/web-eid/eu.webeid.json";
      environment.etc."opt/chrome/native-messaging-hosts/eu.webeid.json".source = "${pkgs.web-eid-app}/share/web-eid/eu.webeid.json";

      #sound.enable = true;

      boot.extraModprobeConfig = ''
        options snd slots=snd_hda_intel
        options snd_hda_intel power_save_controller=0 power_save=1 bdl_pos_adj=2
      '';

      # Enable firmware updates (fwupdmgr update)
      services.fwupd.enable = true;

      # Enable bluetooth
      hardware.bluetooth.enable = true;

      # Enable QMK udev rules
      hardware.keyboard.qmk.enable = true;

      # Enable UHK udev rules
      hardware.keyboard.uhk.enable = true;

      # Enable Ledger udev rules
      hardware.ledger.enable = true;

      # Enable udev rules for gnupg smart cards.
      hardware.gpgSmartcards.enable = true;

      # Expected by android udev rules
      users.groups.adbusers = {};

      services.udev.packages = with pkgs; [
        libmtp.out
        android-udev-rules
      ];
      # Enable PCSCD (smartcard reader)
      services.pcscd = {
        enable = true;
        # Plugin packages to be used for PCSC-Lite.
        plugins = with pkgs; [
          ccid
          pcsc-cyberjack
          scmccid
          libacr38u
        ];
        # Extra command line arguments to be passed to the PCSC daemon.
        #extraArgs = [ ];
        # Configuration for devices that aren’t hotpluggable.
        # See reader.conf(5) for valid options.
        #readerConfig = "";
      };


      services.udev.extraRules = ''
        # SATA Active Link Power Management - https://insanity.industries/post/sata-power-consumption/
        SUBSYSTEM=="scsi_host", KERNEL=="host*", ATTR{link_power_management_policy}="${sata_lpm_policy}"

        # PCI Runtime Power Management
        SUBSYSTEM=="pci", TEST=="power/control", ATTR{power/control}="auto"
        # TODO: probably not correct/useful:
        SUBSYSTEM=="ata_port", KERNEL=="ata*", TEST=="power/control", ATTR{power/control}="auto"

        ${builtins.readFile ./usb-power-save.rules}
      '';
    };
}
