{ ... }:

# Use kmscon instead of gettys. Useful on laptops and desktops with drm
# graphics.
#
# Disables gettys.

{
  services.kmscon = {
    enable = true;
    # Get keyboard config from xkb config
    useXkbConfig = true;
    # Use hardware acceleration
    hwRender = true;
    extraConfig = "font-size=14";
    #extraOptions = "--term xterm-256color";
    #fonts = [ { name = ""; package = ?; } ];
  };
}
