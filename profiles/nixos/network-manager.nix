# Enable networking using network manager
{ lib, config, pkgs, ... }:

{
  networking.networkmanager = {
    enable = true; # Easiest to use and most distros use this by default.
    plugins = with pkgs; [
      networkmanager-fortisslvpn
      networkmanager-openconnect
    ];
  };

  # Also enable systemd-resolved
  services.resolved = {
    enable = true;
  };

  # Dynamic DNS
  services.ddns = lib.mkIf (config.networking.domain != null) {
    enable = lib.mkDefault true;
    provider = lib.mkDefault "hetzner";
    zone = lib.mkDefault config.networking.domain;
    envFile = config.age.secrets."ddns.conf".path or null;
  };
}
