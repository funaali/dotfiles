{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    # linux/misc
    binutils
    kitty.terminfo
    lsof
    killall
    file
    ncdu
    git
    git-crypt
    jq
    zip
    iotop
    dstat

    # hardware
    pciutils
    usbutils
    smartmontools
    hwinfo
    ethtool
    lshw
    lm_sensors

    # filesystem
    cryptsetup
    gptfdisk
    dosfstools
    exfat
    exfatprogs
    parted # provides partprobe!

    # networking
    dnsutils
    nmap
    iftop
    iputils
    tcpdump
  ];
}
