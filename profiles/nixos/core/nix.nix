{ config, lib, pkgs, mylib, ... }:
let
  bytesFromMB = m: m * 1024 * 1024;

  # nix-daemon use /nix/tmp instead of /tmp
  moveNixTmpdir = let tmp = "/nix/tmp"; in {
    systemd.services.nix-daemon.environment.TMPDIR = tmp;
    systemd.tmpfiles.rules = [ "d ${tmp} 0755 root root 1d" ];
  };

  # Add the nixConfig attributes (substituters and public keys foremost) from the
  # flake to the system nix.conf.
  useFlakeNixConfig = {
    nix.settings = (import (mylib.relativeToRoot "flake.nix")).nixConfig;
  };

  # Nix errors if a netrc-file is configured but the file does not exist.
  # So we create an empty file if necessary.
  setupNetrcFile = { config, ... }: let
    netrcFile = config.nix.settings.netrc-file or null;
  in {
    nix.settings.netrc-file = lib.mkDefault "/etc/nix/netrc";
    nix.settings.extra-sandbox-paths = lib.mkIf (!isNull netrcFile) [ netrcFile ];
    system.activationScripts.touch-nix-netrc-file.text = lib.mkIf (!isNull netrcFile) ''
      if ! [[ -e "${netrcFile}" ]]; then touch "${netrcFile}"; fi
    '';
  };
in
{
  imports = [
    moveNixTmpdir
    useFlakeNixConfig
    setupNetrcFile
  ];

  config = {
    nix = {
      package = pkgs.nixVersions.latest;
      settings = {
        allowed-users = [ "@users" ];
        trusted-users = [ "root" "@wheel" ];
        auto-optimise-store = true;
        warn-dirty = false;
        log-lines = 120;
        extra-experimental-features = [
          "nix-command"
          "flakes"
          "cgroups"
          "auto-allocate-uids"
          "configurable-impure-env"
          "recursive-nix"
          "ca-derivations"
        ];
        auto-allocate-uids = true;
        builders-use-substitutes = true;
        connect-timeout = 5;
        download-buffer-size = bytesFromMB 128;

        # When free disk space in /nix/store drops below min-free during a build,
        # Nix performs a garbage-collection until max-free bytes are available
        # or there is no more garbage. (bytes)
        min-free = bytesFromMB 150;
        max-free = bytesFromMB 5000;

        # Fall back to building from source when binary substitution fails.
        fallback = true;

        # Faster not to fsync.
        fsync-metadata = false;

        max-jobs = lib.mkDefault 4;
        cores = lib.mkDefault 2;

        # Whether to preallocate files when writing objects with known size.
        preallocate-contents = lib.mkDefault true;

        # Whether to execute builds inside cgroups. This is only supported on Linux.
        use-cgroups = lib.mkDefault true;

        # Enable the default features
        system-features = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
      };

      gc = {
        automatic = true;
        dates = "daily";
        options = lib.mkDefault "--delete-older-than 1d";
      };

      distributedBuilds = true;
      optimise.automatic = true;
    };

    # "idle" for desktop use
    nix.daemonCPUSchedPolicy = "idle";
    nix.daemonIOSchedClass = "idle";
    nix.daemonIOSchedPriority = 5;
    systemd.services.nix-daemon.serviceConfig = {
      Nice = 15;
    };

    # Options defined in flake-utils-plus module
    nix.linkInputs = true;
    nix.generateRegistryFromInputs = true;
    nix.generateNixPathFromInputs = true;

    # make system nix path follow flakes in registry
    # https://github.com/NobbZ/nixos-config/blob/main/nixos/modules/flake.nix
    nix.nixPath = [
      "nixpkgs=/etc/nixpkgs/channels/nixpkgs"
    ];
    systemd.tmpfiles.rules = [
      "L+ /etc/nixpkgs/channels/nixpkgs - - - - ${config.nix.registry.nixpkgs.flake}"
    ];
  };
}
