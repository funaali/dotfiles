# https://www.kernel.org/doc/Documentation/sysctl/net.txt
# https://www.kernel.org/doc/Documentation/networking/ip-sysctl.txt

{ config, lib, ... }:

with lib;

{
  config = {
    security.pam.loginLimits = [
      # For realtime privileges
      { domain = "@users"; type = "-"; item = "rtprio"; value = "98"; }
      { domain = "@users"; type = "-"; item = "memlock"; value = "unlimited"; }
      { domain = "@users"; type = "-"; item = "nice"; value = "-11"; }
    ];

    # Disable sudo password
    security.sudo.wheelNeedsPassword = false;

    security.polkit.extraConfig = ''
      polkit.addRule(function(action, subject) {
        if (subject.isInGroup("wheel")) {
          return polkit.Result.YES;
        }
      });
    '';

    security.acme.acceptTerms = true;

    services.sshguard = {
      enable = mkDefault config.services.openssh.enable;
      services = [
        "sshd"
        "sshd-session"
      ];
      attack_threshold = 50;
      detection_time = 86400; # seconds to remember attackers
      whitelist = [
        "::1/128"                 # localhost
        "127.0.0.0/8"             # localhost
        "10.0.0.0/8"              # private network (RFC1918)
        "172.16.0.0/12"           # private network (RFC1918)
        "192.168.0.0/16"          # private network (RFC1918)
        "fd00::/8"                # private network (RFC 4193)
        "100.64.0.0/10"           # Dedicated for CGNAT, these can't really be blocked
        "116.203.49.115/32"       # morphism static ipv4
        "2a01:4f8:1c1c:2605::/64" # morphism static ipv6
        "144.208.197.112/32"      # telematica static ipv4
        "2a01:aea0:dd3:2aa::/64"  # telematica ipv6 prefix XXX: static???
      ];
    };
  };
}
