{ config, lib, pkgs, ... }:

# Virtual console related stuff.

{
  config = {
    boot.loader.systemd-boot.consoleMode = "max";

    console = {
      keyMap = "dvorak-programmer";
      font = "ter-powerline-v20n";
      packages = [ pkgs.powerline-fonts ];
      earlySetup = true;
    };

    services.physlock = {
      enable = true;
      allowAnyUser = true;
      lockMessage = lib.replaceStrings [ "\n" "\"" "\\E" ] [ "\\n" "\\\"" "\\033" ] ''
        \E[32m<<< ${config.networking.fqdnOrHostName} >>>\E[39;49m

        All consoles / VTs are locked (physlock).'';
    };
  };
}
