{ config, pkgs, lib, ... }:
let
  inherit (lib) mkDefault;

  get-smtp-password = pkgs.runCommandCC "get-smtp-password" { } ''
    substitute ${./get-msmtp-password.c} main.c \
       --replace-fail @EXPECTED_BINARY@ /run/current-system/sw/bin/msmtp \
       --replace-fail @PASSWORD_FILE@ ${config.age.secrets.msmtp-default-password.path}
    gcc -o $out main.c
  '';
in
{
  imports = [
    ./console.nix
    ./packages.nix
    ./nix.nix
    ./security.nix
  ];

  config = {

    # Set default state version
    system.stateVersion = "24.11";

    security.wrappers.msmtp-get-smtp-password = {
      setuid = true;
      owner = "root";
      group = "root";
      source = "${get-smtp-password}";
    };

    custom = {
      TCPBBRCongestionControl = lib.mkDefault true;
      enableNativeOptimizationsByDefault = lib.mkDefault [ "znver2" ];
    };

    # Enable non-free firmware too
    hardware.enableAllFirmware = lib.mkDefault true;

    time.timeZone = "Europe/Vienna";
    i18n.defaultLocale = "en_US.UTF-8";
    i18n.supportedLocales = [
      "C.UTF-8/UTF-8"
      "en_US.UTF-8/UTF-8"
    ];

    boot = {
      kernelPackages = lib.mkDefault pkgs.linuxPackages_latest;

      loader = {
        efi.canTouchEfiVariables = lib.mkDefault true;
        systemd-boot.enable = lib.mkDefault true;
      };

      tmp = {
        cleanOnBoot = true;
        useTmpfs = true; # Mount a tmpfs at /tmp
        tmpfsSize = "80%";
      };

      kernel.sysctl = {
        # hide kernel symbols regardless of permissions.
        "kernel.kptr_restrict" = 2;

        # Increase amount of inotify watches
        "fs.inotify.max_user_instances" = 2000000;
        "fs.inotify.max_user_watches" = 2000000;

        # % of RAM (max) for dirty pages, new I/O blocks until flushed (safeguard), Or absolute value:
        # default: 20
        "vm.dirty_ratio" = 0;
        "vm.dirty_bytes" = 4294967296;

        # % of RAM for dirty pages before async flush is triggered, or absolute value
        "vm.dirty_background_ratio" = 0;
        "vm.dirty_background_bytes" = 1610612736;

        "vm.dirty_writeback_centisecs" = 2000; # 20 seconds

        "vm.swappiness" = 25;

        # Avoid the possibly expensive memory dump on OOM.
        "vm.oom_dump_tasks" = lib.mkDefault 0;

        # Memory allocation tuning
        "vm.overcommit_memory" = lib.mkDefault 2;
        "vm.overcommit_ratio" = lib.mkDefault 140;
        "vm.admin_reserve_kbytes" = lib.mkDefault 2048;

        "vm.vfs_cache_pressure" = 50;

        # net.core
        "net.core.somaxconn" = 8192;
        "net.core.netdev_max_backlog" = 50000;
        "net.core.rmem_max" = 64000000; # 64M
        "net.core.wmem_max" = 33554432; # 32M
        "net.core.rmem_default" = 1048576; # 1M
        "net.core.wmem_default" = 16777216; # 16M
        "net.core.optmem_max" = 25165824; # 24M

        # UDP
        "net.ipv4.udp_rmem_min" = 16384;
        "net.ipv4.udp_wmem_min" = 16384;

        # TCP
        # NOTE: tcp_* settings under ipv4 apply to ipv6 as well.
        "net.ipv4.tcp_rmem" = "4096 1048576 64000000"; # 4K 1M 64M
        "net.ipv4.tcp_wmem" = "4096 1048576 33554432"; # 4K 1M 32M
        "net.ipv4.tcp_synack_retries" = 2;
        "net.ipv4.tcp_keepalive_time" = 90;
        "net.ipv4.tcp_keepalive_probes" = 5;
        "net.ipv4.tcp_keepalive_intvl" = 15;
        "net.ipv4.tcp_mtu_probing" = 1; # better reliability with JFs
        "net.ipv4.tcp_rfc1337" = 1; # RFC1337
        # Tells the system whether it should start at the default window size only for new TCP connections or also for existing TCP connections that have been idle for too long. Default: 1
        "net.ipv4.tcp_slow_start_after_idle" = 0;
        # Minimum time a socket will stay in TIME_WAIT state (unusable after being used once). Default: 60
        "net.ipv4.tcp_fin_timeout" = 10;
        "net.ipv4.tcp_max_syn_backlog" = 30000;
        "net.ipv4.tcp_max_tw_buckets" = 2000000;
        "net.ipv4.tcp_tw_reuse" = 1;
        "net.ipv4.tcp_fastopen" = 3;

        # Use wider IPv4 local port range. Default: 32768 60999
        "net.ipv4.ip_local_port_range" = "2000 65535";

        "net.ipv4.ip_nonlocal_bind" = 1;

        # Enable strict reverse path filtering (that is, do not attempt to route
        # packets that "obviously" do not belong to the iface's network; dropped
        # packets are logged as martians).
        "net.ipv4.conf.all.log_martians" = mkDefault true;
        "net.ipv4.conf.all.rp_filter" = mkDefault "1";
        "net.ipv4.conf.default.log_martians" = mkDefault true;
        "net.ipv4.conf.default.rp_filter" = mkDefault "1";

        # Ignore outgoing ICMP redirects (this is ipv4 only)
        "net.ipv4.conf.all.send_redirects" = mkDefault false;
        "net.ipv4.conf.default.send_redirects" = mkDefault false;

        # Don't map IPv4 to IPv6 listeners.
        # XXX: don't actually want this enabled usually?
        "net.ipv6.bindv6only" = 0;

        # Increase route table max_size
        "net.ipv6.route.max_size" = 2147483;

        # Allow processes to bind to non-local IPv6 addresses.
        "net.ipv6.ip_nonlocal_bind" = 1;

        # IPv6 Proxy NDP enable
        # Note: every address to proxy has to be specified individually:
        #    ip -6 neigh add proxy 2a02:8388:e101:4b80:3ade:adff:fec6:4c62 dev wlp2s0
        "net.ipv6.conf.all.proxy_ndp" = 1;
        "net.ipv6.conf.default.proxy_ndp" = 1;
      };
    };

    # Linux kernel Same-Page Merging
    hardware.ksm = {
      enable = true;
      sleep = 100;
    };

    # enable nftables
    networking.nftables = {
      enable = true;
    };

    # Enable the firewall module to use nftables
    networking.firewall = {
      package = pkgs.iptables-nftables-compat;
      extraPackages = with pkgs; [ nftables ipset ];
    };

    programs.vim = {
      enable = true;
      package = pkgs.my-vim;
      defaultEditor = true;
    };

    programs.tmux = {
      enable = true;
      aggressiveResize = true;
      historyLimit = 8000;
      keyMode = "vi";
      newSession = true;
      terminal = "tmux-256color";
    };

    services.getty = {
      greetingLine = ''<<< Welcome to ${config.system.nixos.distroName} ${config.system.nixos.label} (\m) - \l >>>'';
      extraArgs = [ "--long-hostname" ];
      #loginOptions = lib.escapeShellArgs [ ];
    };

    # Enable systembus-notify explicitly (even though it's required by many
    # other services).
    services.systembus-notify.enable = true;

    # Enable EarlyOOM
    services.earlyoom = {
      enable = true;
      enableNotifications = true; # via d-bus
      extraArgs = [ "-g" ];
      freeMemThreshold = lib.mkDefault 2;
      freeSwapThreshold = lib.mkDefault 5;
    };

    # Enable automatic BPF tuning.
    services.bpftune.enable = true;

    # Enable irqbalance.
    services.irqbalance.enable = true;
    systemd.services.irqbalance.serviceConfig.ExecStart = [
      ""
      "${pkgs.irqbalance}/bin/irqbalance --foreground --journal"
    ];

    # OpenSSH with hpn + kerberos + gssapi
    programs.ssh = {
      package = pkgs.openssh_hkg;
    };

    # Enable the OpenSSH daemon.
    services.openssh = {
      enable = true;
      banner = ''Welcome to ${config.system.nixos.distroName} ${config.system.nixos.label} - ${config.networking.fqdnOrHostName}'';
      settings = {
        PrintMotd = true;
      };
    };

    # Enable sshd in initrd, too.
    boot.initrd.network.ssh.enable = true;

    programs.msmtp = {
      enable = true;
      defaults = {
        aliases = "/etc/aliases";
        port = 587;
        tls = true;
        #read-recipients = true;
        #read-envelope-from = true;
      };
      accounts = {
        default = {
          host = "smtp.protonmail.ch";
          user = "automations@funktionaali.com";
          auth = true;
          passwordeval = "/run/wrappers/bin/msmtp-get-smtp-password";
          port = 587;
          tls_starttls = true;
          from = "automations@funktionaali.com";
          from_full_name = "${config.networking.fqdnOrHostName}";
        };
      };
    };

    age.secrets."msmtp-default-password".file = ../../../secrets/smtp.protonmail.ch-password.age;

    # Enable GnuPG (for users). Also makes the gpg command available for
    # everyone.
    programs.gnupg = {
      agent = {
        enable = true;
        enableExtraSocket = true;
        enableSSHSupport = true;
        enableBrowserSocket = true;
      };
      dirmngr.enable = true;
    };

    # XXX: No option for this like gnupg.agent.settings
    environment.etc."gnupg/gpg.conf".text = ''
      expert
      no-greeting
      personal-compress-preferences ZIP Uncompressed
    '';
  };
}
