#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <limits.h>

int check_parent_binary(const char *allowed_path) {
    char path[PATH_MAX];
    char expected_path[PATH_MAX];
    char parent_pid[10];

    // Get the parent PID
    pid_t ppid = getppid();
    sprintf(parent_pid, "%d", ppid);

    // Construct the path to the parent process's executable
    snprintf(path, sizeof(path), "/proc/%s/exe", parent_pid);

    // Read the symbolic link to get the executable name of the parent process
    ssize_t len = readlink(path, path, sizeof(path) - 1);
    if (len == -1) {
        perror("readlink");
        exit(EXIT_FAILURE);
    }
    path[len] = '\0';  // Null-terminate the string

    // Read the symbolic link allowed_path if it is one to get the actual
    // binary.
    if ((len = readlink(allowed_path, expected_path, sizeof(expected_path) - 1)) != -1) {
            expected_path[len] = '\0';
    } else if (errno == EINVAL) { // is already a file, not symlink
            strcpy(expected_path, allowed_path);
    } else {
            perror("expected file could not be found");
            exit(EXIT_FAILURE);
    }
    // second layer of links
    if ((len = readlink(expected_path, expected_path, sizeof(expected_path) - 1)) != -1) {
            expected_path[len] = '\0';
    } else if (errno != EINVAL) {
            perror("error while reading second hop link");
            exit(EXIT_FAILURE);
    }

    // Print the path of the parent process's binary
    //printf("Parent process binary: %s\n", path);

    // Check if the parent process is the expected binary
    if (strcmp(path, expected_path) == 0) {
        //printf("Parent process is the expected binary.\n");
        return 0;
    } else {
        fprintf(stderr, "Parent process is not the expected binary: %s.\n", path);
        return 1;
    }
}

void read_first_line(const char *filename) {
        FILE *file;
        char *line = NULL;
        size_t len = 0;
        ssize_t read;

        file = fopen(filename, "r");
        if (file == NULL) {
                perror("Error opening file");
                exit(EXIT_FAILURE);
        }
        if ((read = getline(&line, &len, file)) != -1) {
                printf("%s", line);
        }
        fclose(file);
        if (line)
                free(line);
        exit(EXIT_SUCCESS);
}

int main() {
    // This function checks the parent process's binary
    if (check_parent_binary("@EXPECTED_BINARY@") == 0) {
        read_first_line("@PASSWORD_FILE@");
    } else {
        exit(EXIT_FAILURE);
    }
}
