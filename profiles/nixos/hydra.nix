{ config, pkgs, ... }:

# After startup need to create an admin user for the web ui:
#
# su - hydra
# $ hydra-create-user sim --full-name 'S. Admin' \
#     --email-address 'sim@localhost' --password-prompt --role admin

let
  hostName = config.networking.hostName;
in
{
  config = {
    services.hydra = {
      enable = true;
      # XXX: trying hydra from overlay from upstream flake
      # XXX: trying hydra from overlay from upstream flake
      ## Attempt to match the nix version used by hydra to one we use on the
      ## system.
      ## optimized build takes ages b/c builds ghc
      #package = pkgs.hydra_unstable.override {
      #  nix = config.nix.package;
      #};
      port = 6265;
      hydraURL = "http://127.0.0.1:${toString config.services.hydra.port}"; # externally visible URL
      notificationSender = "hydra@${hostName}"; # e-mail of hydra service
      # a standalone hydra will require you to unset the buildMachinesFiles list to avoid using a nonexistant /etc/nix/machines
      buildMachinesFiles = [ ];
      # you will probably also want, otherwise *everything* will be built from scratch
      useSubstitutes = true;
      minimumDiskFreeEvaluator = 8; # GiB
      minimumDiskFree = 16; # GiB
    };

    # Must allow the hydra user to connect to the nix-daemon
    nix.settings.extra-allowed-users = [ "@hydra" ];

    # Hydra evaluates in restricted mode; must allow URIs used as flake
    # inputs etc.
    nix.settings.allowed-uris = [
      "github:"
      "https://github.com/"
      "git+https://github.com/"
      "git+ssh://github.com/"
      "gitlab:"
    ];
  };
}
