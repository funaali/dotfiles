# Enable virtualbox (host)

{ ... }:

{
  virtualisation.virtualbox.host = {
    enable = true;
    enableKvm = true;

    # Enable oracle's unfree extensions for forwarding usb2/3.
    # NOTE: takes long time to compile; no official binary substitution
    # possible.
    enableExtensionPack = true;

    addNetworkInterface = false;
  };
}
