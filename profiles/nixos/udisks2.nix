{ pkgs, ... }:

# NOTE: to ignore disks by UUID:
#
#   SUBSYSTEM=="block",
#   ENV{ID_FS_UUID}=="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXXX",
#   ENV{UDISKS_IGNORE}="1"

{
  config = {

    # Install udiskie, too
    environment.systemPackages = [ pkgs.udiskie ];

    services.udisks2 = {
      enable = true;

      settings."udisks2.conf" = {
        defaults.encryption = "luks2";
        udisks2.modules = [ "*" ];
        udisks2.modules_load_preference = "ondemand";
      };

      settings."mount_options.conf" = {
        defaults.btrfs_defaults = "compress=zstd";
        # add iocharset=utf8
        defaults.vfat_defaults = "uid=$UID,gid=$GID,shortname=mixed,utf8=1,showexec,flush,iocharset=utf8";
      };
    };

    systemd.tmpfiles.rules = [ "D! /media 0755 root root 0 -" ];

    services.udev.extraRules = ''
      # udisks2: mount as /media/$Volume instead of /run/media/$USER/$Volume
      ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"
    '';
  };
}
