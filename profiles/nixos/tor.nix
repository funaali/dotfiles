{ ... }:

/* Enable Tor with sensible settings for client-only. */

{
  config = {
    services.tor = {
      enable = true;
      openFirewall = true;
      client = {
        enable = true;
        dns.enable = true;
        socksListenAddress = {
          addr = "[::1]";
          port = 9050;
          IPv6Traffic = true; # allow IPv6 over socks5
          PreferIPv6 = true;
        };
      };
      settings = {
        ClientUseIPv6 = true; # Allow ipv6 connections
        ClientPreferIPv6ORPort = true;
        ControlPort = 9051;
      };
    };
  };
}
