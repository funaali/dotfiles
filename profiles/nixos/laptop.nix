{ lib, pkgs, ... }:

with lib;

let
  xuser = "sim";

  xpub = pkgs.xpub;

  runxuser = pkgs.writeScript "runxuser" ''
    su "$1" -c "$2"
  '';
in
{
  imports = [
    ./hardware.nix
  ];

  config = {
    environment.systemPackages = with pkgs; [
      acpi
    ];

    # requires xpub, notify-send
    services.udev.path = with pkgs; [
      xpub
      libnotify
    ];

    services.udev.extraRules = ''
      ACTION=="change", SUBSYSTEM=="rfkill", ENV{RFKILL_STATE}=="1" IMPORT{program}="${xpub}/bin/xpub", RUN+="${runxuser} ${xuser} '${pkgs.libnotify}/bin/notify-send %E{RFKILL_TYPE}\ enabled %E{RFKILL_NAME}'"
      ACTION=="change", SUBSYSTEM=="rfkill", ENV{RFKILL_STATE}=="0" IMPORT{program}="${xpub}/bin/xpub", RUN+="${runxuser} ${xuser} '${pkgs.libnotify}/bin/notify-send %E{RFKILL_TYPE}\ blocked %E{RFKILL_NAME}'"

      ACTION=="add",    SUBSYSTEM=="rfkill" IMPORT{program}="${xpub}/bin/xpub", RUN+="${runxuser} %E{XUSER} '${pkgs.libnotify}/bin/notify-send rfkill %E{RFKILL_TYPE}:\ %E{RFKILL_NAME}\ \(added\)'"
      ACTION=="remove", SUBSYSTEM=="rfkill" IMPORT{program}="${xpub}/bin/xpub", RUN+="${runxuser} %E{XUSER} '${pkgs.libnotify}/bin/notify-send rfkill %E{RFKILL_TYPE}:\ %E{RFKILL_NAME}\ \(removed\)'"

      # battery
      ACTION=="change", SUBSYSTEM=="power_supply", KERNEL=="*", ATTR{type}=="Battery", ATTR{status}=="Unknown|Discharging" IMPORT{program}="${xpub}/bin/xpub", RUN+="${runxuser} %E{XUSER} '${pkgs.libnotify}/bin/notify-send Battery:\ Discharging:\ $attr{capacity}%%'"
      ACTION=="change", SUBSYSTEM=="power_supply", KERNEL=="*", ATTR{type}=="Battery", ATTR{status}!="Unknown|Discharging" IMPORT{program}="${xpub}/bin/xpub", RUN+="${runxuser} %E{XUSER} '${pkgs.libnotify}/bin/notify-send Battery:\ $attr{status}:\ $attr{capacity}%%'"

      # suspend when battery status drops to under 5%
      #SUBSYSTEM=="power_supply", ATTR{status}=="Discharging", ATTR{capacity}=="[0-5]", RUN+="/run/current-system/systemd/bin/systemctl suspend"
    '';
  };
}
