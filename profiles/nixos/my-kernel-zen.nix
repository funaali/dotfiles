{ config, lib, pkgs, ... }:

with lib;
with lib.kernel;

let
  generic-tweaks = {
    name = "my-generic-tweaks";
    patch = null;
    extraStructuredConfig = {
      # Default TCP congestion algorithm BBR
      TCP_CONG_BBR = yes;
      DEFAULT_BBR = yes;
      DEFAULT_TCP_CONG = freeform "bbr";

      # Enable PDS Process Scheduler
      SCHED_ALT = yes;
      SCHED_PDS = yes;

      # Enable more optimizations
      CC_OPTIMIZE_FOR_PERFORMANCE_O3 = yes;

      # Misc.
      BOOT_CONFIG = yes;

      # Fix error: unused option: XXX.
      # from pkgs/os-specific/linux/kernel/zen-kernels.nix
      CFS_BANDWIDTH = mkForce (option no);
      PSI = mkForce (option no);
      RT_GROUP_SCHED = mkForce (option no);
      SCHED_AUTOGROUP = mkForce (option no);
      SCHED_CORE = mkForce (option no);
      UCLAMP_TASK = mkForce (option no);
      UCLAMP_TASK_GROUP = mkForce (option no);
      DEBUG_INFO = mkForce (option no);
      SCHED_CLASS_EXT = mkForce (option no);

      # processor type and features
      JAILHOUSE_GUEST = yes;
      ACRN_GUEST = yes;
      INTEL_TDX_GUEST = yes;

      # Powersaving
      PM_ADVANCED_DEBUG = mkForce (option no);
      PM_WAKELOCKS = mkForce (option no);

      # CPU freq scaling
      CPU_FREQ_GOV_POWERSAVE = yes;
      CPU_FREQ_GOV_USERSPACE = yes;
      CPU_FREQ_GOV_ONDEMAND = yes;
      CPU_FREQ_GOV_CONSERVATIVE = yes;
      CPU_IDLE_GOV_LADDER = yes;
      CPU_IDLE_GOV_TEO = yes;

      # Swap storage compressed with zswap
      ZSWAP_DEFAULT_ON = yes;
      ZSWAP_SHRINKER_DEFAULT_ON = yes;
      ZBUD = yes;
      #Z3FOLD = mkForce (option yes);
      ZSMALLOC_STAT = yes;

      # Tune ECC engine
      MTD_NAND_ECC_SW_HAMMING_SMC = yes;
      MTD_NAND_ECC_SW_BCH = yes;
      MTD_NAND_ECC_MXIC = yes;

      # IPMI stuff compiled in, not just modules
      IPMI_HANDLER = yes;
      IPMI_PANIC_EVENT = yes;
      IPMI_DEVICE_INTERFACE = yes;
      IPMI_SI = yes;
      IPMI_SSIF = yes;
      IPMI_POWEROFF = yes;
      IPMI_WATCHDOG = yes;

      # Disable unnecessary debugging options
      DEBUG_INFO_DWARF_TOOLCHAIN_DEFAULT = mkForce no;
      DEBUG_LIST = mkForce no;
      RCU_TRACE = no;
      X86_VERBOSE_BOOTUP = no;
      DEBUG_ENTRY = no;
      X86_DEBUG_FPU = no;
      PUNIT_ATOM_DEBUG = no;
    };
  };

  zen2-tweaks = {
    name = "zen2-tweaks";
    patch = null;
    extraStructuredConfig = {
      # enable zen2 optimizations
      GENERIC_CPU = no;
      MZEN2 = yes;
    };
  };

in

{
  config = {
    boot.kernelPackages = pkgs.linuxPackages_zen;
    boot.kernelPatches = [ generic-tweaks ]
    ++ optional (config.custom.arch == "znver2") zen2-tweaks;
  };
}
