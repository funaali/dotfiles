{ config, lib, pkgs, ... }:

let
  inherit (lib) mkIf;

  hasAMDGPU = builtins.elem "amdgpu" config.services.xserver.videoDrivers;

  # TODO: really only Quadro cards and such I guess?
  #hasNVIDIA = builtins.elem "nvidia" config.services.xserver.videoDrivers;

  dvp-my-xkb = builtins.toFile "layout.xkb" ''
    # partial alphanumeric_keys
    xkb_symbols "dvp-my" {
        include "us(dvp)"              // includes the base US keys
        include "level3(ralt_switch)"  // configures right alt as a third level switch

        name[Group1]="Programmer Dvorak - Custom";

        // home row, left side
        //             Unmodified Shift AltGr       Shift+AltGr
        key <AC01> { [ a,         A,    adiaeresis, Adiaeresis ] };
        key <AC02> { [ o,         O,    odiaeresis, Odiaeresis ] };

        // lower row, left side
        //             Unmodified Shift AltGr       Shift+AltGr
        key <AB02> { [ q,         Q,    aring,      Aring      ] };
        key <AB05> { [ x,         X,    oslash,     Ooblique   ] };
    };
  '';
in
{
  config = {
    # Extra xserver utilities
    environment.systemPackages = with pkgs; mkIf config.services.xserver.enable ([
      autorandr # xrandr utility
      scrot # X11 screenshot utility
      clipnotify # X11 clipboards
      xclip # X11 clipboards
      xorg.xdpyinfo
      xorg.xdriinfo
      xorg.xwininfo
      xorg.xbacklight
      xorg.xev
      xorg.xhost
      xorg.xkbprint # xkbprint $DISPLAY -o - | ps2pdf - > xkblayout.pdf
      xorg.xkill
      xorg.xmag
      xorg.xmessage
      xorg.mkfontdir
      xorg.mkfontscale
      xorg.xkbutils
      xorg.xrefresh
      xorg.xlsatoms
      xorg.xlsfonts
      # monitoring tools
      nvtopPackages.full # For AMD, Intel and NVidia
    ] ++ lib.optionals hasAMDGPU [ amdgpu_top ]);

    hardware.graphics.extraPackages = with pkgs.xorg; [
      libX11
      libXrandr
      libXpm
      libXft
      libXrender
      libXcomposite
      libxcb
    ];

    boot.extraModprobeConfig =
      let
        amdgpu.enable_exp_hw_support = "options amdgpu exp_hw_support=1";
        amdgpu.enable_dpm = "options amdgpu dpm=1";
        amdgpu.enable_mes = "options amdgpu mes=1";

        # NOTE: abmlevel requires BMCU loaded
        amdgpu.misc = "options amdgpu deep_color=1 ngg=1 send_sigterm=1 abmlevel=3 sched_jobs=64 sched_hw_submission=6 mcbp=1";

        #amdgpu.ppfeaturemask = "options amdgpu ppfeaturemask=";
        #amdgpu.dcfeaturemask = "";

        # blacklist the old radeon driver... probably unnecessary, but won't
        # hurt either.
        amdgpu.blacklistRadeon = ''
          blacklist radeon
        '';
        # hid_sensor_*_3d group of kernel modules can cause system lockups on
        # bootup, shutdown, and suspend.
        blacklistHIDSensors = ''
          blacklist hid_sensor_accel_3d
          blacklist hid_sensor_gyro_3d
          blacklist hid_sensor_magn_3d
        '';
      in
      lib.mkMerge [
        amdgpu.blacklistRadeon
        blacklistHIDSensors
        amdgpu.enable_exp_hw_support
        amdgpu.enable_dpm
        amdgpu.enable_mes
        amdgpu.misc
      ];

    services.xserver = {
      enable = true;
      autorun = false;
      tty = null;
      terminateOnReset = false;
      exportConfiguration = true;
      autoRepeatDelay = 150;
      autoRepeatInterval = 50;

      excludePackages = [
        pkgs.xterm
      ];

      displayManager.startx.enable = true;

      # XXX: causes too many issues with apps not supporting 30 depth properly
      # AMDGPU supports 10-bit (depth 30)
      #defaultDepth = lib.mkIf (hasAMDGPU || hasNVIDIA) 30;

      # Additional settings for specific output drivers
      config = lib.mkBefore ''
        Section "OutputClass"
          Identifier "Additional Intel XE Settings"
          Driver "modesetting"
          MatchDriver "xe"

          Option "TearFree"             "true"
          Option "VariableRefresh"      "on"
          Option "AsyncFlipSecondaries" "on"
        EndSection

        Section "OutputClass"
          Identifier "Additional AMDGPU Settings"
          Driver "amdgpu"
          MatchDriver "amdgpu"

          Option "EnablePageFlip"       "true"
          Option "TearFree"             "true"
          Option "VariableRefresh"      "on"
          Option "AsyncFlipSecondaries" "on"
        EndSection

        Section "OutputClass"
          Identifier "Additional NVIDIA Settings"
          Driver "nvidia"
          MatchDriver "nvidia"

          Option "LimitFrameRateWhenHeadless" "on"
          Option "MultisampleCompatibility" "true"
          #Option "HardDPMS" "false"
        EndSection
      '';

      xkb = {
        layout = "dvp-my";
        variant = "dvp-my";
        options = lib.concatStringsSep "," [ "terminate:ctrl_alt_bksp" "lv3:menu_switch" ];

        extraLayouts.dvp-my = {
          description = "Programmers Dvorak with custom alt-gr maps";
          languages = [ "eng" ];
          symbolsFile = dvp-my-xkb;
        };
      };

    };

    services.libinput = {
      # Enable touchpad support (enabled default in most desktopManager).
      enable = true;
      # Configure touchpad
      touchpad.disableWhileTyping = true;
      # On a clickpad enabling this disables the middle button
      touchpad.middleEmulation = false;
      touchpad.sendEventsMode = "disabled-on-external-mouse";
    };
  };
}
