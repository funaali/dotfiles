#!/usr/bin/env bash

set -euo pipefail

mainSink="main" # the main virtual sink
mainSinkProperties='device.description="Main"'

notify() {
    notify-send "PulseAudio" -i /usr/share/icons/Adwaita/48x48/legacy/audio-card.png "$@"
}

switchable_sinks () {
    #echo UMC1820-surround21
    echo main.speakers
    echo main.phones
    echo alsa_output.usb-SAVITECH_Bravo-X_USB_Audio-01.analog-stereo
}

# usage: get_sink_input__pipewire [ARG_NAME] [ARG_VALUE]
# output: [#SINK-INPUT] [#SINK]
get_sink_input__pipewire () {
    #awk -F "\t" -v m="$1" '
    #arg=module
    awk -v arg="$1" -v val="$2" '
        $0 ~ /^Sink Input/    { i=gensub(/.*#/,"",1) }
        $0 ~ /^\s*Sink:/         { inputs[i]["sink"]=gensub(/.*:\s/,"",1,$0) }
        $2 ~ /^Owner Module:/ { inputs[i]["module"]=gensub(/.*:\s*/,"",1,$2) }
	$2 ~ /^=$/ { inputs[i][$1] = gensub(/.*"(.*)"/,"\\1",1,$0) }
        END {
          for (j in inputs) if (inputs[j][arg] == val) { print j, inputs[j]["sink"]; exit }
          exit 1
        }
    ' <(pactl list sink-inputs)
}

# List the sink inputs for a virtual sink
# usage: get_sink_inputs SINK
# output: [#SINK-INPUT] [#SINK]
get_sink_inputs () {
	sink=$1
	read -r sinkId moduleId < <(pactl -f json list sinks \
		| jq -r --arg sink "$sink" '.[]|select(.name==$sink)|"\(.index) \(.owner_module)"')
	if [[ -z $sinkId ]]; then
		return
	fi
	pactl -f json list sink-inputs \
		| jq -r --argjson moduleId "$moduleId" '.[]|select(.owner_module == "\($moduleId)")|"\(.index) \(.sink)"'
}

# usage $0 [SINK]
ensure_default_sink(){
    local defaultSink mainSink=$1
    defaultSink=$(pactl get-default-sink) # @DEFAULT_SINK@
    if [[ $defaultSink != "$mainSink" ]]; then
        notify "Setting default sink to '$mainSink' (was: $defaultSink)"
        pactl set-default-sink "$mainSink"
    fi
}

# usage: $0 [MODULE_NAME] [ARGUMENT_NAME] [ARGUMENT_VALUE]
# output: [#MODULE]
get_module_id(){
    local name=$1 argName=$2 argValue=$3
    awk -v "name=$name" -v "argName=$argName" -v "argValue=$argValue" '
        {switch($1) {
            case "Module"    : m=substr($2,2); break;
            case "Name:"     : names[m]=$2; break;
            case "Argument:" :
                str=$0
                while ((i = match(str,/(\S+)=("([^"]*)"|'\''([^'\'']*)'\''|(\S+))/,res)) > 0) {
                    str = substr(str,res[0,"length"]+i)
                    args[m, res[1]] = res[3]res[4]res[5]
                }
                break
            case "node.description" : args[m, $1] = $3; break;
        }}
        END {
            c=0
            for (m in names) if (names[m] == name && args[m, argName] == argValue) {print m;c++}
            exit c==0
        }' <(pactl list modules)
}

# Next sink of those that we want to switch between that is currently connected
get_next_sinks(){
	gawk -v current="$currentSinkId" '
# these hacks ensure that switching is done in index order
# sinks: array of available sinks (name -> id)
# names: array of sinks (names) to switch between
{
	switch(ARGIND) {
		case 1 : sinks[$2]=$1; break;
		case 2 : names[isarray(names) ? length(names)+1 : 1]=$1; break;
	}
}
END {
	# sinks with index higher than current sink
	for (i in names) {
		s=sinks[names[i]]
		if (s != "" && s > current) print s
	}
	# index lower than current sink
	for (i in names) {
		s=sinks[names[i]]
		if (s != "" && s < current) print s
	}
	# back to current sink
	print current
}
' <(pactl list short sinks) <(switchable_sinks)
}

# Ensure the main virtual sink exists
if ! pactl list short sinks | cut -f2 | grep "^$mainSink$" >/dev/null; then
    notify "The main virtual sink was not found. Attempting to create it"
    pactl load-module module-virtual-sink sink_name="$mainSink" sink_properties="$mainSinkProperties" master='main.speakers'
fi

# ID of the sink input that is currently connected to the main sink
read -r currentSinkInputId currentSinkId < <(get_sink_inputs "$mainSink") # NOTE: PulseAudio:
#read -r currentSinkInputId currentSinkId < <(get_sink_input__pipewire node.name main.output) NOTE: Pipewire:

read -r sink < <(get_next_sinks | head -n1)

# Switch to the next eligible sink
if [[ -n $sink ]]; then
    pactl move-sink-input "$currentSinkInputId" "$sink"
    sinkName=$(pactl list short sinks | grep "^$sink\s" | cut -f2)
    notify "Active output: <b>$sinkName</b>"
else
    notify "No eligible alternative sink!"
fi

# Ensure default sink is the main virtual sink
ensure_default_sink "$mainSink"
