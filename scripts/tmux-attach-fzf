#!/usr/bin/env bash

set -euo pipefail

# TODO: add support for renaming sessions without leaving fzf
# --expect=ctrl-v,ctrl-t,...
# --bind "enter

fWindows='#{W:#{E:window-status-format} ,#{E:window-status-current-format} }'
fAttached='#{?session_attached, (attached),}'
fSession=$'#{p-3:session_id} #{p8:session_name}#{?session_group, (#{session_group}),}  '"${fAttached}  [#{t/p:session_created}]   ${fWindows}"$'\n'

mapfile -t strings < <(tmux display-message -p -F "#{S:${fSession}}" | grep -v '^$')

mapfile -t response < <(fzf < <(printf %s\\n "${strings[@]}") \
	--header 'Select TMUX session to attach' \
	--print-query --tac --cycle --layout reverse-list \
	--height=40% --margin 0,2% --border)

case "${#response[@]}" in
	0)
		: # aborted by user
		;;
	1)
		[[ -n ${response[0]} ]] || exit
		echo "New session '${response[0]}'" >&2
		exec tmux new-session -A -s "${response[0]}"
		;;
	2)
		sessionId=$(awk '{print $1}' <<< "${response[1]}")
		echo "Attach session id=${sessionId} (${response[1]})" >&2
		exec tmux attach-session -t "=$sessionId"
		;;
	*)
		exit 1
		;;
esac
