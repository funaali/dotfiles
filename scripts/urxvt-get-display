#!/bin/sh

# Default TERM value if it doesn't exist.
TERM="${TERM:-xterm}"

# Sanity checks.
case "$TERM" in
	xterm|rxvt ) : ;;
	*          ) printf "Don't know what to do with TERM='%s'. Some tools will not be available.\n" "${TERM:-}" >&2; exit 0 ;;
esac

__urxvtc__auto_start_daemon () {
	urxvtc "$@"
	if [ $? -eq 2 ]; then
		systemctl --user start urxvtd.service
		urxvtc "$@"
	fi
}

__urxvt__version_info () {
	stty -icanon -echo
	printf "\e]702;?\x9c"
	IFS=$(printf ";\x9c") read -r _ term resource major minor
	stty icanon echo
	echo "term='$term' ressource='$resource' ver_major='$major' ver_minor='$minor'"
}

__urxvt__get_and_set_DISPLAY () {
	printf '\e[7n'
	read -r DISPLAY
	export DISPLAY
}

__urxvt__set_locale () {
	printf "\e]701;%s\007" "${LC_CTYPE}"
}

# Bracket arbitrary command between "get window position" and "restore window
# position" actions. If the command resulted in the window changing its
# position this wrapper restores the window to its earlier position.
#
# Mostly useful when changing fonts dynamically on floating windows, since
# URxvt tends to reset the position to origin when dimensions change as a
# result of changed display font.
#
# Usage: __urxvt__retain_window_pos command [arg...]
__urxvt__retain_window_pos () {
	printf "\e[13;?t" #  =>  ESC [ 3 ; X ; Y t
	read -r position
	command "$@"
	printf %s "$position"
}
