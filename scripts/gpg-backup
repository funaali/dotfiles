#!/usr/bin/env bash

set -euo pipefail

backup(){
	timestamp=$(date +%FT%H:%M)
	mkdir -p "$backupRoot"
	for old in "$backupTrust" "$backupPub" "$backupPriv"; do
		if [[ -e $old ]]; then
			mv -v "$old" "$old"."$timestamp"
		fi
	done
	gpg --export --armor --export-options backup --output "$backupPub" $keyId
	gpg --export-secret-keys --armor --export-options backup --output "$backupPriv" $keyId
	gpg --export-ownertrust > "$backupTrust"
}

restore(){
	gpg --import --import-options restore "$backupPub"
	gpg --import --import-options restore "$backupPriv"
	gpg --import-ownertrust "$backupTrust"
}

symmetric_gpgtar_encrypt(){
    backupRoot=$1
    if symmetricKey=$(pass show hw/gpg-backup-symmetric); then
        echo "Symmetric encryption key got from password-store"
    else
        read -rp "Symmetric enc key: " symmetricKey
    fi

    gpg-connect-agent reloadagent /bye

    # new temp gnupg instance
    # shellcheck disable=SC2155
    export GNUPGHOME=$(mktemp -d)

    tar -cf "$backupRoot.tar" "$backupRoot"
    gpg --batch --yes --pinentry-mode loopback --passphrase "$symmetricKey" --symmetric "$backupRoot.tar"
    rm -r "$backupRoot.tar" "$backupRoot"
    unset GNUPGHOME
    echo "Ready encrypted archive at $backupRoot.tar.gpg"
}

symmetric_gpgtar_decrypt(){
    backupRoot=$1
    if symmetricKey=$(pass show hw/gpg-backup-symmetric); then
        echo "Symmetric encryption key got from password-store"
    else
        read -rp "Symmetric enc key: " symmetricKey
    fi

    gpg-connect-agent reloadagent /bye

    # shellcheck disable=SC2155
    export GNUPGHOME=$(mktemp -d)
    gpg --batch --yes --pinentry-mode loopback --passphrase "$symmetricKey" --decrypt "$backupRoot.tar.gpg" > "$backupRoot.tar"
    (cd "$(dirname "$backupRoot")" && tar xvf "$backupRoot.tar")
    unset GNUPGHOME
}


keyId=F3D3AB3309F04D1CA4D851D068F82A4F3ECA091D
backupRoot=${2:-./gpg-backup}

backupPub=${backupRoot}/public.asc
backupPriv=${backupRoot}/private.asc
backupTrust=${backupRoot}/ownertrust.txt


case ${1:-} in
	--backup) backup ;;
	--restore) restore ;;
    --encrypt) symmetric_gpgtar_encrypt "$backupRoot" ;;
    --decrypt) symmetric_gpgtar_decrypt "$backupRoot" ;;
	*)
		echo "usage: $0 [--backup|--restore] <directory>" >&2
		exit 2
		;;
esac
