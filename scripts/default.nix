{ runCommand
}:
runCommand "scripts"
{
  src = ./.;
  preferLocalBuild = true;
}
  ''
    install -vD -m 0755 -t $out/bin $src/*
  ''
