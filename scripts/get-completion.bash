#!/usr/bin/env -S bash -i
# Added shebang
#
# Added modifications from https://github.com/tillig/ps-bash-completions/blob/master/PSBashCompletions/bash_completion_bridge.sh
#
# Author: Brian Beffa <brbsix@gmail.com>
# Original source: https://brbsix.github.io/2015/11/29/accessing-tab-completion-programmatically-in-bash/
# License: LGPLv3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
#

get_completions(){
    local completion COMP_CWORD=${COMP_CWORD-} COMP_LINE="${COMP_LINE-}" COMP_POINT=${COMP_POINT-} COMP_WORDS COMPREPLY=()

    # load bash-completion if necessary
    declare -F _completion_loader &>/dev/null || {
        source /usr/share/bash-completion/bash_completion
    }

    : "${COMP_KEY:=9}"
    : "${COMP_TYPE:=9}"
    : "${COMP_LINE:="$*"}"

    # index of cursor relative to the beginning of the line
    : "${COMP_POINT:=${#COMP_LINE}}"

    eval set -- "$@"

    # array of the individual words from the current line
    COMP_WORDS=("$@")

    # add '' to COMP_WORDS if the last character of the command line is a space
    [[ "${COMP_LINE[*]: -1}" = ' ' ]] && COMP_WORDS+=('')

    # index of the last word
    : "${COMP_CWORD:=$(( ${#COMP_WORDS[@]} - 1 ))}"

    # determine completion function
    completion=$(complete -p "$1" 2>/dev/null | awk '{print $(NF-1)}')

    # run _completion_loader only if necessary
    [[ -n $completion ]] || {

        # load completion
        _completion_loader "$1"

        # detect completion
        completion=$(complete -p "$1" 2>/dev/null | awk '{print $(NF-1)}')
    }

    # ensure completion was detected
    [[ -n $completion ]] || return 1

    prevWord=""
    [[ $COMP_CWORD -ge 1 ]] && prevWord="${COMP_WORDS["${COMP_CWORD}"-1]}"

    # execute completion function
    "${completion}" "${COMP_WORDS[0]}" "${COMP_WORDS["${COMP_CWORD}"]}" "${prevWord}"

    # print completions to stdout
    printf '%s\n' "${COMPREPLY[@]}" #| LC_ALL=C sort
}

while [[ $# -gt 0 ]]; do case "$1" in
    COMP_*) declare "${1%=*}=${1#*=}"; shift ;;
    *) get_completions "$@"; break ;;
esac done
