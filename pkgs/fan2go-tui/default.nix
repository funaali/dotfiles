{ buildGo123Module, fetchFromGitHub, lib }:

buildGo123Module rec {
  pname = "fan2go-tui";
  version = "0.2.1";
  #version = "0.2.1-20241102";

  src = fetchFromGitHub {
    owner = "markusressel";
    repo = pname;
    rev = "refs/tags/${version}";
    hash = "sha256-wE/3hkeFQXzR2GWOJLIwI7nFYhcSiHqB2tujEmyEazk=";
    #rev = "bf2a89fe9ab160cdff24ebd3e93c18d0bbd0adea"; # master @ Nov 2, 2024
    #hash = "sha256-g+ySFR1R9dcEeypXSrbSYWCxaoGdNLRpSBZ74vpAGvQ=";
  };

  vendorHash = "sha256-IMMnZZ6oJMRxtfT9iFM7pbAauvglt74i4Nco/rMwX/g=";
  #vendorHash = "sha256-fyKNOnvCthn7hzpPN/CdMlH4zo4ImCFmUsYNXa+2hTY=";

  meta = with lib; {
    mainProgram = "fan2go-tui";
    homepage = "https://github.com/markusressel/fan2go-tui";
    license = licenses.agpl3Plus;
    platforms = platforms.linux;
  };
}
