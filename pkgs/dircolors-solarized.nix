{ stdenvNoCC }:
stdenvNoCC.mkDerivation {
  name = "dircolors-solarized";
  src = builtins.fetchGit {
    url = "https://github.com/seebi/dircolors-solarized";
    rev = "03393f25e241e45fe9341d7baaa2507fd9741a53";
  };
  phases = [ "unpackPhase" "installPhase" ];
  installPhase = ''
    install -Dm0644 -t $out/share/dircolors/solarized dircolors.* README* LICENSE
  '';
}
