{ stdenvNoCC
, fetchFromGitHub
, ripgrep
, gawk
, gnused
}:
stdenvNoCC.mkDerivation {
  name = "fzf-tab-completion";

  src = fetchFromGitHub {
    owner = "lincheney";
    repo = "fzf-tab-completion";
    rev = "edbccc266319c9216bba0446471f76215629b5f0";
    sha256 = "sha256-y5qYErwqo4+OZ4odTAevJW5gd2oIHHcMqdHI/ZUR5Pw=";
  };

  patchPhase = ''
    sed -i 's@^\(_fzf_bash_completion_egrep=\).*@\1${ripgrep}/bin/rg@' bash/fzf-bash-completion.sh
    sed -i 's@^\(_fzf_bash_completion_awk=\).*@\1${gawk}/bin/gawk@' bash/fzf-bash-completion.sh
    sed -i 's@^\(_fzf_bash_completion_sed=\).*@\1${gnused}/bin/sed@' bash/fzf-bash-completion.sh
  '';

  # TODO build readline

  installPhase = ''
    install -Dm644 bash/fzf-bash-completion.sh $out/share/fzf-tab-completion/completion.bash
    install -Dm644 zsh/fzf-zsh-completion.sh $out/share/fzf-tab-completion/completion.zsh
  '';
}
