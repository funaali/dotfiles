# vim used by both system and home-manager configs

{ vim-full, python3 }:

(vim-full.override ({
  python3 = python3.withPackages (ps: [ ps.pip ]);
  wrapPythonDrv = true;
})).overrideAttrs (oa: {
  # Fix an error in Ex mode
  postFixup = oa.postFixup or "" + ''
    sed -i 's/^execute /call /' $out/share/vim/vimrc
  '';
})
