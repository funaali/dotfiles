{ callPackage }:

{
  # Misc. scripts
  scripts = callPackage ../scripts { };

  dummy = callPackage ./dummy.nix { };

  diff-paths = callPackage ./diff-paths.nix { };

  pwgen-mine = callPackage ./pwgen_mine.nix { };

  bash-complete-alias = callPackage ./bash-complete-alias { };

  ansible-completion = callPackage ./ansible-completion { };

  lennartcl-gitl = callPackage ./lennartcl-gitl { };

  kitty-themes = callPackage ./kitty-themes { };

  dunst-sounds = callPackage ./dunst-sounds { };

  fzf-tab-completion = callPackage ./fzf-tab-completion.nix { };

  fzf-obc = callPackage ./fzf-obc.nix { };

  dircolors-solarized = callPackage ./dircolors-solarized.nix { };

  forgit = callPackage ./forgit.nix { };

  vim-configured = callPackage ./vim-configured.nix { };

  mpd-notification = callPackage ./mpd-notification { };

  xpub = callPackage ./xpub.nix { };

  barriersXTRA = callPackage ./barriersXTRA { };

  qrencode-wifi-password = callPackage ./qrencode-wifi-password { };

  mpollux-digisign-client = callPackage ./mpollux-digisign-client { };

  a2ln = callPackage ./a2ln { };

  my-vim = callPackage ./my-vim.nix { };

  fan2go-tui = callPackage ./fan2go-tui { };

  open-eid = callPackage ./open-eid { };
}
