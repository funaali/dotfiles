{ stdenv
, pkg-config
, fetchFromGitHub
, glib
, systemd
, libnotify
, libmpdclient
, ffmpeg
, file
, iniparser
, discount # markdown
}:
stdenv.mkDerivation rec {
  name = "mpd-notification";
  version = "0.9.1";

  patches = [ ./0001-custom-regex.patch ];

  src = fetchFromGitHub {
    owner = "eworm-de";
    repo = "mpd-notification";
    rev = "refs/tags/${version}";
    hash = "sha256-8iBG1IdbERB2gOALvVBNJ3/hhiou3D/azSRkRD+u9O8=";
  };

  postPatch = ''
    sed 's|$(DESTDIR)/usr/|$(DESTDIR)/|g' -i Makefile
  '';

  buildInputs = [
    glib
    libnotify
    libmpdclient
    ffmpeg
    file
    iniparser
    systemd
    discount
  ];

  nativeBuildInputs = [
    pkg-config
  ];

  makeFlags = [ "DESTDIR=$(out)" ];
}
