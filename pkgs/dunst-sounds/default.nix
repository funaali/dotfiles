{ runCommand }:

let
  play-sound = ''
    p=/usr/share/sounds/freedesktop/stereo
    n=$(basename "$0")

    file=$p/$n.oga

    if [ -z "$XDG_RUNTIME_DIR" ]; then
      XDG_RUNTIME_DIR=/run/user/$(id -u)
    fi

    if [ -e "$file" ]; then
      echo "Playing $file" >&2

      paplay -- "$file"
    fi
  '';

  sounds = [ "message" "message-new-instant" "dilaog-error" "dialog-information" "audio-volume-change" ];
in

runCommand "dunst-sounds" { }
  ''
    install -vD -m 0755 ${builtins.toFile "play" play-sound} $out/bin/dunst-play-sound

    mkdir -p $out/libexec/dunst-sounds

    for sound in ${builtins.concatStringsSep " " sounds}; do
      ln -s $out/bin/dunst-play-sound $out/libexec/dunst-sounds/$sound
    done
  ''
