{ stdenv }:

stdenv.mkDerivation {
  name = "kitty-themes";
  version = "2022-11-08";

  src = builtins.fetchGit {
    url = "https://github.com/SimSaladin/kitty-themes";
    rev = "afc5223ddd4ecdcbb19a7c2474ef03291887ce21";
  };

  buildPhase = null;

  installPhase = ''
    mkdir -p $out/share/kitty-themes
    cp -v $src/themes/*.conf $out/share/kitty-themes
  '';
}
