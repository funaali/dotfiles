{ lib
, stdenv
, fetchFromGitHub
, makeWrapper
, gnugrep
, gawk
, procps
}:
stdenv.mkDerivation {
  name = "xpub";
  src = fetchFromGitHub {
    owner = "Ventto";
    repo = "xpub";
    rev = "f2eadaaf9c8539abbd46b23c0b2f6d93d2ebd658";
    hash = "sha256-72sZHZRAfQf2tBFn5MD8UyeTvabSYZjF1I1l/eKuWx0=";
  };

  nativeBuildInputs = [ makeWrapper ];

  prePatch = ''
  '';

  installPhase = ''
    mkdir -p $out/bin

    sed 's/Xorg/ X$/' $src/src/xpub.sh > $out/xpub.sh
    chmod +x $out/xpub.sh
    patchShebangs $out/xpub.sh

    makeWrapper $out/xpub.sh $out/bin/xpub \
      --prefix PATH : "${lib.makeBinPath [ gnugrep gawk procps ]}"
  '';
}
