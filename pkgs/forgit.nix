{ zsh-forgit
, installShellFiles
}:
# bash completion is missing from the zsh-forgit drv
zsh-forgit.overrideAttrs (oldAttrs: {
  name = "forgit";

  nativeBuildInputs = (oldAttrs.nativeBuildInputs or [ ]) ++ [ installShellFiles ];

  postInstall = ''
    installShellCompletion completions/git-forgit.bash
  '';
})
