{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  pname = "open-eid";
  version = "24.9.0.1948-2004";

  src = fetchurl {
    url = "https://installer.id.ee/media/ubuntu/pool/main/o/${pname}/${pname}_${version}.tar.xz";
    hash = "sha256-+4jAbznCkFvsgJ8g5ds0o3SxtHeeo74qv+XYtY6Tbzg=";
  };
}
