{ stdenv }:

stdenv.mkDerivation {
  name = "bash-complete-alias";

  src = ./.;

  buildPhase = null;

  installPhase = ''
    mkdir -p $out/share/bash-completion/completions
    cp -v $src/_complete_alias $out/share/bash-completion/completions/
  '';
  # mkdir -p $out/etc/bash_completion.d
  # echo "__load_completion _complete_alias" > $out/etc/bash_completion.d/complete_alias

}
