# Gawk script which parses barriers log output and raises desktop
# notifications where appropriate and also logs to syslog more meaningful
# messages.
#
# Reads env variable SCREEN_CHANGE_SCRIPT for executable to use.

# Main (line) loop
{ processLine($0) }

# Setup
BEGIN {
	# output file
	LogFile = (1 in ARGV) ? ARGV[1] : "/dev/stdout"
	# force stdin as input
	ARGV[1] = "-"
	ARGC = 2
	# notify cmd
	NoteCmd = "LC_ALL=C " ENVIRON["SCREEN_CHANGE_SCRIPT"]
	# do not care about output IO errors
	PROCINFO[NoteCmd, "NONFATAL"] = 1
	PROCINFO[LogFile, "NONFATAL"] = 1
}

# Teardown
END { close(LogFile) }

# Auxiliary data streams
BEGINFILE { writeLog2("INFO", "Started aux log processing") }
ENDFILE   { writeLog2("INFO", "Stopped aux log processing") }

# Parse syslog textual level to its numeric value
function syslogPrio(lvl) {
	switch (lvl) {
	  case /^[[:digit:]]$/: return lvl
	  case "FATAL":     return 2
	  case "ERROR":     return 3
	  case "WARNING":   return 4
	  case "NOTE":      return 5
	  case "INFO":      return 6
	  case /DEBUG.?/:   return 7
	  default:          return 5
	}
}

# writeLog2(p, m): Log message m with priority p (text)
function writeLog2(p, m) {
	printf("<%s>%s\n", syslogPrio(p), m) >> LogFile
	fflush(LogFile)
	if (ERRNO) {
		print("Log output failed:", ERRNO) > "/dev/stderr"
		exit 1
	}
}

# sendNote2(tag, prio, msg): Create desktop notification with the data, or log
# error to stderr if failed.
function sendNote2(t, p, m) {
	printf("tag=%s.%s\n%s\n", t, p, m) | NoteCmd
	close(NoteCmd)
	if (ERRNO) {
		print("Rising a desktop notification failed:", ERRNO) > "/dev/stderr"
		ERRNO = 0
	}
}

# Process line of (logging) output from barriers.
function processLine(s,		l, r, m, p, t) {
	# "[TIMESTAMP] LEVEL MESSAGE"
	if (match(s, /^(\[([0-9T:-]+)\])?\s*(FATAL|ERROR|WARNING|NOTE|INFO|DEBUG.?):\s+(.*)$/, r)) {
		#ts = r[2]
		l = r[3]
		m = r[4]
	} else {
		l = "NOTE"
		m = s
	}
	p = syslogPrio(l)
	if (m ~ /^(stopped|started) server/) {
		t = "server"
	} else if (m ~ /^(client|disconnecting client)/) {
		t = "client"
	} else if (m ~ /^cursor (|un)locked/) {
	        t = "cursor"
	#} else if (match(m, /^screen "(\S+)" (updated|grabbed) (clipboard \S+)/, r)) {
	#	# https://github.com/debauchee/barrier/blob/master/src/lib/barrier/clipboard_types.h
	#	t = "clipboard"
	#	gsub(/\<clipboard 0\>/, "CLIPBOARD", m)
	#	gsub(/\<clipboard 1\>/, "PRIMARY SELECTION", m)
	#	# r[2] == 0: CLIPBOARD
	#	# r[2] == 1: SELECTION
	#} else if (match(m, /^switch from "(\S+)" to "(\S+)"/, r)) { # parse from & to screens
	#        t = "screen"
	#	screenCur  = r[2]
	#	screenPrev = r[1]
	}
	writeLog2(p, m)
	if ((t != "") || (p <= 3))
		sendNote2(t, p, (p <= 3) ? sprintf("<b>[%2$s]</b>\t%1$s", m, l) : m)
}
