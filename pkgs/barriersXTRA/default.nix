{ barrier, coreutils, libnotify, writeShellApplication, gawk }:

let
  barriersXTRA = writeShellApplication {
    name = "barriers";
    runtimeInputs = [ coreutils gawk ];
    text = ''
      runtimeDir=$XDG_RUNTIME_DIR/barriers
      mkdir -p "$runtimeDir"

      BASH_ARGV0=barriers
      LOG_LEVEL=''${LOG_LEVEL:-INFO}
      BARRIER_CONFIG=''${BARRIER_CONFIG:-$HOME/.barrier.conf}
      export SCREEN_CHANGE_SCRIPT=${screenChangeScript}/bin/barriersXTRA-screen-change-script

      processOut() {
        trap "" EXIT PIPE HUP TERM
        stdbuf -i0 -oL -eL gawk -f ${./processOut.gawk}
      }

      exec 5>&1 6>&2
      exec > >(processOut >&5- 2>&6-)

      ${barrier}/bin/barriers \
              --no-daemon \
              --no-restart \
              --no-tray \
              --screen-change-script $SCREEN_CHANGE_SCRIPT \
              --debug "$LOG_LEVEL" \
              --address 0.0.0.0 \
              --address :: \
              --config "$BARRIER_CONFIG" \
              --display "$DISPLAY" \
              "$@"
    '';
  };

  screenChangeScript = writeShellApplication {
    name = "barriersXTRA-screen-change-script";
    runtimeInputs = [ libnotify ];
    text = ''
      runtimeDir=$XDG_RUNTIME_DIR/barriers

      app=Barrier
      icon=barrier
      urgency=low
      summary=Barrier
      body=
      tag=

      if  [ "$#" -eq 1 ]; then
              screen=$1
              echo "$screen" > "$runtimeDir/current"
              body="Active screen: <b>$screen</b>"
              tag=screen
      else
              IFS= mapfile -t xs < <(timeout --foreground 2 cat)
              set -- "$@" "''${xs[@]}"
              if  [ $# -gt 1 ]; then
                      eval "$1"
                      shift
              fi
              body=$(IFS=$'\n'; printf %s "$*")
      fi

      notify-send -a "$app" -i "$icon" -u "$urgency" \
              -h "string:x-canonical-private-synchronous:$tag" \
              -- "$summary" "$body"
    '';
  };


in
  barriersXTRA
