{ stdenv }:
let
  # these conflict with git-extra:: git-info git-pull-request
  cmds = "git-cherry-copy git-cherry-move git-get-branch git-is-current git-l git-out git-prompt";
in

stdenv.mkDerivation {
  name = "lennartcl-gitl";
  src = builtins.fetchGit {
    url = "https://github.com/lennartcl/gitl";
    rev = "100f180fa7adbd50f8c8cd7390729c730d3fafd9";
  };
  buildPhase = null;
  installPhase = ''
    mkdir -p $out/bin
    env -C $src cp -v -t $out/bin/ ${cmds}
  '';
}
