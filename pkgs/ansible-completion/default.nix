{ stdenv
, fetchFromGitHub
}:
stdenv.mkDerivation {
  name = "ansible-completion";

  src = fetchFromGitHub {
    owner = "dysosmus";
    repo = "ansible-completion";
    rev = "master";
    sha256 = "1jqrjmx4piiphf1bkiivjwslbdrgbm4wbiak7rkv67j847nhm3p5";
  };

  installPhase = ''
    mkdir -p $out/share/bash-completion/completions
    for file in $src/*-completion.bash; do
      cmd=$(basename -s -completion.bash "$file")
      install -Dm644 "$file" $out/share/bash-completion/completions/$cmd
    done
  '';
}
