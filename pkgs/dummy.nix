{ runCommandLocal }:

# An empty package, useful to override home-manager "package" attributes
# when system package is desired instead.

runCommandLocal "dummy" { } "mkdir $out"
