#!/usr/bin/env bash

set -euo pipefail

# The QR string ought to be like:
#   WIFI:S:<SSID>;T:<WEP|WPA|blank>;P:<PASSWORD>;H:<true|false|blank>;;
# NOTE: could add R:1 for WPA3

# escape character '\', for ( \;,": )
qresc() { sed 's/\([:;,"\\]\)/\\\1/g'; }

read -re -p "SSID: " ssid
read -re -p "Security [WEP|WPA|-]: " -i WPA security
read -re -p "Passphrase: " passphrase

printf -v data 'WIFI:T:%s;S:%s;P:%s;;' \
    "$(qresc <<<"$security")" \
    "$(qresc <<<"$ssid")" \
    "$(qresc <<<"$passphrase")"

echo

qrencode -t utf8 "$data"

echo
echo "SSID:     $ssid"
echo "Security: ${security:-(none)}"
echo "Password: $passphrase"
echo

filename="wifi-$ssid-$security.svg"

if [[ -e $filename ]]; then
    read -erN 1 -p "File $filename already exists, overwrite? [y/N]: " input
    case $input in
        y|Y)
            :
            ;;
        *)
            echo "warn: no picture file created" >&2
            exit
            ;;
    esac
fi

qrencode -t svg -o "$filename" "$data"
echo "Wrote file: $filename"
