{ writeShellApplication, qrencode }:

writeShellApplication {
  name = "qrencode-wifi-password";
  runtimeInputs = [ qrencode ];
  text = ./app.sh;
}
