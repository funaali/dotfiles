{ vim-full, vimPlugins, vimUtils, fetchFromGitHub }:

let
  vim-jetpack = vimUtils.buildVimPlugin {
    pname = "vim-jetpack";
    version = "2023-12-26";
    src = fetchFromGitHub {
      owner = "tani";
      repo = "vim-jetpack";
      rev = "1615114345988fa488ae3e8e98dcbee6c62bb2a4";
      hash = "sha256-LtwFxcAHgblZezEMhbCpfkAlj3npkQC9BIkomneYUsU=";
    };
  };
in
vim-full.customize {
  # set custom name of the executable
  # TODO non-standard for testing only for now
  name = "vim-configured";

  vimrcConfig = {
    customRC = ''
      " customizations go here...
    '';
    packages.myPlugins = {
      start = [ ];
      opt = [
        vim-jetpack
        vimPlugins.vim-polyglot
      ];
    };
  };
}
