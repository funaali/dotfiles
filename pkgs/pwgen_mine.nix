{ writeScriptBin, pwgen }:

# pwgen wrapped with alt default settings.

writeScriptBin "pwgen" ''
  exec ${pwgen}/bin/pwgen -cnsB1 -r "0" "''${@:-20}"
''
