{ python3 }:

let

  pythonPkg =
    {
      buildPythonApplication,
      fetchPypi,
      pillow,
      pygobject3,
      pyzmq,
      qrcode,
      libnotify,
      gobject-introspection,
      wrapGAppsHook3,
      poetry-core,
      setuptools,
      zeromq
    }:
    let
      # A zeromq build with specific flags turned ON is required.
      zeromq_custom = zeromq.overrideAttrs (oa: {
        cmakeFlags = oa.cmakeFlags or [] ++ [ "-DWITH_LIBSODIUM=ON" "-DENABLE_CURVE=ON" ];
        # libsodium.so not found in python otherwise
        propagatedBuildInputs = oa.buildInputs;
      });
      pyzmq_custom = pyzmq.overrideAttrs (_: {
        buildInputs = [ zeromq_custom ];
      });
    in

    buildPythonApplication rec {
      pname = "a2ln";
      version = "1.1.14";
      pyproject = true;

      src = fetchPypi {
        inherit pname version;
        hash = "sha256-ce9fq9jgZp71E8pT/5PAFtYD4ijlKHAnS9Z1wUCxTlQ=";
      };

      build-system = [
        poetry-core
        setuptools
      ];

      dependencies = [
        pygobject3
        pillow
        qrcode
        pyzmq_custom
      ];

      buildInputs = [
        libnotify
      ];

      nativeBuildInputs = [
        gobject-introspection
        wrapGAppsHook3
      ];

      meta = {
        description = "Android 2 Linux Notifications Server";
        homepage = "https://patri9ck.dev/a2ln/";
        mainProgram = "a2ln";
      };
    };
in
  python3.pkgs.callPackage pythonPkg { }
