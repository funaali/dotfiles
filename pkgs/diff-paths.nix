{ writeScriptBin }:

writeScriptBin "diff-paths" ''
  if [ $# -ne 2 ] || [ "$1" = --help ] || [ "$1" = -h ]; then
    echo "usage: diff-paths <path1> <path2>" >&2
    exit 2
  fi
  diff <(find -L "$1" -printf %P\\n | sort) <(find -L "$2" -printf %P\\n | sort)
''
