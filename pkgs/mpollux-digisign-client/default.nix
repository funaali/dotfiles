{ stdenv, fetchurl, dpkg, glibc, gcc-unwrapped, autoPatchelfHook, pcsclite, qt5
}:
let

  # Please keep the version x.y.0.z and do not update to x.y.76.z because the
  # source of the latter disappears much faster.
  #version = "4.2.2b-8136";
  version = "4.3.0-8707";

  src = fetchurl {
    url = "https://dvv.fi/documents/16079645/118383788/mpollux-digisign-client-for-dvv_${version}_amd64.deb/49f1d5d3-3d23-6e9d-866b-71cd25e61177";
    hash = "sha256-IVfslYJ6DQFwxTv+3essDJAmWqA0xMHOy/k2306JW5E=";
  };

in stdenv.mkDerivation {
  name = "mpollux-digisign-client${version}";

  system = "x86_64-linux";

  inherit src;

  # Required for compilation
  nativeBuildInputs = [
    qt5.wrapQtAppsHook
    autoPatchelfHook # Automatically setup the loader, and do the magic
    dpkg
  ];

  # Required at running time
  buildInputs = [
    glibc
    gcc-unwrapped
    qt5.qtbase
    pcsclite
  ];

  unpackPhase = "true";

  # Extract and copy executable in $out/bin
  installPhase = ''
    mkdir -p $out
    dpkg -x $src $out
    mv $out/usr/* $out
    rmdir $out/usr
  '';
}
