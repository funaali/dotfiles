{ stdenvNoCC
, fetchFromGitHub
}:
stdenvNoCC.mkDerivation {
  name = "fzf-obc";

  buildPhase = "true";
  doCheck = false;

  installPhase = ''
    mkdir -p $out/share/fzf-obc
    cp -r {bin,lib,plugins} $out/share/fzf-obc
  '';

  src = fetchFromGitHub {
    owner = "rockandska";
    repo = "fzf-obc";
    rev = "fdfb34dd7d9b714c8fc2adcc986add159977de7d";
    sha256 = "sha256-vv8Cl/qyZqwFic4VP4SOZYkTcmy86EOmVH3/NUcE2Ys=";
  };
}
