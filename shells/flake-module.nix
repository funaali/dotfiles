{ ... }:
{
  config.perSystem = { pkgs, inputs', ... }: {
    devShells.default = pkgs.callPackage ./. {
      inherit (inputs'.agenix.packages) agenix;
    };
  };
}
