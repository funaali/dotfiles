{ bashInteractive, mkShellNoCC, ventoy-full, agenix }:

mkShellNoCC {
  packages = [
    bashInteractive
    agenix
    ventoy-full
  ];
}
