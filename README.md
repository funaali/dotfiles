dotfiles
========

My user home (dotfiles) and NixOS system configurations using Nix.

## NixOS Systems

### Install on the local system

1. Clone this repository somewhere on the system.
2. Make `/etc/nixos/flake.nix` a symlink to `flake.nix` n the cloned repo.
3. Use `nixos-rebuild` as usual.

Delete unused store paths:

```bash
nix store gc
```

### Install on remote target machine

```bash
nixos-rebuild --flake '.#morphism' --target-host morphism.funktionaali.com --use-remote-sudo switch
```

### Live USB

Build a Live USB ISO image:

```bash
nix build .#nixosConfigurations.live-usb.config.system.build.isoImage
```

Install the ISO to a USB device:

```bash
sudo cp ./result/iso/*.iso /dev/sdX
```

Or better yet use Ventoy:

```bash
# install ventoy, if not installed already
ventoy -i -g -L VentoySim1 -r 32768 /dev/sda

# mount the ventoy partition
sudo mount /dev/disk/by-label/VentoySim1 /mnt

# copy the ISO to the ventoy partition
sudo cp ./result/iso/*.iso /mnt
```

## Hacking

```bash
nix develop -c repl .
```

## Home Manager only (non-NixOS)

```bash
hm build  # build only
hm switch # build and switch
```

`hm` is an alias for `home-manager --impure --flake ~/dotfiles`, available
after activation.

Alternative methods:

```bash
nix run .\#home-switch -- -n       # dry-run
nix run .\#home-switch             # activate
home-manager switch --impure --flake .\#$USER@$HOSTNAME -n
```

## Maintenance

Update dependencies (flake inputs):

```bash
nix flake update
nix build
```

## Custom Keyboard layout (X11)

```bash
sudo install -vTC -m 0644 -g 0 -o 0 ~/.config/xkb/symbols/dvp /usr/share/X11/xkb/symbols/dvp
setxkbmap -verbose 10 -layout dvp -variant intl -option lv3:ralt_switch
xkbcomp -I ~/.config/xkb ~/.config/xkb/keymap.xkb "$DISPLAY"
```

## Misc. Notes

- `git-forgit`: interactive `fzf`-based interface for some Git commands.
- `vimfiles` - <https://gitlab.com/funaali/vimfiles>
- XMonad configuration - <https://github.com/SimSaladin/xmonad-configuration>
- `home-manager` options - <https://rycee.gitlab.io/home-manager/options.html>
- `home-manager` source - <https://github.com/nix-community/home-manager>

## Secrets Management (Agenix)

Setup new agenix key for home-manager user:

1.  Generate new ssh key:

    ```bash
    ssh-keygen -t ed25519 -N '' -f ~/.ssh/id_agenix_ed25519 -C "agenix/$USER@$HOSTNAME"
    ```

2.  Add new key to `secrets.nix`.
3.  Rekey existing secrets: `agenix --rekey`.

Add new secret:

1.  Add secret to `secrets.nix`.
2.  Create the secret: `agenix -e secrets/secret_name.age`.
3.  Add the secret file to git: `git add secrets/secret_name.age`.
3.  Add the secret to nix configuration:

    ```nix
    { age.secrets."secret_name".file = ../secrets/secret_name.age; }
    ```

4.  Refer to the decrypted path in the configuration:

    ```nix
    { passwordFile = config.age.secrets."secret_name".path; }
    ```
