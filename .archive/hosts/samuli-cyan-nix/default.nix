{ /* lib, */ /*config,*/ pkgs, suites, profiles, nixos-hardware, ... }:

{
  imports = [
    "${nixos-hardware}/common/cpu/amd"
    "${nixos-hardware}/common/gpu/amd"
    "${nixos-hardware}/common/pc/laptop/acpi_call.nix"
    "${nixos-hardware}/common/pc/laptop/ssd"
    "${nixos-hardware}/lenovo/thinkpad"
  ]
  # Profile collections
  ++ suites.desktop
  # Additional profiles
  ++ (with profiles; [
    laptop
    printing
    dev
  ]);

  custom = {
    arch = "znver2";
    #enableNativeOptimizations = true; # Enable building with GCC optimizations for Ryzen CPU
    displaylink.enable = true;
  };

  # HARDWARE

  fileSystems = {
    "/" =
      { device = "/dev/disk/by-label/NIXROOT";
        fsType = "btrfs";
        options = [ "noatime" "compress=zstd:5" ];
      };

    "/boot" =
      { device = "/dev/disk/by-label/NIXBOOT";
        fsType = "vfat";
        options = [ "fmask=0022" "dmask=0022" ];
      };
  };

  #swapDevices = [ ];

  # Always latest AMD microcode
  services.ucodenix = {
    enable = true;
    cpuSerialNumber = "0086-0F01-0000-0000-0000-0000";
  };

  boot.initrd.availableKernelModules = [ "nvme" "thunderbolt" "ehci_pci" "rtsx_pci_sdmmc" "xhci_pci" "usbhid" "usb_storage" "sd_mod" ];

  boot.kernelModules = [ "kvm-amd" /*"amdgpu" */ ];

  # https://www.chiark.greenend.org.uk/doc/linux-doc/html/admin-guide/kernel-parameters.html
  boot.kernelParams = [
    # Try make baacklight work properly...
    "acpi_backlight=vendor"
    "acpi_enforce_resources=lax"
    "acpi_no_watchdog"
    # grep . -r /sys/firmware/acpi/interrupts/
    # /sys/firmware/acpi/interrupts/gpe03:   59324  EN     enabled      unmasked
    # /sys/firmware/acpi/interrupts/sci:   59547
    "acpi_mask_gpe=0x03"
    "acpi_sci=low"

    # include EFI memory map of available physical RAM
    "add_efi_memmap"

    # Use GB pages for kernel direct mappings.
    "gbpages"

    # Do a warm reboot, is faster.
    "reboot=warm"

    # (AMD) IOMMU tunables
    "iommu=merge"
    "amd_iommu=force_isolation"

    "consoleblank=600" # Console blank timeout
    "earlyprintk=ttyS1" # Enable early printk messages
    "agp=off" # Disable AGP support
    "pcie_port_pm=force" # breaks stuff or not ???
    "rcutree.use_softirq=0"
    "slab_merge"
  ];

  # Borrowed from Framework: https://community.frame.work/t/tracking-linux-battery-life-tuning/6665/161
  boot.extraModprobeConfig = "options nvme.noacpi=1";

  # Use systemd-boot
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Power management
  powerManagement.cpuFreqGovernor = "powersave";

  # NETWORKING

  # Enable NetworkManager
  networking.networkmanager = {
    enable = true;
    wifi.powersave = true; # TODO for laptop only
  };

  # ENVIRONMENT and SERVICES

  environment.systemPackages = [
    pkgs.openfortivpn
  ];

  hardware.amdgpu = {
    legacySupport.enable = true;
    initrd.enable = true;
    amdvlk.enable = true;
    amdvlk.supportExperimental.enable = true;
    amdvlk.support32Bit.enable = true;
    opencl.enable = true;
  };

  # TODO: move these into some module instead.
  services.avahi.enable = true;
  services.avahi.nssmdns4 = true;

  # Enable fingerprint reader
  services.fprintd = {
    enable = true;
    package = pkgs.fprintd-tod;
    tod = {
      enable = true;
      driver = pkgs.libfprint-2-tod1-goodix;
    };
  };
}
