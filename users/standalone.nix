{
  "sim@applicative" = {
    modules = [
      ({ profiles, suites, pkgs, ... }: {
        imports = with profiles; suites.workstation ++ [
          sim
          not-nixos
          pulseaudio
          mpd
          vdirsyncer
          accounts.pvl
        ];
        config = {
          home.username = "sim";
          #home.packages = with pkgs; [
          #  nixgl.auto.nixGLDefault
          #  nixgl.auto.nixGLNvidia
          #];
          programs.rtorrent-ps = {
            enable = true;
            baseDir = "~/RT";
            package = pkgs.rtorrent-ps;
          };
          services.nextcloud-client = {
            enable = true;
            startInBackground = true;
          };
        };
      })
    ];
  };

  "samuli@samuli-ThinkPad-L15-Gen-1" = {
    modules = [
      ({ profiles, suites, ... }: {
        imports = with profiles; suites.workstation ++ [
          not-nixos
          cyan
        ];
        services.barrier.client.server = "192.168.2.2";
      })
    ];
  };
}
