let

  barrierServerConfig = {
    # preserveFocus = false
    # switchCorners = none
    # switchCornerSize = 0
    # switchCorners = none +top-left +top-right +bottom-left +bottom-right
    # switchCornerSize = 50

    screens.lambda-nebula = {
      links.up = "work";
      links.right = "applicative";
    };

    screens.work = {
      aliases = [ "samuli-cyan-nix" ];
      links.down = "lambda-nebula";
    };

    screens.applicative = {
      links.left = "lambda-nebula";
    };

    options.clipboardSharing = true;
    options.screenSaverSync = true;
    #relativeMouseMoves = false
    keybindings = [
        { key = "Control+Alt+Shift+F4"; action = "lockCursorToScreen(toggle)"; }
        { key = "Control+Alt+Shift+F1"; action = "switchInDirection(left)"; }
	{ key = "Control+Alt+Shift+F3"; action = "switchInDirection(right)"; }
	{ key = "Control+Alt+Shift+F2"; action = "toggleScreen"; }
    ];
  };
in
{
  "sim@lambda-nebula" = { profiles, suites, ... }: {
    imports = with profiles; suites.workstation ++ [
      sim
      pipewire-user
      #pulseaudio
      mpd
      rtorrent
    ];
    programs.libreoffice.enable = true;
    programs.git.extraConfig.annex.jobs = 8;

    services.barrier.server.enable = true;
    services.barrier.server.config = barrierServerConfig;
  };

  "sim@defiant" = { profiles, suites, ... }: {
    imports = with profiles; suites.workstation ++ [
      accounts.pvl
      sim
      pipewire-user
    ];
  };

  "sim@morphism" = { profiles, suites, ... }: {
    imports = with profiles; suites.server ++ [
      sim
    ];
  };

  "sim@samuli-cyan-nix" = { profiles, suites, ... }: {
    imports = with profiles; suites.workstation ++ [
      pipewire-user
      cyan
    ];
  };
}
