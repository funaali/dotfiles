{
  allowAliases = true;
  allowUnfree = true;
  replaceStdenv = { pkgs }: pkgs.stdenvAdapters.overrideCC pkgs.stdenv pkgs.gcc_latest;
}
