
let
  # ssh-keyscan <hostname>
  hostKeys = {
    lambda-nebula = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAyK+3lDRDYYSNzu5KFmpZDVrM9Xu1OXRpJGvSDXYAy3";
    morphism = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAbU5VRlsvSzAjGPGgPqp0ZYBogw3cYgdVW31m1Hoiky";
    defiant = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICBqsKkFoe4btWwNXX1HUJPEa6I8kvbfgrXVy/IqRzGG";
  };

  userKeys = {
    "agenix/sim@lambda-nebula" = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA0SihBKYZy9oq/JunaAoAb1EoufqP20t949OtEYHWix";
    "agenix/sim@morphism" = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID6zyOOWlgYUbeaeaohRG14WSJWNBGA5gNull5UrYv5Z";
    "agenix/sim@defiant" = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA7yBf3/K80Q33uNXFPdY5Xgh9jfNbOQFyTMU+DYFlrh";
  };

  users = builtins.attrValues userKeys;
  hosts = builtins.attrValues hostKeys;
in
{
  # nixos
  "secrets/hetzner-dns.env.age".publicKeys = hosts ++ users;
  "secrets/ddns.conf.age".publicKeys = hosts ++ users;
  "secrets/lambda-nebula.lan-1.pem.age".publicKeys = [ hostKeys.lambda-nebula ] ++ users;
  "secrets/smtprelay-tls.key.age".publicKeys = [ hostKeys.lambda-nebula ] ++ users;
  "secrets/lnd-bitcoin-mainnet-wallet-password.age".publicKeys = [ hostKeys.lambda-nebula ] ++ users;
  "secrets/smtp.protonmail.ch-password.age".publicKeys = hosts ++ users;
  "secrets/nextcloud-whiteboard.env.age".publicKeys = [ hostKeys.morphism ] ++ users;
  # home-manager
  "secrets/taskwarrior-sim.key.age".publicKeys = users;
  "secrets/chatgpt-api-key.age".publicKeys = users;
  "secrets/deepl-free-api-key-01.age".publicKeys = users;
}
