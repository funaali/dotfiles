final: prev: {

  xorg = prev.xorg.overrideScope (_xself: xsuper: {
    # The tearfree modesetting patch is only in xwayland. Add it to the the
    # original too.
    xorgserver = xsuper.xorgserver.overrideAttrs (oa: {
      #pname = "xorg-server-kms-tearfree";
      patches = oa.patches or [] ++ [ ./xorgserver-modesetting-tearfree.patch ];
    });
  });

  # See: https://github.com/phillipberndt/autorandr/pull/219
  autorandr = prev.autorandr.overrideAttrs (oldAttrs: {
    patches = oldAttrs.patches or [ ] ++ [ ./autorandr-disable-without-fb.patch ];
  });

  # Checks are broken for some reason.
  rnix-lsp = prev.rnix-lsp.overrideAttrs (_oldAttrs: {
    doCheck = false;
  });

  # Build with soxr and zlib enabled.
  mpd = prev.mpd.overrideAttrs (oldAttrs: {
    mesonFlags = oldAttrs.mesonFlags ++ [
      "-Dzlib=enabled"
      "-Dipv6=enabled"
      "-Diso9660=enabled"
      "-Dopenmpt=enabled"
      "-Dsndfile=enabled"
      "-Dwavpack=enabled"
      "-Dwildmidi=enabled"
      "-Dcdio_paranoia=enabled"
      "-Diconv=enabled"
      "-Dpcre=enabled"
      "-Dsqlite=enabled"
      "-Dupnp=pupnp"
    ];
    buildInputs = oldAttrs.buildInputs ++ [
      final.libupnp
      final.libopenmpt
      final.libsndfile
      final.libcdio
      final.libcdio-paranoia
      final.wavpack
      final.wildmidi
    ];
  });

  # https://github.com/MusicPlayerDaemon/libmpdclient/pull/74
  libmpdclient = prev.libmpdclient.overrideDerivation (oldAttrs: {
    patches = oldAttrs.patches or [ ] ++ [ ./0001-Make-buffer-size-configurable-at-compile-time-and-se.patch ];
  });

  # custom patch for UMC1820
  pulsemixer = prev.pulsemixer.overrideAttrs (_oldAttrs: {
    src = final.fetchFromGitHub {
      owner = "SimSaladin";
      repo = "pulsemixer";
      rev = "master";
      hash = "sha256-JD0pGljZjr3uPJw/rokkpjg1iSAPavce3vAzjlxK43Y=";
    };
  });

  # tmux master
  tmux = prev.tmux.overrideAttrs (_: rec {
    version = "3.5-${prev.lib.substring 0 8 src.rev}";
    patches = [ ];
    src = final.fetchFromGitHub {
      owner = "tmux";
      repo = "tmux";
      rev = "fc204bb5e5e6ef4e48f6d6d2b087d2881ad9cf6e"; # 25.3.2024
      hash = "sha256-xRFQccBJHr/5QUqw90SzyCFvqGQtzYBzbQfBEDfRJAg=";
    };
  });

  # Nix 2.4 comes with its own completion, which collides with this one. Fix from
  # https://github.com/hedning/nix-bash-completions/pull/17
  nix-bash-completions = prev.nix-bash-completions.overrideAttrs (_oldAttrs: {
    patches = [
      # nom-build
      (final.fetchpatch {
        url =
          "https://github.com/hedning/nix-bash-completions/commit/2861e9b1b7351b77852444a79d164469ebbedd52.patch";
        sha256 = "sha256-L3NhFbnKwqP8+DaqG2/cufjQU7fuNhmXNMaC9m0YDEg=";
      })
      # nix 2.4 support
      (final.fetchpatch {
        url =
          "https://github.com/hedning/nix-bash-completions/commit/5d28f3de9244ccf679630770b90448a22a65aced.patch";
        sha256 = "sha256-Kez20a8YMvUe8lDVmzDCC8OmNX3taUMtjy917whlPUU=";
      })
    ];
    postPatch = ''
      substituteInPlace _nix --replace "nix nixos-option" "nixos-option"
      cat ${final.nix}/share/bash-completion/completions/nix >> _nix
    '';
  });

  abook = prev.runCommand "abook-wrapped" { nativeBuildInputs = [ final.makeWrapper ]; } ''
    makeWrapper ${prev.abook}/bin/abook $out/bin/abook \
      --add-flags '-C "''${XDG_CONFIG_HOME:-$HOME/.config}/abook/abookrc"'
  '';

  # Make sure our MYVIMRC can know for sure when it's being sourced by
  # vimpager.
  vimpager-mine = final.vimpager-latest.overrideAttrs (oldAttrs: {
    version = "2.2022-10-02";
    src = oldAttrs.src.override {
      rev = "2d3af2bf3d09602d43cbfc294566869a89edfd46";
      sha256 = "sha256-HHLa7zOkbv7vvlKvhRWkqcUVlHQUq7/+FxOiZZEX4dc=";
    };
    nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [ final.makeWrapper ];
    fixupPhase = ''
      wrapProgram $out/bin/vimpager --add-flags '--cmd "set rtp='$HOME'/.vim"'
      wrapProgram $out/bin/vimcat   --add-flags '--cmd "set rtp='$HOME'/.vim" --cmd "let g:vimcat_mode = 1"'
    '';
  });

  pass = prev.pass.overrideAttrs (old: {
    patches = old.patches or [ ] ++ [
      # Fix loading of extensions completions
      # https://github.com/xundeenergie/password-store/commit/3cd5641eab95714ba65b960a4d74111b95e1486a
      ./pass-fix-completion-loading.patch
    ];
  });

  # Temporary fix for 29/30: allow converting to postgresql db...
  nextcloud29 = prev.nextcloud29.overrideAttrs (_: {
    patches = [ ./db-revert.patch ];
  });

  # hpnKerberosGSSAPI
  openssh_hkg = prev.openssh_hpnWithKerberos.overrideDerivation (oa: {
    pname = "${oa.pname}-gssapi";
    patches = oa.patches ++ [
      # Adjust the gssapi patch for the hpn patches
      (final.fetchpatch {
        name = "openssh-gssapi-${oa.version}.patch";
        url = "https://salsa.debian.org/ssh-team/openssh/raw/debian/1%25${oa.version}-3/debian/patches/gssapi.patch";
        hash = "sha256-TiqFcIZs5IkJgZuuB4U3tkmrlGbvNV7SiNWuzDBy5mI=";
        postFetch = ''
          ${final.buildPackages.patch}/bin/patch $out ${./openssh-gssapi-adjust-for-hpn.patch}
        '';
      })
    ];
  });


  # broken kvm patch in release channel..
  virtualboxKvm = final.__nixpkgs-dev.virtualboxKvm;
  virtualbox = final.__nixpkgs-dev.virtualbox;
  virtualboxHeadless = final.__nixpkgs-dev.virtualboxHeadless;
  virtualboxWithExtpack = final.__nixpkgs-dev.virtualboxWithExtpack;
}
