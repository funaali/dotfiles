let
  pred = final: prev: final != null && final.stdenv.cc.isGNU && final.lib.versionAtLeast final.stdenv.cc.version "14";

  patch_systemtap = systemtap: systemtap.overrideAttrs (oa: {
    env.NIX_CFLAGS_COMPILE = oa.NIX_CFLAGS_COMPILE or "" + " -Wno-error=calloc-transposed-args";
    postPatch = oa.postPatch or "" + ''
      echo "RUNNING POSTPATCH"
      ls -la
      sed -ie 's/calloc (ninsns, sizeof(bpf_insn))/calloc (sizeof(bpf_insn), ninsns)/g' bpf-translate.cxx
      sed -ie 's/calloc (nreloc, sizeof(Elf64_Rel))/calloc (sizeof(Elf64_Rel), nreloc)/g' bpf-translate.cxx
    '';
  });

  patch_kernelPackages = kernelPackages: kernelPackages.extend (kself: kprev: {
    systemtap = patch_systemtap kprev.systemtap;
  });

  patch_nixpkgs = pkgs: pkgs // {
    libsystemtap = patch_systemtap pkgs.libsystemtap;
    linuxPackages = patch_kernelPackages pkgs.linuxPackages;
    linuxPackages_latest = patch_kernelPackages pkgs.linuxPackages_latest;
  };

  predAttrs = final: prev: builtins.mapAttrs (n: v: if pred final prev then v else prev.${n});
in
let
  ov = final: prev: predAttrs final prev {
    # Fix for GCC 14 compatibility
    liblangtag = prev.liblangtag.overrideAttrs (_: {
      env.NIX_CFLAGS_COMPILE = "-Wno-error=format-overflow";
    });

    uwimap = prev.uwimap.overrideAttrs (oa: {
      env.NIX_CFLAGS_COMPILE = oa.env.NIX_CFLAGS_COMPILE or "" + " -Wno-implicit-function-declaration -Wno-incompatible-pointer-types";
    });

    dev86 = prev.dev86.overrideAttrs (oa: {
      env.NIX_CFLAGS_COMPILE = oa.env.NIX_CFLAGS_COMPILE or "" + " -Wno-implicit-int -Wno-implicit-function-declaration";
    });

    vbetool = prev.vbetool.overrideAttrs (oa: {
      env.NIX_CFLAGS_COMPILE = oa.env.NIX_CFLAGS_COMPILE or "" + " -Wno-int-conversion";
    });

    syslinux = prev.syslinux.overrideAttrs (oa: {
      env.NIX_CFLAGS_COMPILE = oa.env.NIX_CFLAGS_COMPILE or "" + " -Wno-incompatible-pointer-types";
    });

    testdisk = prev.testdisk.overrideAttrs (oa: {
      env.NIX_CFLAGS_COMPILE = oa.env.NIX_CFLAGS_COMPILE or "" + " -Wno-incompatible-pointer-types";
    });

    # Fix for GCC 14 compatibility
    openjdk8 = prev.openjdk8.overrideAttrs (oa: {
      env.NIX_CFLAGS_COMPILE = oa.env.NIX_CFLAGS_COMPILE or "" + " -Wno-int-conversion -Wno-incompatible-pointer-types";
    });

    # Fix for GCC 14 compatibility; dunno why the tests fail
    nodejs = prev.nodejs.overrideAttrs (_: { doCheck = false; });
    tinysparql = prev.tinysparql.overrideAttrs (_: { doCheck = false; });
    libfprint = prev.libfprint.overrideAttrs (_: { installCheckPhase = ""; });

    # fix systemtap build error
    libsystemtap = patch_systemtap prev.libsystemtap;
    # Use glib from master to fix it...
    glib = final.__nixpkgs-dev.glib;
    fontforge = final.__nixpkgs-dev.fontforge;

    linuxPackages = patch_kernelPackages prev.linuxPackages;
    linuxPackages_latest = patch_kernelPackages prev.linuxPackages_latest;

    #pkgsBuildHost = patch_nixpkgs prev.pkgsBuildHost;
    #buildPackages = patch_nixpkgs prev.buildPackages;

    #buildPackages = prev.buildPackages.extend (_: _: { }); # (bself: bprev: {
      #linuxPackages = patch_kernelPackages bprev.linuxPackages;
      #linuxPackages_latest = patch_kernelPackages bprev.linuxPackages_latest;
    # });
  };
in

self: prev:

  ov self prev # // { }
