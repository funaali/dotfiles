final: prev:

let
  inherit (final.stdenv) system;
  inherit (final.lib.systems) equals elaborate;

  platformIsGeneric = platform: equals platform (elaborate platform.system);
in
{
  # pkgs without extra features (gccarch).
  # If the host platform already equals the generic one we can just re-use this
  # package set.
  genericPkgs =
    if platformIsGeneric final.stdenv.hostPlatform && !(final.config ? replaceStdenv) then final
    else
      import final.path {
        inherit system;
        config = builtins.removeAttrs final.config [ "replaceStdenv" ];

        # FIXME: infinite memory if add overlays here
        #inherit (prev) overlays;
      };

  # Packages that are broken with march=native
  inherit (final.genericPkgs)

    # Just takes forever to compile
    chromium
    electron
    electron-source

    # Use pre-built libreoffice
    libreoffice-bin # for Darwin
    libreoffice-qt-still # Qt versions for KDE
    libreoffice-qt-fresh
    libreoffice-qt6-still
    libreoffice-qt6-fresh
    libreoffice-fresh
    libreoffice-still
    libreoffice-collabora # ???

    # tests fail
    fan2go
    mailutils
    redis
    tracker
    upower

    # Some non-sense build error
    xsane sane-frontends

    # Python-related build errors
    hplip
    hplipWithPlugin
    networkmanager
    networkmanager-openconnect
    networkmanager-fortisslvpn

    # Tricky builds
    khal
    lbdb
    virtualbox
    zig
    nodejs_18
    python3
    python3Packages
    mariadb
    ;

  # Tests broken if GCC platform optimizations are on
  jose = prev.jose.overrideAttrs (_: { doCheck = false; });

  # Tests broken for some reason here for these haskell packages
  haskellPackages = prev.haskellPackages.override ({ haskellLib, ... }: {
    overrides = _hself: hsuper: {
      jose = haskellLib.dontCheck hsuper.jose;
      cryptonite = haskellLib.dontCheck hsuper.cryptonite;
      crypton-x509-validation = haskellLib.dontCheck hsuper.crypton-x509-validation;
    };
  });

  python312 = prev.python312.override {
    packageOverrides = pyself: pysuper: {
      # These  fail tests if compiling with optimisations andd/or GCC 14
      numpy = pysuper.numpy.overrideAttrs (_: { dontUsePytestCheck = true; });
      cattrs = pysuper.cattrs.overrideAttrs (_: { dontUsePytestCheck = true; });
      tqdm = pysuper.tqdm.overrideAttrs (_: { dontUsePytestCheck = true; });

      # Patch  pyqt6 working from nixpkgs master... Remove this mess someday
      pyqt-builder = final.__nixpkgs-dev.python312.pkgs.pyqt-builder.override {
        inherit (pyself) buildPythonPackage packaging sip;
      };
      pyqt6 = final.__nixpkgs-dev.python312.pkgs.pyqt6.override {
        inherit (pyself) buildPythonPackage pyqt-builder pyqt6-sip dbus-python
        setuptools sip;
      };
      pyqt6-sip = final.__nixpkgs-dev.python312.pkgs.pyqt6-sip.override {
        inherit (pyself) buildPythonPackage;
      };
      sip = final.__nixpkgs-dev.python312.pkgs.sip.override {
        inherit (pyself) buildPythonPackage setuptools packaging;
      };
    };
  };

  qt6 = prev.qt6.overrideScope (_lself: lsuper: {
    # Because bundled embree has to be built without extra features
    qtquick3d = lsuper.qtquick3d.overrideAttrs {
      env.NIX_CFLAGS_COMPILE = "-march=x86-64";
    };
  });
}
